**POLYSCRABBLE**

# Installation

Desktop:

```
cd desktop
npm ci
```

Server:

```
cd server
npm ci
```

# Starting

Desktop:

```
cd desktop
npm start
```

Server:

```
cd server
npm start
```
