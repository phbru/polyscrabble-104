# To rebuild image and push to DockerHub

```
cd docker
docker build -t "phbru/node-chromium-mongo:1.0" .
docker push "phbru/node-chromium-mongo:1.0"
```
