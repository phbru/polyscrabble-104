const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const path = require('path');
var fs = require('fs');
const os = require('os');
const { globalShortcut } = require('electron');

let mainWindow;

async function getTheme() {
    const tmpDir = os.tmpdir();
    const pathUrl = path.join(tmpDir, 'theme.json');

    try {
        const out = fs.readFileSync(pathUrl);
        return JSON.parse(out).theme;
    } catch (err) {
        return '';
    }
}

async function getLanguage() {
    const tmpDir = os.tmpdir();
    const pathUrl = path.join(tmpDir, 'language.json');

    try {
        const out = fs.readFileSync(pathUrl);
        return JSON.parse(out).language;
    } catch (err) {
        return '';
    }
}

app.on('browser-window-focus', function () {
    globalShortcut.register('CommandOrControl+R', () => {
        console.log('CommandOrControl+R is pressed: Shortcut Disabled');
    });
    globalShortcut.register('F5', () => {
        console.log('F5 is pressed: Shortcut Disabled');
    });

    globalShortcut.register('CommandOrControl+-', () => {
        console.log('Zoom disabled');
    });

    globalShortcut.register('CommandOrControl+Shift+Plus', () => {
        console.log('Zoom disabled');
    });

    globalShortcut.register('CommandOrControl+Plus', () => {
        console.log('Zoom disabled');
    });
});

app.on('browser-window-blur', function () {
    globalShortcut.unregister('CommandOrControl+R');
    globalShortcut.unregister('F5');
    globalShortcut.unregister('CommandOrControl+-');
    globalShortcut.unregister('CommandOrControl+Shift+Plus');
    globalShortcut.unregister('CommandOrControl+Plus');
});

function setTheme(event, theme) {
    const tmpDir = os.tmpdir();
    const pathUrl = path.join(tmpDir, 'theme.json');
    var content;

    try {
        var file_content = fs.readFileSync(pathUrl);
        content = JSON.parse(file_content);
    } catch (err) {
        content = new Object();
    }

    content.theme = String(theme);
    fs.writeFileSync(pathUrl, JSON.stringify(content));
}

function setLanguage(event, language) {
    const tmpDir = os.tmpdir();
    const pathUrl = path.join(tmpDir, 'language.json');
    var content;

    try {
        var file_content = fs.readFileSync(pathUrl);
        content = JSON.parse(file_content);
    } catch (err) {
        content = new Object();
    }

    content.language = String(language);
    fs.writeFileSync(pathUrl, JSON.stringify(content));
}

function createWindow() {
    mainWindow = new BrowserWindow({
        icon: path.join(__dirname, 'logo.png'),
        // width: 1920,
        // height: 1080,
        webPreferences: {
            zoomFactor: 1,
            nodeIntegration: true,
            preload: path.join(__dirname, 'preload.js'),
        },
        resizable: false,
        // maximizable: true,
        // minimizable: true,
    });

    mainWindow.setMenuBarVisibility(false);
    mainWindow.setMaximizable(true);
    mainWindow.setMinimizable(true);
    mainWindow.maximize();
    mainWindow.focus();

    const urlPath = `file://${__dirname}/dist/desktop/index.html`;

    mainWindow.loadURL(urlPath);

    mainWindow.on('closed', function () {
        mainWindow = null;
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit();
});

app.on('activate', function () {
    if (mainWindow === null) createWindow();
});
app.on('activate', function () {
    if (mainWindow === null) createWindow();
});

app.whenReady().then(() => {
    ipcMain.handle('theme', getTheme);
    ipcMain.on('setTheme', setTheme);
    ipcMain.handle('language', getLanguage);
    ipcMain.on('setLanguage', setLanguage);
});
