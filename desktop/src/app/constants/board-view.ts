/* eslint-disable @typescript-eslint/no-magic-numbers */
export const AMOUNT_OF_COLUMNS = 16;
export const AMOUNT_OF_LINES = 16;
export const MIN_CHAR_PROPORTION = 0.42;
export const MAX_CHAR_PROPORTION = 0.85;

export const MIN_WEIGHT_PROPORTION = 0.2;
export const MAX_WEIGHT_PROPORTION = 0.32;

export const DEFAULT_CHAR = 0.68;
export const DEFAULT_WEIGHT = 0.28;

export const CHAR_FONT_INCREASER = 0.02;
export const WEIGHT_FONT_INCREASER = 0.005;

export const DEFAULT_WIDTH = 820 / window.devicePixelRatio;
