/* eslint-disable prettier/prettier */
export const ALL = 'mainPage.filterGames.all';
export const ACCESS = 'mainPage.joinableGames.accessType';
export const DICTIONARY = 'mainPage.joinableGames.dictionary';
export const TIMER = 'mainPage.filterGames.timer';
export const PLAYERS = 'mainPage.filterGames.players';

export const publicAccess = 'mainPage.filterGames.publicAccess';
export const privateAccess = 'mainPage.filterGames.privateAccess';
export const frenchDictionary = 'mainPage.filterGames.frenchDictionary';
export const englishDictionary = 'mainPage.filterGames.englishDictionary';
