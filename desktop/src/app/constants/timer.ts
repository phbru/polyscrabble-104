export const CRITICAL_MINUTES = 0;
export const CRITICAL_SECONDS = 10;
export const MAX_VALUE_TIMER_MINUTE = 5;
export const TIMER_CHANGE_VALUE = 30;
export const ONE_MINUTE = 60;
