/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { User } from '@app/classes/user';
import { MenuSettingComponent } from '@app/components/menu-setting/menu-setting.component';
import { ColorThemeService } from '@app/services/color-theme/color-theme.service';
import { HttpClientService } from '@app/services/http-client/http-client.service';
import { LanguageService } from '@app/services/language/language.service';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';
import { first } from 'rxjs';

const MODAL_OFFSET = 15;

@Component({
    selector: 'app-login',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
    loginForm = new FormGroup({
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required]),
    });

    errorMsg: string = '';

    constructor(
        public colorThemeService: ColorThemeService,
        private httpClientService: HttpClientService,
        private socket: Socket,
        private router: Router,
        private matDialog: MatDialog,
        private languageService: LanguageService,
        private translateService: TranslateService,
    ) {}

    get username() {
        return this.loginForm.get('username');
    }

    async ngOnInit(): Promise<void> {
        this.languageService.selectedLanguage = await (window as any).electronAPI.getLanguage();
        this.translateService.use(this.languageService.selectedLanguage);
        const theme: string = await (window as any).electronAPI.getTheme();
        this.colorThemeService.changeTheme(theme);
    }

    async onSubmit() {
        const userToLogin: User = User.login(this.loginForm.controls.username.value as string, this.loginForm.controls.password.value as string);

        this.httpClientService
            .login(userToLogin)
            .pipe(first())
            .subscribe({
                next: (res: User) => {
                    this.socket.connect();
                    this.socket.emit('login', res.id.toString());
                    this.router.navigate(['/home']);
                },
                error: (res) => {
                    this.errorMsg = res.error;
                },
            });
    }
    openMenu(event: MouseEvent) {
        const locationY = (event.clientY + MODAL_OFFSET).toString() + 'px';
        this.matDialog.open(MenuSettingComponent, {
            data: false,
            position: { top: locationY, right: '2%' },
            width: '200px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
        });
    }
}
