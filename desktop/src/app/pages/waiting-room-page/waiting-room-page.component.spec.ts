// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { MatDialog } from '@angular/material/dialog';
// import { MatIcon } from '@angular/material/icon';
// import { HeaderComponent } from '@app/components/header/header.component';
// import { LobbyService } from '@app/services/lobby/lobby.service';
// import { Socket } from 'ngx-socket-io';
// import { MainPageComponent } from './main-page.component';
// import SpyObj = jasmine.SpyObj;

// describe('MainPageComponent', () => {
//     let component: MainPageComponent;
//     let fixture: ComponentFixture<MainPageComponent>;
//     let matDialogSpy: SpyObj<MatDialog>;
//     let lobbyServiceSpy: jasmine.SpyObj<LobbyService>;
//     let socketSpy: jasmine.SpyObj<Socket>;

//     beforeEach(async () => {
//         lobbyServiceSpy = jasmine.createSpyObj('LobbyService', []);
//         matDialogSpy = jasmine.createSpyObj('MatDialog', ['open']);
//         socketSpy = jasmine.createSpyObj('Socket', ['navigate']);
//         await TestBed.configureTestingModule({
//             declarations: [MainPageComponent, HeaderComponent, MatIcon],
//             providers: [
//                 { provide: LobbyService, useValue: lobbyServiceSpy },
//                 { provide: MatDialog, useValue: matDialogSpy },
//                 { provide: Socket, useValue: socketSpy },
//             ],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(MainPageComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });
// });
