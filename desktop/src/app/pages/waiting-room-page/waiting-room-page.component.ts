import { Component } from '@angular/core';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';

@Component({
    selector: 'app-main-page',
    templateUrl: './waiting-room-page.component.html',
    styleUrls: ['./waiting-room-page.component.scss'],
})
export class WaitingRoomPageComponent {
    constructor(public waitingRoomService: WaitingRoomServiceProxy) {}
}
