import { Component } from '@angular/core';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent {
    constructor(public waitingRoomService: WaitingRoomServiceProxy) {}
}
