/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { User } from '@app/classes/user';
import { MenuSettingComponent } from '@app/components/menu-setting/menu-setting.component';
import { ColorThemeService } from '@app/services/color-theme/color-theme.service';
import { HttpClientService } from '@app/services/http-client/http-client.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';
import { first } from 'rxjs';

const MODAL_OFFSET = 15;

@Component({
    selector: 'app-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.scss'],
})
export class RegisterPageComponent {
    registerForm = new FormGroup({
        username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(10), Validators.pattern('[a-zA-Z0-9]*')]),
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[a-zA-Z0-9]*')]),
        confirmedPassword: new FormControl('', [Validators.required, Validators.minLength(8), Validators.pattern('[a-zA-Z0-9]*')]),
    });

    errorMsg: string;
    avatarUrl: any = 'assets/images/avatars/avatar-1.svg';
    fileName: string;

    constructor(
        public colorThemeService: ColorThemeService,
        private snackBarService: SnackBarService,
        private httpClient: HttpClientService,
        private router: Router,
        private socket: Socket,
        private matDialog: MatDialog,
        private translateService: TranslateService,
    ) {}

    openMenu(event: MouseEvent) {
        const locationY = (event.clientY + MODAL_OFFSET).toString() + 'px';
        this.matDialog.open(MenuSettingComponent, {
            data: false,
            position: { top: locationY, right: '2%' },
            width: '200px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
        });
    }

    showPreview(event: any) {
        if (event.target.files && event.target.files[0]) {
            const file: File = event.target.files[0];

            const maxFileSize = 32000;
            if (file.size > maxFileSize) {
                this.errorMsg = this.translateService.instant('registerPage.incorrectFileSize');
                return;
            }

            if (this.errorMsg === this.translateService.instant('registerPage.incorrectFileSize')) {
                this.errorMsg = '';
            }

            this.fileName = file.name;
            const reader: FileReader = new FileReader();
            reader.onload = () => (this.avatarUrl = reader.result);

            reader.readAsDataURL(file);
        }
    }

    getUploadIcon() {
        if (this.colorThemeService.selectedTheme === 'dark') return 'assets/images/avatars/upload_icon_dark.png';
        else return 'assets/images/avatars/upload_icon_light.png';
    }

    getAvatar() {
        return this.avatarUrl;
    }

    selectAvatar(index: number) {
        if (this.errorMsg === this.translateService.instant('registerPage.incorrectFileSize')) {
            this.errorMsg = '';
        }
        this.avatarUrl = `assets/images/avatars/avatar-${index}.svg`;
        this.fileName = '';
    }

    isSubmitDisabled() {
        return !(this.registerForm.valid && this.avatarUrl !== undefined);
    }

    onSubmit() {
        const userToRegister: User = User.register(
            this.registerForm.controls.username.value as string,
            this.avatarUrl,
            this.registerForm.controls.email.value as string,
            this.registerForm.controls.password.value as string,
            this.registerForm.controls.confirmedPassword.value as string,
        );

        this.httpClient
            .register(userToRegister)
            .pipe(first())
            .subscribe({
                next: (id: string) => {
                    this.socket.connect();
                    this.socket.emit('login', id);
                    this.router.navigate(['/home']);
                    this.snackBarService.open(this.translateService.instant('registerPage.successfulRegistration'), 'OK');
                },
                error: (res) => {
                    const ALREADY_EXIST = 11000;
                    if (res.error === ALREADY_EXIST) {
                        this.errorMsg = this.translateService.instant('registerPage.alreadyUsedUsername');
                    } else {
                        this.errorMsg = this.translateService.instant(res.error.errors[0].msg);
                    }
                },
            });
    }
}
