// import { HttpClient, HttpHandler } from '@angular/common/http';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { MatCardModule } from '@angular/material/card';
// import { MatDialog } from '@angular/material/dialog';
// import { MatInputModule } from '@angular/material/input';
// import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
// import { Player } from '@app/classes/player';
// import { ChatDisplayComponent } from '@app/components/chat-display/chat-display.component';
// import { ChatInputComponent } from '@app/components/chat-input/chat-input.component';
// import { CommunicationBoxComponent } from '@app/components/communication-box/communication-box.component';
// import { FontIncreaserComponent } from '@app/components/font-increaser/font-increaser.component';
// import { GameSettingPanelComponent } from '@app/components/game-setting-panel/game-setting-panel.component';
// import { InfoPanelComponent } from '@app/components/info-panel/info-panel.component';
// import { LetterRackComponent } from '@app/components/letter-rack/letter-rack.component';
// import { PlayAreaComponent } from '@app/components/play-area/play-area.component';
// import { TimerComponent } from '@app/components/timer/timer.component';
// import { Color } from '@app/enums/color';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-proxy.service';
// import { Socket } from 'ngx-socket-io';
// import { of } from 'rxjs';
// import { GamePageComponent } from './game-page.component';

// export class MatDialogMock {
//     open() {
//         return {
//             afterClosed: () => of({}),
//         };
//     }
// }

// describe('GamePageComponent', () => {
//     let component: GamePageComponent;
//     let fixture: ComponentFixture<GamePageComponent>;
//     let gameSpy: jasmine.SpyObj<GameManagerServiceProxy>;
//     let socketSpy: jasmine.SpyObj<Socket>;

//     beforeEach(async () => {
//         const player = new Player({ playerName: 'John', color: Color.Green }, []);
//         gameSpy = jasmine.createSpyObj('GameManagerServiceProxy', ['players', 'validateIsOurTurn'], {
//             ourIndex: 0,
//             ourPlayer: player,
//             opponentPlayer: player,
//         });
//         socketSpy = jasmine.createSpyObj('Socket', ['on']);
//         gameSpy.players[0] = new Player({ playerName: 'Jacob', color: 1 }, []);
//         await TestBed.configureTestingModule({
//             imports: [MatCardModule, MatInputModule, NoopAnimationsModule, BrowserAnimationsModule],
//             declarations: [
//                 GamePageComponent,
//                 GameSettingPanelComponent,
//                 PlayAreaComponent,
//                 CommunicationBoxComponent,
//                 ChatDisplayComponent,
//                 ChatInputComponent,
//                 PlayAreaComponent,
//                 InfoPanelComponent,
//                 LetterRackComponent,
//                 TimerComponent,
//                 FontIncreaserComponent,
//             ],
//             providers: [
//                 { provide: GameManagerServiceProxy, useValue: gameSpy },
//                 { provide: Socket, useValue: socketSpy },
//                 { provide: MatDialog, useValue: {} },
//                 { provide: HttpClient, useValue: {} },
//                 { provide: HttpHandler, useValue: {} },
//             ],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(GamePageComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });
// });
