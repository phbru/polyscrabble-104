import { Component } from '@angular/core';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';

@Component({
    selector: 'app-game-page',
    templateUrl: './game-page.component.html',
    styleUrls: ['./game-page.component.scss'],
})
export class GamePageComponent {
    constructor(public gameService: GameServiceProxy) {}
}
