/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { LanguageService } from '@app/services/language/language.service';

@Component({
    selector: 'app-language-selector',
    templateUrl: './language-selector.component.html',
    styleUrls: ['./language-selector.component.scss'],
})
export class LanguageSelectorComponent {
    constructor(public languageService: LanguageService) {}

    changeLanguage(selectedLangage: string) {
        this.languageService.changeLanguage(selectedLangage);
    }
}
