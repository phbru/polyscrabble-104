import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';

@Component({
    selector: 'app-launch-game-dialog',
    templateUrl: './launch-game-dialog.component.html',
    styleUrls: ['./launch-game-dialog.component.scss'],
})
export class LaunchGameDialogComponent {
    openSpots: number = 0;
    constructor(private waitingRoomService: WaitingRoomServiceProxy, private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) openSpots: number) {
        this.openSpots = openSpots;
    }

    async launchGame(): Promise<void> {
        for (let i = 1; i < this.openSpots + 1; i++) this.waitingRoomService.addVirtualPlayer();
        await this.waitingRoomService.launchGame();
        this.closeDialog();
    }

    closeDialog(): void {
        this.dialog.closeAll();
    }
}
