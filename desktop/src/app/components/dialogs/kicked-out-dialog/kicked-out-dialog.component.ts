import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-kicked-out-dialog',
    templateUrl: './kicked-out-dialog.component.html',
    styleUrls: ['./kicked-out-dialog.component.scss'],
})
export class KickedOutDialogComponent {
    constructor(private matDialog: MatDialog) {}

    closeModal(): void {
        this.matDialog.closeAll();
    }
}
