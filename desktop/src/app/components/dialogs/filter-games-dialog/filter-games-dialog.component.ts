/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAX_PLAYER_GAME, MIN_PLAYER_GAME } from '@app/constants/create-game';
import { ACCESS, ALL, DICTIONARY, PLAYERS, TIMER } from '@app/constants/game-filters';
import { MAX_VALUE_TIMER_MINUTE, ONE_MINUTE, TIMER_CHANGE_VALUE } from '@app/constants/timer';
import { DictionaryProxyService } from '@app/services/dictionary-proxy/dictionary-proxy.service';
import { GameFiltersService } from '@app/services/game-filters/game-filters.service';
import { Filter } from '@app/types/game-filters';
import { Time } from '@app/types/time';

@Component({
    selector: 'app-filter-games-dialog',
    templateUrl: './filter-games-dialog.component.html',
    styleUrls: ['./filter-games-dialog.component.scss'],
})
export class FilterGamesDialogComponent implements AfterViewInit {
    @ViewChild('publicAccess') publicAccess: ElementRef;
    @ViewChild('privateAccess') privateAccess: ElementRef;
    @ViewChild('frenchDictionary') frenchDictionary: ElementRef;
    @ViewChild('englishDictionary') englishDictionary: ElementRef;
    @ViewChild('allTimer') allTimer: ElementRef;
    @ViewChild('timer') timer: ElementRef;
    @ViewChild('allPlayers') allPlayers: ElementRef;
    @ViewChild('players') players: ElementRef;
    appliedGameFilters: Filter[] = [];
    dropdownStyleAccess: string = 'title-container';
    dropdownStyleDictionary: string = 'title-container';
    isFormValid: boolean = true;
    numberPlayer: number = 2;
    timerValue: Time = { minutes: 2, seconds: 0 };
    isChangeBonus = false;
    selectedDictionary: string = ALL;
    selectedAccessType: string = ALL;
    accessType: string[] = [ALL, 'mainPage.filterGames.publicAccess', 'mainPage.filterGames.privateAccess'];
    dictionaries: string[] = [ALL, 'mainPage.filterGames.frenchDictionary', 'mainPage.filterGames.englishDictionary'];

    constructor(
        private matDialog: MatDialog,
        private gameFiltersService: GameFiltersService,
        private changeDetectorRef: ChangeDetectorRef,
        public dictionaryProxyService: DictionaryProxyService,
        public dialogRef: MatDialogRef<FilterGamesDialogComponent>,
    ) {}

    @HostListener('document:keydown.enter', ['$event']) onKeydownHandler(event: KeyboardEvent) {
        event.preventDefault();
        this.applyFilters();
    }

    ngAfterViewInit(): void {
        this.setCurrentFilters();
    }

    triggerClosedStyleAccess(): void {
        this.dropdownStyleAccess = 'title-container';
    }

    triggerOpenedStyleAccess(): void {
        this.dropdownStyleAccess = 'title-container-clicked';
    }

    triggerClosedStyleDictionary(): void {
        this.dropdownStyleDictionary = 'title-container';
    }

    triggerOpenedStyleDictionary(): void {
        this.dropdownStyleDictionary = 'title-container-clicked';
    }

    closeModal(): void {
        this.matDialog.closeAll();
    }

    setFilter(translatedLabel: string, value: string): void {
        const filterToSet: Filter = { translatedLabel, value };
        const foundIndex = this.appliedGameFilters.findIndex((filter) => filter.translatedLabel === translatedLabel);
        if (foundIndex >= 0) {
            if (value === ALL) this.appliedGameFilters.splice(foundIndex, 1);
            else this.appliedGameFilters[foundIndex] = filterToSet;
        } else if (value !== ALL) this.appliedGameFilters.push(filterToSet);
    }

    getFilter(translatedLabel: string): string {
        const foundIndex = this.gameFiltersService.appliedGameFilters.findIndex((filter) => filter.translatedLabel === translatedLabel);
        if (foundIndex >= 0) return this.gameFiltersService.appliedGameFilters[foundIndex].value;
        return ALL;
    }

    setCurrentFilters(): void {
        this.selectedAccessType = this.getFilter(ACCESS);
        this.setFilter(ACCESS, this.getFilter(ACCESS));
        this.selectedDictionary = this.getFilter(DICTIONARY);
        this.setFilter(DICTIONARY, this.getFilter(DICTIONARY));

        if (this.getFilter(TIMER) === ALL) {
            this.toggleAllTimer();
        } else {
            this.timerValue = this.getTime(this.getFilter(TIMER));
            this.setFilter(TIMER, this.getFilter(TIMER));
            this.allTimer.nativeElement.checked = false;
            this.timer.nativeElement.classList.remove('disabled-container');
        }

        if (this.getFilter(PLAYERS) === ALL) {
            this.toggleAllPlayers();
        } else {
            this.numberPlayer = Number(this.getFilter(PLAYERS));
            this.setFilter(PLAYERS, this.getFilter(PLAYERS));
            this.allPlayers.nativeElement.checked = false;
            this.players.nativeElement.classList.remove('disabled-container');
        }

        this.changeDetectorRef.detectChanges();
    }

    getTimeString(timerValue: number): string {
        if (timerValue % 1 === 0) return `${timerValue}:00`;
        return `${Math.trunc(timerValue)}:30`;
    }

    getTime(timerValue: string): Time {
        const minutes = Number(timerValue.split(':')[0]);
        const seconds = Number(timerValue.split(':')[1]);
        return { minutes, seconds };
    }

    decrementTimerValue(): void {
        if (this.timerValue.minutes === 0 && this.timerValue.seconds <= TIMER_CHANGE_VALUE) return;

        this.timerValue.seconds -= TIMER_CHANGE_VALUE;
        if (this.timerValue.seconds === -TIMER_CHANGE_VALUE) {
            this.timerValue.seconds = TIMER_CHANGE_VALUE;
            this.timerValue.minutes--;
        }
        this.allTimer.nativeElement.checked = false;
        const time = this.timerValue.minutes + this.timerValue.seconds / ONE_MINUTE;
        this.setFilter(TIMER, this.getTimeString(time));
        this.timer.nativeElement.classList.remove('disabled-container');
    }

    incrementTimerValue(): void {
        if (this.timerValue.minutes === MAX_VALUE_TIMER_MINUTE && this.timerValue.seconds <= 0) return;

        this.timerValue.seconds += TIMER_CHANGE_VALUE;
        if (this.timerValue.seconds === ONE_MINUTE) {
            this.timerValue.seconds = 0;
            this.timerValue.minutes++;
        }
        this.allTimer.nativeElement.checked = false;
        const time = this.timerValue.minutes + this.timerValue.seconds / ONE_MINUTE;
        this.setFilter(TIMER, this.getTimeString(time));
        this.timer.nativeElement.classList.remove('disabled-container');
    }

    decrementPlayerNumber(): void {
        if (this.numberPlayer <= MIN_PLAYER_GAME) return;
        else this.numberPlayer--;

        this.allPlayers.nativeElement.checked = false;
        this.setFilter(PLAYERS, this.numberPlayer.toString());
        this.players.nativeElement.classList.remove('disabled-container');
    }

    incrementPlayerNumber(): void {
        if (this.numberPlayer >= MAX_PLAYER_GAME) return;
        else this.numberPlayer++;

        this.allPlayers.nativeElement.checked = false;
        this.setFilter(PLAYERS, this.numberPlayer.toString());
        this.players.nativeElement.classList.remove('disabled-container');
    }

    selectDictionary(dictionaryTitle: string): void {
        this.selectedDictionary = dictionaryTitle;
        this.setFilter(DICTIONARY, dictionaryTitle);
    }

    selectAccessType(accessType: string): void {
        this.selectedAccessType = accessType;
        this.setFilter(ACCESS, accessType);
    }

    toggleAllTimer(): void {
        if (this.allTimer.nativeElement.checked) {
            this.timer.nativeElement.classList.add('disabled-container');
            this.setFilter(TIMER, ALL);
        } else {
            this.timer.nativeElement.classList.remove('disabled-container');
            const time = this.timerValue.minutes + this.timerValue.seconds / ONE_MINUTE;
            this.setFilter(TIMER, this.getTimeString(time));
        }
    }

    toggleAllPlayers(): void {
        if (this.allPlayers.nativeElement.checked) {
            this.players.nativeElement.classList.add('disabled-container');
            this.setFilter(PLAYERS, ALL);
        } else {
            this.players.nativeElement.classList.remove('disabled-container');
            this.setFilter(PLAYERS, this.numberPlayer.toString());
        }
    }

    applyFilters(): void {
        this.gameFiltersService.appliedGameFilters = this.appliedGameFilters;
        this.matDialog.closeAll();
    }
}
