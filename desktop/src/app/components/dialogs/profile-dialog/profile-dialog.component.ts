/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '@app/classes/user';

@Component({
    selector: 'app-profile-dialog',
    templateUrl: './profile-dialog.component.html',
    styleUrls: ['./profile-dialog.component.scss'],
})
export class ProfileDialogComponent {
    user: User;
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
        this.user = data.user;
    }

    displaySeconds(): number | string {
        const TEN = 10;
        const second: number = Math.round(this.user.statistics.averageTimePerGame.seconds);
        return second < TEN ? '0' + second : second;
    }
}
