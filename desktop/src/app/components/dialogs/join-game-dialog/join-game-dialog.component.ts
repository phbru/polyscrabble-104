import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameParticipantType } from '@app/enums/player-type';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';
import { GameInfo } from '@app/types/game-info';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-join-game-dialog',
    templateUrl: './join-game-dialog.component.html',
    styleUrls: ['./join-game-dialog.component.scss'],
})
export class JoinGameDialogComponent {
    @ViewChild('password') password: ElementRef;
    playerType: typeof GameParticipantType = GameParticipantType;
    gameToJoin: GameInfo;
    isGamePublic: boolean = true;
    isPasswordProtected: boolean = true;
    isPasswordValid: boolean = true;
    isPasswordShown: boolean = false;
    constructor(
        private waitingRoomService: WaitingRoomServiceProxy,
        private matDialog: MatDialog,
        private snackBarService: SnackBarService,
        private translateService: TranslateService,
        @Inject(MAT_DIALOG_DATA) gameData: GameInfo,
    ) {
        this.gameToJoin = gameData;
        this.isPasswordProtected = this.gameToJoin.password.length === 0 ? false : true;
    }

    closeModal(): void {
        this.matDialog.closeAll();
    }

    togglePasswordVisibility(): void {
        if (!this.isPasswordShown) this.password.nativeElement.type = 'text';
        else this.password.nativeElement.type = 'password';
        this.isPasswordShown = !this.isPasswordShown;
    }

    validatePassword(): void {
        if (this.gameToJoin.password === this.password.nativeElement.value) this.isPasswordProtected = false;
        else {
            this.isPasswordValid = false;
        }
    }

    async joinWaitingRoom(playerType: GameParticipantType): Promise<void> {
        if (this.gameToJoin.openSpots === 0 && playerType === GameParticipantType.RegularPlayer) {
            this.snackBarService.open(this.translateService.instant('waitingRoom.joinGame.gameFull'), 'OK');
            return;
        }
        await this.waitingRoomService.joinGame(this.gameToJoin, playerType);
        this.matDialog.closeAll();
    }
}
