// describe('JoinGameDialogComponent', () => {
// let component: JoinGameDialogComponent;
// let fixture: ComponentFixture<JoinGameDialogComponent>;
// let gameSpy: jasmine.SpyObj<GameManagerServiceProxy>;
// let lobbyServiceSpy: jasmine.SpyObj<LobbyService>;
// let socketSpy: jasmine.SpyObj<Socket>;
// let chatHandlerSpy: jasmine.SpyObj<ChatHandlerService>;
// let userSpy: jasmine.SpyObj<SelfUserServiceProxy>;
// beforeEach(async () => {
//     lobbyServiceSpy = jasmine.createSpyObj('LobbyService', ['fetchGames', 'isGameExist', 'deleteGame']);
//     lobbyServiceSpy.gameToJoin = { creatorName: 'Jacob', roomName: 'testroom', time: 0 };
//     gameSpy = jasmine.createSpyObj('GameManagerServiceProxy', ['addHumanPlayer', 'preparePlayers', 'startGame']);
//     socketSpy = jasmine.createSpyObj('Socket', ['emit']);
//     chatHandlerSpy = jasmine.createSpyObj('ChatHandlerService', ['clear']);
//     userSpy = jasmine.createSpyObj('UserService', ['getUserName']);
//     await TestBed.configureTestingModule({
//         declarations: [JoinGameDialogComponent],
//         imports: [AppMaterialModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, MatIconModule, MatDialogModule],
//         providers: [
//             { provide: FormBuilder },
//             { provide: LobbyService, useValue: lobbyServiceSpy },
//             { provide: GameManagerServiceProxy, useValue: gameSpy },
//             { provide: Socket, useValue: socketSpy },
//             { provide: ChatHandlerService, useValue: chatHandlerSpy },
//             { provide: SelfUserServiceProxy, useValue: userSpy },
//         ],
//     }).compileComponents();
//     ServiceLocator.injector = { get: TestBed.inject };
// });
// beforeEach(() => {
//     fixture = TestBed.createComponent(JoinGameDialogComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
// });
// it('should create', () => {
//     expect(component).toBeTruthy();
// });
// it('should start game with player name', async () => {
//     userSpy.user.username = 'Jacob';
//     await component.setupGame();
//     expect(socketSpy.emit).toHaveBeenCalledWith('joinGame', 'Jacob', 'testroom');
//     expect(gameSpy.addHumanPlayer).toHaveBeenCalled();
//     expect(gameSpy.preparePlayers).toHaveBeenCalled();
//     expect(gameSpy.startGame).toHaveBeenCalled();
//     expect(lobbyServiceSpy.deleteGame).toHaveBeenCalled();
// });
// it('should do nothing with undefined player name', () => {
//     spyOn(component.dialogForm, 'get').and.returnValue(null);
//     component.setupGame();
//     expect(socketSpy.emit).not.toHaveBeenCalled();
//     expect(gameSpy.addHumanPlayer).not.toHaveBeenCalled();
//     expect(gameSpy.preparePlayers).not.toHaveBeenCalled();
//     expect(gameSpy.startGame).not.toHaveBeenCalled();
//     expect(lobbyServiceSpy.deleteGame).not.toHaveBeenCalled();
// });
// });
