import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/classes/user';
import { ClientFriendServiceProxyService } from '@app/services/client-friend-service-proxy/client-friend-service-proxy.service';
import { GlobalUserProxyService } from '@app/services/global-user-proxy/global-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-invite-user-dialog',
    templateUrl: './invite-user-dialog.component.html',
    styleUrls: ['./invite-user-dialog.component.scss'],
})
export class InviteUserDialogComponent {
    searchBarContent: string = '';
    filteredUsers: User[] = this.globalUserService.onlineUsers;
    constructor(
        public globalUserService: GlobalUserProxyService,
        private dialog: MatDialog,
        private clientFriendService: ClientFriendServiceProxyService,
        private waitingRoomService: WaitingRoomServiceProxy,
        private snackBarService: SnackBarService,
        private translate: TranslateService,
    ) {
        this.filterOnlineUsers();
    }

    closeModal(): void {
        this.dialog.closeAll();
    }

    // Put friends first
    reorderUsersForFriends() {
        const newUsers = [];
        for (const user of this.filteredUsers) {
            if (this.clientFriendService.checkIsFriend(user.id)) {
                newUsers.unshift(user);
            } else {
                newUsers.push(user);
            }
        }
        this.filteredUsers = newUsers;
    }

    // eslint-disable-next-line no-unused-vars
    filterOnlineUsers(): void {
        if (this.searchBarContent === '') {
            this.filteredUsers = this.globalUserService.onlineUsers.filter(
                (user) => !this.waitingRoomService.checkIsUserInGame(user.id) && !this.waitingRoomService.checkIsInvited(user.id),
            );
        } else {
            this.filteredUsers = this.globalUserService.onlineUsers.filter(
                (user) =>
                    user.username.toUpperCase().includes(this.searchBarContent.toUpperCase()) &&
                    !this.waitingRoomService.checkIsUserInGame(user.id) &&
                    !this.waitingRoomService.checkIsInvited(user.id),
            );
        }

        this.reorderUsersForFriends();
    }

    inviteUser(user: User): void {
        this.waitingRoomService.inviteUser(user.id);
        this.waitingRoomService.addInvitedUser(user.id);
        this.closeModal();
        this.snackBarService.open(this.translate.instant('userInteractions.invite'), 'OK');
    }
}
