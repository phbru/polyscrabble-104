import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/classes/user';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';

@Component({
    selector: 'app-create-channel-dialog',
    templateUrl: './create-channel-dialog.component.html',
    styleUrls: ['./create-channel-dialog.component.scss'],
})
export class CreateChannelDialogComponent {
    @ViewChild('channelName') channelName: ElementRef;
    searchBarContent: string = '';
    filteredUsers: User[] = [];
    isFormValid: boolean = true;
    constructor(private dialog: MatDialog, public globalChannelService: GlobalChannelProxyService) {}

    @HostListener('document:keydown.enter', ['$event'])
    onKeydownHandler(event: KeyboardEvent) {
        event.preventDefault();
        this.confirm();
    }

    closeModal(): void {
        this.isFormValid = true;
        this.dialog.closeAll();
    }

    async confirm() {
        const MAX_CHANNEL_LENGTH = 25;
        if (this.channelName.nativeElement.value === '' || this.channelName.nativeElement.value.length >= MAX_CHANNEL_LENGTH) {
            this.isFormValid = false;
            return;
        }
        const channel = await this.globalChannelService.createChannel(this.channelName.nativeElement.value);
        this.globalChannelService.joinChannel(channel.id);
        this.closeModal();
    }
}
