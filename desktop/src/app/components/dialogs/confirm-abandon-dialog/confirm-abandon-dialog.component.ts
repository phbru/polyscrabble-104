import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';

@Component({
    selector: 'app-confirm-abandon-dialog',
    templateUrl: './confirm-abandon-dialog.component.html',
    styleUrls: ['./confirm-abandon-dialog.component.scss'],
})
export class ConfirmAbandonDialogComponent {
    constructor(private gameServiceProxy: GameServiceProxy, private dialog: MatDialog) {}

    quitGame(): void {
        if (this.gameServiceProxy.isPlayer) {
            this.gameServiceProxy.confirmAbandon();
        } else {
            this.gameServiceProxy.quitAsObserver();
        }
    }

    closeDialog(): void {
        this.dialog.closeAll();
    }
}
