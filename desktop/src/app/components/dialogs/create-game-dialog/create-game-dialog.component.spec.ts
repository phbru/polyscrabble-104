/* eslint-disable dot-notation -- test private methods*/

// describe('CreateGameDialogComponent', () => {
// let component: CreateGameDialogComponent;
// let fixture: ComponentFixture<CreateGameDialogComponent>;
// let gameSpy: jasmine.SpyObj<GameManagerServiceProxy>;
// let communicationServiceSpy: jasmine.SpyObj<CommunicationService>;
// let socketSpy: jasmine.SpyObj<Socket>;
// let boardModelSpy: jasmine.SpyObj<BoardModelServiceProxy>;
// let timerSpy: jasmine.SpyObj<TimerServiceProxy>;
// let dictionarySpy: jasmine.SpyObj<DictionaryServiceProxy>;
// let chatHandlerSpy: jasmine.SpyObj<ChatHandlerService>;
// let lobbyServiceSpy: jasmine.SpyObj<LobbyService>;
// let routerSpy: jasmine.SpyObj<Router>;
// let dialogSpy: jasmine.SpyObj<MatDialog>;
// let userSpy: jasmine.SpyObj<SelfUserServiceProxy>;
// beforeEach(async () => {
//     gameSpy = jasmine.createSpyObj('GameManagerServiceProxy', ['addHumanPlayer', 'set', 'startGame', 'setModeServer']);
//     communicationServiceSpy = jasmine.createSpyObj('CommunicationService', ['addGame']);
//     communicationServiceSpy.addGame.and.returnValue(new Observable());
//     socketSpy = jasmine.createSpyObj('Socket', ['emit', 'on']);
//     boardModelSpy = jasmine.createSpyObj('BoardModelServiceProxy', ['shuffleBonuses']);
//     timerSpy = jasmine.createSpyObj('TimerServiceProxy', ['numberToTime', 'setTimePerTurn']);
//     routerSpy = jasmine.createSpyObj('Router', ['navigate']);
//     dialogSpy = jasmine.createSpyObj('MatDialog', ['open', 'closeAll']);
//     dictionarySpy = jasmine.createSpyObj('DictionaryServiceProxy', ['setupDictionary']);
//     chatHandlerSpy = jasmine.createSpyObj('ChatHandlerService', ['clear']);
//     lobbyServiceSpy = jasmine.createSpyObj('LobbyService', ['fetchGames']);
//     userSpy = jasmine.createSpyObj('UserService', ['getUserName', 'setUser']);
//     await TestBed.configureTestingModule({
//         declarations: [CreateGameDialogComponent],
//         imports: [AppMaterialModule, BrowserAnimationsModule, FormsModule, ReactiveFormsModule, MatIconModule, MatDialogModule],
//         providers: [
//             { provide: FormBuilder },
//             { provide: GameManagerServiceProxy, useValue: gameSpy },
//             { provide: CommunicationService, useValue: communicationServiceSpy },
//             { provide: Socket, useValue: socketSpy },
//             { provide: BoardModelServiceProxy, useValue: boardModelSpy },
//             { provide: TimerServiceProxy, useValue: timerSpy },
//             { provide: Router, useValue: routerSpy },
//             { provide: DictionaryServiceProxy, useValue: dictionarySpy },
//             { provide: ChatHandlerService, useValue: chatHandlerSpy },
//             { provide: LobbyService, useValue: lobbyServiceSpy },
//             { provide: MatDialog, useValue: dialogSpy },
//             { provide: SelfUserServiceProxy, useValue: userSpy },
//         ],
//     }).compileComponents();
// });
// beforeEach(() => {
//     fixture = TestBed.createComponent(CreateGameDialogComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
// });
// it('should create', () => {
//     expect(component).toBeTruthy();
// });
// // it('should create game', async () => {
// //     userSpy.user.username = 'Jacob';
// //     const joinGameSpy = spyOn<any>(component, 'joinOurGame');
// //     await component.createGame();
// //     dialogSpy.closeAll();
// //     expect(joinGameSpy).toHaveBeenCalled();
// // });
// it('should not create game if name and time are undefined', () => {
//     spyOn(component.gameMenuForm, 'get').and.returnValue(null);
//     const joinGameSpy = spyOn<any>(component, 'joinOurGame');
//     component.createGame();
//     expect(communicationServiceSpy.addGame).not.toHaveBeenCalled();
//     expect(joinGameSpy).not.toHaveBeenCalled();
// });
// it('should shuffle bonuses if necessary', async () => {
//     component.gameMenuForm.get('playerName')?.setValue('Jacob');
//     component.gameMenuForm.get('time')?.setValue('1.5');
//     component.isChangeBonus = true;
//     await component['joinOurGame']('', 0);
//     expect(boardModelSpy.shuffleBonuses).toHaveBeenCalled();
// });
// it('should change bonus boolean', () => {
//     component.setChangeBonus();
//     expect(component.isChangeBonus).toBeTruthy();
// });
// it('should join our game', async () => {
//     await component['joinOurGame']('Test', 0);
//     expect(gameSpy.addHumanPlayer).toHaveBeenCalledWith('Test');
// });
// it('should get roomName', () => {
//     component['joinOurGame']('Test', 0);
//     const callback = socketSpy.emit.calls.mostRecent().args[3];
//     callback('roomtest');
//     expect(communicationServiceSpy.addGame).toHaveBeenCalled();
// });
// });
