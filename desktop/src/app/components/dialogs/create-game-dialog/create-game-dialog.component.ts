import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MAX_PLAYER_GAME, MIN_PLAYER_GAME } from '@app/constants/create-game';
import { MAX_VALUE_TIMER_MINUTE, ONE_MINUTE, TIMER_CHANGE_VALUE } from '@app/constants/timer';
import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
import { DictionaryProxyService } from '@app/services/dictionary-proxy/dictionary-proxy.service';
// import { LobbyService } from '@app/services/lobby/lobby.service'; //TODO : remove
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';
import { DictionaryName } from '@app/types/dictionary';
import { GameInfo } from '@app/types/game-info';
import { Time } from '@app/types/time';
import { Socket } from 'ngx-socket-io';

const PUBLIC_ACCESS = 'mainPage.filterGames.publicAccess';
const PRIVATE_ACCESS = 'mainPage.filterGames.privateAccess';

@Component({
    selector: 'app-create-game-dialog',
    templateUrl: './create-game-dialog.component.html',
    styleUrls: ['./create-game-dialog.component.scss'],
})
export class CreateGameDialogComponent {
    @ViewChild('decrementPlayer') decrementPlayer: ElementRef;
    @ViewChild('incrementPlayer') incrementPlayer: ElementRef;
    @ViewChild('decrementTimer') decrementTimer: ElementRef;
    @ViewChild('incrementTimer') incrementTimer: ElementRef;
    @ViewChild('password') password: ElementRef;
    dropdownStyle: string = 'title-container';
    dropdownStyleAccess: string = 'title-container';
    isPasswordShown: boolean = false;
    isFormValid: boolean = true;
    numberPlayer: number = 2;
    timerValue: Time = { minutes: 1, seconds: 0 };
    gameMenuForm: FormGroup;
    isChangeBonus = false;
    selectedDictionary: string = 'Français';
    dictionaryList: DictionaryName[] = [];
    selectedAccessType: string = PUBLIC_ACCESS;
    accessType: string[] = [PUBLIC_ACCESS, PRIVATE_ACCESS];

    constructor(
        private fb: FormBuilder,
        private waitingRoomService: WaitingRoomServiceProxy,
        private socket: Socket,
        public boardModel: BoardModelServiceProxy,
        private dialog: MatDialog,
        private userService: SelfUserServiceProxy,
        public dictionaryProxyService: DictionaryProxyService,
    ) {
        this.gameMenuForm = this.fb.group({
            turnTime: ['1', Validators.required],
        });
        this.gameMenuForm.get('dictionaryName')?.markAsTouched();
        dictionaryProxyService.getDictionaryList();
        this.dictionaryList = dictionaryProxyService.dictionaryList;
    }

    @HostListener('document:keydown.enter', ['$event']) onKeydownHandler(event: KeyboardEvent) {
        event.preventDefault();
        this.createGame();
    }

    closeModal(): void {
        this.isFormValid = true;
        this.dialog.closeAll();
    }

    decrementTimerValue(): void {
        if (this.timerValue.minutes === 0 && this.timerValue.seconds <= TIMER_CHANGE_VALUE) return;

        this.timerValue.seconds -= TIMER_CHANGE_VALUE;
        if (this.timerValue.seconds === -TIMER_CHANGE_VALUE) {
            this.timerValue.seconds = TIMER_CHANGE_VALUE;
            this.timerValue.minutes--;
        }
    }

    incrementTimerValue(): void {
        if (this.timerValue.minutes === MAX_VALUE_TIMER_MINUTE && this.timerValue.seconds <= 0) return;

        this.timerValue.seconds += TIMER_CHANGE_VALUE;
        if (this.timerValue.seconds === ONE_MINUTE) {
            this.timerValue.seconds = 0;
            this.timerValue.minutes++;
        }
    }

    decrementPlayerNumber(): void {
        if (this.numberPlayer <= MIN_PLAYER_GAME) return;
        else this.numberPlayer--;
    }

    incrementPlayerNumber(): void {
        if (this.numberPlayer >= MAX_PLAYER_GAME) return;
        else this.numberPlayer++;
    }

    togglePasswordVisibility(): void {
        if (!this.isPasswordShown) this.password.nativeElement.type = 'text';
        else this.password.nativeElement.type = 'password';
        this.isPasswordShown = !this.isPasswordShown;
    }

    triggerClosedStyleAccess(): void {
        this.dropdownStyleAccess = 'title-container';
    }

    triggerOpenedStyleAccess(): void {
        this.dropdownStyleAccess = 'title-container-clicked';
    }

    triggerClosedStyle(): void {
        this.dropdownStyle = 'title-container';
    }

    triggerOpenedStyle(): void {
        this.dropdownStyle = 'title-container-clicked';
    }

    selectDictionary(dictionaryTitle: string): void {
        this.selectedDictionary = dictionaryTitle;
    }

    selectAccessType(accessType: string): void {
        this.selectedAccessType = accessType;
    }

    async createGame() {
        this.dialog.closeAll();
        const time = this.timerValue.minutes + this.timerValue.seconds / ONE_MINUTE;
        if (this.selectedDictionary !== undefined) {
            await this.connectToWaitingRoom(this.userService.user.username, time, this.numberPlayer);
        }
    }

    // joining waiting room without providing a gameId (2nd parameter of message 'joingGame') creates it
    private async connectToWaitingRoom(creatorName: string, time: number, openSpots: number) {
        this.socket.emit('joinGame', creatorName, '', (gameId: string) => {
            const gameInfo: GameInfo = {
                id: gameId,
                creatorName,
                time,
                openSpots,
                dictionary: this.selectedDictionary,
                password: this.selectedAccessType === PUBLIC_ACCESS ? this.password.nativeElement.value : '',
                players: [],
                observers: [],
                virtualPlayers: [],
                invitedUsers: [],
                isGameStarted: false,
                isPrivate: this.selectedAccessType === PUBLIC_ACCESS ? false : true,
            };
            this.waitingRoomService.createWaitingRoom(gameInfo, this.userService.user);
        });
    }
}
