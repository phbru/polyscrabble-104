import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-canceled-game-dialog',
    templateUrl: './canceled-game-dialog.component.html',
    styleUrls: ['./canceled-game-dialog.component.scss'],
})
export class CanceledGameDialogComponent {
    constructor(private matDialog: MatDialog) {}

    closeModal(): void {
        this.matDialog.closeAll();
    }
}
