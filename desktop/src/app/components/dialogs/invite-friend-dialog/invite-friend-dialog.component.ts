import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/classes/user';
import { ClientFriendServiceProxyService } from '@app/services/client-friend-service-proxy/client-friend-service-proxy.service';
import { GlobalInteractionProxyService } from '@app/services/global-interaction-proxy/global-interaction-proxy.service';
import { GlobalUserProxyService } from '@app/services/global-user-proxy/global-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-invite-friend-dialog',
    templateUrl: './invite-friend-dialog.component.html',
    styleUrls: ['./invite-friend-dialog.component.scss'],
})
export class InviteFriendDialogComponent {
    searchBarContent: string = '';
    filteredUsers: User[] = this.globalUserService.onlineUsers;
    constructor(
        public globalUserService: GlobalUserProxyService,
        private globalInteractionService: GlobalInteractionProxyService,
        public friendService: ClientFriendServiceProxyService,
        private dialog: MatDialog,
        private translate: TranslateService,
        private snackBarService: SnackBarService,
    ) {
        this.filterUsers();
    }

    closeModal(): void {
        this.dialog.closeAll();
    }

    // eslint-disable-next-line no-unused-vars
    filterUsers(): void {
        if (this.searchBarContent === '') {
            this.filteredUsers = this.globalUserService.onlineUsers.filter((user) => this.friendService.checkIfRequestPossible(user.id));
        } else
            this.filteredUsers = this.globalUserService.onlineUsers.filter(
                (user) => user.username.includes(this.searchBarContent) && this.friendService.checkIfRequestPossible(user.id),
            );
    }
    inviteUser(userId: string): void {
        this.globalInteractionService.createFriendRequest(userId);
        this.closeModal();
        this.snackBarService.open(this.translate.instant('userInteractions.invite'), 'OK');
    }
}
