import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/classes/user';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';
import { GlobalUserProxyService } from '@app/services/global-user-proxy/global-user-proxy.service';

@Component({
    selector: 'app-channel-members-dialog',
    templateUrl: './channel-members-dialog.component.html',
    styleUrls: ['./channel-members-dialog.component.scss'],
})
export class ChannelMembersDialogComponent {
    searchBarContent: string = '';
    filteredUsers: User[] = [];
    constructor(
        private dialog: MatDialog,
        public clientChannelService: ClientChannelProxyService,
        public globalUserService: GlobalUserProxyService,
    ) {}

    closeModal(): void {
        this.dialog.closeAll();
    }
}
