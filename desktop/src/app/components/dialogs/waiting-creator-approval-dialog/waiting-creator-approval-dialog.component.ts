import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { Socket } from 'ngx-socket-io';

@Component({
    selector: 'app-waiting-creator-approval-dialog',
    templateUrl: './waiting-creator-approval-dialog.component.html',
    styleUrls: ['./waiting-creator-approval-dialog.component.scss'],
})
export class WaitingCreatorApprovalDialogComponent {
    gameId: string = '';
    constructor(
        @Inject(MAT_DIALOG_DATA) gameId: string,
        private socket: Socket,
        private selfUserService: SelfUserServiceProxy,
        private matDialog: MatDialog,
    ) {
        this.gameId = gameId;
    }

    cancelRequest(): void {
        this.socket.emit('cancelJoin', this.selfUserService.user, this.gameId);
        this.matDialog.closeAll();
    }
}
