import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ClientFriendServiceProxyService } from '@app/services/client-friend-service-proxy/client-friend-service-proxy.service';
import { GlobalInteractionProxyService } from '@app/services/global-interaction-proxy/global-interaction-proxy.service';

@Component({
    selector: 'app-friend-requests-dialog',
    templateUrl: './friend-requests-dialog.component.html',
    styleUrls: ['./friend-requests-dialog.component.scss'],
})
export class FriendRequestsDialogComponent {
    constructor(
        private globalInteractionService: GlobalInteractionProxyService,
        public friendService: ClientFriendServiceProxyService,
        private dialog: MatDialog,
    ) {}

    closeModal(): void {
        this.dialog.closeAll();
    }

    acceptInvite(userId: string): void {
        this.globalInteractionService.acceptInvite(userId);
    }

    removeInvite(userId: string): void {
        this.globalInteractionService.removeInvite(userId);
    }

    refuseInvite(userId: string): void {
        this.globalInteractionService.refuseInvite(userId);
    }
}
