import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';
import { GameInfo } from '@app/types/game-info';
import { Socket } from 'ngx-socket-io';
@Component({
    selector: 'app-end-game-dialog',
    templateUrl: './end-game-dialog.component.html',
    styleUrls: ['./end-game-dialog.component.scss'],
})
export class EndGameDialogComponent {
    winner: string;
    constructor(
        public gameServiceProxy: GameServiceProxy,
        @Inject(MAT_DIALOG_DATA) winner: string,
        private dialog: MatDialog,
        private selfUserService: SelfUserServiceProxy,
        private socket: Socket,
        private waitingRoomService: WaitingRoomServiceProxy,
    ) {
        this.winner = winner;
    }

    quitGame(): void {
        if (this.gameServiceProxy.isPlayer) {
            this.gameServiceProxy.confirmAbandon();
        } else {
            this.gameServiceProxy.quitAsObserver();
        }
    }

    closeDialog(): void {
        this.dialog.closeAll();
    }

    async createRematch() {
        const creatorName = this.selfUserService.user.username;
        const time = this.gameServiceProxy.partialGameInfo.time;
        const openSpots = this.gameServiceProxy.players.length;
        const dictionary = this.gameServiceProxy.partialGameInfo.dictionary;
        const password = this.gameServiceProxy.partialGameInfo.password;

        await this.socket.emit('leaveGame');
        this.closeDialog();
        await this.joinWaitingRoom(creatorName, time, openSpots, dictionary, password, this.gameServiceProxy.copyOfPlayersForRematch);
    }

    // joining waiting room without providing a gameId (2nd parameter of message 'joingGame') creates it
    private async joinWaitingRoom(
        creatorName: string,
        time: number,
        openSpots: number,
        dictionary: string,
        password: string,
        playerIdsToInvite: string[],
    ) {
        await this.socket.emit('joinGame', creatorName, '', async (gameId: string) => {
            const gameInfo: GameInfo = {
                id: gameId,
                creatorName,
                time,
                dictionary,
                openSpots,
                password,
                players: [],
                observers: [],
                virtualPlayers: [],
                invitedUsers: [],
                isGameStarted: false,
                isPrivate: false,
            };
            await this.waitingRoomService.createWaitingRoom(gameInfo, this.selfUserService.user);
            for (const playerId of playerIdsToInvite) {
                this.waitingRoomService.addInvitedUser(playerId);
                this.socket.emit('inviteUser', playerId);
            }
        });
    }
}
