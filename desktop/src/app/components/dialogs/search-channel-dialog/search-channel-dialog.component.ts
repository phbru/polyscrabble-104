import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';
import { Channel } from '@app/types/channel';

@Component({
    selector: 'app-search-channel-dialog',
    templateUrl: './search-channel-dialog.component.html',
    styleUrls: ['./search-channel-dialog.component.scss'],
})
export class SearchChannelDialogComponent {
    searchBarContent: string = '';
    filteredChannels: Channel[] = [];

    constructor(
        public globalChannelService: GlobalChannelProxyService,
        public clientChannelService: ClientChannelProxyService,
        private dialog: MatDialog,
    ) {
        this.filteredChannels = globalChannelService.channels;
    }

    closeModal(): void {
        this.dialog.closeAll();
    }

    filterChannels(channels: Channel[]): Channel[] {
        this.filteredChannels = channels.filter(
            (channel) => channel.searchable && channel.name.toLowerCase().includes(this.searchBarContent.toLowerCase()),
        );
        return this.filteredChannels;
    }

    joinedChannel(channelId: string): boolean {
        for (const c of this.clientChannelService.joinedChannels) {
            if (c.channel.id === channelId) {
                return true;
            }
        }

        return false;
    }
}
