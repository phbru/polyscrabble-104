import { Component, Inject, OnDestroy } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameParticipantType } from '@app/enums/player-type';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';
import { GameInfo } from '@app/types/game-info';

@Component({
    selector: 'app-game-invitation-dialog',
    templateUrl: './game-invitation-dialog.component.html',
    styleUrls: ['./game-invitation-dialog.component.scss'],
})
export class GameInvitationDialogComponent implements OnDestroy {
    gameInvitationData: GameInfo;
    constructor(
        private waitingRoomService: WaitingRoomServiceProxy,
        private matDialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) gameData: GameInfo,
        private selfUserService: SelfUserServiceProxy,
    ) {
        this.gameInvitationData = gameData;
    }

    ngOnDestroy() {
        this.declineInvitation();
    }

    async acceptInvitation(): Promise<void> {
        await this.waitingRoomService.joinGame(this.gameInvitationData, GameParticipantType.RegularPlayer);
        this.matDialog.closeAll();
    }

    declineInvitation(): void {
        this.waitingRoomService.declineInvite(this.selfUserService.user.id, this.gameInvitationData.id);
        this.matDialog.closeAll();
    }
}
