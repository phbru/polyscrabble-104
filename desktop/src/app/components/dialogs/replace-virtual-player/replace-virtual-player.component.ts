/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Player } from '@app/classes/player';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';

@Component({
    selector: 'app-replace-virtual-player',
    templateUrl: './replace-virtual-player.component.html',
    styleUrls: ['./replace-virtual-player.component.scss'],
})
export class ReplaceVirtualPlayerComponent {
    botPlayer: Player;
    constructor(
        private gameServiceProxy: GameServiceProxy,
        private matDialog: MatDialog,
        private selfUserService: SelfUserServiceProxy,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.botPlayer = data.botPlayer;
    }

    takeBotPlace() {
        this.gameServiceProxy.takeBotPlace(this.botPlayer);
        this.matDialog.closeAll();
    }

    checkIsObserver(): boolean {
        if (this.gameServiceProxy.partialGameInfo === undefined) return false;
        for (const player of this.gameServiceProxy.players) {
            if (player.user.id === this.selfUserService.user.id) return false;
        }
        for (const observer of this.gameServiceProxy.partialGameInfo.observers) {
            if (observer.id === this.selfUserService.user.id) return true;
        }
        return false;
    }

    watchPlayer(): void {
        this.matDialog.closeAll();
        this.gameServiceProxy.setWatchedPlayer(this.data.index);
    }
}
