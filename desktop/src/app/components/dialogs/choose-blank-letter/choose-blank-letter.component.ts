import { Component, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BoardEventHandlerService } from '@app/services/board-event-handler/board-event-handler.service';

@Component({
    selector: 'app-choose-blank-letter',
    templateUrl: './choose-blank-letter.component.html',
    styleUrls: ['./choose-blank-letter.component.scss'],
})
export class ChooseBlankLetterComponent {
    letter: string = '';
    isLetterValid: boolean = true;
    constructor(private matDialog: MatDialog, private boardEventHandlerService: BoardEventHandlerService) {}

    @HostListener('document:keydown.enter', ['$event'])
    onKeydownHandler(event: KeyboardEvent) {
        event.preventDefault();
        this.validateLetter();
    }

    closeModal(): void {
        this.boardEventHandlerService.selectedToken.letter.character = '*';
        this.boardEventHandlerService.insertLetterInRack();
        this.matDialog.closeAll();
        this.boardEventHandlerService.selectedToken.letter.character = '';
    }

    validateLetter(): void {
        if (/^[a-zA-Z\s]+$/.test(this.letter) && this.letter.length === 1) this.submit();
        else this.isLetterValid = false;
    }

    submit(): void {
        this.boardEventHandlerService.blankLetter = this.letter.toUpperCase();
        this.closeModal();
    }
}
