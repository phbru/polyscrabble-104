import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ChannelInfoComponent } from '@app/components/channel-info/channel-info.component';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';

@Component({
    selector: 'app-communication-box',
    templateUrl: './communication-box.component.html',
    styleUrls: ['./communication-box.component.scss'],
})
export class CommunicationBoxComponent {
    constructor(private matDialog: MatDialog, public clientChannelService: ClientChannelProxyService) {}

    openChannelInfo() {
        this.matDialog.open(ChannelInfoComponent, {
            position: { top: '6.5%', right: '5.5%' },
            width: '200px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
        });
    }
}
