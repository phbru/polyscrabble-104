// import { HttpClient } from '@angular/common/http';
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ChatDisplayComponent } from '@app/components/chat-display/chat-display.component';
// import { ChatInputComponent } from '@app/components/chat-input/chat-input.component';
// import { AppMaterialModule } from '@app/modules/material.module';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-proxy.service';
// import { ServiceLocator } from '@app/services/service-locator';
// import { Socket } from 'ngx-socket-io';
// import { CommunicationBoxComponent } from './communication-box.component';

// describe('CommunicationBoxComponent', () => {
//     let socketSpy: jasmine.SpyObj<Socket>;
//     let component: CommunicationBoxComponent;
//     let fixture: ComponentFixture<CommunicationBoxComponent>;

//     beforeEach(async () => {
//         socketSpy = jasmine.createSpyObj('Socket', ['emit', 'on']);
//         await TestBed.configureTestingModule({
//             declarations: [CommunicationBoxComponent, ChatInputComponent, ChatDisplayComponent],
//             imports: [FormsModule, ReactiveFormsModule, AppMaterialModule, BrowserAnimationsModule],
//             providers: [
//                 { provide: GameManagerServiceProxy, useValue: {} },
//                 { provide: Socket, useValue: socketSpy },
//                 { provide: HttpClient, useValue: {} },
//             ],
//         }).compileComponents();
//         ServiceLocator.injector = { get: TestBed.inject };
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(CommunicationBoxComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });
// });
