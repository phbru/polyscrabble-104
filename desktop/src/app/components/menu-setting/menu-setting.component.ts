/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ProfileComponent } from '@app/components/profile/profile.component';
import { ColorThemeService } from '@app/services/color-theme/color-theme.service';
import { LanguageService } from '@app/services/language/language.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { Socket } from 'ngx-socket-io';

@Component({
    selector: 'app-menu-setting',
    templateUrl: './menu-setting.component.html',
    styleUrls: ['./menu-setting.component.scss'],
})
export class MenuSettingComponent {
    isLoggedIn: boolean;
    isThemeSelected: boolean = false;
    isLanguageSelected: boolean = false;
    constructor(
        public colorThemeService: ColorThemeService,
        public languageService: LanguageService,
        private socket: Socket,
        private router: Router,
        public userService: SelfUserServiceProxy,
        private matDialog: MatDialog,
        private dlgRef: MatDialogRef<MenuSettingComponent>,
        @Inject(MAT_DIALOG_DATA) isLoggedInInput: boolean,
    ) {
        this.isLoggedIn = isLoggedInInput;
    }

    showThemes() {
        this.isThemeSelected = true;
    }

    showLanguages() {
        this.isLanguageSelected = true;
    }

    logout() {
        this.dlgRef.close();
        this.socket.disconnect();
        this.router.navigate(['/login']);
        this.userService.clear();
    }

    openProfile() {
        this.dlgRef.close();
        this.matDialog.open(ProfileComponent, {
            position: { top: '2.5%', right: '1%' },
            backdropClass: 'cdk-overlay-transparent-backdrop',
        });
    }
}
