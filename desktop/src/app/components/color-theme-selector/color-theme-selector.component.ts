/* eslint-disable @typescript-eslint/no-explicit-any */
import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { BoardViewService } from '@app/services/board-view/board-view';
import { ColorThemeService } from '@app/services/color-theme/color-theme.service';

@Component({
    selector: 'app-color-theme-selector',
    templateUrl: './color-theme-selector.component.html',
    styleUrls: ['./color-theme-selector.component.scss'],
})
export class ColorThemeSelectorComponent {
    constructor(
        public colorThemeService: ColorThemeService,
        private changeDetectorRef: ChangeDetectorRef,
        private boardView: BoardViewService,
        private router: Router,
    ) {}

    changeTheme(theme: string) {
        this.colorThemeService.changeTheme(theme);
        if (this.router.url === '/game') this.boardView.drawBoardContent();
        this.changeDetectorRef.detectChanges();
        (window as any).electronAPI.setTheme(theme);
    }
}
