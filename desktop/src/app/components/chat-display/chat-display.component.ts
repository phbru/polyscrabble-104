import { AfterViewInit, Component, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/classes/user';
import { UserClickOptionsComponent } from '@app/components/user-click-options/user-click-options.component';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';

const MODAL_OFFSET = 20;
// source to fix scroll : https://stackoverflow.com/questions/50208193/scroll-last-element-of-ngfor-loop-into-view
@Component({
    selector: 'app-chat-display',
    templateUrl: './chat-display.component.html',
    styleUrls: ['./chat-display.component.scss'],
})
export class ChatDisplayComponent implements AfterViewInit {
    @ViewChildren('textBox') chatDisplay: QueryList<ElementRef>;
    constructor(
        public clientChannelService: ClientChannelProxyService,
        public globalChannelService: GlobalChannelProxyService,
        public selfUserServiceProxy: SelfUserServiceProxy,
        public matDialog: MatDialog,
    ) {}

    async ngAfterViewInit() {
        this.chatDisplay.changes.subscribe(this.onChatDisplayChange.bind(this));
    }

    openUserClickOptions(user: User, event: MouseEvent) {
        if (user.id === this.selfUserServiceProxy.user.id) return;
        const locationX = (event.clientX - MODAL_OFFSET).toString() + 'px';
        const locationY = (event.clientY - MODAL_OFFSET).toString() + 'px';
        this.matDialog.open(UserClickOptionsComponent, {
            position: { top: locationY, left: locationX },
            width: '220px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
            data: {
                user,
                locationY,
                locationX,
            },
        });
    }

    private onChatDisplayChange() {
        if (this.chatDisplay && this.chatDisplay.last) {
            this.chatDisplay.last.nativeElement.scrollIntoView(false);
        }
    }
}
