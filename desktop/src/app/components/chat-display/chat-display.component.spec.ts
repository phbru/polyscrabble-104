// /* eslint-disable dot-notation -- for private methods */
// /* eslint-disable @typescript-eslint/no-explicit-any -- for private methods */
// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { Color } from '@app/enums/color';
// import { AppMaterialModule } from '@app/modules/material.module';
// import { ChatHandlerService } from '@app/services/chat-handler/chat-handler.service';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-proxy.service';
// import { ChatDisplayComponent } from './chat-display.component';

// describe('ChatDisplayComponent', () => {
//     let component: ChatDisplayComponent;
//     let fixture: ComponentFixture<ChatDisplayComponent>;

//     beforeEach(async () => {
//         await TestBed.configureTestingModule({
//             declarations: [ChatDisplayComponent],
//             imports: [AppMaterialModule],
//             providers: [
//                 { provide: GameManagerServiceProxy, useValue: {} },
//                 { provide: ChatHandlerService, useValue: { messages: [] } },
//             ],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ChatDisplayComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });

//     // it('should scroll view when changed', () => {
//     //     component.chatHandler.messages.push({ user: 'Carlito', color: Color.Green, output: 'Test' });
//     //     fixture.detectChanges();
//     //     expect(component.chatHandler.messages.length).toEqual(1);
//     // });

//     it('should not scroll view when invalid chatDisplay', () => {
//         component.chatHandler.messages.push({ user: 'Carlito', color: Color.Green, output: 'Test' });
//         (component.chatDisplay as any).last = null;
//         component['onChatDisplayChange']();
//         expect(component.chatHandler.messages.length).toEqual(1);
//     });

//     // it('should return green for getStyle ', () => {
//     //     const mockMessage: Message = { user: 'mockUser', color: Color.Green, output: 'mock message' };
//     //     expect(component.getStyle(mockMessage)).toEqual({ color: 'green' });
//     // });

//     // it('should return red for getStyle ', () => {
//     //     const mockMessage: Message = { user: 'mockUser', color: Color.Red, output: 'mock message' };
//     //     expect(component.getStyle(mockMessage)).toEqual({ color: 'red' });
//     // });

//     // it('should return blue for getStyle ', () => {
//     //     const mockMessage: Message = { user: 'mockUser', color: Color.Blue, output: 'mock message' };
//     //     expect(component.getStyle(mockMessage)).toEqual({ color: 'blue' });
//     // });

//     // it('should return black for getStyle ', () => {
//     //     const mockMessage: Message = { user: 'mockUser', color: Color.Black, output: 'mock message' };
//     //     expect(component.getStyle(mockMessage)).toEqual({ color: 'black' });
//     // });
// });
