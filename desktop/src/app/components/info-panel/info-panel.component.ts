import { Component, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Player } from '@app/classes/player';
import { User } from '@app/classes/user';
import { ConfirmAbandonDialogComponent } from '@app/components/dialogs/confirm-abandon-dialog/confirm-abandon-dialog.component';
import { ReplaceVirtualPlayerComponent } from '@app/components/dialogs/replace-virtual-player/replace-virtual-player.component';
import { UserClickOptionsComponent } from '@app/components/user-click-options/user-click-options.component';
import { ActionCode } from '@app/enums/action-code';
import { Color } from '@app/enums/color';
import { ActionProxyService } from '@app/services/action-proxy/action-proxy.service';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { LetterBankServiceProxy } from '@app/services/letter-bank-proxy/letter-bank-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { TimerServiceProxy } from '@app/services/timer-proxy/timer-proxy.service';
import { ActionMessage } from '@app/types/action-message';
import { TranslateService } from '@ngx-translate/core';

const ACTION_TOOLTIP_DELAY = 2500;
const MODAL_OFFSET = 20;
@Component({
    selector: 'app-info-panel',
    templateUrl: './info-panel.component.html',
    styleUrls: ['./info-panel.component.scss'],
})
export class InfoPanelComponent {
    @ViewChild('onlineUsers') onlineUsers: ElementRef;
    currentAction: ActionMessage = { player: new User(), actionCode: -1, action: '', param: '' };
    areUsersShown: boolean = false;
    isActionShown: boolean = false;

    constructor(
        public gameServiceProxy: GameServiceProxy,
        public selfUserService: SelfUserServiceProxy,
        public timerService: TimerServiceProxy,
        public letterBankService: LetterBankServiceProxy,
        private dialog: MatDialog,
        private actionProxyService: ActionProxyService,

        translateService: TranslateService,
    ) {
        this.letterBankService.getInitialLetterBank();

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.actionProxyService.currentActionObservable.subscribe((action: any) => {
            switch (action.actionCode) {
                case ActionCode.Pass: {
                    this.currentAction.player = action.player;
                    this.currentAction.action = translateService.instant(action.action);
                    break;
                }
                case ActionCode.Exchange: {
                    this.currentAction.player = action.player;
                    this.currentAction.action = translateService.instant(action.action, { numberLetters: action.param });
                    break;
                }
                case ActionCode.Place: {
                    this.currentAction.player = action.player;
                    this.currentAction.action = translateService.instant(action.action, { placedLetters: action.param });
                    break;
                }
            }
            this.showAction();
        });
    }

    getClass() {
        if (this.areUsersShown) return 'class.opened';
        return 'class.close';
    }

    getName() {
        if (this.gameServiceProxy.ourPlayer === undefined) return '';
        return this.gameServiceProxy.ourPlayer.user.username;
    }

    getTimeInterval() {
        return this.timerService.timerLength.minutes + ':' + this.timerService.timerLength.seconds;
    }

    getOurPlayerColor(): string {
        const player = this.gameServiceProxy.ourPlayer;

        if (player !== undefined) {
            switch (player.color) {
                case Color.Green:
                    return 'green';
                case Color.Blue:
                    return 'blue';
            }
        }

        return '';
    }

    quitGame(): void {
        this.dialog.open(ConfirmAbandonDialogComponent, { backdropClass: 'backdrop' });
    }

    showAction(): void {
        this.isActionShown = true;
        setTimeout(() => {
            this.isActionShown = false;
        }, ACTION_TOOLTIP_DELAY);
    }

    takeBotPlace(botPlayer: Player) {
        this.gameServiceProxy.takeBotPlace(botPlayer);
    }

    openUserClickOptions(player: Player, event: MouseEvent, index: number) {
        if (player.user.id === this.selfUserService.user.id) return;
        if (player.isBotPlayer) {
            if (this.gameServiceProxy.isPlayer) return;
            this.openVirtualPlayerClickOptions(player, event, index);
            return;
        }
        const user = player.user;
        const locationX = (event.clientX - MODAL_OFFSET).toString() + 'px';
        const locationY = (event.clientY - MODAL_OFFSET).toString() + 'px';
        this.dialog.open(UserClickOptionsComponent, {
            position: { top: locationY, left: locationX },
            width: '220px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
            data: {
                user,
                locationY,
                locationX,
                index,
            },
        });
    }
    openVirtualPlayerClickOptions(botPlayer: Player, event: MouseEvent, index: number) {
        const locationX = (event.clientX - MODAL_OFFSET).toString() + 'px';
        const locationY = (event.clientY - MODAL_OFFSET).toString() + 'px';
        this.dialog.open(ReplaceVirtualPlayerComponent, {
            position: { top: locationY, left: locationX },
            width: '220px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
            data: {
                botPlayer,
                locationY,
                locationX,
                index,
            },
        });
    }
}
