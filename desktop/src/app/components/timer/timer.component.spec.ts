// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { TimerServiceProxy } from '@app/services/timer-proxy/timer-proxy.service';
// import { Time } from '@app/types/time';
// import { TimerComponent } from './timer.component';

// class TimerServiceMock {
//     time: Time = { minutes: 0, seconds: 0 };
// }

// describe('TimerComponent', () => {
//     let component: TimerComponent;
//     let fixture: ComponentFixture<TimerComponent>;

//     beforeEach(async () => {
//         await TestBed.configureTestingModule({
//             declarations: [TimerComponent],
//             providers: [{ provide: TimerServiceProxy, useClass: TimerServiceMock }],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(TimerComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });

//     it('should add leading zero', () => {
//         const NUMBER = 8;
//         const NUMBER_WITH_LEADING_ZERO = '08';

//         expect(component.addLeadingZero(NUMBER)).toEqual(NUMBER_WITH_LEADING_ZERO);
//     });

//     it('should not add leading zero', () => {
//         const NUMBER = 12;
//         const NUMBER_NO_LEADING_ZERO = '12';

//         expect(component.addLeadingZero(NUMBER)).toEqual(NUMBER_NO_LEADING_ZERO);
//     });

//     describe('isTimeCritical', () => {
//         it('time should be critical', () => {
//             const CRITICAL_TIME: Time = { minutes: 0, seconds: 5 };

//             // Required since 'time' is readonly
//             Object.defineProperty(component.timerService, 'time', { value: CRITICAL_TIME });
//             expect(component.isTimeCritical()).toBeTrue();
//         });

//         it('time should not be critical', () => {
//             const TIME: Time = { minutes: 10, seconds: 5 };

//             // Required since 'time' is readonly
//             Object.defineProperty(component.timerService, 'time', { value: TIME });
//             expect(component.isTimeCritical()).toBeFalse();
//         });
//     });
// });
