import { Component } from '@angular/core';
import * as constant from '@app/constants/timer';
import { TimerServiceProxy } from '@app/services/timer-proxy/timer-proxy.service';

@Component({
    selector: 'app-timer',
    templateUrl: './timer.component.html',
    styleUrls: ['./timer.component.scss'],
})
export class TimerComponent {
    constructor(public timerService: TimerServiceProxy) {}

    addLeadingZero(value: number): string {
        const threshold = 10;
        return value < threshold ? '0' + value : value.toString();
    }

    isTimeCritical() {
        return this.timerService.time.minutes <= constant.CRITICAL_MINUTES && this.timerService.time.seconds <= constant.CRITICAL_SECONDS;
    }
}
