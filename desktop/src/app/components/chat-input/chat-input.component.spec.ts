// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { Player } from '@app/classes/player';
// import { Color } from '@app/enums/color';
// import { AppMaterialModule } from '@app/modules/material.module';
// import { ChatHandlerService } from '@app/services/chat-handler/chat-handler.service';
// import { GameServiceProxy } from '@app/services/game-manager-proxy/game-proxy.service';
// import { Socket } from 'ngx-socket-io';
// import { ChatInputComponent } from './chat-input.component';
// import SpyObj = jasmine.SpyObj;

// describe('ChatInputComponent', () => {
//     let chatHandlerServeSpy: SpyObj<ChatHandlerService>;
//     let gameSpy: SpyObj<GameServiceProxy>;
//     let socketSpy: SpyObj<Socket>;
//     let component: ChatInputComponent;
//     let fixture: ComponentFixture<ChatInputComponent>;

//     beforeEach(() => {
//         const player = new Player({ playerName: 'John', color: Color.Green }, []);
//         chatHandlerServeSpy = jasmine.createSpyObj('ChatHandlerService', ['validateInputNotEmpty', 'printMessage']);
//         gameSpy = jasmine.createSpyObj('GameManagerServiceProxy', ['changeTurn'], { currentPlayer: player, ourPlayer: player });
//         gameSpy.players = [player];
//         gameSpy.ourIndex = 0;
//         socketSpy = jasmine.createSpyObj('Socket', ['emit']);
//     });

//     beforeEach(async () => {
//         await TestBed.configureTestingModule({
//             declarations: [ChatInputComponent],
//             imports: [AppMaterialModule, BrowserAnimationsModule],
//             providers: [
//                 { provide: GameServiceProxy, useValue: gameSpy },
//                 { provide: ChatHandlerService, useValue: chatHandlerServeSpy },
//                 { provide: Socket, useValue: socketSpy },
//             ],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(ChatInputComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });
// });
