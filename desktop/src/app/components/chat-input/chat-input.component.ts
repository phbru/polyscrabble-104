import { Component, ElementRef, ViewChild } from '@angular/core';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';

import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';

@Component({
    selector: 'app-chat-input',
    templateUrl: './chat-input.component.html',
    styleUrls: ['./chat-input.component.scss'],
})
export class ChatInputComponent {
    @ViewChild('userInput') chatInput: ElementRef;

    constructor(
        public clientChannelService: ClientChannelProxyService,
        public globalChannelService: GlobalChannelProxyService,
        private selfUserServiceProxy: SelfUserServiceProxy,
    ) {}

    submitChatInput(chatInput: string) {
        if (chatInput.length === 0 || !chatInput.trim()) return;
        this.chatInput.nativeElement.value = '';

        if (this.clientChannelService.current !== undefined) {
            this.globalChannelService.sendMessage(this.clientChannelService.current?.channel?.id, {
                user: this.selfUserServiceProxy.user,
                output: chatInput,
                timestamp: '',
            });
        }
    }
}
