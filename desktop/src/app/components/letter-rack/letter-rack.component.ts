import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { BoardEventHandlerService } from '@app/services/board-event-handler/board-event-handler.service';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { LetterBankServiceProxy } from '@app/services/letter-bank-proxy/letter-bank-proxy.service';
import { LetterRackService } from '@app/services/letter-rack/letter-rack.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-letter-rack',
    templateUrl: './letter-rack.component.html',
    styleUrls: ['./letter-rack.component.scss'],
})
export class LetterRackComponent {
    @ViewChild('rack') rack: ElementRef;
    constructor(
        public game: GameServiceProxy,
        public letterRackService: LetterRackService,
        private boardEventHandlerService: BoardEventHandlerService,
        private snackBarService: SnackBarService,
        private translate: TranslateService,
        private letterBankServiceProxy: LetterBankServiceProxy,
        private selfUserService: SelfUserServiceProxy,
    ) {}

    @HostListener('document:click', ['$event'])
    onClick() {
        if (!this.activeIsTarget()) this.letterRackService.resetAll();
    }

    activeIsTarget() {
        return document.activeElement === this.rack.nativeElement.parentNode;
    }

    exchangeLettersEmpty() {
        return this.letterRackService.exchangeIndexes.length === 0;
    }

    isOurTurn() {
        return this.game.validateIsOurTurn();
    }

    getWatchedName(): string {
        return this.game.players[this.game.watchedPlayer].user.username;
    }

    // TODO : change to put it in game-service -> this.gameService.remainingLettersAmount   ; this.
    exchangeLetters() {
        if (!this.isOurTurn()) {
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.exchangeTurn'), 'OK');
        } else if (this.letterRackService.exchangeIndexes.length > this.letterBankServiceProxy.letterBank.length) {
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.exchangeReserve'), 'OK');
        } else if (this.letterBankServiceProxy.letterBank.length === 0) {
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.exchangeEmpty'), 'OK');
        } else return this.letterRackService.exchange(this.game.ourPlayer.currentLetters);
    }

    cancelExchange() {
        return this.letterRackService.cancelExchange();
    }

    checkIsObserver(): boolean {
        if (this.game.partialGameInfo === undefined) return false;
        for (const player of this.game.players) {
            if (player.user.id === this.selfUserService.user.id) return false;
        }
        for (const observer of this.game.partialGameInfo.observers) {
            if (observer.id === this.selfUserService.user.id) return true;
        }
        return false;
    }

    async submitCommand() {
        if (!this.isOurTurn()) {
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.notYourTurn'), 'OK');
        } else if (this.boardEventHandlerService.enteredLetters.map.length === 0) {
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.noLetters'), 'OK');
        } else await this.boardEventHandlerService.submitCommand();
    }

    async passTurn() {
        if (!this.isOurTurn()) {
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.notYourTurn'), 'OK');
        } else await this.game.executePass();
    }
}
