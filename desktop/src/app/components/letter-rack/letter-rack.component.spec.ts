// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { Player } from '@app/classes/player';
// import { FontIncreaserComponent } from '@app/components/font-increaser/font-increaser.component';
// import { TokenComponent } from '@app/components/token/token.component';
// import { Color } from '@app/enums/color';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-proxy.service';
// import { LetterRackService } from '@app/services/letter-rack/letter-rack.service';
// import { Letter } from '@app/types/letter';
// import { PlayerInfo } from '@app/types/player-info';
// import { Socket } from 'ngx-socket-io';
// import { LetterRackComponent } from './letter-rack.component';
// import SpyObj = jasmine.SpyObj;

// const fakeLetters: Letter[] = [
//     { character: 'A', weight: 1, isBlankLetter: false },
//     { character: 'B', weight: 2, isBlankLetter: false },
//     { character: 'C', weight: 3, isBlankLetter: false },
// ];

// describe('LetterRackComponent', () => {
//     let component: LetterRackComponent;
//     let fixture: ComponentFixture<LetterRackComponent>;
//     let letterRackServiceSpy: SpyObj<LetterRackService>;
//     let gameSpy: SpyObj<GameManagerServiceProxy>;
//     let player: Player;
//     let socketSpy: SpyObj<Socket>;

//     beforeEach(() => {
//         const playerInfo: PlayerInfo = { playerName: 'test', color: Color.Green };
//         player = new Player(playerInfo, fakeLetters);

//         letterRackServiceSpy = jasmine.createSpyObj('LetterRackService', [
//             'processInput',
//             'exchangeIndexes',
//             'resetAll',
//             'exchange',
//             'cancelExchange',
//         ]);
//         socketSpy = jasmine.createSpyObj('Socket', ['on']);
//         gameSpy = jasmine.createSpyObj('GameManagerServiceProxy', ['players', 'resetRackViewToModel', 'updatePlayerLetters'], {
//             ourIndex: 0,
//             ourPlayer: player,
//             currentLetters: fakeLetters,
//         });
//         gameSpy.ourIndex = 0;
//         gameSpy.players[0] = player;
//     });

//     beforeEach(async () => {
//         await TestBed.configureTestingModule({
//             declarations: [LetterRackComponent, TokenComponent, FontIncreaserComponent],
//             providers: [
//                 { provide: GameManagerServiceProxy, useValue: gameSpy },
//                 { provide: LetterRackService, useValue: letterRackServiceSpy },
//                 { provide: Socket, useValue: socketSpy },
//             ],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(LetterRackComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });

//     it('detect key pressed is "" since not in active element', () => {
//         const event = new KeyboardEvent('keypress', {
//             key: 'a',
//         });
//         spyOn(component, 'activeIsTarget').and.returnValue(true);
//         component.buttonDetect(event);
//         expect(letterRackServiceSpy.processInput).toHaveBeenCalledWith('a', 0, fakeLetters);
//     });

//     it('detect key pressed is "a" ', () => {
//         const event = new KeyboardEvent('keypress', {
//             key: 'a',
//         });
//         spyOn(component, 'activeIsTarget').and.returnValue(false);
//         component.buttonDetect(event);
//         expect(letterRackServiceSpy.processInput).not.toHaveBeenCalled();
//     });

//     it('detect key pressed is "ArrowRight" since in active element', () => {
//         const event = new KeyboardEvent('keypress', {
//             key: 'ArrowRight',
//         });
//         spyOn(component, 'activeIsTarget').and.returnValue(true);
//         component.buttonDetect(event);
//         expect(letterRackServiceSpy.processInput).toHaveBeenCalledWith('ArrowRight', 0, fakeLetters);
//     });

//     it('detect key pressed is "ArrowLeft" since in active element', () => {
//         const event = new KeyboardEvent('keypress', {
//             key: 'ArrowLeft',
//         });
//         spyOn(component, 'activeIsTarget').and.returnValue(true);
//         component.buttonDetect(event);
//         expect(letterRackServiceSpy.processInput).toHaveBeenCalledWith('ArrowLeft', 0, fakeLetters);
//     });

//     it('detect mouseWheel event with negative delta', () => {
//         const event = new WheelEvent('s', {
//             deltaY: -100,
//         });
//         component.mouseScroll(event);
//         expect(letterRackServiceSpy.processInput).toHaveBeenCalledWith('', event.deltaY, fakeLetters);
//     });

//     it('detect mouseWheel event with positive delta', () => {
//         const event = new WheelEvent('s', {
//             deltaY: 100,
//         });
//         component.mouseScroll(event);
//         expect(letterRackServiceSpy.processInput).toHaveBeenCalledWith('', event.deltaY, fakeLetters);
//     });

//     it('detect click outside of the letterRack', () => {
//         component.onClick();
//         expect(letterRackServiceSpy.resetAll).toHaveBeenCalled();
//     });

//     it('detect click inside of the letterRack', () => {
//         spyOn(component, 'activeIsTarget').and.returnValue(true);
//         component.onClick();
//         expect(letterRackServiceSpy.resetAll).not.toHaveBeenCalled();
//     });

//     it('should check if is our turn', () => {
//         gameSpy.currentlyPlayingIndex = 0; // ourIndex = 0;
//         expect(component.isOurTurn()).toBeTrue();
//     });

//     it('should check if is our turn', () => {
//         gameSpy.currentlyPlayingIndex = 1; // ourIndex = 0;
//         expect(component.isOurTurn()).toBeFalse();
//     });

//     it('should call exchange', () => {
//         component.exchangeLetters();
//         expect(letterRackServiceSpy.exchange).toHaveBeenCalledWith(fakeLetters);
//     });

//     it('should call cancelExchange', () => {
//         component.cancelExchange();
//         expect(letterRackServiceSpy.cancelExchange).toHaveBeenCalled();
//     });
// });
