/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MenuSettingComponent } from '@app/components/menu-setting/menu-setting.component';
import { ColorThemeService } from '@app/services/color-theme/color-theme.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';

const MODAL_OFFSET = 20;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
    constructor(public colorThemeService: ColorThemeService, public userService: SelfUserServiceProxy, private matDialog: MatDialog) {}

    openMenu(event: MouseEvent) {
        const locationY = (event.clientY + MODAL_OFFSET).toString() + 'px';
        this.matDialog.open(MenuSettingComponent, {
            data: true,
            position: { top: locationY, right: '2%' },
            width: '200px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
        });
    }

    getAvatar() {
        if (this.userService.user) return this.userService.user.avatar;
        return;
    }
}
