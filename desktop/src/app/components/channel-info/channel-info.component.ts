/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ChannelMembersDialogComponent } from '@app/components/dialogs/channel-members-dialog/channel-members-dialog.component';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';
@Component({
    selector: 'app-channel-info',
    templateUrl: './channel-info.component.html',
    styleUrls: ['./channel-info.component.scss'],
})
export class ChannelInfoComponent {
    constructor(
        private matDialog: MatDialog,
        private clientChannelService: ClientChannelProxyService,
        private globalChannelService: GlobalChannelProxyService,
    ) {}

    viewMembers() {
        this.matDialog.open(ChannelMembersDialogComponent, { backdropClass: 'backdrop' });
    }

    leaveChannel() {
        if (this.clientChannelService.current !== undefined) {
            this.globalChannelService.leaveChannel(this.clientChannelService.current.channel.id);
            this.matDialog.closeAll();
        }
    }
}
