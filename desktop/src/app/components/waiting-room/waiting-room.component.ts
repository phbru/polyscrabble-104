import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/classes/user';
import { InviteUserDialogComponent } from '@app/components/dialogs/invite-user-dialog/invite-user-dialog.component';
import { LaunchGameDialogComponent } from '@app/components/dialogs/launch-game-dialog/launch-game-dialog.component';
import { UserClickOptionsComponent } from '@app/components/user-click-options/user-click-options.component';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { WaitingRoomServiceProxy } from '@app/services/waiting-room-proxy/waiting-room-proxy.service';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';

const MODAL_OFFSET = 20;

@Component({
    selector: 'app-waiting-room',
    templateUrl: './waiting-room.component.html',
    styleUrls: ['./waiting-room.component.scss'],
})
export class WaitingRoomComponent {
    isWaitingSelected: boolean = false;
    isObserverSelected: boolean = true;

    constructor(
        public waitingRoomService: WaitingRoomServiceProxy,
        private matDialog: MatDialog,
        private snackBarService: SnackBarService,
        private translateService: TranslateService,
        public selfUserService: SelfUserServiceProxy,
        private socket: Socket,
    ) {}

    // @HostListener('window:click', ['$event'])
    // closeMenu(event: Event) {
    //     if ((event.target as HTMLElement).innerHTML === 'add') {
    //         this.isOptionsMenuShown = false;
    //     } else if ((event.target as HTMLElement).innerHTML === 'more_vert') {
    //         this.isAddMenuShown = false;
    //     } else {
    //         this.isAddMenuShown = false;
    //         this.isOptionsMenuShown = false;
    //     }
    // }

    getTime(timerValue: number): string {
        if (timerValue % 1 === 0) return `${timerValue}:00`;
        return `${Math.trunc(timerValue)}:30`;
    }

    // Takes the place of first bot
    takeBotSpot(): void {
        this.waitingRoomService.takeBotSpot();
    }

    takePlayerSpot(): void {
        if (this.waitingRoomService.gameInfo.openSpots > 0) {
            this.waitingRoomService.takePlayerSpot();
        } else if (this.waitingRoomService.gameInfo.virtualPlayers.length > 0) {
            this.takeBotSpot();
        }
    }

    takeObserverSpot(): void {
        this.waitingRoomService.takeObserverSpot();
    }

    leaveGame(): void {
        this.waitingRoomService.leaveGame();
    }

    cancelGame(): void {
        this.waitingRoomService.cancelGame();
    }

    setObserver(): void {
        this.isWaitingSelected = false;
        this.isObserverSelected = true;
    }

    setWaiting(): void {
        this.isObserverSelected = false;
        this.isWaitingSelected = true;
        this.waitingRoomService.notificationOn = false;
    }

    // toggleAddMenu(): void {
    //     this.isAddMenuShown = !this.isAddMenuShown;
    // }

    addSpot(): void {
        this.waitingRoomService.addSpot();
    }
    addVirtualPlayer(): void {
        this.waitingRoomService.addVirtualPlayer();
    }

    inviteUser(): void {
        this.matDialog.open(InviteUserDialogComponent, { backdropClass: 'backdrop' });
    }

    removeVirtualPlayer(botName: string): void {
        this.waitingRoomService.removeVirtualPlayer(botName);
    }

    kickUserOut(userIdToKickout: string): void {
        this.waitingRoomService.kickUserOut(userIdToKickout);
    }

    revokeInvite(userIdToKickout: string): void {
        this.waitingRoomService.removeInvitedUser(userIdToKickout);
        this.socket.emit('revokeInvite', userIdToKickout);
    }

    removeSpot(): void {
        if (this.validateSpotIsRemovable()) {
            this.waitingRoomService.removeSpot();
        } else {
            this.snackBarService.open(this.translateService.instant('waitingRoom.cantRemoveSpot'), 'OK');
        }
    }

    async launchGame(): Promise<void> {
        if (this.waitingRoomService.gameInfo.openSpots + this.waitingRoomService.gameInfo.invitedUsers.length > 0)
            this.matDialog.open(LaunchGameDialogComponent, {
                data: this.waitingRoomService.gameInfo.openSpots + this.waitingRoomService.gameInfo.invitedUsers.length,
                backdropClass: 'backdrop',
            });
        else await this.waitingRoomService.launchGame();
    }

    validateSpotIsAddable(): boolean {
        const MAX_PLAYER = 4;
        return (
            this.waitingRoomService.isSelfGameCreator &&
            this.waitingRoomService.gameInfo.openSpots +
                this.waitingRoomService.gameInfo.invitedUsers.length +
                this.waitingRoomService.gameInfo.virtualPlayers.length +
                this.waitingRoomService.gameInfo.players.length <
                MAX_PLAYER
        );
    }

    validateSpotIsRemovable(): boolean {
        return (
            this.waitingRoomService.isSelfGameCreator &&
            !(
                this.waitingRoomService.gameInfo.openSpots +
                    this.waitingRoomService.gameInfo.invitedUsers.length +
                    this.waitingRoomService.gameInfo.virtualPlayers.length +
                    this.waitingRoomService.gameInfo.players.length ===
                2
            )
        );
    }

    openUserClickOptions(user: User, event: MouseEvent) {
        if (user.id === this.selfUserService.user.id) return;
        const locationX = (event.clientX - MODAL_OFFSET).toString() + 'px';
        const locationY = (event.clientY - MODAL_OFFSET).toString() + 'px';
        this.matDialog.open(UserClickOptionsComponent, {
            position: { top: locationY, left: locationX },
            width: '220px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
            data: {
                user,
                locationX,
                locationY,
            },
        });
    }

    acceptRequest(user: User) {
        this.socket.emit('acceptJoin', user.id);
        this.waitingRoomService.removeUserToJoin(user.id);
    }

    declineRequest(user: User) {
        this.waitingRoomService.removeUserToJoin(user.id);
        this.socket.emit('declineJoin', user.id);
    }
}
