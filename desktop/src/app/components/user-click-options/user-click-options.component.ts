import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProfileDialogComponent } from '@app/components/dialogs/profile-dialog/profile-dialog.component';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';
import { ClientFriendServiceProxyService } from '@app/services/client-friend-service-proxy/client-friend-service-proxy.service';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';
import { GlobalInteractionProxyService } from '@app/services/global-interaction-proxy/global-interaction-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-user-click-options',
    templateUrl: './user-click-options.component.html',
    styleUrls: ['./user-click-options.component.scss'],
})
export class UserClickOptionsComponent {
    userId: string;
    constructor(
        private globalInteractionService: GlobalInteractionProxyService,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
        private snackBarService: SnackBarService,
        private globalChannelService: GlobalChannelProxyService,
        private clientChannelService: ClientChannelProxyService,
        private translateService: TranslateService,
        public selfUserService: SelfUserServiceProxy,
        public friendServiceProxy: ClientFriendServiceProxyService,
        private gameServiceProxy: GameServiceProxy,
    ) {
        this.userId = data.user.id;
    }

    closeModal(): void {
        this.dialog.closeAll();
    }

    checkIsObserver(): boolean {
        if (this.gameServiceProxy.partialGameInfo === undefined) return false;
        for (const player of this.gameServiceProxy.players) {
            if (player.user.id === this.selfUserService.user.id) return false;
        }
        for (const observer of this.gameServiceProxy.partialGameInfo.observers) {
            if (observer.id === this.selfUserService.user.id) return true;
        }
        return false;
    }

    watchPlayer(): void {
        this.dialog.closeAll();
        this.gameServiceProxy.setWatchedPlayer(this.data.index);
    }

    sendFriendRequest() {
        this.globalInteractionService.createFriendRequest(this.userId);
        this.closeModal();
        this.snackBarService.open(this.translateService.instant('userInteractions.successfulInvite'), 'OK');
    }

    openProfileDialog() {
        this.closeModal();
        const user = this.data.user;
        const locationX = this.data.locationX;
        const locationY = this.data.locationY;
        this.dialog.open(ProfileDialogComponent, {
            position: { top: locationY, left: locationX },
            width: '280px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
            data: {
                user,
            },
        });
    }

    removeFriend() {
        this.globalInteractionService.removeFriend(this.userId);
        this.closeModal();
        this.snackBarService.open(this.translateService.instant('userInteractions.successfulRemoval'), 'OK');
    }

    async sendDM() {
        const newChannel = await this.globalChannelService.createDM(this.userId);
        this.clientChannelService.currentChannelIndex = this.clientChannelService.findIndex(newChannel.id);
        document.getElementById('chat-input-box')?.focus();
        this.closeModal();
    }
}
