/* eslint-disable @typescript-eslint/consistent-type-assertions */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateChannelDialogComponent } from '@app/components/dialogs/create-channel-dialog/create-channel-dialog.component';
import { SearchChannelDialogComponent } from '@app/components/dialogs/search-channel-dialog/search-channel-dialog.component';
import { ClientChannelProxyService } from '@app/services/client-channel-proxy/client-channel-proxy.service';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';

@Component({
    selector: 'app-channels-banner',
    templateUrl: './channels-banner.component.html',
    styleUrls: ['./channels-banner.component.scss'],
})
export class ChannelsBannerComponent {
    constructor(
        private matDialog: MatDialog,
        public selfUserService: SelfUserServiceProxy,
        public clientChannelService: ClientChannelProxyService,
        private globalChannelService: GlobalChannelProxyService,
    ) {}

    createChannel(): void {
        this.matDialog.open(CreateChannelDialogComponent, { backdropClass: 'backdrop' });
    }

    searchChannels(): void {
        this.matDialog.open(SearchChannelDialogComponent, { backdropClass: 'backdrop' });
    }

    changeChannel(index: number): void {
        this.clientChannelService.currentChannelIndex = index;
        (<HTMLInputElement>document.getElementById('chat-input-box')).value = '';
        document.getElementById('chat-input-box')?.focus();
        // Set new 'current' channel as read
        if (this.clientChannelService.current !== undefined) {
            this.globalChannelService.setChannelAsRead(this.clientChannelService.current.channel.id);
        }
    }
}
