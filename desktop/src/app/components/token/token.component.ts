import { Component, ElementRef, HostListener, Input, ViewChild } from '@angular/core';
import { BoardEventHandlerService } from '@app/services/board-event-handler/board-event-handler.service';
import { BoardViewService } from '@app/services/board-view/board-view';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { LetterRackService } from '@app/services/letter-rack/letter-rack.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';

const LEFT_CLICK_EVENT = 0;
@Component({
    selector: 'app-token',
    templateUrl: './token.component.html',
    styleUrls: ['./token.component.scss'],
})
export class TokenComponent {
    @ViewChild('token') token: ElementRef;
    @Input() letter: string;
    @Input() weight: number;
    @Input() isBlank: boolean;
    @Input() index: number;

    constructor(
        public letterRackService: LetterRackService,
        private gameServiceProxy: GameServiceProxy,
        private boardEventHandler: BoardEventHandlerService,
        private boardViewService: BoardViewService,
        private selfUserService: SelfUserServiceProxy,
    ) {}

    @HostListener('contextmenu', ['$event'])
    onRightClick(e: Event) {
        e.preventDefault();
        if (this.checkIsObserver()) return;
        if (this.boardEventHandler.isDragAndDropMode) this.boardEventHandler.resetBoardEventHandler();
        if (this.gameServiceProxy.validateIsOurTurn()) this.letterRackService.handleLetterExchange(this.index);
    }

    checkIsObserver(): boolean {
        if (this.gameServiceProxy.partialGameInfo === undefined) return false;
        for (const player of this.gameServiceProxy.players) {
            if (player.user.id === this.selfUserService.user.id) return false;
        }
        for (const observer of this.gameServiceProxy.partialGameInfo.observers) {
            if (observer.id === this.selfUserService.user.id) return true;
        }
        return false;
    }

    selectLetter(event: MouseEvent): void {
        event.preventDefault();
        if (this.boardEventHandler.isWritingMode || event.button !== LEFT_CLICK_EVENT || this.gameServiceProxy.isGameEnded || this.checkIsObserver())
            return;

        this.boardEventHandler.isDragAndDropMode = true;
        this.boardEventHandler.selectedToken = {
            letter: { character: this.letter, weight: this.weight, isBlankLetter: this.isBlank },
            line: '',
            column: -1,
        };
        this.boardViewService.createTemporaryCanvas(event, this.boardEventHandler.selectedToken.letter);
        this.gameServiceProxy.lettersView.splice(this.index, 1);
    }

    unselectLetter(event: MouseEvent): void {
        if (
            this.boardEventHandler.isWritingMode ||
            event.button !== LEFT_CLICK_EVENT ||
            this.boardEventHandler.selectedToken.letter.character === '' ||
            this.checkIsObserver()
        )
            return;
        this.gameServiceProxy.lettersView.splice(this.index, 0, this.boardEventHandler.selectedToken.letter);
        this.boardEventHandler.clearSelectedToken();
    }
}
