// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { LetterRackService } from '@app/services/letter-rack/letter-rack.service';
// import { TokenComponent } from './token.component';
// import SpyObj = jasmine.SpyObj;

// describe('TokenComponent', () => {
//     let component: TokenComponent;
//     let fixture: ComponentFixture<TokenComponent>;
//     let letterRackServiceSpy: SpyObj<LetterRackService>;

//     beforeEach(() => {
//         letterRackServiceSpy = jasmine.createSpyObj('LetterRackService', ['currentIndex', 'exchangeIndexes', 'handleLetterExchange']);
//     });

//     beforeEach(async () => {
//         await TestBed.configureTestingModule({
//             declarations: [TokenComponent],
//             providers: [{ provide: LetterRackService, useValue: letterRackServiceSpy }],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(TokenComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });

//     it('should verify that index is updated on click', () => {
//         component.index = 1;
//         component.onClick();
//         expect(letterRackServiceSpy.currentIndex).toEqual(component.index);
//     });

//     it('should check that exchange index is added on right click', () => {
//         component.index = 0;
//         const event = new Event('a');
//         component.onRightClick(event);
//         expect(letterRackServiceSpy.handleLetterExchange).toHaveBeenCalledWith(component.index);
//     });
// });
