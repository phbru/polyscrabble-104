/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ColorThemeService } from '@app/services/color-theme/color-theme.service';
import { HttpClientService } from '@app/services/http-client/http-client.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs';

@Component({
    selector: 'app-avatar-select',
    templateUrl: './avatar-select.component.html',
    styleUrls: ['./avatar-select.component.scss'],
})
export class AvatarSelectComponent {
    fileName: string;
    avatarUrl: any;
    errorMsg: string;

    constructor(
        public colorThemeService: ColorThemeService,
        private selfUserService: SelfUserServiceProxy,
        private httpClient: HttpClientService,
        private snackBarService: SnackBarService,
        private dialogRef: MatDialogRef<AvatarSelectComponent>,
        private translate: TranslateService,
    ) {}

    selectAvatar(index: number) {
        this.errorMsg = '';
        this.avatarUrl = `assets/images/avatars/avatar-${index}.svg`;
        this.fileName = '';
    }

    showPreview(event: any) {
        if (event.target.files && event.target.files[0]) {
            const file: File = event.target.files[0];
            const maxFileSize = 2097152;
            if (file.size > maxFileSize) {
                this.avatarUrl = '';
                this.errorMsg = 'Fichier trop gros. Taille maximale de 2mb.';
                return;
            }
            this.errorMsg = '';
            this.fileName = file.name;
            const reader: FileReader = new FileReader();
            reader.onload = () => (this.avatarUrl = reader.result);

            reader.readAsDataURL(file);
        }
    }

    getUploadIcon() {
        if (this.colorThemeService.selectedTheme === 'dark') return 'assets/images/avatars/upload_icon_dark.png';
        else return 'assets/images/avatars/upload_icon_light.png';
    }

    getAvatar() {
        if (this.avatarUrl) {
            return this.avatarUrl;
        }

        return this.selfUserService.user.avatar;
    }

    disableAvatarChange() {
        if (!this.avatarUrl) return true;
        return this.avatarUrl === this.selfUserService.user.avatar;
    }

    updateAvatar() {
        this.httpClient
            .updateAvatar(this.selfUserService.user, this.avatarUrl)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.selfUserService.user.avatar = this.avatarUrl;
                    this.dialogRef.close();
                    this.snackBarService.open(this.translate.instant('profile.avatar.updated'), 'OK');
                },
                error: () => {
                    // gestion des erreurs d'upload d'avatar ici...
                },
            });
    }
}
