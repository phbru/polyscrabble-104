/* eslint-disable max-len */
import { AfterViewChecked, ChangeDetectorRef, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateGameDialogComponent } from '@app/components//dialogs/create-game-dialog/create-game-dialog.component';
import { FilterGamesDialogComponent } from '@app/components/dialogs/filter-games-dialog/filter-games-dialog.component';
import { JoinGameDialogComponent } from '@app/components/dialogs/join-game-dialog/join-game-dialog.component';
import { WaitingCreatorApprovalDialogComponent } from '@app/components/dialogs/waiting-creator-approval-dialog/waiting-creator-approval-dialog.component';
import {
    ACCESS,
    ALL,
    DICTIONARY,
    englishDictionary,
    frenchDictionary,
    PLAYERS,
    privateAccess,
    publicAccess,
    TIMER,
} from '@app/constants/game-filters';
import { ONE_MINUTE } from '@app/constants/timer';
import { GameFiltersService } from '@app/services/game-filters/game-filters.service';
import { GlobalGameListProxyService } from '@app/services/global-game-list-proxy/global-game-list-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { GameInfo } from '@app/types/game-info';
import { Socket } from 'ngx-socket-io';

@Component({
    selector: 'app-joinable-games',
    templateUrl: './joinable-games.component.html',
    styleUrls: ['./joinable-games.component.scss'],
})
export class JoinableGamesComponent implements AfterViewChecked {
    searchBarContent: string = '';
    filteredGames: GameInfo[] = [];

    constructor(
        public gamesListService: GlobalGameListProxyService,
        public gameFiltersService: GameFiltersService,
        private selfUserService: SelfUserServiceProxy,
        private matDialog: MatDialog,
        private socket: Socket,
        private cdRef: ChangeDetectorRef, // private translateService: TranslateService,
    ) {
        this.filteredGames = this.gamesListService.games;
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    filterGames(): GameInfo[] {
        this.filteredGames = this.gamesListService.games.filter((game) => this.applyFilter(game));
        return this.filteredGames;
    }

    applyFilter(game: GameInfo): boolean {
        if (game.isGameStarted) return false;
        if (this.searchBarContent.length === 0)
            return this.checkAccessType(game) && this.checkDictionary(game) && this.checkTimer(game) && this.checkNumberPlayers(game);
        else {
            return (
                this.searchBarContent.length > 0 &&
                game.creatorName.toUpperCase().includes(this.searchBarContent.toUpperCase()) &&
                this.checkAccessType(game) &&
                this.checkDictionary(game) &&
                this.checkTimer(game) &&
                this.checkNumberPlayers(game)
            );
        }
    }

    checkAccessType(game: GameInfo): boolean {
        const accessType = this.gameFiltersService.getFilter(ACCESS);
        if (accessType === ALL) return true;
        return (accessType === publicAccess && !game.isPrivate) || (accessType === privateAccess && game.isPrivate);
    }

    checkDictionary(game: GameInfo): boolean {
        const dictionary = this.gameFiltersService.getFilter(DICTIONARY);
        if (dictionary === ALL) return true;
        return (
            (dictionary === frenchDictionary && game.dictionary === 'Français') || (dictionary === englishDictionary && game.dictionary === 'English')
        );
    }

    checkTimer(game: GameInfo): boolean {
        const time = this.gameFiltersService.getFilter(TIMER);
        if (time === ALL) return true;
        return this.getTimeNumber(time) === game.time;
    }

    checkNumberPlayers(game: GameInfo): boolean {
        const numberPlayer = this.gameFiltersService.getFilter(PLAYERS);
        if (numberPlayer === ALL) return true;
        return Number(numberPlayer) === game.players.length + game.virtualPlayers.length + game.invitedUsers.length + game.openSpots;
    }

    getTimeString(timerValue: number): string {
        if (timerValue % 1 === 0) return `${timerValue}:00`;
        return `${Math.trunc(timerValue)}:30`;
    }

    getTimeNumber(timerValue: string): number {
        const minutes = Number(timerValue.split(':')[0]);
        const seconds = Number(timerValue.split(':')[1]);
        return minutes + seconds / ONE_MINUTE;
    }

    openCreateGameDialog(): void {
        this.matDialog.open(CreateGameDialogComponent, { backdropClass: 'backdrop' });
    }

    openfilterGamesDialog(): void {
        this.matDialog
            .open(FilterGamesDialogComponent, { backdropClass: 'backdrop' })
            .afterClosed()
            .subscribe(() => {
                this.filterGames();
            });
    }

    removeFilter(filter: string) {
        this.gameFiltersService.removeFilter(filter);
    }

    joinGame(game: GameInfo): void {
        if (game.isPrivate) {
            this.socket.emit('askToJoin', this.selfUserService.user, game.id);
            this.matDialog.open(WaitingCreatorApprovalDialogComponent, { data: game.id, backdropClass: 'backdrop', width: '550px' });
        } else this.matDialog.open(JoinGameDialogComponent, { data: game, backdropClass: 'backdrop' });
    }
}
