import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FriendRequestsDialogComponent } from '@app/components/dialogs/friend-requests-dialog/friend-requests-dialog.component';
import { InviteFriendDialogComponent } from '@app/components/dialogs/invite-friend-dialog/invite-friend-dialog.component';
import { ClientFriendServiceProxyService } from '@app/services/client-friend-service-proxy/client-friend-service-proxy.service';
import { GlobalUserProxyService } from '@app/services/global-user-proxy/global-user-proxy.service';
@Component({
    selector: 'app-online-users',
    templateUrl: './online-users.component.html',
    styleUrls: ['./online-users.component.scss'],
})
export class OnlineUsersComponent implements AfterViewInit {
    @ViewChild('onlineUsersButton') onlineUsersButton: ElementRef;
    @ViewChild('friendsButton') friendsButton: ElementRef;
    areOnlineUsersShown: boolean = true;
    areFriendsShown: boolean = false;

    constructor(
        public globalUserService: GlobalUserProxyService,
        public clientFriendService: ClientFriendServiceProxyService,
        private matDialog: MatDialog,
    ) {}

    ngAfterViewInit() {
        this.showOnlineUsers();
    }

    showOnlineUsers() {
        this.areOnlineUsersShown = true;
        this.areFriendsShown = false;
        this.onlineUsersButton.nativeElement.style.backgroundColor = 'var(--color-background-1)';
        this.friendsButton.nativeElement.style.backgroundColor = 'var(--color-background-2)';
        this.onlineUsersButton.nativeElement.style.color = 'var(--color-text)';
        this.friendsButton.nativeElement.style.color = 'var(--color-border-1)';
    }

    showFriends() {
        this.areFriendsShown = true;
        this.areOnlineUsersShown = false;
        this.friendsButton.nativeElement.style.backgroundColor = 'var(--color-background-1)';
        this.onlineUsersButton.nativeElement.style.backgroundColor = 'var(--color-background-2)';
        this.friendsButton.nativeElement.style.color = 'var(--color-text)';
        this.onlineUsersButton.nativeElement.style.color = 'var(--color-border-1)';
    }

    openAddFriendDialog() {
        this.matDialog.open(InviteFriendDialogComponent, { backdropClass: 'backdrop' });
    }

    openRequestsDialog() {
        this.clientFriendService.removeNotification();
        this.matDialog.open(FriendRequestsDialogComponent, { backdropClass: 'backdrop' });
    }
}
