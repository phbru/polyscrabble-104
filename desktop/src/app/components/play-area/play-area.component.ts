import { AfterViewInit, Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Vec2 } from '@app/classes/vec2';
import { DEFAULT_WIDTH } from '@app/constants/board-view';
import { BoardEventHandlerService } from '@app/services/board-event-handler/board-event-handler.service';
import { BoardViewService } from '@app/services/board-view/board-view';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { GridControllerService } from '@app/services/grid-controller/grid-controller.service';

export enum MouseButton {
    Left = 0,
    Middle = 1,
    Right = 2,
    Back = 3,
    Forward = 4,
}

@Component({
    selector: 'app-play-area',
    templateUrl: './play-area.component.html',
    styleUrls: ['./play-area.component.scss'],
})
export class PlayAreaComponent implements AfterViewInit {
    @ViewChild('mainDiv') mainDiv: ElementRef;
    @ViewChild('gridCanvas', { static: false }) private gridCanvas!: ElementRef<HTMLCanvasElement>;
    mousePosition: Vec2 = { x: 0, y: 0 };
    word: string = '';
    event: KeyboardEvent;

    private canvasSize = { x: DEFAULT_WIDTH, y: DEFAULT_WIDTH };

    constructor(
        public boardView: BoardViewService,
        private gridController: GridControllerService,
        private boardEventHandler: BoardEventHandlerService,
        private gameServiceProxy: GameServiceProxy,
        private matDialog: MatDialog,
    ) {}

    get width(): number {
        return this.canvasSize.x;
    }

    get height(): number {
        return this.canvasSize.y;
    }

    @HostListener('window:keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {
        if (this.matDialog.openDialogs.length > 0) return;
        this.boardEventHandler.onKeyDown(event);
    }

    @HostListener('document:mousedown', ['$event'])
    onMouseDown(event: MouseEvent) {
        if (
            this.boardEventHandler.isWritingMode &&
            !(event.target as HTMLElement).classList.contains('play-button') &&
            !(event.target as HTMLElement).classList.contains('validate-button')
        ) {
            this.boardEventHandler.resetBoardEventHandler();
            return;
        }
        if (event.target === this.gridCanvas.nativeElement) this.boardEventHandler.selectToken(event);
    }

    @HostListener('document:mousemove', ['$event'])
    onMouseMove(event: MouseEvent) {
        if (this.boardEventHandler.selectedToken && this.boardEventHandler.selectedToken.letter.character !== '')
            this.boardEventHandler.moveToken(event);
    }

    @HostListener('document:mouseup', ['$event'])
    onMouseUp(event: MouseEvent) {
        if (this.boardEventHandler.selectedToken && this.boardEventHandler.selectedToken.letter.character !== '') {
            if (event.target === document.getElementById('rack') || event.target !== this.gridCanvas.nativeElement) {
                this.boardEventHandler.insertLetterInRack();
            } else if (event.target === this.gridCanvas.nativeElement) this.boardEventHandler.getDroppedLetterPosition(event);
        }
        if (
            !(event.target as HTMLElement).classList.contains('play-button') &&
            !(event.target as HTMLElement).classList.contains('validate-button')
        ) {
            if (this.boardEventHandler.enteredLetters.map.length === 0) this.boardEventHandler.isDragAndDropMode = false;
            if (this.boardEventHandler.selectedToken) this.boardEventHandler.clearSelectedToken();

            this.boardEventHandler.isWritingMode = false;
        }
        this.boardView.deleteTemporaryCanvas();
    }

    @HostListener('document:click', ['$event'])
    onClick(event: MouseEvent) {
        if (event.target !== this.gridCanvas.nativeElement || this.boardEventHandler.isDragAndDropMode) return;

        if (event.target === this.gridCanvas.nativeElement) {
            this.boardEventHandler.selectCase(event);
        } else if (
            !(event.target as HTMLElement).classList.contains('play-button') &&
            !(event.target as HTMLElement).classList.contains('validate-button')
        ) {
            this.boardEventHandler.resetBoardEventHandler();
        }
    }

    ngAfterViewInit(): void {
        this.boardView.gridContext = this.gridCanvas.nativeElement.getContext('2d') as CanvasRenderingContext2D;
        this.boardView.initializeGrid(DEFAULT_WIDTH);
        this.gridCanvas.nativeElement.focus();

        this.gridController.initDrawingEvents(); // Draw when we receive new data! --> TODO : SHOULD BE MOVED TO BOARD_VIEW
        this.boardView.drawBoardContent();
    }

    allowDrop(event: Event): boolean {
        event.preventDefault();
        if (!this.gameServiceProxy.validateIsOurTurn()) return false;
        return true;
    }

    dropItem(event: Event): void {
        event.preventDefault();
        if (!this.gameServiceProxy.validateIsOurTurn()) return;
        if (event.target === this.gridCanvas.nativeElement && this.boardEventHandler.selectedToken.letter.character !== '')
            this.boardEventHandler.getDroppedLetterPosition(event as MouseEvent);
        this.boardEventHandler.clearSelectedToken();
    }
}
