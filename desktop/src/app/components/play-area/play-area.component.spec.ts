// import { ComponentFixture, TestBed } from '@angular/core/testing';
// import { PlayAreaComponent } from '@app/components/play-area/play-area.component';
// import { BoardEventHandlerService } from '@app/services/board-event-handler/board-event-handler.service';
// import { BoardViewService } from '@app/services/board-view/board-view';
// import { GridControllerService } from '@app/services/grid-controller/grid-controller.service';

// describe('PlayAreaComponent', () => {
//     let component: PlayAreaComponent;
//     let fixture: ComponentFixture<PlayAreaComponent>;

//     let boardViewSpy: jasmine.SpyObj<BoardViewService>;
//     let gridControllerSpy: jasmine.SpyObj<GridControllerService>;
//     let repositioningSpy: jasmine.SpyObj<BoardEventHandlerService>;

//     beforeEach(async () => {
//         boardViewSpy = jasmine.createSpyObj('BoardViewService', ['initializeGrid', 'drawBoardContent']);
//         gridControllerSpy = jasmine.createSpyObj('GridControllerService', ['initDrawingEvents']);
//         repositioningSpy = jasmine.createSpyObj('boardEventHandler', ['selectCase', 'resetBoardEventHandler', 'onKeyDown']);

//         await TestBed.configureTestingModule({
//             declarations: [PlayAreaComponent],
//             providers: [
//                 { provide: BoardViewService, useValue: boardViewSpy },
//                 { provide: GridControllerService, useValue: gridControllerSpy },
//                 { provide: BoardEventHandlerService, useValue: repositioningSpy },
//             ],
//         }).compileComponents();
//     });

//     beforeEach(() => {
//         fixture = TestBed.createComponent(PlayAreaComponent);
//         component = fixture.componentInstance;
//         fixture.detectChanges();
//     });

//     it('should create', () => {
//         expect(component).toBeTruthy();
//     });

//     it('should detect keyboard', () => {
//         const event = new KeyboardEvent('keydown', { key: 's' });
//         component.onKeyDown(event);
//         expect(repositioningSpy.onKeyDown).toHaveBeenCalledWith(event);
//     });

//     it('should call select case when mouse is detected and activeIsTarget is true', () => {
//         const event = { offsetX: 20, offsetY: 10 } as MouseEvent;
//         spyOn(component, 'activeIsTarget').and.returnValue(true);
//         component.onClick(event);
//         expect(repositioningSpy.selectCase).toHaveBeenCalledWith(event);
//     });

//     it('should call resetBoardEventHandler when mouse is detected and activeIsTarget is false', () => {
//         const event = { offsetX: 20, offsetY: 10 } as MouseEvent;
//         component.onClick(event);
//         expect(repositioningSpy.resetBoardEventHandler).toHaveBeenCalled();
//     });
// });
