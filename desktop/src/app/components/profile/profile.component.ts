/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AvatarSelectComponent } from '@app/components/avatar-select/avatar-select.component';
import { HttpClientService } from '@app/services/http-client/http-client.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent {
    editingUsername: boolean = false;

    errorMsg: string;

    constructor(
        public selfUserService: SelfUserServiceProxy,
        private httpClientService: HttpClientService,
        private matDialog: MatDialog,
        private snackBarService: SnackBarService,
        private translate: TranslateService,
        private router: Router,
    ) {}

    getAvatar() {
        if (this.selfUserService.user) {
            return this.selfUserService.user.avatar;
        }
        return 'assets/images/avatars/default.png';
    }

    getUsername() {
        if (this.selfUserService.user) {
            return this.selfUserService.user.username;
        }
        return 'Username';
    }

    enableEditing() {
        if (this.checkIsInGame()) return;
        this.editingUsername = true;
        this.errorMsg = '';
    }

    checkIsInGame() {
        if (this.router.url === '/game') return true;
        return false;
    }

    isValidUsername(username: string) {
        if (username.match('^[A-Za-z0-9]+$')) return true;
        return false;
    }

    retrieveNewUsername(event: any) {
        const newUsername: string = event.target.value.toString().trim();

        if (newUsername.length === 0 || newUsername.length > 10) {
            this.errorMsg = this.translate.instant('profile.username.length');
            return;
        }

        if (!this.isValidUsername(newUsername) || newUsername.indexOf(' ') >= 0) {
            this.errorMsg = this.translate.instant('profile.username.invalid');
            return;
        }

        if (newUsername === this.getUsername()) return;

        this.httpClientService
            .updateUsername(this.selfUserService.user, newUsername)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.errorMsg = '';
                    this.editingUsername = false;
                    this.selfUserService.user.username = newUsername;
                    this.snackBarService.open(this.translate.instant('profile.username.updated'), 'OK');
                },
                error: (res) => {
                    this.errorMsg =
                        res.error === 11000 ? this.translate.instant('profile.username.alreadyUsed') : this.translate.instant('common.error');
                },
            });
    }

    disableEditing() {
        this.editingUsername = false;
        this.errorMsg = '';
    }

    openAvatarDlg() {
        this.matDialog.open(AvatarSelectComponent, {
            width: '420px',
        });
    }

    displaySeconds(): number | string {
        return this.selfUserService.user.statistics.averageTimePerGame.seconds < 10
            ? '0' + Math.round(this.selfUserService.user.statistics.averageTimePerGame.seconds)
            : Math.round(this.selfUserService.user.statistics.averageTimePerGame.seconds);
    }
}
