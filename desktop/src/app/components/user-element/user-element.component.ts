import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '@app/classes/user';
import { UserClickOptionsComponent } from '@app/components/user-click-options/user-click-options.component';
import { GlobalUserProxyService } from '@app/services/global-user-proxy/global-user-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';

const MODAL_OFFSET = 20;

@Component({
    selector: 'app-user-element',
    templateUrl: './user-element.component.html',
    styleUrls: ['./user-element.component.scss'],
})
export class UserElementComponent {
    @Input() user: User;

    constructor(public matDialog: MatDialog, public globalUserService: GlobalUserProxyService, public selfUserService: SelfUserServiceProxy) {}

    openUserClickOptions(event: MouseEvent) {
        if (this.user.id === this.selfUserService.user.id) return;
        const locationX = (event.clientX - MODAL_OFFSET).toString() + 'px';
        const locationY = (event.clientY - MODAL_OFFSET).toString() + 'px';
        const user = this.user;
        this.matDialog.open(UserClickOptionsComponent, {
            position: { top: locationY, left: locationX },
            width: '220px',
            backdropClass: 'cdk-overlay-transparent-backdrop',
            panelClass: 'backdrop-dropdown',
            data: {
                user,
                locationY,
                locationX,
            },
        });
    }
}
