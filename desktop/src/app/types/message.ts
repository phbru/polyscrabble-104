// export type Message = {
//     user?: string;
//     color: Color;
//     output: string;
//     timestamp?: string;
// };

import { User } from '@app/classes/user';

export type Message = {
    user: User;
    output: string;
    timestamp: string;
};
