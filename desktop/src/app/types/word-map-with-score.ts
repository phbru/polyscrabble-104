import { WordMap } from './word-map';

export type WordMapWithScore = {
    wordMap: WordMap;
    score: number;
};
