import { CharacterPosition } from './character-position';

export type WordMap = {
    word: string;
    map: CharacterPosition[];
};
