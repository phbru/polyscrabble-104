import { Letter } from '@app/types/letter';

export type CharacterPosition = {
    letter: Letter;
    line: string;
    column: number;
};
