import { User } from '@app/classes/user';

export type GameInfo = {
    id: string;
    creatorName: string;
    time: number;
    dictionary: string;
    openSpots: number;
    password: string;
    players: User[];
    observers: User[];
    virtualPlayers: User[];
    invitedUsers: User[];
    isGameStarted: boolean;
    isPrivate: boolean;
};
