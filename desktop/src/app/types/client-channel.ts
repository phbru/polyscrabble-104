import { Channel } from './channel';

export type ClientChannel = {
    newMessages: boolean;
    channel: Channel;
};
