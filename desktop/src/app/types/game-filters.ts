/* eslint-disable prettier/prettier */
// export type GameFilters = {
//     publicAccess: Filter;
//     privateAccess: Filter;
//     frenchDictionary: Filter;
//     englishDictionary: Filter;
//     timer: Filter;
//     players: Filter;
// };

export type Filter = {
    translatedLabel: string;
    value: string;
};
