export type CaseParameters = {
    bonusInfo: { word: string; bonusNumber: string };
    color: string;
};
