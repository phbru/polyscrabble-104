export type Letter = {
    character: string;
    weight: number;
    isBlankLetter: boolean;
};
