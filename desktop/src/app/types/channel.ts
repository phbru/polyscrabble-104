import { User } from '@app/classes/user';
import { Message } from '@app/types/message';

export type Channel = {
    id: string;
    name: string;
    users: User[];
    owner: User;
    searchable: boolean;
    directMessage: boolean;
    messages: Message[];
};
