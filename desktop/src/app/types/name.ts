export type Name = {
    name: string;
    isBeginner: boolean;
    isDefault: boolean;
};
