import { Statistics } from '@app/types/statistics';

export class User {
    username: string;
    avatar: string;
    email: string;
    password: string;
    confirmPassword: string;
    id: string;
    statistics: Statistics = {
        gamesPlayed: 0,
        gamesWon: 0,
        gamesLost: 0,
        elo: 0,
        rank: 0,
        averagePointByGame: 0,
        averageTimePerGame: { minutes: 0, seconds: 0 },
    };

    static register(username: string, avatar: string, email: string, password: string, confirmPassword: string): User {
        const user: User = new User();
        user.username = username;
        user.avatar = avatar;
        user.email = email;
        user.password = password;
        user.confirmPassword = confirmPassword;

        return user;
    }

    static login(username: string, password: string) {
        const user: User = new User();
        user.username = username;
        user.password = password;

        return user;
    }
}
