import { Color } from '@app/enums/color';
import { Letter } from '@app/types/letter';
import { User } from './user';

export class Player {
    user: User;
    currentLetters: Letter[];
    points: number = 0;
    color: Color = Color.Green;
    isBotPlayer: boolean = false;

    constructor(user: User, currentLetters: Letter[], points?: number) {
        this.user = user;
        this.currentLetters = currentLetters;
        if (points !== undefined) {
            this.points = points;
        }
    }
}
