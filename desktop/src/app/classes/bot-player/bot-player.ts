import { Player } from '@app/classes/player';
import { Letter } from '@app/types/letter';

export class BotPlayer extends Player {
    currentLetters: Letter[];
    points: number = 0;
    startActionTime: number = 0;
}
