/* eslint-disable prettier/prettier */
export enum GameParticipantType {
    CreatorPlayer = 0,
    RegularPlayer = 1,
    Observer = 2,
}
