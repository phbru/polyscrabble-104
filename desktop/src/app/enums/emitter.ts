export enum Emitter {
    Player0 = 0,
    Player1 = 1,
    Bot = 2,
    System = 3,
    Error = 4,
}
