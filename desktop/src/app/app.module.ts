/* eslint-disable max-len */
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PlayAreaComponent } from '@app/components/play-area/play-area.component';
import { TokenComponent } from '@app/components/token/token.component';
import { ReflectPropSetup } from '@app/decorators/reflect-prop/reflect-prop';
import { AppRoutingModule } from '@app/modules/app-routing.module';
import { AppMaterialModule } from '@app/modules/material.module';
import { AppComponent } from '@app/pages/app/app.component';
import { GamePageComponent } from '@app/pages/game-page/game-page.component';
import { MainPageComponent } from '@app/pages/main-page/main-page.component';
import { WaitingRoomPageComponent } from '@app/pages/waiting-room-page/waiting-room-page.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';
import { AvatarSelectComponent } from './components/avatar-select/avatar-select.component';
import { ChannelInfoComponent } from './components/channel-info/channel-info.component';
import { ChannelsBannerComponent } from './components/channels-banner/channels-banner.component';
import { ChatDisplayComponent } from './components/chat-display/chat-display.component';
import { ChatInputComponent } from './components/chat-input/chat-input.component';
import { CommunicationBoxComponent } from './components/communication-box/communication-box.component';
import { CanceledGameDialogComponent } from './components/dialogs/canceled-game-dialog/canceled-game-dialog.component';
import { ChannelMembersDialogComponent } from './components/dialogs/channel-members-dialog/channel-members-dialog.component';
import { ConfirmAbandonDialogComponent } from './components/dialogs/confirm-abandon-dialog/confirm-abandon-dialog.component';
import { CreateChannelDialogComponent } from './components/dialogs/create-channel-dialog/create-channel-dialog.component';
import { CreateGameDialogComponent } from './components/dialogs/create-game-dialog/create-game-dialog.component';
import { FriendRequestsDialogComponent } from './components/dialogs/friend-requests-dialog/friend-requests-dialog.component';
import { GameInvitationDialogComponent } from './components/dialogs/game-invitation-dialog/game-invitation-dialog.component';
import { InviteFriendDialogComponent } from './components/dialogs/invite-friend-dialog/invite-friend-dialog.component';
import { InviteUserDialogComponent } from './components/dialogs/invite-user-dialog/invite-user-dialog.component';
import { JoinGameDialogComponent } from './components/dialogs/join-game-dialog/join-game-dialog.component';
import { KickedOutDialogComponent } from './components/dialogs/kicked-out-dialog/kicked-out-dialog.component';
import { LaunchGameDialogComponent } from './components/dialogs/launch-game-dialog/launch-game-dialog.component';
import { SearchChannelDialogComponent } from './components/dialogs/search-channel-dialog/search-channel-dialog.component';
import { HeaderComponent } from './components/header/header.component';
import { InfoPanelComponent } from './components/info-panel/info-panel.component';
import { JoinableGamesComponent } from './components/joinable-games/joinable-games.component';
import { LetterRackComponent } from './components/letter-rack/letter-rack.component';
import { MenuSettingComponent } from './components/menu-setting/menu-setting.component';
import { OnlineUsersComponent } from './components/online-users/online-users.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TimerComponent } from './components/timer/timer.component';
import { WaitingRoomComponent } from './components/waiting-room/waiting-room.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { ServiceLocator } from './services/service-locator';

import { ChooseBlankLetterComponent } from './components/dialogs/choose-blank-letter/choose-blank-letter.component';

import { TranslateLoader, TranslateModule, TranslateModuleConfig } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ColorThemeSelectorComponent } from './components/color-theme-selector/color-theme-selector.component';
import { EndGameDialogComponent } from './components/dialogs/end-game-dialog/end-game-dialog.component';
import { FilterGamesDialogComponent } from './components/dialogs/filter-games-dialog/filter-games-dialog.component';
import { ProfileDialogComponent } from './components/dialogs/profile-dialog/profile-dialog.component';
import { ReplaceVirtualPlayerComponent } from './components/dialogs/replace-virtual-player/replace-virtual-player.component';
import { WaitingCreatorApprovalDialogComponent } from './components/dialogs/waiting-creator-approval-dialog/waiting-creator-approval-dialog.component';
import { LanguageSelectorComponent } from './components/language-selector/language-selector.component';
import { UserClickOptionsComponent } from './components/user-click-options/user-click-options.component';
import { UserElementComponent } from './components/user-element/user-element.component';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function httpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
const socketConfig: SocketIoConfig = {
    url: environment.serverUrl,
    options: { autoConnect: false },
};

const translateConfig: TranslateModuleConfig = {
    loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient],
    },
};
/**
 * Main module that is used in main.ts.
 * All automatically generated components will appear in this module.
 * Please do not move this module in the module folder.
 * Otherwise Angular Cli will not know in which module to put new component
 */
@NgModule({
    declarations: [
        AppComponent,
        GamePageComponent,
        WaitingRoomPageComponent,
        MainPageComponent,
        PlayAreaComponent,
        CommunicationBoxComponent,
        TokenComponent,
        ChatInputComponent,
        CreateChannelDialogComponent,
        SearchChannelDialogComponent,
        ChannelInfoComponent,
        ChannelMembersDialogComponent,
        LetterRackComponent,
        ChatDisplayComponent,
        TimerComponent,
        CreateGameDialogComponent,
        JoinGameDialogComponent,
        JoinableGamesComponent,
        InfoPanelComponent,
        RegisterPageComponent,
        LoginPageComponent,
        HeaderComponent,
        OnlineUsersComponent,
        ChannelsBannerComponent,
        ProfileComponent,
        AvatarSelectComponent,
        MenuSettingComponent,
        WaitingRoomComponent,
        KickedOutDialogComponent,
        InviteUserDialogComponent,
        CanceledGameDialogComponent,
        GameInvitationDialogComponent,
        ConfirmAbandonDialogComponent,
        LaunchGameDialogComponent,
        InviteFriendDialogComponent,
        FriendRequestsDialogComponent,
        UserClickOptionsComponent,
        UserElementComponent,
        ChooseBlankLetterComponent,
        LanguageSelectorComponent,
        ColorThemeSelectorComponent,
        FilterGamesDialogComponent,
        EndGameDialogComponent,
        ProfileDialogComponent,
        ReplaceVirtualPlayerComponent,
        WaitingCreatorApprovalDialogComponent,
    ],
    imports: [
        AppMaterialModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        SocketIoModule.forRoot(socketConfig),
        MatFormFieldModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatCardModule,
        MatDialogModule,
        MatSelectModule,
        DragDropModule,
        MatOptionModule,
        MatInputModule,
        MatCardModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTooltipModule,
        MatMenuModule,
        MatSnackBarModule,
        BrowserModule,
        TranslateModule.forRoot(translateConfig),
    ],
    providers: [ReflectPropSetup, HttpClient],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor(injector: Injector, reflectPropSetup: ReflectPropSetup) {
        ServiceLocator.injector = injector;
        reflectPropSetup.setupEvents();
        // botManager.setupBotEvents();
    }
}
