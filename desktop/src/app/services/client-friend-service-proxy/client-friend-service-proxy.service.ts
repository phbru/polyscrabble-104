/* eslint-disable no-unused-vars -- needed cause we are using @CallServer : calling method on server. */
/* eslint-disable @typescript-eslint/no-empty-function -- needed cause we are using @CallServer : calling method on server. */
import { Injectable } from '@angular/core';
import { User } from '@app/classes/user';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class ClientFriendServiceProxyService {
    @ReflectProp('ClientFriendService') friends: User[] = [];
    @ReflectProp() friendsRequested: User[] = [];
    @ReflectProp() receivedInvites: User[] = [];
    @ReflectProp() notificationOn: boolean = false;

    constructor(private socket: Socket, private selfUserService: SelfUserServiceProxy) {
        this.socket.on('propUpdate_ClientFriendService_friends', (friends: User[]) => {
            this.friends = friends;
        });

        this.socket.on('propUpdate_ClientFriendService_friendsRequested', (friendsRequested: User[]) => {
            this.friendsRequested = friendsRequested;
        });

        this.socket.on('propUpdate_ClientFriendService_receivedInvites', (receivedInvites: User[]) => {
            this.receivedInvites = receivedInvites;
        });
    }

    @CallServer() removeNotification() {}

    checkIsFriend(userId: string): boolean {
        for (const friend of this.friends) {
            if (userId === friend.id) {
                return true;
            }
        }
        return false;
    }

    checkIfRequestPossible(userId: string): boolean {
        if (userId === this.selfUserService.user.id) {
            return false;
        }

        for (const friend of this.friends) {
            if (userId === friend.id) {
                return false;
            }
        }
        for (const requested of this.friendsRequested) {
            if (userId === requested.id) {
                return false;
            }
        }
        for (const invite of this.receivedInvites) {
            if (userId === invite.id) {
                return false;
            }
        }

        return true;
    }
}
