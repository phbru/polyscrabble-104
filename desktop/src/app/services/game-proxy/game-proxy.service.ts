/* eslint-disable no-unused-vars -- needed cause we are using @CallServer : calling method on server. */
/* eslint-disable @typescript-eslint/no-empty-function -- needed cause we are using @CallServer : calling method on server. */
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Player } from '@app/classes/player';
import { User } from '@app/classes/user';
import { ConfirmAbandonDialogComponent } from '@app/components/dialogs/confirm-abandon-dialog/confirm-abandon-dialog.component';
import { EndGameDialogComponent } from '@app/components/dialogs/end-game-dialog/end-game-dialog.component';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { GameInfo } from '@app/types/game-info';
import { Letter } from '@app/types/letter';
import { WordMap } from '@app/types/word-map';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class GameServiceProxy {
    @ReflectProp('GameService') currentlyPlayingIndex: number; // index of player whose turn it is
    @ReflectProp() players: Player[] = [];
    @ReflectProp() isGameEnded: boolean = false;
    @ReflectProp() sumOfLettersLeftByPlayer: number[] = [0, 0];
    @ReflectProp() winner: Player | undefined;
    @ReflectProp() partialGameInfo: GameInfo;

    isPlayer = false;
    watchedPlayer: number = 0;

    ourIndex: number = 0;
    lettersView: Letter[] = [];
    copyOfPlayersForRematch: string[] = [];

    constructor(
        private socket: Socket,
        private router: Router,
        private dialog: MatDialog,
        public boardModel: BoardModelServiceProxy,
        public selfUserService: SelfUserServiceProxy,
    ) {
        this.socket.on('propUpdate_GameService_players', (players: Player[]) => {
            this.players = players;
            if (players.length > 1) this.updateObserverRack();
        });

        this.socket.on('initiateClientPlayer', () => {
            this.initiateSelfPlayer();
        });

        this.socket.on('goToGamePage', () => {
            this.goToGamePage();
        });

        this.socket.on('takeIndex', (index: number) => {
            this.ourIndex = index;
            this.isPlayer = true;
            this.resetRackViewToModel();
        });

        this.socket.on('gameEnded', (winner: string) => {
            this.copyOfPlayersForRematch = [];
            this.dialog.open(EndGameDialogComponent, { data: winner });
            for (const player of this.players) {
                if (!player.isBotPlayer && player.user.id !== this.selfUserService.user.id) {
                    this.copyOfPlayersForRematch.push(player.user.id);
                }
            }
            socket.emit('leaveGame');
        });
    }

    get ourPlayer() {
        return this.players[this.ourIndex] !== undefined ? this.players[this.ourIndex] : new Player(new User(), []);
    }

    get opponentIndex() {
        return this.ourIndex === 0 ? 1 : 0;
    }
    get opponentPlayer() {
        return this.players[this.opponentIndex] !== undefined ? this.players[this.opponentIndex] : new Player(new User(), []);
    }

    get currentPlayer() {
        return this.players[this.currentlyPlayingIndex] !== undefined ? this.players[this.currentlyPlayingIndex] : new Player(new User(), []);
    }

    @CallServer() async executePass() {}
    @CallServer() async executeExchange(_lettersToExchange: string[]) {}
    @CallServer() async executePlace(_wordMapToPlace: WordMap) {}
    @CallServer() async abandonGame(_leavingPlayerId: string) {}
    @CallServer() async terminateGame() {}
    @CallServer() async takeBotPlace(_botPlayer: Player): Promise<void> {}

    resetLocalParameters() {
        this.isPlayer = false;
        this.watchedPlayer = 0;
        this.ourIndex = 0;
        this.lettersView = [];
        this.copyOfPlayersForRematch = [];
    }

    abandonConfirmation() {
        this.dialog.open(ConfirmAbandonDialogComponent, { data: { confirmAbandon: this.confirmAbandon.bind(this) } });
    }

    setWatchedPlayer(playerIndex: number): void {
        this.watchedPlayer = playerIndex;
        this.updateObserverRack();
    }

    updateObserverRack(): void {
        if (!this.isPlayer) {
            this.lettersView = this.players[this.watchedPlayer].currentLetters;
        }
    }

    async confirmAbandon() {
        if (!this.isGameEnded) {
            await this.abandonGame(this.players[this.ourIndex].user.id);
            this.socket.emit('leaveGame');
        }
        this.router.navigate(['/home']);
        this.dialog.closeAll();
    }

    async quitAsObserver() {
        if (!this.isGameEnded) this.socket.emit('leaveGame');
        this.router.navigate(['/home']);
        this.dialog.closeAll();
    }

    goToGamePage() {
        this.setWatchedPlayer(0);
        this.resetRackViewToModel();
        this.router.navigate(['/game']);
    }

    validatePlayerHasLetters(playerIndex: number, lettersToVerify: Letter[]) {
        // TODO ... in server ? ...
        // TODO : playerIndex is useless here. Should be something like : validateSelfPlayerHasLetter
        if (playerIndex >= this.players.length || playerIndex < 0) {
            return false;
        }

        const currentLetters = Object.assign([], this.players[playerIndex].currentLetters);
        const invalidIndex = -1;
        let indexOfLetter;
        for (const letter of lettersToVerify) {
            if (letter.isBlankLetter) {
                indexOfLetter = currentLetters.findIndex((currentLetter: Letter) => currentLetter.isBlankLetter);
            } else {
                indexOfLetter = currentLetters.findIndex((currentLetter: Letter) => currentLetter.character === letter.character);
            }

            if (indexOfLetter === invalidIndex) {
                return false;
            } else {
                currentLetters.splice(indexOfLetter, 1);
            }
        }
        return true;
    }

    resetRackViewToModel() {
        const letters = this.players[this.ourIndex]?.currentLetters;
        if (letters !== undefined) {
            this.lettersView = this.cloneLetterArray(letters);
        }
    }

    removeLettersFromRackView(letters: Letter[]) {
        const tempLettersView = this.cloneLetterArray(this.lettersView);
        for (const letter of letters) {
            for (let i = 0; i < tempLettersView.length; i++) {
                if (tempLettersView[i].character === letter.character || (tempLettersView[i].isBlankLetter && letter.isBlankLetter)) {
                    tempLettersView.splice(i, 1);
                    break;
                }
            }
        }
        this.lettersView = this.cloneLetterArray(tempLettersView);
    }

    validateIsOurTurn() {
        return this.currentlyPlayingIndex === this.ourIndex;
    }

    private cloneLetter(copiedLetter: Letter): Letter {
        return { character: copiedLetter.character, isBlankLetter: copiedLetter.isBlankLetter, weight: copiedLetter.weight };
    }

    private cloneLetterArray(letters: Letter[]): Letter[] {
        const copiedLetters: Letter[] = [];
        for (const letter of letters) {
            copiedLetters.push(this.cloneLetter(letter));
        }
        return copiedLetters;
    }

    // Gives index and indicates this client is a player
    private initiateSelfPlayer() {
        this.players.forEach((player, index) => {
            if (player.user.id === this.selfUserService.user.id) {
                this.isPlayer = true;
                this.ourIndex = index;
            }
        });
    }
}
