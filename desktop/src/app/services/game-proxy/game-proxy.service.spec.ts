/* eslint-disable @typescript-eslint/no-explicit-any -- private methods*/
// import { TestBed } from '@angular/core/testing';
// import { Router } from '@angular/router';
// import { ServiceLocator } from '@app/services/service-locator';
// import { Socket } from 'ngx-socket-io';

// describe('GameManagerServiceProxy', () => {
//     // let service: GameManagerServiceProxy;
//     let socketSpy: jasmine.SpyObj<Socket>;
//     let routerSpy: jasmine.SpyObj<Router>;

//     // let mockPlayerInfo1: PlayerInfo;
//     // let mockPlayerInfo2: PlayerInfo;
//     // let mockPlayer1: Player;
//     // let mockPlayer2: Player;
//     // let mockBotPlayer: BotPlayer;

//     beforeEach(() => {
//         socketSpy = jasmine.createSpyObj('Socket', ['emit', 'on', 'emitPropUpdate']);
//         routerSpy = jasmine.createSpyObj('Router', ['navigate']);
//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: Socket, useValue: socketSpy },
//                 { provide: Router, useValue: routerSpy },
//             ],
//         });
//         // service = TestBed.inject(GameManagerServiceProxy);
//         ServiceLocator.injector = { get: TestBed.inject };
//     });

//     // it('should be created', () => {
//     //     expect(service).toBeTruthy();
//     // });

//     // describe('with mock players', () => {
//     //     beforeEach(() => {
//     //         mockPlayerInfo1 = { playerName: 'John', color: Color.Green };
//     //         mockPlayerInfo2 = { playerName: 'Paul', color: Color.Green };
//     //         mockPlayer1 = new Player(mockPlayerInfo1, []);
//     //         mockPlayer2 = new Player(mockPlayerInfo2, []);

//     //         service.players = [mockPlayer1, mockPlayer2];
//     //     });

//     //     it('should return current player', () => {
//     //         service.currentlyPlayingIndex = 1;
//     //         expect(service.currentPlayer).toEqual(mockPlayer2);
//     //     });

//     //     it('should return our player', () => {
//     //         service.ourIndex = 1;
//     //         expect(service.ourPlayer).toEqual(mockPlayer2);
//     //     });

//     //     describe('When second player is a Bot, ', () => {
//     //         beforeEach(() => {
//     //             mockPlayerInfo1 = { playerName: 'John', color: Color.Green };
//     //             mockPlayerInfo2 = { playerName: 'Paul', color: Color.Green };
//     //             mockPlayer1 = new Player(mockPlayerInfo1, []);
//     //             mockBotPlayer = new BotPlayer(mockPlayerInfo2, [], BotLevel.Beginner);

//     //             service.players = [mockPlayer1, mockBotPlayer];
//     //         });
//     //         describe('get botIndex', () => {
//     //             it('should return index 1', () => {
//     //                 service.ourIndex = 0;
//     //                 expect(service.botIndex).toEqual(1);
//     //             });

//     //             it('should return index 0', () => {
//     //                 service.ourIndex = 1;
//     //                 expect(service.botIndex).toEqual(0);
//     //             });
//     //         });

//     //         it('should get botPlayer', () => {
//     //             service.ourIndex = 0;
//     //             expect(service.botPlayer).toEqual(mockBotPlayer);
//     //         });
//     //     });

//     //     describe('validatePlayerHasLetters()', () => {
//     //         beforeEach(() => {
//     //             const playerLetters = [
//     //                 { character: 'A', weight: 2, isBlankLetter: false },
//     //                 { character: 'B', weight: 3, isBlankLetter: false },
//     //             ];
//     //             mockPlayer1.currentLetters = playerLetters;
//     //         });

//     //         it('should validate players have letters', () => {
//     //             const tooManyLetters = [
//     //                 { character: 'A', weight: 2, isBlankLetter: false },
//     //                 { character: 'B', weight: 3, isBlankLetter: false },
//     //                 { character: 'C', weight: 4, isBlankLetter: false },
//     //             ];

//     //             expect(service.validatePlayerHasLetters(0, service.players[0].currentLetters)).toBeTrue();
//     //             expect(service.validatePlayerHasLetters(0, tooManyLetters)).toBeFalse();
//     //             expect(service.validatePlayerHasLetters(1, tooManyLetters)).toBeFalse();
//     //         });

//     //         it('should validate players has blank letters', () => {
//     //             const lettersToCheck = [
//     //                 { character: 'A', weight: 2, isBlankLetter: false },
//     //                 { character: 'C', weight: 5, isBlankLetter: true },
//     //             ];
//     //             service.players[0].currentLetters.push({ character: '*', weight: 0, isBlankLetter: true });

//     //             expect(service.validatePlayerHasLetters(0, lettersToCheck)).toBeTrue();
//     //         });

//     //         it('should return false on invalid index', () => {
//     //             expect(service.validatePlayerHasLetters(2, [{ character: 'D', weight: 3, isBlankLetter: false }])).toBeFalse();
//     //         });
//     //     });

//     //     it('should validate our turn', () => {
//     //         service.currentlyPlayingIndex = 0;
//     //         service.ourIndex = 0;
//     //         expect(service.validateIsOurTurn()).toBeTrue();
//     //     });

//     //     it('should validate bot turn', () => {
//     //         service.currentlyPlayingIndex = 1;
//     //         service.ourIndex = 0;
//     //         expect(service.verifyBotTurn()).toBeTrue();
//     //     });
//     // });
//     // describe('startGame', () => {
//     //     beforeEach(() => {
//     //         mockPlayerInfo1 = { playerName: 'John', color: Color.Green };
//     //         mockPlayerInfo2 = { playerName: 'Paul', color: Color.Green };
//     //         const playerLetters = [
//     //             { character: 'A', weight: 2, isBlankLetter: false },
//     //             { character: 'B', weight: 3, isBlankLetter: false },
//     //         ];
//     //         mockPlayer1 = new Player(mockPlayerInfo1, playerLetters);
//     //         mockPlayer2 = new Player(mockPlayerInfo2, playerLetters);
//     //     });
//     //     it('should start game when two players join', () => {
//     //         const startGameSpy = spyOn(service, 'startGame');
//     //         const resetRackViewSpy = spyOn(service, 'resetRackViewToModel');
//     //         const mockPlayers = [mockPlayer1, mockPlayer2];
//     //         service.players = mockPlayers;
//     //         service.ourIndex = 0;
//     //         socketSpy.on.calls.argsFor(0)[1](mockPlayers);
//     //         expect(startGameSpy).toHaveBeenCalled();
//     //         expect(resetRackViewSpy).toHaveBeenCalled();
//     //     });
//     //     it('should do nothing with one player', () => {
//     //         const serviceSpy = spyOn(service, 'startGame');
//     //         const mockPlayers = [mockPlayer1];
//     //         socketSpy.on.calls.argsFor(0)[1](mockPlayers);
//     //         expect(serviceSpy).not.toHaveBeenCalled();
//     //     });
//     // });
//     // describe('rackView', () => {
//     //     beforeEach(() => {
//     //         mockPlayerInfo1 = { playerName: 'John', color: Color.Green };
//     //         mockPlayerInfo2 = { playerName: 'Paul', color: Color.Green };
//     //         const playerLetters = [
//     //             { character: 'A', weight: 2, isBlankLetter: false },
//     //             { character: 'B', weight: 3, isBlankLetter: false },
//     //         ];
//     //         mockPlayer1 = new Player(mockPlayerInfo1, playerLetters);
//     //         mockPlayer2 = new Player(mockPlayerInfo2, playerLetters);
//     //     });
//     //     it('should reset rack view', () => {
//     //         service.players = [mockPlayer1, mockPlayer2];
//     //         service.ourIndex = 0;
//     //         service.lettersView = [];
//     //         service.resetRackViewToModel();
//     //         expect(service.lettersView).toEqual([
//     //             { character: 'A', weight: 2, isBlankLetter: false },
//     //             { character: 'B', weight: 3, isBlankLetter: false },
//     //         ]);
//     //     });

//     //     it('should not reset with no player', () => {
//     //         const cloneArraySpy = spyOn<any>(service, 'cloneLetterArray');
//     //         service.players = [];
//     //         service.resetRackViewToModel();
//     //         expect(cloneArraySpy).not.toHaveBeenCalled();
//     //     });

//     //     it('should remove letters from rack view', () => {
//     //         service.lettersView = [
//     //             { character: 'A', weight: 2, isBlankLetter: false },
//     //             { character: 'B', weight: 3, isBlankLetter: false },
//     //         ];
//     //         service.removeLettersFromRackView([{ character: 'B', weight: 3, isBlankLetter: false }]);
//     //         expect(service.lettersView).toEqual([{ character: 'A', weight: 2, isBlankLetter: false }]);
//     //     });

//     //     it('should not remove if wrong letter', () => {
//     //         service.lettersView = [
//     //             { character: 'A', weight: 2, isBlankLetter: false },
//     //             { character: 'B', weight: 3, isBlankLetter: false },
//     //         ];
//     //         service.removeLettersFromRackView([{ character: 'C', weight: 2, isBlankLetter: false }]);
//     //         expect(service.lettersView).toEqual([
//     //             { character: 'A', weight: 2, isBlankLetter: false },
//     //             { character: 'B', weight: 3, isBlankLetter: false },
//     //         ]);
//     //     });

//     //     it('should remove if both blank letters', () => {
//     //         service.lettersView = [
//     //             { character: '*', weight: 2, isBlankLetter: true },
//     //             { character: 'B', weight: 3, isBlankLetter: false },
//     //         ];
//     //         service.removeLettersFromRackView([{ character: 'C', weight: 2, isBlankLetter: true }]);
//     //         expect(service.lettersView).toEqual([{ character: 'B', weight: 3, isBlankLetter: false }]);
//     //     });
//     // });
// });
