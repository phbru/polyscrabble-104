import { Injectable } from '@angular/core';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { Letter } from '@app/types/letter';

const NOT_FOUND = -1;
@Injectable({
    providedIn: 'root',
})
export class LetterRackService {
    currentIndex: number = NOT_FOUND; // For manipulation
    letterIndexes: number[] = []; // For manipulation
    exchangeIndexes: number[] = []; // For exchange
    isExchangeButtonShown: boolean = false;

    constructor(
        private game: GameServiceProxy, // private exchangeCommand: ExchangeCommandService
    ) {}

    processInput(pressedKey: string, mouseScroll: number, letterList: Letter[]) {
        if (pressedKey === '' && mouseScroll === 0) {
            this.currentIndex = -1;
            return;
        }
        if ((pressedKey === 'ArrowRight' || pressedKey === 'ArrowLeft') && this.currentIndex !== NOT_FOUND) {
            return this.swapLetters(pressedKey, letterList);
        } else if (mouseScroll > 0 && this.currentIndex !== NOT_FOUND) {
            return this.swapLetters('ArrowRight', letterList);
        } else if (mouseScroll < 0 && this.currentIndex !== NOT_FOUND) {
            return this.swapLetters('ArrowLeft', letterList);
        } else {
            return this.findIndex(pressedKey, letterList);
        }
    }

    swapLetters(pressedKey: string, letterList: Letter[]) {
        if (pressedKey === 'ArrowRight') {
            if (this.currentIndex === letterList.length - 1) {
                const tmp: Letter = letterList[0];
                letterList[0] = letterList[this.currentIndex];
                letterList[this.currentIndex] = tmp;
                this.currentIndex = 0;
            } else {
                const tmp: Letter = letterList[this.currentIndex + 1];
                letterList[this.currentIndex + 1] = letterList[this.currentIndex];
                letterList[this.currentIndex] = tmp;
                this.currentIndex++;
            }
        } else {
            if (this.currentIndex === 0) {
                const tmp: Letter = letterList[letterList.length - 1];
                letterList[letterList.length - 1] = letterList[this.currentIndex];
                letterList[this.currentIndex] = tmp;
                this.currentIndex = letterList.length - 1;
            } else {
                const tmp: Letter = letterList[this.currentIndex - 1];
                letterList[this.currentIndex - 1] = letterList[this.currentIndex];
                letterList[this.currentIndex] = tmp;
                this.currentIndex--;
            }
        }
    }

    findIndex(pressedKey: string, letterList: Letter[]) {
        if (pressedKey === '') return (this.currentIndex = -1);

        this.letterIndexes = [];
        // Create map of pressedKey
        for (let i = 0; i < letterList.length; i++) {
            if (letterList[i].character.toLowerCase() === pressedKey.toLowerCase()) this.letterIndexes.push(i);
        }
        const LAST_INDEX = this.letterIndexes.length - 1;

        if (!this.letterIndexes.includes(this.currentIndex)) this.currentIndex = -1;
        if (this.letterIndexes.includes(this.currentIndex) && this.letterIndexes[LAST_INDEX] === this.currentIndex) this.currentIndex = -1;

        for (const index of this.letterIndexes) {
            if (index > this.currentIndex) {
                this.currentIndex = index;
                break;
            }
        }
        return;
    }

    exchange(playerLetters: Letter[]) {
        const lettersExchange = [];
        for (const index of this.exchangeIndexes) {
            lettersExchange.push(playerLetters[index].character);
        }

        this.game.executeExchange(lettersExchange);
        // this.exchangeCommand.execute(lettersExchange);
        // TODO : remove :
        // const isExchangeValid = this.exchangeCommand.execute(lettersExchange);
        // const letterToPrint = lettersExchange.join('').toLowerCase();
        // if (isExchangeValid) this.chatHandler.printMessage('!echanger ' + letterToPrint, this.game.ourIndex);
    }

    cancelExchange() {
        this.isExchangeButtonShown = false;
        this.exchangeIndexes = [];
    }

    handleLetterExchange(index: number) {
        this.currentIndex = -1;
        for (let i = 0; i < this.exchangeIndexes.length; i++) {
            if (this.exchangeIndexes[i] === index) {
                if (this.exchangeIndexes.length === 1) this.isExchangeButtonShown = false;
                return this.exchangeIndexes.splice(i, 1);
            }
        }
        this.isExchangeButtonShown = true;
        return this.exchangeIndexes.push(index);
    }

    resetAll() {
        this.currentIndex = -1;
        this.cancelExchange();
    }
}
