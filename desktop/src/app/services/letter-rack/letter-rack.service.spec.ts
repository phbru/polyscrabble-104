// import { TestBed } from '@angular/core/testing';
// import { Player } from '@app/classes/player';
// import { Color } from '@app/enums/color';
// import { ChatHandlerService } from '@app/services/chat-handler/chat-handler.service';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-proxy.service';
// import { LetterBankServiceProxy } from '@app/services/letter-bank-proxy/letter-bank-proxy.service';
// import { LetterRackService } from '@app/services/letter-rack/letter-rack.service';
// import { ServiceLocator } from '@app/services/service-locator';
// import { Letter } from '@app/types/letter';
// import SpyObj = jasmine.SpyObj;

// let fakeLetters: Letter[];

// const NULL_MOUSE_DELTA = 0;
// describe('LetterRackService', () => {
//     let service: LetterRackService;
//     let gameSpy: SpyObj<GameManagerServiceProxy>;
//     let chatHandlerSpy: SpyObj<ChatHandlerService>;
//     let letterBankSpy: SpyObj<LetterBankServiceProxy>;

//     beforeEach(() => {
//         const player = new Player({ playerName: 'John', color: Color.Green }, []);
//         const playerLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
//         for (const letter of playerLetters) player.currentLetters.push({ character: letter, weight: 0, isBlankLetter: false });

//         gameSpy = jasmine.createSpyObj('GameManagerServiceProxy', ['removeLettersFromPlayer',
// 'giveLettersToPlayer', 'changeTurn', 'verifyBotTurn'], {
//             currentlyPlayingIndex: 0,
//             currentPlayer: player,
//         });
//         gameSpy.currentlyPlayingIndex = 0;
//         gameSpy.players = [player];

//         gameSpy.players = [new Player({ playerName: 'John', color: Color.Green }, [])];
//         gameSpy.currentlyPlayingIndex = 0;
//         gameSpy.ourIndex = 0;

//         chatHandlerSpy = jasmine.createSpyObj('ChatHandlerService', ['printMessage']);
//         letterBankSpy = jasmine.createSpyObj('LetterBankServiceProxy', ['reinitializeLetterBank', 'giveBackLetterToBank']);
//         letterBankSpy.letterBank = [];
//     });

//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: GameManagerServiceProxy, useValue: gameSpy },
//                 { provide: ChatHandlerService, useValue: chatHandlerSpy },
//                 { provide: LetterBankServiceProxy, useValue: letterBankSpy },
//             ],
//         });
//         service = TestBed.inject(LetterRackService);
//         ServiceLocator.injector = { get: TestBed.inject };

//         fakeLetters = [
//             { character: 'A', weight: 1, isBlankLetter: false },
//             { character: 'B', weight: 2, isBlankLetter: false },
//             { character: 'C', weight: 3, isBlankLetter: false },
//             { character: 'D', weight: 3, isBlankLetter: false },
//             { character: 'E', weight: 3, isBlankLetter: false },
//             { character: 'A', weight: 3, isBlankLetter: false },
//             { character: 'G', weight: 3, isBlankLetter: false },
//         ];
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     it('should set currentIndex to -1', () => {
//         service.processInput('', NULL_MOUSE_DELTA, fakeLetters);
//         const NOT_FOUND = -1;
//         expect(service.currentIndex).toEqual(NOT_FOUND);
//     });

//     it('should set find the right index for the pressedKey', () => {
//         const input = 'a';
//         const findIndexSpy = spyOn(service, 'findIndex');
//         service.processInput(input, NULL_MOUSE_DELTA, fakeLetters);
//         expect(findIndexSpy).toHaveBeenCalledWith(input, fakeLetters);
//     });

//     it('should call swapLetters when using ArrowRight or ArrowLeft', () => {
//         const input = 'ArrowRight';
//         service.currentIndex = 0;
//         const swapLetterSpy = spyOn(service, 'swapLetters');
//         service.processInput(input, NULL_MOUSE_DELTA, fakeLetters);
//         expect(swapLetterSpy).toHaveBeenCalledWith(input, fakeLetters);
//     });

//     it('should call swapLetters when using ArrowRight or ArrowLeft', () => {
//         const input = 'ArrowLeft';
//         service.currentIndex = 1;
//         const swapLetterSpy = spyOn(service, 'swapLetters');
//         service.processInput(input, NULL_MOUSE_DELTA, fakeLetters);
//         expect(swapLetterSpy).toHaveBeenCalledWith(input, fakeLetters);
//     });

//     it('should call swapLetters when scrolling up', () => {
//         const input = '';
//         const mouseScroll = 100;
//         service.currentIndex = 1;
//         const swapLetterSpy = spyOn(service, 'swapLetters');
//         service.processInput(input, mouseScroll, fakeLetters);
//         expect(swapLetterSpy).toHaveBeenCalledWith('ArrowRight', fakeLetters);
//     });

//     it('should call swapLetters when scrolling down', () => {
//         const input = '';
//         const mouseScroll = -100;
//         service.currentIndex = 1;
//         const swapLetterSpy = spyOn(service, 'swapLetters');
//         service.processInput(input, mouseScroll, fakeLetters);
//         expect(swapLetterSpy).toHaveBeenCalledWith('ArrowLeft', fakeLetters);
//     });

//     it('should swap Letters with ArrowRight and currentIndex is last index', () => {
//         service.currentIndex = fakeLetters.length - 1;
//         const previousIndex = service.currentIndex;
//         const nextIndex = 0;
//         const tmpLetter: Letter = fakeLetters[service.currentIndex];
//         const secondTmpLetter: Letter = fakeLetters[nextIndex];
//         service.swapLetters('ArrowRight', fakeLetters);
//         expect(fakeLetters[nextIndex]).toEqual(tmpLetter);
//         expect(fakeLetters[previousIndex]).toEqual(secondTmpLetter);
//     });

//     it('should swap Letters with ArrowRight and index is middle', () => {
//         service.currentIndex = 3;
//         const previousIndex = service.currentIndex;
//         const nextIndex = 4;
//         const tmpLetter: Letter = fakeLetters[previousIndex];
//         const secondTmpLetter: Letter = fakeLetters[nextIndex];
//         service.swapLetters('ArrowRight', fakeLetters);
//         expect(fakeLetters[nextIndex]).toEqual(tmpLetter);
//         expect(fakeLetters[previousIndex]).toEqual(secondTmpLetter);
//     });

//     it('should swap Letters with ArrowLeft and currentIndex is last index', () => {
//         service.currentIndex = 0;
//         const previousIndex = service.currentIndex;
//         const nextIndex = fakeLetters.length - 1;
//         const tmpLetter: Letter = fakeLetters[service.currentIndex];
//         const secondTmpLetter: Letter = fakeLetters[nextIndex];
//         service.swapLetters('ArrowLeft', fakeLetters);
//         expect(fakeLetters[nextIndex]).toEqual(tmpLetter);
//         expect(fakeLetters[previousIndex]).toEqual(secondTmpLetter);
//     });

//     it('should swap Letters with ArrowLeft and index is middle', () => {
//         service.currentIndex = 3;
//         const previousIndex = service.currentIndex;
//         const nextIndex = 2;
//         const tmpLetter: Letter = fakeLetters[previousIndex];
//         const secondTmpLetter: Letter = fakeLetters[nextIndex];
//         service.swapLetters('ArrowLeft', fakeLetters);
//         expect(fakeLetters[nextIndex]).toEqual(tmpLetter);
//         expect(fakeLetters[previousIndex]).toEqual(secondTmpLetter);
//     });

//     it('findIndex returns currentIndex= -1', () => {
//         service.findIndex('', fakeLetters);
//         const NOT_FOUND = -1;
//         expect(service.currentIndex).toEqual(NOT_FOUND);
//     });

//     it('should findIndex with input "b" ', () => {
//         service.findIndex('b', fakeLetters);
//         const indexB = 1;
//         expect(service.letterIndexes).toEqual([indexB]);
//         expect(service.currentIndex).toEqual(indexB);
//     });

//     it('should findIndex with input "a" with index already on "a" ', () => {
//         service.currentIndex = 0;
//         service.findIndex('a', fakeLetters);
//         const first = 0;
//         const second = 5;
//         const indexA = [first, second];
//         expect(service.letterIndexes.length).toEqual(indexA.length);
//     });

//     it('should findIndex with input "a" with index already on "a" ', () => {
//         service.currentIndex = 5;
//         service.findIndex('a', fakeLetters);
//         const indexA = [0, service.currentIndex];
//         expect(service.letterIndexes.length).toEqual(indexA.length);
//     });

//     // it('should print message in chat when exchange', () => {
//     //     letterBankSpy.letterBank = fakeLetters.slice();
//     //     gameSpy.players[0].currentLetters = fakeLetters.slice();

//     //     service.exchangeIndexes = [1, 2, 3];
//     //     service.exchange(fakeLetters);
//     //     expect(chatHandlerSpy.printMessage).toHaveBeenCalled();
//     // });

//     // it('should print error message in chat when exchange failed ', () => {
//     //     letterBankSpy.letterBank = [];
//     //     gameSpy.players[0].currentLetters = fakeLetters.slice();

//     //     service.exchangeIndexes = [1, 2, 3];
//     //     service.exchange(fakeLetters);
//     //     expect(chatHandlerSpy.printMessage).toHaveBeenCalledWith(jasmine.any(String), Emitter.Error);
//     // });

//     it('should test exchangeIndexes is cleared up', () => {
//         service.cancelExchange();
//         expect(service.exchangeIndexes).toEqual([]);
//     });

//     it('should remove index from exchangeIndexes', () => {
//         service.exchangeIndexes = [1, 2, 3];
//         const index = 1;
//         service.handleLetterExchange(index);
//         const NOT_FOUND = -1;
//         expect(service.currentIndex).toEqual(NOT_FOUND);
//         expect(service.exchangeIndexes).toEqual([2, 3]);
//     });

//     it('should remove index from exchangeIndexes', () => {
//         service.exchangeIndexes = [1, 2, 3];
//         const index = 3;
//         service.handleLetterExchange(index);
//         const NOT_FOUND = -1;
//         expect(service.currentIndex).toEqual(NOT_FOUND);
//         expect(service.exchangeIndexes).toEqual([1, 2]);
//     });

//     it('should add index from exchangeIndexes', () => {
//         service.exchangeIndexes = [];
//         const index = 1;
//         service.handleLetterExchange(index);
//         const NOT_FOUND = -1;
//         expect(service.currentIndex).toEqual(NOT_FOUND);
//         expect(service.exchangeIndexes).toEqual([1]);
//     });

//     it('current index and exchangeIndexes to be resetted', () => {
//         service.resetAll();
//         const NOT_FOUND = -1;
//         expect(service.currentIndex).toEqual(NOT_FOUND);
//         expect(service.exchangeIndexes).toEqual([]);
//     });

//     it('cancel exchange Letters', () => {
//         service.cancelExchange();
//         expect(service.exchangeIndexes).toEqual([]);
//     });
// });
