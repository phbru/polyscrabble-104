// import { TestBed } from '@angular/core/testing';
// import { Bonus } from '@app/enums/bonus';
// import { ServiceLocator } from '@app/services/service-locator';
// import { Case } from '@app/types/case';
// import { Socket } from 'ngx-socket-io';
// import { BoardModelServiceProxy } from './board-model-proxy.service';
// import SpyObj = jasmine.SpyObj;

// describe('BoardModel', () => {
//     let service: BoardModelServiceProxy;
//     let socketSpy: SpyObj<Socket>;
//     let mockBlankGrid: Case[][];

//     beforeEach(async () => {
//         mockBlankGrid = [
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//         ];

//         socketSpy = jasmine.createSpyObj('Socket', ['emit', 'on']);
//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: Socket, useValue: socketSpy },
//                 { provide: DictionaryServiceProxy, useValue: {} },
//             ],
//         });
//         ServiceLocator.injector = { get: TestBed.inject };
//         service = TestBed.inject(BoardModelServiceProxy);
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     describe('propUpdate', () => {
//         it('should listen for boardMatrix propUpdate', () => {
//             expect(socketSpy.on).toHaveBeenCalledWith('propUpdate_BoardModelService_boardMatrix', jasmine.any(Function));
//         });

//         it('should update viewMatrix', () => {
//             const callback = socketSpy.on.calls.argsFor(0)[1];

//             expect(service.viewBoardMatrix.length).toEqual(0);
//             callback(mockBlankGrid);
//             expect(service.viewBoardMatrix).toEqual(mockBlankGrid);
//         });
//     });

//     describe('deepCopyBoardMatrix', () => {
//         it('should deep copy all cases', () => {
//             const returnedCopy = service.deepCopyBoardMatrix(mockBlankGrid);
//             expect(returnedCopy).toEqual(mockBlankGrid);
//             expect(returnedCopy).not.toBe(mockBlankGrid);
//         });
//     });

//     describe('deepCopyCase', () => {
//         it('should deep copy case', () => {
//             const caseToCopy: Case = { letter: { character: 'A', weight: 10, isBlankLetter: true }, bonus: Bonus.LetterX2 };
//             const destCase: Case = { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank };

//             service.deepCopyCase(caseToCopy, destCase);
//             expect(destCase).toEqual(caseToCopy);
//             expect(destCase).not.toBe(caseToCopy);
//         });
//     });

//     describe('addWordToView', () => {
//         it('should add word to view', () => {
//             const wordToAdd = {
//                 word: 'est',
//                 map: [
//                     { letter: { character: 'e', weight: 1, isBlankLetter: false }, line: 'A', column: 4 },
//                     { letter: { character: 's', weight: 1, isBlankLetter: false }, line: 'A', column: 5 },
//                     { letter: { character: 't', weight: 1, isBlankLetter: false }, line: 'A', column: 6 },
//                 ],
//             };

//             service.viewBoardMatrix = mockBlankGrid;
//             service.addWordToView(wordToAdd);

//             const expectedGridData: Case[][] = [
//                 [
//                     { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                     { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                     { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                     { letter: { character: 'e', weight: 1, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                     { letter: { character: 's', weight: 1, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                     { letter: { character: 't', weight: 1, isBlankLetter: false }, bonus: Bonus.Star },
//                     { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 ],
//             ];
//             expect(service.viewBoardMatrix).toEqual(expectedGridData);
//         });
//     });

//     it('should validateBounds horizontal word', () => {
//         const wordToAdd = {
//             word: 'est',
//             map: [
//                 { letter: { character: 'e', weight: 1, isBlankLetter: false }, line: 'A', column: 1 },
//                 { letter: { character: 's', weight: 1, isBlankLetter: false }, line: 'A', column: 2 },
//                 { letter: { character: 't', weight: 1, isBlankLetter: false }, line: 'A', column: 3 },
//             ],
//         };

//         service.viewBoardMatrix = mockBlankGrid;
//         service.addWordToView(wordToAdd);

//         expect(service.validateBounds(wordToAdd)).toEqual(false);
//     });
// });
