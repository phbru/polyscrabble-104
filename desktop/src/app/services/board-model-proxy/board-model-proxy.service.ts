// CallServer replaces defined methods
/* eslint-disable no-unused-vars -- needed cause we are using @CallServer : calling method on server. */
/* eslint-disable @typescript-eslint/no-empty-function  -- needed cause we are using @CallServer : calling method on server. */
import { Injectable } from '@angular/core';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { Line } from '@app/enums/line';
import { Case } from '@app/types/case';
import { MatrixCoordinates } from '@app/types/matrix-coordinates';
import { WordMap } from '@app/types/word-map';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class BoardModelServiceProxy {
    @ReflectProp('BoardModelService') boardMatrix: Case[][] = []; // "Real" board state
    @ReflectProp() startingCase: MatrixCoordinates = { line: -1, column: -1 };
    viewBoardMatrix: Case[][] = []; // Board state displayed by BoardViewService
    constructor(private socket: Socket) {
        // Copy to view incase we already received a matrix
        this.viewBoardMatrix = this.deepCopyBoardMatrix(this.boardMatrix);

        this.socket.on('propUpdate_BoardModelService_boardMatrix', (newMatrix: Case[][]) => {
            this.viewBoardMatrix = this.deepCopyBoardMatrix(newMatrix);
        });

        this.socket.on('propUpdate_BoardModelService_startingCase', (startingCase: MatrixCoordinates) => {
            this.startingCase = startingCase;
        });
    }

    @CallServer() async setSelectedCaseArrow(_coords: MatrixCoordinates) {
        return false;
    }

    @CallServer() async validateEntryTouchesPlacedLetters(_wordMap: WordMap) {
        return false;
    }

    @CallServer() async isBoardEmpty() {
        return false;
    }

    deepCopyBoardMatrix(matrixToCopy: Case[][]): Case[][] {
        const copiedMatrix: Case[][] = [];
        for (let i = 0; i < matrixToCopy.length; i++) {
            copiedMatrix.push([]);
            for (let j = 0; j < matrixToCopy[i].length; j++) {
                copiedMatrix[i].push({ letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: 0 });
                this.deepCopyCase(matrixToCopy[i][j], copiedMatrix[i][j]);
            }
        }
        return copiedMatrix;
    }

    deepCopyCase(sourceCase: Case, destCase: Case) {
        destCase.bonus = sourceCase.bonus;
        destCase.letter.character = sourceCase.letter.character;
        destCase.letter.weight = sourceCase.letter.weight;
        destCase.letter.isBlankLetter = sourceCase.letter.isBlankLetter;
    }

    addWordToView(wordMap: WordMap) {
        for (const letter of wordMap.map) {
            const column = letter.column - 1;
            const line = Line[letter.line as keyof typeof Line];
            this.viewBoardMatrix[line][column].letter = letter.letter;
        }
    }
}
