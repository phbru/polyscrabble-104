import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class ColorThemeService {
    selectedTheme: string = '';

    changeTheme(theme: string) {
        this.selectedTheme = theme;
        document.documentElement.setAttribute('data-theme', theme);
    }
}
