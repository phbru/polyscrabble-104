// import { TestBed } from '@angular/core/testing';
// import { Time } from '@app/types/time';
// import { TimerServiceProxy } from './timer-proxy.service';

// describe('TimerService', () => {
//     let service: TimerServiceProxy;

//     beforeEach(() => {
//         TestBed.configureTestingModule({});
//         service = TestBed.inject(TimerServiceProxy);
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     it('should get time', () => {
//         const time: Time = { minutes: 12, seconds: 34 };
//         // eslint-disable-next-line dot-notation -- testing private attribute
//         service['mTime'] = time;
//         expect(service.time).toEqual(time);
//     });

//     it('should convert to right time', () => {
//         const time = 3.5;
//         const expectedValue: Time = { minutes: 3, seconds: 30 };
//         expect(service.numberToTime(time)).toEqual(expectedValue);
//     });
// });
