/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable } from '@angular/core';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { Time } from '@app/types/time';

@Injectable({
    providedIn: 'root',
})
export class TimerServiceProxy {
    @ReflectProp('TimerService') timerLength: Time = { minutes: 9999, seconds: 99 };
    @ReflectProp('TimerService') private mTime: Time = { minutes: 0, seconds: 0 };

    get time() {
        return this.mTime;
    }

    numberToTime(time: number): Time {
        const minutes = Math.floor(time);
        const secondsInMinute = 60;
        const seconds = (time - minutes) * secondsInMinute;
        return { minutes, seconds };
    }
}
