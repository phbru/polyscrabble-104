/* eslint-disable max-lines */
import { ComponentRef, Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Vec2 } from '@app/classes/vec2';
import { ChooseBlankLetterComponent } from '@app/components/dialogs/choose-blank-letter/choose-blank-letter.component';
import { PlayAreaComponent } from '@app/components/play-area/play-area.component';
import * as constant from '@app/constants/command';
import { MAX_LETTERS_PLAYER } from '@app/constants/command';
import { Direction } from '@app/enums/direction';
import { Line } from '@app/enums/line';
import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
import { BoardViewService } from '@app/services/board-view/board-view';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { LetterBankServiceProxy } from '@app/services/letter-bank-proxy/letter-bank-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { CharacterPosition } from '@app/types/character-position';
import { Letter } from '@app/types/letter';
import { MatrixCoordinates } from '@app/types/matrix-coordinates';
import { WordMap } from '@app/types/word-map';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';

const MAX_COLUMN = 14;
const MAX_LINE = 14;

@Injectable({
    providedIn: 'root',
})
export class BoardEventHandlerService {
    target: ComponentRef<PlayAreaComponent>;
    selectedCase: MatrixCoordinates | undefined;
    arrowDirection: Direction;
    enteredLetters: WordMap = { word: '', map: [] };
    isWritingMode: boolean = false;
    isDragAndDropMode: boolean = false;
    mouseFirstCoord: MatrixCoordinates = { line: 0, column: 0 };
    isRangeBusted: boolean = false;
    selectedToken: CharacterPosition;
    blankLetter: string = '';

    constructor(
        private socket: Socket,
        private boardViewService: BoardViewService,
        private game: GameServiceProxy,
        private boardModel: BoardModelServiceProxy,
        private matDialog: MatDialog,
        private snackBarService: SnackBarService,
        private translate: TranslateService,
    ) {
        this.turnSocketOn();
    }

    turnSocketOn() {
        this.socket.on('propUpdate_GameService_currentlyPlayingIndex', () => {
            if (this.game.players.length === 0) return; // TODO : Needed ? ...
            this.resetBoardEventHandler();
        });
    }

    async getDroppedLetterPosition(event: MouseEvent) {
        const mouseClickPosition: Vec2 = { x: event.offsetX, y: event.offsetY };
        const matrixCoords: MatrixCoordinates = this.determineMatrixCoordinate(mouseClickPosition);
        this.setSelectedCase(matrixCoords);
        if (!this.isClickInBoard(matrixCoords) || this.checkCaseIsTaken(matrixCoords)) {
            this.boardViewService.deleteTemporaryCanvas();
            this.insertLetterInRack();
            return;
        }
        if (!this.game.validateIsOurTurn()) {
            this.clearSelectedToken();
            this.resetBoardEventHandler();
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.notYourTurn'), 'OK');
            return;
        }

        for (const character of this.enteredLetters.map) {
            const charCodeA = 65;
            const matrixCoordsLine = String.fromCharCode(charCodeA + matrixCoords.line);
            if (
                character.column === matrixCoords.column + 1 &&
                character.line === matrixCoordsLine &&
                character.letter.character !== this.selectedToken.letter.character
            ) {
                this.game.lettersView.push({ ...this.selectedToken.letter });
                this.drawEnteredLetters();
                this.clearSelectedToken();
                return;
            }
        }

        const letterObj = this.selectedToken.letter;
        if (letterObj.character !== '') {
            if (letterObj.character === '*') {
                const matDialogRef = this.matDialog.open(ChooseBlankLetterComponent, { backdropClass: 'backdrop' });
                matDialogRef.afterClosed().subscribe(() => {
                    if (this.blankLetter === '') return;
                    LetterBankServiceProxy.getLetterFromCharacter(this.blankLetter);
                    this.writeLetter(LetterBankServiceProxy.getLetterFromCharacter(this.blankLetter));
                    this.game.removeLettersFromRackView([{ character: '*', weight: 0, isBlankLetter: true }]);
                });
            } else {
                this.writeLetter(letterObj);
                if (constant.MAX_LETTERS_PLAYER - this.enteredLetters.map.length !== this.game.lettersView.length)
                    this.game.removeLettersFromRackView([letterObj]);
            }
        }
        if (this.enteredLetters.word.length === 1) this.boardModel.setSelectedCaseArrow(matrixCoords);
        this.clearSelectedToken();
    }

    selectToken(event: MouseEvent): void {
        const mouseClickPosition: Vec2 = { x: event.offsetX, y: event.offsetY };
        const matrixCoords: MatrixCoordinates = this.determineMatrixCoordinate(mouseClickPosition);
        for (const character of this.enteredLetters.map) {
            const charCodeA = 65;
            const matrixCoordsLine = String.fromCharCode(charCodeA + matrixCoords.line);
            if (character.column === matrixCoords.column + 1 && character.line === matrixCoordsLine) {
                this.selectedToken = character;
            }
        }
    }

    moveToken(event: MouseEvent): void {
        this.deleteCharacter(this.selectedToken);
        this.drawEnteredLetters();
        this.boardViewService.createTemporaryCanvas(event, this.selectedToken.letter);
        if (this.enteredLetters.word.length === 0) this.boardModel.setSelectedCaseArrow({ line: -1, column: -1 });
    }

    selectCase(event: MouseEvent): void {
        if (this.isWritingMode || this.game.isGameEnded || this.isDragAndDropMode) return;
        const mouseClickPosition: Vec2 = { x: event.offsetX, y: event.offsetY };
        const matrixCoords: MatrixCoordinates = this.determineMatrixCoordinate(mouseClickPosition);
        if (!this.isClickInBoard(matrixCoords)) {
            this.clearSelectedToken();
            return this.resetBoardEventHandler();
        }
        if (this.checkCaseIsTaken(matrixCoords)) return;

        if (this.game.validateIsOurTurn()) {
            if (this.verifyEqualCases(this.selectedCase, matrixCoords)) {
                this.changeArrowDirection();
            } else {
                this.setSelectedCase(matrixCoords);
            }
            this.drawSelectedCaseArrow(matrixCoords);
        } else {
            this.isWritingMode = false;
            this.selectedCase = undefined;
        }
    }

    onKeyDown(event: KeyboardEvent) {
        const unAccentedKey = event.key.normalize('NFD').replace(/\p{Diacritic}/gu, '');
        if (this.selectedCase === undefined || (this.isDragAndDropMode && unAccentedKey !== 'Enter')) return;
        if (!this.game.validateIsOurTurn()) return;
        if (this.validateKeyIsLetter(unAccentedKey)) {
            this.isWritingMode = true;
            const letterObj = LetterBankServiceProxy.getLetterFromCharacter(unAccentedKey);
            const tempsLettersArray = this.convertWordMapToLetters(this.enteredLetters); // vide premiere fois?
            tempsLettersArray.push(letterObj);
            if (this.game.validatePlayerHasLetters(this.game.ourIndex, tempsLettersArray)) {
                if (this.isRangeBusted) return;
                this.writeLetter(letterObj);
                this.game.removeLettersFromRackView([letterObj]);
                this.incrementSelectedCase();
            }
        } else {
            this.manageSpecialKeys(unAccentedKey);
        }
    }

    async manageSpecialKeys(key: string) {
        switch (key) {
            case 'Escape': {
                this.resetBoardEventHandler();
                break;
            }
            case 'Backspace': {
                this.deleteLastCharacter();
                break;
            }
            case 'Enter': {
                await this.submitCommand();
                break;
            }
        }
    }

    resetBoardEventHandler() {
        if (this.game.players[this.game.currentlyPlayingIndex] === undefined) return;
        this.selectedCase = undefined;
        this.enteredLetters = { word: '', map: [] };

        this.boardViewService.drawBoardContent(); // TODO : needed ? ...
        this.game.resetRackViewToModel(); // TODO : needed ? ...
        this.isWritingMode = false;
        this.isRangeBusted = false;
        this.isDragAndDropMode = false;
        this.clearSelectedToken();
        this.boardModel.setSelectedCaseArrow({ line: -1, column: -1 });
    }

    clearSelectedToken(): void {
        this.selectedToken = {
            letter: { character: '', weight: 0, isBlankLetter: false },
            line: '',
            column: -1,
        };
        this.blankLetter = '';
    }

    insertLetterInRack(): void {
        if (!this.selectedToken.letter.character) return;
        if (this.selectedToken.letter.isBlankLetter) this.selectedToken.letter.character = '*';
        this.game.lettersView.push({ ...this.selectedToken.letter });
    }

    async submitCommand() {
        if (this.selectedCase === undefined) return;
        // something must have been written
        if (this.enteredLetters.map.length <= 0 || (this.isWritingMode === false && this.isDragAndDropMode === false)) return;
        if (!(await this.validateBoardPlacement(this.enteredLetters)) || !this.validateEnteredLettersAreAligned()) {
            this.isDragAndDropMode = false;
            this.resetBoardEventHandler();
            this.snackBarService.open(this.translate.instant('gamePage.forbiddenActions.invalidPlacement'), 'OK');
            return;
        }
        this.selectedCase = undefined; // unselect case
        await this.game.executePlace(this.enteredLetters);
        this.resetBoardEventHandler();
    }

    private async validateBoardPlacement(wordMap: WordMap): Promise<boolean> {
        return (await this.validateEmptyBoardPlacement(wordMap)) && (await this.validateWordIsAttached(wordMap));
    }

    private async validateEmptyBoardPlacement(wordMap: WordMap): Promise<boolean> {
        if (await this.boardModel.isBoardEmpty()) {
            if (wordMap.map.length < 2) return false;
            for (const letter of wordMap.map) {
                if (letter.column === constant.CENTER_COLUMN && letter.line === constant.CENTER_LINE) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }

    private async validateWordIsAttached(wordMap: WordMap): Promise<boolean> {
        const wordIsAttached = await this.boardModel.validateEntryTouchesPlacedLetters(wordMap);

        if (!wordIsAttached) {
            // this.chatHandlerService.printMessage( // TODO
            //     'Commande impossible à réaliser : Votre mot doit se rattacher à un autre sur le plateau. Veuillez faire une autre action.',
            //     Emitter.Error,
            // );
            return false;
        }

        return true;
    }

    private convertWordMapToLetters(wordMap: WordMap) {
        const lettersArray: Letter[] = [];
        for (const letterMap of wordMap.map) {
            lettersArray.push(letterMap.letter);
        }
        return lettersArray;
    }

    private deleteLastCharacter() {
        if (this.enteredLetters.map.length <= 0) return;
        this.enteredLetters.map.pop();
        this.enteredLetters.word = this.enteredLetters.word.substring(0, this.enteredLetters.word.length - 1);
        this.drawEnteredLetters();
        this.game.resetRackViewToModel();
        this.game.removeLettersFromRackView(this.convertWordMapToLetters(this.enteredLetters));
        this.decrementSelectedCase();
        if (this.enteredLetters.map.length <= 0) {
            this.isWritingMode = false;
            return;
        }
    }

    private deleteCharacter(letter: CharacterPosition) {
        if (this.enteredLetters.map.length <= 0) return;
        this.enteredLetters.map.forEach((value, index) => {
            if (value.column === letter.column && value.line === letter.line) {
                this.enteredLetters.map.splice(index, 1);
                const wordToArray = this.enteredLetters.word.split('');
                wordToArray.splice(index, 1);
                this.enteredLetters.word = wordToArray.join('');
                this.drawEnteredLetters();
                if (this.enteredLetters.map.length <= 0) this.isWritingMode = false;
                return;
            }
        });
    }

    private writeLetter(letter: Letter) {
        this.addLetterToWordMap(letter);
        this.drawEnteredLetters();
    }

    private drawEnteredLetters() {
        this.boardViewService.drawBoardContent(); // draws the board without the currently enteredLetters
        const charCodeA = 65;
        for (const letter of this.enteredLetters.map) {
            this.boardViewService.placeToken(letter.letter, { line: letter.line.charCodeAt(0) - charCodeA, column: letter.column - 1 });
        }
    }

    private addLetterToWordMap(letter: Letter) {
        if (this.selectedCase === undefined) return;
        const charPosition: CharacterPosition = { letter, line: '', column: 0 };
        charPosition.column = this.selectedCase.column + 1;
        const charCodeA = 65;
        charPosition.line = String.fromCharCode(charCodeA + this.selectedCase.line);
        this.enteredLetters.map.push(charPosition);
        this.enteredLetters.word += letter.character; // TODO : REMOVE ?
    }

    private incrementSelectedCase() {
        if (this.selectedCase === undefined) return;
        if (this.selectedCase.column === MAX_COLUMN && this.arrowDirection === Direction.Horizontal) {
            this.isRangeBusted = true;
        } else if (this.selectedCase.line === MAX_LINE && this.arrowDirection === Direction.Vertical) {
            this.isRangeBusted = true;
        } else {
            this.isRangeBusted = false;
        }

        if (this.isRangeBusted) return;
        if (this.arrowDirection === Direction.Vertical && this.selectedCase.line < MAX_LINE) {
            do {
                this.selectedCase.line++;
            } while (this.boardModel.boardMatrix[this.selectedCase.line][this.selectedCase.column].letter.character !== '?');
        } else if (this.arrowDirection === Direction.Horizontal && this.selectedCase.column < MAX_COLUMN) {
            do {
                this.selectedCase.column++;
            } while (this.boardModel.boardMatrix[this.selectedCase.line][this.selectedCase.column].letter.character !== '?');
        }
        if (this.enteredLetters.word.length !== MAX_LETTERS_PLAYER) this.boardViewService.drawArrow(this.selectedCase, this.arrowDirection);
    }

    private decrementSelectedCase() {
        if (this.selectedCase === undefined) return;
        if (this.arrowDirection === Direction.Vertical && this.selectedCase.line > 0) {
            do {
                if (!this.isRangeBusted) {
                    this.selectedCase.line--;
                } else if (this.isRangeBusted) this.isRangeBusted = false;
            } while (this.boardModel.boardMatrix[this.selectedCase.line][this.selectedCase.column].letter.character !== '?');
        } else if (this.arrowDirection === Direction.Horizontal && this.selectedCase.column > 0) {
            do {
                if (!this.isRangeBusted) {
                    this.selectedCase.column--;
                } else if (this.isRangeBusted) this.isRangeBusted = false;
            } while (this.boardModel.boardMatrix[this.selectedCase.line][this.selectedCase.column].letter.character !== '?');
        }
        this.boardViewService.drawArrow(this.selectedCase, this.arrowDirection);
    }

    private changeArrowDirection() {
        this.arrowDirection = this.arrowDirection === Direction.Horizontal ? Direction.Vertical : Direction.Horizontal;
    }

    private setSelectedCase(coord: MatrixCoordinates) {
        this.selectedCase = coord;
        this.arrowDirection = Direction.Horizontal;
    }

    private async drawSelectedCaseArrow(coord: MatrixCoordinates) {
        this.boardViewService.drawBoardContent();
        this.boardViewService.drawArrow(coord, this.arrowDirection);
        this.boardModel.setSelectedCaseArrow(coord);
    }

    private verifyEqualCases(case1: MatrixCoordinates | undefined, case2: MatrixCoordinates | undefined): boolean {
        if (case1 === undefined || case2 === undefined) {
            return false;
        } else {
            return case1.line === case2.line && case1.column === case2.column;
        }
    }

    private determineMatrixCoordinate(position: Vec2): MatrixCoordinates {
        const mouseCoord: MatrixCoordinates = { line: 0, column: 0 };
        mouseCoord.line = this.boardViewService.convertPositionToCoord(position.y);
        mouseCoord.column = this.boardViewService.convertPositionToCoord(position.x);
        return mouseCoord;
    }

    private isClickInBoard(coords: MatrixCoordinates) {
        return coords.column <= MAX_COLUMN && coords.column >= 0 && coords.line <= MAX_LINE && coords.line >= 0;
    }

    private validateKeyIsLetter(key: string) {
        if (key.length > 1) {
            return false;
        }
        const res = /^[a-zA-Z]+$/.test(key);
        return res;
    }

    private checkCaseIsTaken(matrixCoords: MatrixCoordinates): boolean {
        return this.boardModel.boardMatrix[matrixCoords.line][matrixCoords.column].letter.character !== '?';
    }

    private validateEnteredLettersAreAligned(): boolean {
        if (this.enteredLetters.word.length === 1) return true;
        const firstTokenLine = Line[this.enteredLetters.map[0].line as keyof typeof Line];
        const secondTokenLine = Line[this.enteredLetters.map[1].line as keyof typeof Line];
        const firstTokenColumn = this.enteredLetters.map[0].column;
        const secondTokenColumn = this.enteredLetters.map[1].column;

        // horizontal: check if all letters are on the same line
        if (firstTokenLine === secondTokenLine) {
            const minRangeValue = Math.min(...this.enteredLetters.map.map((character) => character.column));
            const maxRangeValue = Math.max(...this.enteredLetters.map.map((character) => character.column));
            return this.checkIsEnteredLetterHorizontal(firstTokenLine, minRangeValue, maxRangeValue);
        }
        // vertical: check if all letters are in the same column
        else if (firstTokenColumn === secondTokenColumn) {
            const minRangeValue = Math.min(...this.enteredLetters.map.map((character) => Line[character.line as keyof typeof Line]));
            const maxRangeValue = Math.max(...this.enteredLetters.map.map((character) => Line[character.line as keyof typeof Line]));
            return this.checkIsEnteredLetterVertical(firstTokenColumn, minRangeValue, maxRangeValue);
        }
        return false;
    }

    private checkIsEnteredLetterHorizontal(line: number, minRangeValue: number, maxRangeValue: number): boolean {
        const placementRange = Array.from({ length: maxRangeValue - minRangeValue + 1 }, (element, index) => index + minRangeValue);
        // check if each letter is on the same line or outside the range
        for (const letter of this.enteredLetters.map) {
            if (Line[letter.line as keyof typeof Line] !== line || letter.column < minRangeValue || letter.column > maxRangeValue) return false;
            const foundIndex = placementRange.indexOf(letter.column);
            if (foundIndex >= 0) placementRange.splice(foundIndex, 1);
        }
        // check if placement is combined with another word
        for (let i = placementRange.length - 1; i >= 0; i--) {
            if (this.checkCaseIsTaken({ line, column: placementRange[i] - 1 })) placementRange.splice(i, 1);
        }
        if (placementRange.length === 0) return true;
        return false;
    }

    // private checkIsEnteredLetterVertical(column: number, minValue: number, maxValue: number): boolean {
    //     const includedValues = Array.from({ length: maxValue - minValue + 1 }, (element, index) => index + minValue);
    //     let foundIndex = -1;
    //     for (const letter of this.enteredLetters.map) {
    //         if (letter.column !== column || Line[letter.line as keyof typeof Line] < minValue || Line[letter.line as keyof typeof Line] > maxValue)
    //             return false;
    //         foundIndex = includedValues.indexOf(Line[letter.line as keyof typeof Line]);
    //         if (foundIndex >= 0) includedValues.splice(foundIndex, 1);
    //     }

    //     for (let i = includedValues.length - 1; i >= 0; i--) {
    //         if (this.checkCaseIsTaken({ line: includedValues[i], column: column - 1 })) includedValues.splice(i, 1);
    //     }
    //     if (includedValues.length === 0) return true;
    //     return false;
    // }

    private checkIsEnteredLetterVertical(column: number, minRangeValue: number, maxRangeValue: number): boolean {
        const placementRange = Array.from({ length: maxRangeValue - minRangeValue + 1 }, (element, index) => index + minRangeValue);
        // check if each letter is on the same column and within the range
        for (const letter of this.enteredLetters.map) {
            const line = Line[letter.line as keyof typeof Line];
            if (letter.column !== column || line < minRangeValue || line > maxRangeValue) return false;
            const foundIndex = placementRange.indexOf(line);
            if (foundIndex >= 0) placementRange.splice(foundIndex, 1);
        }
        // check if another is within the range
        for (let i = placementRange.length - 1; i >= 0; i--) {
            if (this.checkCaseIsTaken({ line: placementRange[i], column: column - 1 })) placementRange.splice(i, 1);
        }

        if (placementRange.length === 0) return true;
        return false;
    }
}
