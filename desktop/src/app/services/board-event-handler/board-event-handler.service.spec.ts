/* eslint-disable max-lines */
// // File is too long, dot notation and any are for private functions and variables
// /* eslint-disable max-lines */
// /* eslint-disable dot-notation */
// /* eslint-disable @typescript-eslint/no-explicit-any */
// import { TestBed } from '@angular/core/testing';
// import { MatDialog } from '@angular/material/dialog';
// import { Player } from '@app/classes/player';
// import { Bonus } from '@app/enums/bonus';
// import { Color } from '@app/enums/color';
// import { Direction } from '@app/enums/direction';
// import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
// import { BoardViewService } from '@app/services/board-view/board-view';
// import { ChatHandlerService } from '@app/services/chat-handler/chat-handler.service';
// import { CommandManagerService } from '@app/services/command-manager/command-manager.service';
// import { DebugService } from '@app/services/debug/debug.service';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-manager-proxy.service';
// import { ServiceLocator } from '@app/services/service-locator';
// import { WordFinderServiceProxy } from '@app/services/word-finder-proxy/word-finder-proxy.service';
// import { Letter } from '@app/types/letter';
// import { MatrixCoordinates } from '@app/types/matrix-coordinates';
// import { WordMap } from '@app/types/word-map';
// import { Socket } from 'ngx-socket-io';
// import { BoardEventHandlerService } from './board-event-handler.service';

// describe('boardEventHandler', () => {
//     let service: BoardEventHandlerService;
//     let boardViewSpy: jasmine.SpyObj<BoardViewService>;
//     let socketSpy: jasmine.SpyObj<Socket>;
//     let matDialogSpy: jasmine.SpyObj<MatDialog>;
//     let commandManagerSpy: jasmine.SpyObj<CommandManagerService>;
//     let chatHandlerSpy: jasmine.SpyObj<ChatHandlerService>;
//     let gameSpy: jasmine.SpyObj<GameManagerServiceProxy>;
//     let wordFinderServiceSpy: jasmine.SpyObj<WordFinderServiceProxy>;
//     let boardModelSpy: jasmine.SpyObj<BoardModelServiceProxy>;
//     let player1: Player;
//     let player2: Player;

//     beforeEach(() => {
//         boardViewSpy = jasmine.createSpyObj('BoardViewService', ['convertPositionToCoord', 'drawBoardContent', 'placeToken', 'drawArrow']);
//         matDialogSpy = jasmine.createSpyObj('MatDialog', ['open']);
//         chatHandlerSpy = jasmine.createSpyObj('ChatHandlerService', ['printMessage']);

//         player1 = new Player({ playerName: 'John', color: Color.Green }, []);
//         player2 = new Player({ playerName: 'Joe', color: Color.Blue }, []);
//         gameSpy = jasmine.createSpyObj(
//             'GameManagerServiceProxy',
//             ['validateIsOurTurn', 'validatePlayerHasLetters', 'removeLettersFromRackView', 'resetRackViewToModel'],
//             {
//                 currentlyPlayingIndex: 0,
//                 currentPlayer: player1,
//                 ourIndex: 0,
//                 ourPlayer: player1,
//             },
//         );
//         gameSpy.currentlyPlayingIndex = 0;
//         gameSpy.players = [player1, player2];

//         commandManagerSpy = jasmine.createSpyObj('CommandManagerService', ['executeCommandString']);
//         commandManagerSpy.executeCommandString.and.resolveTo(true);
//         wordFinderServiceSpy = jasmine.createSpyObj('WordFinderServiceProxy', ['getCompleteWord']);
//         wordFinderServiceSpy.getCompleteWord.and.resolveTo({
//             word: '',
//             map: [
//                 { letter: { character: 'A', weight: 0, isBlankLetter: false }, line: 'A', column: 0 },
//                 { letter: { character: 'B', weight: 0, isBlankLetter: false }, line: 'A', column: 1 },
//             ],
//         });
//         boardModelSpy = jasmine.createSpyObj('BoardModelServiceProxy', ['getCompleteWord']);

//         socketSpy = jasmine.createSpyObj('Socket', ['emit', 'on']);
//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: BoardViewService, useValue: boardViewSpy },
//                 { provide: Socket, useValue: socketSpy },
//                 { provide: MatDialog, useValue: matDialogSpy },
//                 { provide: DebugService, useValue: '' },
//                 { provide: CommandManagerService, useValue: commandManagerSpy },
//                 { provide: ChatHandlerService, useValue: chatHandlerSpy },
//                 { provide: GameManagerServiceProxy, useValue: gameSpy },
//                 { provide: WordFinderServiceProxy, useValue: wordFinderServiceSpy },
//                 { provide: BoardModelServiceProxy, useValue: boardModelSpy },
//             ],
//         });
//         ServiceLocator.injector = { get: TestBed.inject };
//         service = TestBed.inject(BoardEventHandlerService);

//         boardModelSpy.boardMatrix = [
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//         ];
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     it('should call resetBoardEventHandler when there several players', () => {
//         const resetBoardEventSpy = spyOn(service, 'resetBoardEventHandler');
//         socketSpy.on.calls.argsFor(0)[1]();
//         expect(resetBoardEventSpy).toHaveBeenCalled();
//     });

//     describe('writeLetter', () => {
//         it('should call addLetterToWordMap', () => {
//             const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//             const addLetterToWordMapSpy = spyOn(service, 'addLetterToWordMap');
//             service.writeLetter(letter);
//             expect(addLetterToWordMapSpy).toHaveBeenCalled();
//         });

//         it('should call drawEnteredLetters', () => {
//             const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//             const drawEnteredLettersSpy = spyOn(service, 'drawEnteredLetters');
//             service.writeLetter(letter);
//             expect(drawEnteredLettersSpy).toHaveBeenCalled();
//         });
//     });

//     describe('isClickInBoard', () => {
//         it('should be true', () => {
//             const coords: MatrixCoordinates = { line: 7, column: 7 };
//             const valueReturned = service['isClickInBoard'](coords);
//             expect(valueReturned).toEqual(true);
//         });

//         it('should be false', () => {
//             const coords: MatrixCoordinates = { line: -1, column: 7 };
//             const valueReturned = service['isClickInBoard'](coords);
//             expect(valueReturned).toEqual(false);
//         });

//         it('should be false', () => {
//             const coords: MatrixCoordinates = { line: 8, column: -2 };
//             const valueReturned = service['isClickInBoard'](coords);
//             expect(valueReturned).toEqual(false);
//         });
//     });

//     describe('selectCase', () => {
//         it('should call isClickInBoard', () => {
//             const event = { offsetX: 135, offsetY: 89 } as MouseEvent;
//             const isClickInBoardSpy = spyOn<any>(service, 'isClickInBoard');
//             service.isWritingMode = false;
//             service.selectCase(event);
//             expect(isClickInBoardSpy).toHaveBeenCalled();
//         });

//         it('should return since case is not ?', () => {
//             spyOn<any>(service, 'determineMatrixCoordinate').and.returnValue({ line: 1, column: 1 });
//             service.isWritingMode = false;
//             boardModelSpy.boardMatrix[1][1].letter.character = 'a';
//             service.selectCase({} as unknown as MouseEvent);
//             expect(gameSpy.validateIsOurTurn).not.toHaveBeenCalled();
//         });

//         it('isClickInBoard should return', () => {
//             const event = { offsetX: 135, offsetY: 89 } as MouseEvent;
//             const isClickInBoardSpy = spyOn<any>(service, 'isClickInBoard');
//             service.isWritingMode = true;
//             service.selectCase(event);
//             expect(isClickInBoardSpy).toHaveBeenCalledTimes(0);
//         });

//         it(' should call changeArrowDirection', () => {
//             const changeArrowSpy = spyOn<any>(service, 'changeArrowDirection');
//             const event = { offsetX: 20, offsetY: 10 } as MouseEvent;
//             spyOn<any>(service, 'isClickInBoard').and.returnValue(true);
//             spyOn<any>(service, 'determineMatrixCoordinate').and.returnValue({ line: 0, column: 0 });
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             spyOn<any>(service, 'verifyEqualCases').and.returnValue(true);
//             service.selectCase(event);
//             expect(changeArrowSpy).toHaveBeenCalled();
//         });

//         it(' should call setSelectedCase', () => {
//             const setSelectedCaseSpy = spyOn<any>(service, 'setSelectedCase');
//             const event = { offsetX: 20, offsetY: 10 } as MouseEvent;
//             spyOn<any>(service, 'isClickInBoard').and.returnValue(true);
//             spyOn<any>(service, 'determineMatrixCoordinate').and.returnValue({ line: 0, column: 0 });
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             spyOn<any>(service, 'verifyEqualCases').and.returnValue(false);
//             service.selectCase(event);
//             expect(setSelectedCaseSpy).toHaveBeenCalled();
//         });

//         it(' should set writingMode to false and selectedCase to undefined', () => {
//             const event = { offsetX: 20, offsetY: 10 } as MouseEvent;
//             spyOn<any>(service, 'isClickInBoard').and.returnValue(true);
//             spyOn<any>(service, 'determineMatrixCoordinate').and.returnValue({ line: 0, column: 0 });
//             gameSpy.validateIsOurTurn.and.returnValue(false);
//             service.selectCase(event);
//             expect(service.isWritingMode).toBeFalsy();
//             expect(service.selectedCase).toBeUndefined();
//         });
//     });

//     describe('keyDown', () => {
//         it('validateIsOurTurn should return false', () => {
//             service.selectedCase = { line: 7, column: 7 };
//             gameSpy.currentlyPlayingIndex = 0;
//             const event = new KeyboardEvent('keydown', {
//                 key: 'Escape',
//             });
//             const resetBoardEventHandlerSpy = spyOn(service, 'resetBoardEventHandler');
//             service.onKeyDown(event);
//             expect(resetBoardEventHandlerSpy).toHaveBeenCalledTimes(0);
//         });

//         it('should return if range is busted', () => {
//             const writeLetterSpy = spyOn(service, 'writeLetter');
//             const incrementSelectedSpy = spyOn<any>(service, 'incrementSelectedCase');
//             service.selectedCase = { line: 7, column: 7 };
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             gameSpy.currentlyPlayingIndex = 0;
//             service.isRangeBusted = true;
//             const event = new KeyboardEvent('keydown', {
//                 key: 'Escape',
//             });
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             spyOn<any>(service, 'validateKeyIsLetter').and.returnValue(true);
//             service.onKeyDown(event);
//             expect(writeLetterSpy).not.toHaveBeenCalled();
//             expect(gameSpy.removeLettersFromRackView).not.toHaveBeenCalled();
//             expect(incrementSelectedSpy).not.toHaveBeenCalled();
//         });

//         it('should call manageSpecialKeys if validateKeyIsLetter returns false', () => {
//             const writeLetterSpy = spyOn(service, 'writeLetter');
//             const incrementSelectedSpy = spyOn<any>(service, 'incrementSelectedCase');
//             const specialKeysSpy = spyOn<any>(service, 'manageSpecialKeys');
//             service.selectedCase = { line: 7, column: 7 };
//             const event = new KeyboardEvent('keydown', {
//                 key: 'Escape',
//             });
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             spyOn<any>(service, 'validateKeyIsLetter').and.returnValue(false);
//             service.onKeyDown(event);
//             expect(writeLetterSpy).not.toHaveBeenCalled();
//             expect(gameSpy.removeLettersFromRackView).not.toHaveBeenCalled();
//             expect(incrementSelectedSpy).not.toHaveBeenCalled();
//             expect(specialKeysSpy).toHaveBeenCalled();
//         });

//         it('should call functions if player has letters', () => {
//             const writeLetterSpy = spyOn(service, 'writeLetter');
//             const incrementSelectedSpy = spyOn<any>(service, 'incrementSelectedCase');
//             spyOn<any>(service, 'validateKeyIsLetter').and.returnValue(true);
//             service.selectedCase = { line: 1, column: 1 };
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             gameSpy.currentlyPlayingIndex = 0;
//             service.isRangeBusted = false;
//             const event = new KeyboardEvent('keydown', {
//                 key: 'Escape',
//             });
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             service.onKeyDown(event);
//             expect(writeLetterSpy).toHaveBeenCalled();
//             expect(gameSpy.removeLettersFromRackView).toHaveBeenCalled();
//             expect(incrementSelectedSpy).toHaveBeenCalled();
//         });

//         it('validateKeyIsLetterSpy should be true', () => {
//             const event = new KeyboardEvent('keydown', {
//                 key: 'é',
//             });
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             service.onKeyDown(event);
//             service['validateKeyIsLetter']('e');
//             expect(service['validateKeyIsLetter']('e')).toEqual(true);
//         });

//         it('should call convertWordMapToLetters and incrementSelectedCase', () => {
//             service.selectedCase = { line: 7, column: 7 };
//             service.arrowDirection = Direction.Horizontal;
//             const event = new KeyboardEvent('keydown', {
//                 key: 'é',
//             });
//             const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//             service.enteredLetters = {
//                 word: '',
//                 map: [
//                     { letter, line: 'h', column: 8 },
//                     { letter, line: 'h', column: 9 },
//                 ],
//             };
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             service.isRangeBusted = false;
//             const validateKeyIsLetterSpy = spyOn<any>(service, 'validateKeyIsLetter');
//             validateKeyIsLetterSpy.and.returnValue(true);
//             service.enteredLetters = { word: '', map: [] };
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             const convertWordMapToLettersSpy = spyOn(service, 'convertWordMapToLetters').and.returnValue([]);
//             const incrementSelectedCaseSpy = spyOn<any>(service, 'incrementSelectedCase');
//             service.onKeyDown(event);
//             expect(convertWordMapToLettersSpy).toHaveBeenCalled();
//             expect(incrementSelectedCaseSpy).toHaveBeenCalled();
//         });
//     });

//     describe('manageSpecialKeys', () => {
//         it('should call deleteLastCharacter', () => {
//             const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//             service.enteredLetters = {
//                 word: '',
//                 map: [
//                     { letter, line: 'h', column: 8 },
//                     { letter, line: 'h', column: 9 },
//                 ],
//             };
//             const event = 'Backspace';
//             const deleteLastCharacterSpy = spyOn(service, 'deleteLastCharacter');
//             service.manageSpecialKeys(event);
//             expect(deleteLastCharacterSpy).toHaveBeenCalled();
//         });

//         it('should call resetBoardEventHandler', () => {
//             const spy = spyOn(service, 'resetBoardEventHandler');
//             service.manageSpecialKeys('Escape');
//             expect(spy).toHaveBeenCalled();
//         });

//         it('should call deleteLastCharacter', () => {
//             const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//             service.enteredLetters = {
//                 word: '',
//                 map: [
//                     { letter, line: 'h', column: 8 },
//                     { letter, line: 'h', column: 9 },
//                 ],
//             };
//             const event = 'Enter';
//             service.arrowDirection = Direction.Horizontal;
//             const submitCommandSpy = spyOn(service, 'submitCommand').and.resolveTo();
//             service.manageSpecialKeys(event);
//             expect(submitCommandSpy).toHaveBeenCalled();
//         });

//         it('should call deleteLastCharacter', () => {
//             const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//             service.enteredLetters = {
//                 word: '',
//                 map: [
//                     { letter, line: 'h', column: 8 },
//                     { letter, line: 'h', column: 9 },
//                 ],
//             };
//             const event = 'Enter';
//             const submitCommandSpy = spyOn(service, 'submitCommand').and.resolveTo();
//             service.manageSpecialKeys(event);
//             expect(submitCommandSpy).toHaveBeenCalled();
//         });
//     });

//     // it('addLetterToWordMap should change the length', () => {
//     //     const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//     //     service.selectedCase = { line: 7, column: 7 };
//     //     service.addLetterToWordMap(letter);
//     //     expect(service.enteredLetters.map.length).toBeGreaterThan(0);
//     // });

//     // describe('submitCommand', () => {
//     //     it('should return if map length is 0', async () => {
//     //         service.enteredLetters = { word: '', map: [] };
//     //         const decrementSelectedCaseSpy = spyOn<any>(service, 'decrementSelectedCase');
//     //         const determinePlacedWordSpy = spyOn(service, 'determinePlacedWord').and.callThrough();
//     //         const constructStringWordToPlaceSpy = spyOn(service, 'constructStringWordToPlace').and.callThrough();
//     //         await service.submitCommand();

//     //         expect(decrementSelectedCaseSpy).not.toHaveBeenCalled();
//     //         expect(determinePlacedWordSpy).not.toHaveBeenCalled();
//     //         expect(constructStringWordToPlaceSpy).not.toHaveBeenCalled();
//     //         expect(chatHandlerSpy.printMessage).not.toHaveBeenCalled();
//     //         expect(commandManagerSpy.executeCommandString).not.toHaveBeenCalled();
//     //         expect(boardViewSpy.drawBoardContent).not.toHaveBeenCalled();
//     //     });

//     //     it('should call decrementSelectedCase', async () => {
//     //         service.isWritingMode = true;
//     //         service.enteredLetters = {
//     //             word: 'ab',
//     //             map: [
//     //                 { letter: { character: 'A', weight: 1, isBlankLetter: false }, line: 'A', column: 1 },
//     //                 { letter: { character: 'B', weight: 1, isBlankLetter: false }, line: 'A', column: 2 },
//     //             ],
//     //         };
//     //         const decrementSelectedCaseSpy = spyOn<any>(service, 'decrementSelectedCase');
//     //         spyOn(service, 'determinePlacedWord').and.resolveTo(service.enteredLetters);
//     //         await service.submitCommand();
//     //         expect(decrementSelectedCaseSpy).toHaveBeenCalled();
//     //     });

//         it('should return if isWritingMode is false', async () => {
//             service.isWritingMode = false;
//             service.enteredLetters = {
//                 word: '',
//                 map: [
//                     { letter: { character: 'A', weight: 1, isBlankLetter: false }, line: 'A', column: 1 },
//                     { letter: { character: 'B', weight: 1, isBlankLetter: false }, line: 'A', column: 2 },
//                 ],
//             };
//             const decrementSelectedCaseSpy = spyOn<any>(service, 'decrementSelectedCase');
//             const constructStringWordToPlaceSpy = spyOn<any>(service, 'constructStringWordToPlace');
//             constructStringWordToPlaceSpy.and.returnValue(true);
//             await service.submitCommand();
//             expect(decrementSelectedCaseSpy).not.toHaveBeenCalled();
//             expect(constructStringWordToPlaceSpy).not.toHaveBeenCalled();
//             expect(chatHandlerSpy.printMessage).not.toHaveBeenCalled();
//             expect(commandManagerSpy.executeCommandString).not.toHaveBeenCalled();
//             expect(boardViewSpy.drawBoardContent).not.toHaveBeenCalled();
//         });
//     });
//     // it('constructStringWordToPlace should work correctly', () => {
//     //     const mockWordMap: WordMap = {
//     //         word: 'ab',
//     //         map: [
//     //             { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 1 },
//     //             { letter: { character: 'b', weight: 1, isBlankLetter: true }, line: 'A', column: 2 },
//     //         ],
//     //     };
//     //     const expectedWord = 'aB';
//     //     expect(service.constructStringWordToPlace(mockWordMap)).toEqual(expectedWord);
//     // });

//     // describe('determineDirectionOfWord', () => {
//     //     it('should return vertical if vertical word', () => {
//     //         const mockWordMap: WordMap = {
//     //             word: 'ab',
//     //             map: [
//     //                 { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 1 },
//     //                 { letter: { character: 'b', weight: 1, isBlankLetter: true }, line: 'A', column: 2 },
//     //             ],
//     //         };

//     //         expect(service.determineDirectionOfWord(mockWordMap)).toEqual(Direction.Vertical);
//     //     });
//     //     it('should return horizontal if horizontal word', () => {
//     //         const mockWordMap: WordMap = {
//     //             word: 'ab',
//     //             map: [
//     //                 { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 1 },
//     //                 { letter: { character: 'b', weight: 1, isBlankLetter: true }, line: 'B', column: 1 },
//     //             ],
//     //         };

//     //         expect(service.determineDirectionOfWord(mockWordMap)).toEqual(Direction.Horizontal);
//     //     });
//         it('should changeArrowDirection to Horizontal', () => {
//             service.arrowDirection = Direction.Vertical;
//             service['changeArrowDirection']();
//             expect(service.arrowDirection).toEqual(0);
//         });

//         it('should changeArrowDirection to Vertical', () => {
//             service.arrowDirection = Direction.Horizontal;
//             service['changeArrowDirection']();
//             expect(service.arrowDirection).toEqual(1);
//         });

//         it('should set the selectedCase', () => {
//             const coord: MatrixCoordinates = { line: 1, column: 1 };
//             service['setSelectedCase'](coord);
//             expect(service.selectedCase).toEqual(coord);
//             expect(service.arrowDirection).toEqual(0);
//         });

//         // it('should not determinePlacedWord since selectedCase is undefined', async () => {
//         //     service.selectedCase = undefined;
//         //     expect(await service.determinePlacedWord()).toEqual({ word: '', map: [] });
//         // });

//         // it('determinePlacedWord should change arrow direction', async () => {
//         //     const arrowSpy = spyOn<any>(service, 'changeArrowDirection');
//         //     wordFinderServiceSpy.getCompleteWord.and.resolveTo({
//         //         word: 'ab',
//         //         map: [{ letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 1 }],
//         //     });
//         //     service.selectedCase = { line: 2, column: 2 };
//         //     service.arrowDirection = Direction.Horizontal;
//         //     await service.determinePlacedWord();
//         //     expect(arrowSpy).toHaveBeenCalled();
//         // });

//         // it('convertWordMapToLetters should convert correctly', () => {
//         //     const mockWordMap: WordMap = {
//         //         word: 'ab',
//         //         map: [
//         //             { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 1 },
//         //             { letter: { character: 'b', weight: 1, isBlankLetter: false }, line: 'A', column: 2 },
//         //         ],
//         //     };
//         //     const expectedLetter = [
//         //         { character: 'a', weight: 1, isBlankLetter: false },
//         //         { character: 'b', weight: 1, isBlankLetter: false },
//         //     ];
//         //     expect(service.convertWordMapToLetters(mockWordMap)).toEqual(expectedLetter);
//         // });

//         // describe('deleteLastCharacter', () => {
//         //     it('should return if map length is 0', () => {
//         //         const drawEnteredLettersSpy = spyOn(service, 'drawEnteredLetters');
//         //         const decrementSelectedCaseSpy = spyOn<any>(service, 'decrementSelectedCase');
//         //         service.enteredLetters = { word: 'test', map: [] };
//         //         service.deleteLastCharacter();
//         //         expect(drawEnteredLettersSpy).not.toHaveBeenCalled();
//         //         expect(gameSpy.resetRackViewToModel).not.toHaveBeenCalled();
//         //         expect(gameSpy.removeLettersFromRackView).not.toHaveBeenCalled();
//         //         expect(decrementSelectedCaseSpy).not.toHaveBeenCalled();
//         //     });

//         //     it('should delete last character and put writing mode to false if there is only one', () => {
//         //         const drawEnteredLettersSpy = spyOn(service, 'drawEnteredLetters');
//         //         const decrementSelectedCaseSpy = spyOn<any>(service, 'decrementSelectedCase');
//         //         service.enteredLetters = {
//         //             word: 'a',
//         //             map: [{ letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 1 }],
//         //         };
//         //         service.deleteLastCharacter();
//         //         expect(service.enteredLetters.word).toEqual('');
//         //         expect(service.enteredLetters.map).toEqual([]);
//         //         expect(drawEnteredLettersSpy).toHaveBeenCalled();
//         //         expect(gameSpy.resetRackViewToModel).toHaveBeenCalled();
//         //         expect(gameSpy.removeLettersFromRackView).toHaveBeenCalled();
//         //         expect(decrementSelectedCaseSpy).toHaveBeenCalled();
//         //         expect(service.isWritingMode).toBeFalsy();
//         //     });
//         // });

//         // it('drawEnteredLetters should call placeToken', () => {
//         //     service.enteredLetters = {
//         //         word: 'a',
//         //         map: [{ letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 1 }],
//         //     };
//         //     service.drawEnteredLetters();
//         //     expect(boardViewSpy.placeToken).toHaveBeenCalled();
//         // });

//         describe('incrementSelectedCase', () => {
//             it('should do nothing if selected case is undefined', () => {
//                 service.selectedCase = undefined;
//                 service['incrementSelectedCase']();
//                 expect(boardViewSpy.drawArrow).not.toHaveBeenCalled();
//             });

//             it('should bust line if selected line is max line', () => {
//                 service.arrowDirection = Direction.Vertical;
//                 service.isRangeBusted = false;
//                 service.selectedCase = {
//                     line: 14,
//                     column: 1,
//                 };

//                 service['incrementSelectedCase']();
//                 expect(service.isRangeBusted).toBeTruthy();
//             });

//             it('should go down until you find ? and draw arrow', () => {
//                 service.isRangeBusted = false;
//                 service.selectedCase = {
//                     line: 1,
//                     column: 1,
//                 };
//                 service.arrowDirection = Direction.Vertical;

//                 const expectedCase = { line: 2, column: 1 };

//                 service['incrementSelectedCase']();
//                 expect(boardViewSpy.drawArrow).toHaveBeenCalledWith(expectedCase, Direction.Vertical);
//             });

//             it('should go right until you find ? and draw arrow', () => {
//                 service.isRangeBusted = false;
//                 service.selectedCase = {
//                     line: 1,
//                     column: 1,
//                 };
//                 service.arrowDirection = Direction.Horizontal;

//                 const expectedCase = { line: 1, column: 2 };

//                 service['incrementSelectedCase']();
//                 expect(boardViewSpy.drawArrow).toHaveBeenCalledWith(expectedCase, Direction.Horizontal);
//             });
//             it('should bust range if the selected case is at the max column', () => {
//                 service.isRangeBusted = false;
//                 service.selectedCase = {
//                     line: 1,
//                     column: 14,
//                 };
//                 service.arrowDirection = Direction.Horizontal;

//                 service['incrementSelectedCase']();
//                 expect(service.isRangeBusted).toBeTruthy();
//             });
//         });

//         describe('decrementSelectedCase', () => {
//             it('should do nothing if selected case is undefined', () => {
//                 service.selectedCase = undefined;
//                 service['decrementSelectedCase']();
//                 expect(boardViewSpy.drawArrow).not.toHaveBeenCalled();
//             });

//             it('should do nothing if Horizontal and too low', () => {
//                 service.arrowDirection = Direction.Horizontal;
//                 service.selectedCase = {
//                     line: 1,
//                     column: 0,
//                 };

//                 service['decrementSelectedCase']();
//                 expect(service.selectedCase.line).toEqual(1);
//                 expect(service.selectedCase.column).toEqual(0);
//             });

//             it('should go up until you find ? and draw arrow', () => {
//                 service.selectedCase = {
//                     line: 2,
//                     column: 1,
//                 };
//                 service.arrowDirection = Direction.Vertical;

//                 const expectedCase = { line: 1, column: 1 };

//                 service['decrementSelectedCase']();
//                 expect(boardViewSpy.drawArrow).toHaveBeenCalledWith(expectedCase, Direction.Vertical);
//             });

//             it('should go left until you find ? and draw arrow', () => {
//                 service.selectedCase = {
//                     line: 1,
//                     column: 2,
//                 };
//                 service.arrowDirection = Direction.Horizontal;

//                 const expectedCase = { line: 1, column: 1 };

//                 service['decrementSelectedCase']();
//                 expect(boardViewSpy.drawArrow).toHaveBeenCalledWith(expectedCase, Direction.Horizontal);
//             });

//             it('should set isRangeBusted to false when direction is Vertical and rangeBusted is true', () => {
//                 service.selectedCase = {
//                     line: 1,
//                     column: 2,
//                 };
//                 service.arrowDirection = Direction.Vertical;
//                 service.isRangeBusted = true;

//                 service['decrementSelectedCase']();
//                 expect(service.isRangeBusted).toBeFalsy();
//             });

//             it('should set isRangeBusted to false when direction is Horizontal and rangeBusted is true', () => {
//                 service.selectedCase = {
//                     line: 1,
//                     column: 2,
//                 };
//                 service.arrowDirection = Direction.Horizontal;
//                 service.isRangeBusted = true;

//                 service['decrementSelectedCase']();
//                 expect(service.isRangeBusted).toBeFalsy();
//             });
//         });

//         describe('verifyEqualCase', () => {
//             it('should return false if a case is undefined', () => {
//                 expect(service['verifyEqualCases'](undefined, undefined)).toBeFalsy();
//             });

//             it('should return true if cases are equal', () => {
//                 const mockCase = { line: 1, column: 1 };
//                 expect(service['verifyEqualCases'](mockCase, mockCase)).toBeTruthy();
//             });
//         });

//         it('validateKeyIsLetter should return false since key length is greater than 1', () => {
//             expect(service['validateKeyIsLetter']('abc')).toBeFalsy();
//         });

//         it('turnSocketOn should return since no players added', () => {
//             gameSpy.players = [];
//             socketSpy.on.calls.argsFor(0)[1]();
//             const spy = spyOn(service, 'resetBoardEventHandler');
//             expect(spy).not.toHaveBeenCalled();
//         });

//         it('turnSocketOn should return since no players added', () => {
//             gameSpy.players = [];
//             socketSpy.on.calls.argsFor(0)[1]();
//             const spy = spyOn(service, 'resetBoardEventHandler');
//             expect(spy).not.toHaveBeenCalled();
//         });
//     });

//     // it('should not validate player has letters onKeyDown', () => {
//     //     const event = new KeyboardEvent('keypress', {
//     //         key: 'a',
//     //     });
//     //     service.selectedCase = { line: 0, column: 0 };
//     //     gameSpy.validateIsOurTurn.and.returnValue(true);
//     //     const spy = spyOn<any>(service, 'incrementSelectedCase');
//     //     service.onKeyDown(event);
//     //     expect(gameSpy.validatePlayerHasLetters).toHaveBeenCalled();
//     //     expect(spy).not.toHaveBeenCalled();
//     // });

//     // it('resetBoardEventHandler should return since player is not defined', () => {
//     //     gameSpy.currentlyPlayingIndex = 0;
//     //     gameSpy.players = [];
//     //     service.resetBoardEventHandler();
//     //     expect(gameSpy.resetRackViewToModel).not.toHaveBeenCalled();
//     // });
// });
