import { Injectable } from '@angular/core';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { DictionaryName } from '@app/types/dictionary';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class DictionaryProxyService {
    @ReflectProp('GlobalDictionaryService') dictionaryList: DictionaryName[] = [];

    constructor(private socket: Socket) {
        this.socket.on('propUpdate_GlobalDictionaryService_dictionaryList', (dictionaries: DictionaryName[]) => {
            this.dictionaryList = dictionaries;
        });
    }

    @CallServer() async getDictionaryList() {
        return false;
    }
}
