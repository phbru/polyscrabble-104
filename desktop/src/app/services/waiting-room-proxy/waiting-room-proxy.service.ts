/* eslint-disable no-unused-vars -- needed cause we are using @CallServer : calling method on server. */
/* eslint-disable @typescript-eslint/no-empty-function -- needed cause we are using @CallServer : calling method on server. */
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { User } from '@app/classes/user';
import { CanceledGameDialogComponent } from '@app/components/dialogs/canceled-game-dialog/canceled-game-dialog.component';
import { JoinGameDialogComponent } from '@app/components/dialogs/join-game-dialog/join-game-dialog.component';
import { KickedOutDialogComponent } from '@app/components/dialogs/kicked-out-dialog/kicked-out-dialog.component';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { GameParticipantType } from '@app/enums/player-type';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { GlobalGameListProxyService } from '@app/services/global-game-list-proxy/global-game-list-proxy.service';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { GameInfo } from '@app/types/game-info';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class WaitingRoomServiceProxy {
    @ReflectProp('WaitingRoomService') gameInfo: GameInfo = {
        id: '',
        creatorName: '',
        time: 0,
        dictionary: '',
        openSpots: 0,
        password: '',
        players: [],
        observers: [],
        virtualPlayers: [],
        invitedUsers: [],
        isGameStarted: false,
        isPrivate: false,
    };
    waitingToJoin: User[] = [];
    notificationOn: boolean = false;
    isSelfOnPlayerSpot: boolean = false;
    isSelfOnObserverSpot: boolean = false;
    isSelfGameCreator: boolean = false;

    // gameService must remain here even if not used, because the function to change page is in gameServiceProxy
    constructor(
        private readonly selfUserService: SelfUserServiceProxy,
        private dialog: MatDialog,
        private socket: Socket,
        private router: Router,
        private snackBarService: SnackBarService,
        public gameService: GameServiceProxy,
        private gameListService: GlobalGameListProxyService,
        private translateService: TranslateService,
    ) {
        socket.on('updateGameToJoin', (game: GameInfo) => {
            this.gameInfo = game;
        });
        socket.on('kickUserOut', (userIdToKickout: string) => {
            if (userIdToKickout === this.selfUserService.user.id) {
                this.leaveGame();
                this.dialog.open(KickedOutDialogComponent, { backdropClass: 'backdrop' });
            }
        });

        socket.on('canceledGame', () => {
            if (!this.isSelfGameCreator) {
                this.dialog.open(CanceledGameDialogComponent, { backdropClass: 'backdrop' });
            }
            this.leaveGame();
        });

        socket.on('declineInvite', (invitedUser: string) => {
            if (this.isSelfGameCreator) this.removeInvitedUser(invitedUser);
        });

        socket.on('informCreatorJoin', (joinerUser: User) => {
            if (this.gameInfo.creatorName === selfUserService.user.username) {
                this.notificationOn = true;
                this.waitingToJoin.push(joinerUser);
            }
        });

        socket.on('informCreatorCancel', (joinerUser: User) => {
            if (this.gameInfo.creatorName === selfUserService.user.username) this.removeUserToJoin(joinerUser.id);
        });

        socket.on('acceptJoin', async (invitedUserId: string, roomName: string) => {
            if (invitedUserId === selfUserService.user.id) {
                this.dialog.closeAll();
                this.dialog.open(JoinGameDialogComponent, { data: this.gameListService.getGameInfo(roomName), backdropClass: 'backdrop' });
            }
        });

        socket.on('declineJoin', async (invitedUserId: string) => {
            if (invitedUserId === selfUserService.user.id) {
                this.dialog.closeAll();
                this.snackBarService.open(this.translateService.instant('waitingRoom.joinGame.declineRequest'), 'OK');
            }
        });
    }

    @CallServer() async launchGame() {}
    @CallServer() async initiateWaitingRoom(_gameInfo: GameInfo, _creatorId: string) {}
    @CallServer() async joinAsPlayer() {}
    @CallServer() async joinAsObserver() {}
    @CallServer() async addVirtualPlayer(): Promise<void> {}
    @CallServer() async removeVirtualPlayer(_botName: string): Promise<void> {}
    @CallServer() async addInvitedUser(_userId: string): Promise<void> {}
    @CallServer() async removeInvitedUser(_userId: string): Promise<void> {}
    @CallServer() async addSpot(): Promise<void> {}
    @CallServer() async removeSpot(): Promise<void> {}
    @CallServer() async removeUser(_userIdToKickout: string): Promise<void> {}
    @CallServer() async isGameFull(): Promise<boolean> {
        return true;
    }

    async createWaitingRoom(gameInfo: GameInfo, creator: User) {
        this.resetParameters();
        this.gameService.resetLocalParameters();
        await this.initiateWaitingRoom(gameInfo, creator.id);
        this.isSelfOnPlayerSpot = true;
        this.isSelfGameCreator = true;
        this.router.navigate(['/waitingRoom']);
    }
    async joinGame(game: GameInfo, playerType: GameParticipantType): Promise<void> {
        this.resetParameters();
        this.gameService.resetLocalParameters();

        let gameFound = false;
        for (const updatedGame of this.gameListService.games) {
            // Need to do this to handle case where an invitee accepts after the gameInfo (openSpots) changes
            if (game.id === updatedGame.id) {
                this.gameInfo = updatedGame;
                gameFound = true;
                break;
            }
        }

        if (!gameFound) {
            return;
        }

        switch (playerType) {
            case GameParticipantType.RegularPlayer: {
                if (this.gameInfo.openSpots <= 0 && !this.checkIsInvited(this.selfUserService.user.id)) {
                    this.snackBarService.open(this.translateService.instant('mainPage.joinableGames.gameFull'), 'OK');
                    return;
                }

                await this.socket.emit('joinGame', this.selfUserService.user.username, this.gameInfo.id);
                await this.joinAsPlayer();
                this.isSelfOnPlayerSpot = true;
                this.isSelfOnObserverSpot = false;
                this.router.navigate(['/waitingRoom']);

                break;
            }
            case GameParticipantType.Observer: {
                this.socket.emit('joinGame', this.selfUserService.user.username, this.gameInfo.id);
                this.joinAsObserver();
                this.isSelfOnObserverSpot = true;
                this.isSelfOnPlayerSpot = false;
                this.router.navigate(['/waitingRoom']);
                break;
            }
        }
    }

    checkIsUserInGame(userId: string): boolean {
        let isUserExists = false;
        this.gameInfo.players.forEach((value) => {
            if (value.id === userId) {
                isUserExists = true;
            }
        });
        this.gameInfo.observers.forEach((value) => {
            if (value.id === userId) {
                isUserExists = true;
            }
        });
        return isUserExists;
    }

    takeBotSpot(): void {
        if (this.gameInfo.virtualPlayers.length === 0) return;
        this.removeVirtualPlayer(this.gameInfo.virtualPlayers[0].username);
        this.isSelfOnPlayerSpot = true;
        this.isSelfOnObserverSpot = false;
        this.joinAsPlayer();
    }

    takePlayerSpot(): void {
        this.isSelfOnPlayerSpot = true;
        this.isSelfOnObserverSpot = false;
        this.joinAsPlayer();
    }

    takeObserverSpot(): void {
        this.isSelfOnObserverSpot = true;
        this.isSelfOnPlayerSpot = false;
        this.joinAsObserver();
    }

    resetParameters(): void {
        this.isSelfGameCreator = false;
        this.isSelfOnPlayerSpot = false;
        this.isSelfOnObserverSpot = false;
    }

    leaveGame(): void {
        this.removeUser(this.selfUserService.user.id);
        this.socket.emit('leaveGame', this.selfUserService.user);
        this.resetParameters();
        this.router.navigate(['/home']);
    }

    cancelGame(): void {
        this.resetParameters(); // TODO : MOBILE
        this.socket.emit('cancelGame');
        this.router.navigate(['/home']);
    }

    kickUserOut(userIdToKickout: string): void {
        this.removeUser(userIdToKickout); // Removes player from gameInfo
        this.socket.emit('kickUserOut', userIdToKickout); // Kicks the user on it's client machine
    }

    declineInvite(userIdToKickout: string, gameId: string): void {
        this.socket.emit('declineInvite', userIdToKickout, gameId);
    }

    inviteUser(userId: string): void {
        this.socket.emit('inviteUser', userId);
    }

    removeUserToJoin(userId: string): void {
        for (let i = 0; i < this.waitingToJoin.length; i++) {
            if (userId === this.waitingToJoin[i].id) {
                this.waitingToJoin.splice(i, 1);
                return;
            }
        }
    }

    checkIsInvited(userId: string): boolean {
        for (const invitedUser of this.gameInfo.invitedUsers) {
            if (invitedUser.id === userId) return true;
        }
        return false;
    }
}
