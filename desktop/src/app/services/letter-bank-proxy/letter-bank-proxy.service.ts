// TODO Remove?

/* eslint-disable no-unused-vars -- needed for @CallServer() : calling method on server */
/* eslint-disable @typescript-eslint/no-empty-function -- needed for @CallServer() : calling method on server */

import { Injectable } from '@angular/core';
import letterData from '@app/data/letter-data.json';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { Letter } from '@app/types/letter';

@Injectable({
    providedIn: 'root',
})
export class LetterBankServiceProxy {
    @ReflectProp('LetterBankService') letterBank: Letter[] = [];

    // TODO : move to boardEventHandler
    // Converts char to Letter
    static getLetterFromCharacter(character: string): Letter {
        const matchRegex = new RegExp(character, 'i'); // case insensitive

        for (const letterEntry of letterData.letters) {
            if (letterEntry.character.match(matchRegex) !== null) {
                const isBlankLetter = character === character.toUpperCase() ? true : false;
                const letter: Letter = {
                    character: letterEntry.character,
                    weight: letterEntry.weight,
                    isBlankLetter,
                };
                if (isBlankLetter) {
                    letter.weight = 0;
                }

                return letter;
            }
        }
        return { character: '', weight: 0, isBlankLetter: false };
    }

    @CallServer() getInitialLetterBank() {}
}
