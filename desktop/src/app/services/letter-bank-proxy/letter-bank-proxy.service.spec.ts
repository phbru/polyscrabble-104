// import { TestBed } from '@angular/core/testing';
// import letterData from '@app/data/letter-data.json';
// import { LetterBankServiceProxy } from './letter-bank-proxy.service';

// const mockLetters = [
//     { character: 'A', weight: 2, amount: 1 },
//     { character: 'B', weight: 3, amount: 1 },
//     { character: 'C', weight: 4, amount: 1 },
//     { character: '*', weight: 4, amount: 2 },
// ];

// describe('Static LetterBankServiceProxy', () => {
//     beforeAll(() => {
//         // Replace JSON with mock data
//         // https://stackoverflow.com/questions/59945197/karma-property-does-not-have-access-type-get
//         letterData.letters = mockLetters;
//     });

//     it('should get letter from character', () => {
//         expect(LetterBankServiceProxy.getLetterFromCharacter('a')).toEqual({ character: 'A', weight: 2, isBlankLetter: false });
//     });

//     it('should get blank letter from upper-case character', () => {
//         expect(LetterBankServiceProxy.getLetterFromCharacter('A')).toEqual({ character: 'A', weight: 0, isBlankLetter: true });
//     });

//     it('should return empty character if not found', () => {
//         expect(LetterBankServiceProxy.getLetterFromCharacter('w')).toEqual({ character: '', weight: 0, isBlankLetter: false });
//     });
// });

// describe('LetterBankServiceProxy', () => {
//     let service: LetterBankServiceProxy;

//     beforeAll(() => {
//         // Replace JSON with mock data
//         letterData.letters = mockLetters;
//     });

//     beforeEach(() => {
//         TestBed.configureTestingModule({});
//         service = TestBed.inject(LetterBankServiceProxy);
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });
// });
