/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-magic-numbers -- needed for the view positions which are hard coded, thus needing lots of magic numbers. */
import { Injectable } from '@angular/core';
import { Vec2 } from '@app/classes/vec2';
import * as constant from '@app/constants/board-view';
import { Bonus } from '@app/enums/bonus';
import { Direction } from '@app/enums/direction';
import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
import { ColorThemeService } from '@app/services/color-theme/color-theme.service';
import { CaseParameters } from '@app/types/case-parameters';
import { Letter } from '@app/types/letter';
import { MatrixCoordinates } from '@app/types/matrix-coordinates';
// NOTE :
// This code contains magic numbers for certain percentages of widths.
// Those proportions have been determined visually and hence there is no logical way to obtain them in code
// Also, the code looks cleaner and more readable this way then by using constants with names like :
// PROPORTION_OF_SPACER_WIDTH_FOR_COORDINATE_CHAR_SIZE = 0.4
// this.coordinateCharSize = PROPORTION_OF_SPACER_WIDTH_FOR_COORDINATE_CHAR_SIZE * this.spacerWidth;

@Injectable({
    providedIn: 'root',
})
export class BoardViewService {
    gridContext: CanvasRenderingContext2D;

    boardStarterPosition: number;
    canvasWidth: number;
    spacerWidth: number;

    tokenWidth: number;
    coordinateCharSize: number;

    charProportion = constant.DEFAULT_CHAR;

    weightProportion = constant.DEFAULT_WEIGHT;
    constructor(public boardModel: BoardModelServiceProxy, private colorThemeService: ColorThemeService) {}

    initializeGrid(canvasWidth: number) {
        this.canvasWidth = canvasWidth;
        this.spacerWidth = Math.floor(this.canvasWidth / constant.AMOUNT_OF_LINES);
        this.boardStarterPosition = Math.floor(0.75 * this.spacerWidth);
        this.tokenWidth = 0.92 * this.spacerWidth;
        this.coordinateCharSize = 0.4 * this.spacerWidth;
    }

    drawInitialBoard() {
        const rectangleTotalSize = this.spacerWidth * constant.AMOUNT_OF_COLUMNS + this.boardStarterPosition;
        this.gridContext.clearRect(0, 0, rectangleTotalSize, rectangleTotalSize);
        this.drawBackgroundGrid();
        this.drawColumnsNumber();
        this.drawLineLetters();
        this.drawBonusSquares();
    }

    convertPositionToCoord(position: number) {
        return Math.floor((position - this.boardStarterPosition) / this.spacerWidth);
    }

    placeToken(letter: Letter, coords: MatrixCoordinates) {
        const position: Vec2 = { x: 0, y: 0 };
        position.x = this.convertCoordToPosition(coords.column);
        position.y = this.convertCoordToPosition(coords.line);
        const charSize = this.charProportion * this.spacerWidth;
        this.drawTokenBackground(position);
        this.drawCharacter(letter.character, position, charSize);
        this.drawTokenWeight(letter.weight, position);
    }

    createTemporaryCanvas(event: MouseEvent, letter: Letter) {
        this.deleteTemporaryCanvas();
        const canvas = document.createElement('canvas');
        canvas.id = 'tempCanvas';
        canvas.width = 50;
        canvas.height = 50;
        canvas.style.left = event.clientX - this.tokenWidth / 2 + 'px';
        canvas.style.top = event.clientY - this.tokenWidth / 2 + 'px';
        canvas.style.position = 'absolute';
        canvas.style.zIndex = '999';
        canvas.style.pointerEvents = 'none';
        const body = document.getElementsByTagName('body')[0];
        body.appendChild(canvas);

        const ctx = canvas.getContext('2d');

        // background
        const tokenCentralOffset = (this.spacerWidth - this.tokenWidth) / 2;
        const tokenYOffset = 0.05 * this.spacerWidth;
        ctx!.fillStyle = 'rgb(117, 97, 63)';
        ctx!.fillRect(tokenCentralOffset, tokenYOffset, this.tokenWidth, this.tokenWidth);

        const tokenInteriorWidth = 0.9 * this.tokenWidth;
        const tokenInteriorXOffset = (this.spacerWidth - tokenInteriorWidth) / 2;
        const tokenInteriorYOffset = 1.1 * tokenYOffset;
        ctx!.fillStyle = 'rgb(235, 218, 171)';
        ctx!.fillRect(tokenInteriorXOffset, tokenInteriorYOffset, tokenInteriorWidth, tokenInteriorWidth);

        // character
        const charSize = this.charProportion * this.spacerWidth;
        ctx!.fillStyle = 'rgb(5, 5, 5)';
        const charXCentralOffset = (this.spacerWidth - charSize) / 2;
        const charYCentralOffset = 0.7 * this.spacerWidth;
        ctx!.font = `${charSize}px system-ui`;
        ctx!.fillText(letter.character, charXCentralOffset, charYCentralOffset);

        // weight
        const weightSize = this.weightProportion * this.spacerWidth;
        ctx!.font = `${weightSize}px system-ui`;
        const weightXOffset = 0.85 * this.spacerWidth;
        const weightYOffset = 0.8 * this.spacerWidth;
        ctx!.textAlign = 'right';
        ctx!.fillText(letter.weight.toString(), weightXOffset, weightYOffset);
        ctx!.textAlign = 'left';
    }

    drawArrow(coords: MatrixCoordinates, direction: Direction) {
        const arrowSize = 0.4 * this.spacerWidth;
        this.gridContext.fillStyle = 'red';
        this.gridContext.font = `${arrowSize}px system-ui `;
        const position: Vec2 = { x: this.convertCoordToPosition(coords.column), y: this.convertCoordToPosition(coords.line) };
        const weightXOffset = 0.325 * this.spacerWidth;
        const weightYOffset = 0.93 * this.spacerWidth;
        const arrow = direction === Direction.Horizontal ? '▶' : '▼';
        this.gridContext.fillText(arrow, position.x + weightXOffset, position.y + weightYOffset);
    }

    drawStartingCase(coords: MatrixCoordinates) {
        const PROPORTION_OF_SPACER_WIDTH = 0.95;
        const xValue = this.convertCoordToPosition(coords.column);
        const yValue = this.convertCoordUnitAboveToPosition(coords.line);
        const position: Vec2 = { x: xValue, y: yValue };
        this.gridContext.fillStyle = 'rgb(238,130,238)';
        const xOffset = (this.spacerWidth - PROPORTION_OF_SPACER_WIDTH * this.spacerWidth) / 2;
        const yOffset = this.spacerWidth + (this.spacerWidth - PROPORTION_OF_SPACER_WIDTH * this.spacerWidth) / 2;
        this.gridContext.fillRect(position.x + xOffset, position.y + yOffset, this.spacerWidth * 0.95, this.spacerWidth * 0.95);

        const bonusFontSize = 0.65 * this.spacerWidth;
        this.gridContext.font = `${bonusFontSize}px system-ui`;
        this.gridContext.fillStyle = 'black';

        const xNumberOffset = 0.055 * this.spacerWidth;
        const yNumberOffset = 0.7 * this.spacerWidth;

        this.gridContext.fillText(
            '✏️',
            this.convertCoordToPosition(coords.column) + xNumberOffset,
            this.convertCoordToPosition(coords.line) + yNumberOffset,
        );
    }

    drawBoardContent() {
        this.drawInitialBoard(); // Draws empty board
        for (let i = 0; i < this.boardModel.viewBoardMatrix.length; i++) {
            for (let j = 0; j < this.boardModel.viewBoardMatrix[i].length; j++) {
                if (this.boardModel.viewBoardMatrix[j][i].letter.character !== '?') {
                    {
                        this.placeToken(this.boardModel.viewBoardMatrix[j][i].letter, { line: j, column: i });
                    }
                }
            }
        }
    }

    decreaseTokenFontsize() {
        if (this.charProportion >= constant.MIN_CHAR_PROPORTION && this.weightProportion >= constant.MIN_WEIGHT_PROPORTION) {
            this.charProportion -= constant.CHAR_FONT_INCREASER;
            this.weightProportion -= constant.WEIGHT_FONT_INCREASER;
            this.drawBoardContent();
        }
    }

    increaseTokenFontsize() {
        if (this.charProportion <= constant.MAX_CHAR_PROPORTION && this.weightProportion <= constant.MAX_WEIGHT_PROPORTION) {
            this.charProportion += constant.CHAR_FONT_INCREASER;
            this.weightProportion += constant.WEIGHT_FONT_INCREASER;
            this.drawBoardContent();
        }
    }

    convertCoordToPosition(coord: number) {
        return coord * this.spacerWidth + this.boardStarterPosition;
    }

    // This function is for the various cases where a coord is happens to be one unit over what it represents
    convertCoordUnitAboveToPosition(coord: number) {
        return (coord - 1) * this.spacerWidth + this.boardStarterPosition;
    }

    drawCharacter(character: string, position: Vec2, charSize: number) {
        this.gridContext.fillStyle = 'rgb(5, 5, 5)';
        const charXCentralOffset = (this.spacerWidth - charSize) / 2;
        const charYCentralOffset = 0.7 * this.spacerWidth;
        this.gridContext.font = `${charSize}px system-ui`;
        this.gridContext.fillText(character, position.x + charXCentralOffset, position.y + charYCentralOffset);
    }

    deleteTemporaryCanvas(): void {
        const previousCanvas = document.getElementById('tempCanvas');
        if (previousCanvas) previousCanvas?.parentNode?.removeChild(previousCanvas);
    }

    private drawBackgroundGrid() {
        this.gridContext.beginPath();
        this.gridContext.strokeStyle = this.colorThemeService.selectedTheme === 'dark' ? 'darkgrey' : 'black';
        this.gridContext.lineWidth = 2;

        for (let i = 0; i < constant.AMOUNT_OF_LINES; i++) {
            this.gridContext.moveTo(this.boardStarterPosition, this.convertCoordToPosition(i));
            this.gridContext.lineTo(this.convertCoordUnitAboveToPosition(constant.AMOUNT_OF_LINES), this.convertCoordToPosition(i));
        }

        for (let i = 0; i < constant.AMOUNT_OF_COLUMNS; i++) {
            this.gridContext.moveTo(this.convertCoordToPosition(i), this.boardStarterPosition);
            this.gridContext.lineTo(this.convertCoordToPosition(i), this.convertCoordUnitAboveToPosition(constant.AMOUNT_OF_COLUMNS));
        }
        this.gridContext.stroke();
    }

    private drawColumnsNumber() {
        for (let i = 1; i < constant.AMOUNT_OF_COLUMNS; i++) {
            const character = i.toString();
            const positionX = this.convertCoordUnitAboveToPosition(i);
            const positionY = 0;

            this.gridContext.fillStyle = this.colorThemeService.selectedTheme === 'dark' ? 'white' : 'black';
            const charXCentralOffset = (this.spacerWidth - this.coordinateCharSize) / 1.85;
            const charYCentralOffset = 0.5 * this.spacerWidth;
            this.gridContext.font = `${this.coordinateCharSize}px system-ui`;
            this.gridContext.fillText(character, positionX + charXCentralOffset, positionY + charYCentralOffset);
        }
    }

    private drawLineLetters() {
        for (let i = 0; i < constant.AMOUNT_OF_LINES - 1; i++) {
            const character = String.fromCharCode(65 + i); // 65 is ascii for A
            const positionX = 0;
            const positionY = this.convertCoordToPosition(i);

            this.gridContext.fillStyle = this.colorThemeService.selectedTheme === 'dark' ? 'white' : 'black';
            const charXCentralOffset = 0.25 * this.spacerWidth;
            const charYCentralOffset = this.spacerWidth - (this.spacerWidth - this.coordinateCharSize) / 2;
            this.gridContext.font = `${this.coordinateCharSize}px system-ui`;
            this.gridContext.fillText(character, positionX + charXCentralOffset, positionY + charYCentralOffset);
        }
    }

    private drawBonusSquares() {
        for (let i = 0; i < this.boardModel.viewBoardMatrix.length; i++) {
            for (let j = 0; j < this.boardModel.viewBoardMatrix[i].length; j++) {
                const caseParameter = this.determineCaseParameter(i, j);
                this.drawSquareBackground(j, i, caseParameter.color);
                this.drawBonusName(j, i, caseParameter.bonusInfo);
            }
        }
        this.drawCentralCase();
    }

    private drawCentralCase() {
        const CENTRAL_COORD = 7;
        const xLetterOffset = 0.085 * this.spacerWidth;
        const yLetterOffset = 0.825 * this.spacerWidth;
        const bonusFontSize = this.spacerWidth;

        this.gridContext.font = `${bonusFontSize}px system-ui`;
        this.gridContext.fillText(
            '★',
            this.convertCoordToPosition(CENTRAL_COORD) + xLetterOffset,
            this.convertCoordToPosition(CENTRAL_COORD) + yLetterOffset,
        );
    }

    private drawTokenBackground(position: Vec2) {
        const tokenCentralOffset = (this.spacerWidth - this.tokenWidth) / 2;
        const tokenYOffset = 0.05 * this.spacerWidth;
        this.gridContext.fillStyle = 'rgb(117, 97, 63)';
        this.gridContext.fillRect(position.x + tokenCentralOffset, position.y + tokenYOffset, this.tokenWidth, this.tokenWidth);

        const tokenInteriorWidth = 0.9 * this.tokenWidth;
        const tokenInteriorXOffset = (this.spacerWidth - tokenInteriorWidth) / 2;
        const tokenInteriorYOffset = 1.1 * tokenYOffset;
        this.gridContext.fillStyle = 'rgb(235, 218, 171)';
        this.gridContext.fillRect(position.x + tokenInteriorXOffset, position.y + tokenInteriorYOffset, tokenInteriorWidth, tokenInteriorWidth);
    }

    private determineCaseParameter(xCoordinate: number, yCoordinate: number): CaseParameters {
        if (this.boardModel.viewBoardMatrix[xCoordinate][yCoordinate].bonus === Bonus.LetterX2) {
            return { bonusInfo: { word: 'LETTRE', bonusNumber: 'x2' }, color: 'rgb(120, 190, 207)' };
        }
        if (this.boardModel.viewBoardMatrix[xCoordinate][yCoordinate].bonus === Bonus.LetterX3) {
            return { bonusInfo: { word: 'LETTRE', bonusNumber: 'x3' }, color: 'rgb(101, 194, 149)' };
        }
        if (this.boardModel.viewBoardMatrix[xCoordinate][yCoordinate].bonus === Bonus.WordX2) {
            return { bonusInfo: { word: 'MOT', bonusNumber: 'x2' }, color: 'rgb(231, 157, 167)' };
        }
        if (this.boardModel.viewBoardMatrix[xCoordinate][yCoordinate].bonus === Bonus.WordX3) {
            return { bonusInfo: { word: 'MOT', bonusNumber: 'x3' }, color: 'rgb(199, 118, 91)' };
        }
        if (this.boardModel.viewBoardMatrix[xCoordinate][yCoordinate].bonus === Bonus.Star) {
            return { bonusInfo: { word: '', bonusNumber: '' }, color: 'rgb(231, 157, 167)' };
        }
        if (this.colorThemeService.selectedTheme === 'dark') return { bonusInfo: { word: '', bonusNumber: '' }, color: '#292929' };
        return { bonusInfo: { word: '', bonusNumber: '' }, color: '#eceaeb' };
    }

    private drawSquareBackground(line: number, column: number, color: string) {
        const PROPORTION_OF_SPACER_WIDTH = 0.95;
        const xValue = this.convertCoordToPosition(line);
        const yValue = this.convertCoordUnitAboveToPosition(column);
        const position: Vec2 = { x: xValue, y: yValue };
        this.gridContext.fillStyle = color;
        const xOffset = (this.spacerWidth - PROPORTION_OF_SPACER_WIDTH * this.spacerWidth) / 2;
        const yOffset = this.spacerWidth + (this.spacerWidth - PROPORTION_OF_SPACER_WIDTH * this.spacerWidth) / 2;
        this.gridContext.fillRect(position.x + xOffset, position.y + yOffset, this.spacerWidth * 0.95, this.spacerWidth * 0.95);
    }

    private drawBonusName(xCoordinate: number, yCoordinate: number, bonusName: { word: string; bonusNumber: string }) {
        const bonusFontSize = 0.27 * this.spacerWidth;
        this.gridContext.font = `${bonusFontSize}px system-ui`;
        this.gridContext.fillStyle = 'black';
        let xOffsetProportion = 1;
        if (bonusName.word === 'LETTRE') {
            xOffsetProportion = 0.05;
        } else {
            xOffsetProportion = 0.18;
        }

        const xLetterOffset = xOffsetProportion * this.spacerWidth;
        const yLetterOffset = 0.5 * this.spacerWidth;
        this.gridContext.fillText(
            bonusName.word,
            this.convertCoordToPosition(xCoordinate) + xLetterOffset,
            this.convertCoordToPosition(yCoordinate) + yLetterOffset,
        );

        const xNumberOffset = 0.35 * this.spacerWidth;
        const yNumberOffset = 0.8 * this.spacerWidth;

        this.gridContext.fillText(
            bonusName.bonusNumber,
            this.convertCoordToPosition(xCoordinate) + xNumberOffset,
            this.convertCoordToPosition(yCoordinate) + yNumberOffset,
        );
    }

    private drawTokenWeight(weight: number, position: Vec2) {
        const weightSize = this.weightProportion * this.spacerWidth;
        this.gridContext.font = `${weightSize}px system-ui`;
        const weightXOffset = 0.85 * this.spacerWidth;
        const weightYOffset = 0.8 * this.spacerWidth;
        this.gridContext.textAlign = 'right';
        this.gridContext.fillText(weight.toString(), position.x + weightXOffset, position.y + weightYOffset);
        this.gridContext.textAlign = 'left';
    }
}
