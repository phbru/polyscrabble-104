// /* eslint-disable @typescript-eslint/no-explicit-any -- to make things easier with chai*/
// /* eslint-disable @typescript-eslint/no-magic-numbers -- makes testing easier since some positions are hard-coded*/
// /* eslint-disable dot-notation -- needed to test private methods*/
// import { TestBed } from '@angular/core/testing';
// import { CanvasTestHelper } from '@app/classes/canvas-test-helper';
// import { Bonus } from '@app/enums/bonus';
// import { Direction } from '@app/enums/direction';
// import { BoardViewService } from '@app/services/board-view/board-view';
// import { ServiceLocator } from '@app/services/service-locator';
// import { Case } from '@app/types/case';
// import { Letter } from '@app/types/letter';
// import { Socket } from 'ngx-socket-io';

// describe('BoardViewService', () => {
//     let service: BoardViewService;
//     let ctxStub: CanvasRenderingContext2D;
//     let mockMatrix: Case[][];
//     let socketSpy: jasmine.SpyObj<Socket>;
//     const letter: Letter = {
//         character: '',
//         weight: 0,
//         isBlankLetter: false,
//     };
//     const canvasWidth = 680;
//     const wordX3colorMock = 'rgb(199, 118, 91)';
//     const wordX3NameMock = { word: 'MOT', bonusNumber: 'x3' };
//     const wordX3ParamMock = { bonusInfo: wordX3NameMock, color: wordX3colorMock };

//     beforeEach(() => {
//         socketSpy = jasmine.createSpyObj('Socket', ['emit', 'on']);
//         mockMatrix = [
//             [
//                 { letter: { character: 'A', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//             ],
//         ];
//     });

//     beforeEach(() => {
//         TestBed.configureTestingModule({ providers: [{ provide: Socket, useValue: socketSpy }] });
//         ServiceLocator.injector = { get: TestBed.inject };
//         service = TestBed.inject(BoardViewService);
//         ctxStub = CanvasTestHelper.createCanvas(canvasWidth, canvasWidth).getContext('2d') as CanvasRenderingContext2D;
//         service.gridContext = ctxStub;
//         service.initializeGrid(canvasWidth);
//         letter.character = 'D';
//         letter.weight = 2;
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     it('initializeGrid should initialize multiple values according to canvasWidth', () => {
//         service.initializeGrid(1700);
//         expect(service.canvasWidth).toEqual(1700);
//         expect(service.spacerWidth).toEqual(100);
//         expect(service.boardStarterPosition).toEqual(130);
//         expect(service.tokenWidth).toEqual(92);
//         expect(service.coordinateCharSize).toEqual(40);
//     });

//     it('width should convert an x-y coordinate to the appropriate position', () => {
//         expect(service['convertCoordToPosition'](10)).toEqual(452);
//         expect(service['convertCoordUnitAboveToPosition'](10)).toEqual(412);
//     });

//     it('should place token', () => {
//         const drawTokenSpy = spyOn<any>(service, 'drawTokenBackground');
//         const drawCharacterSpy = spyOn<any>(service, 'drawCharacter');
//         const drawTokenWeight = spyOn<any>(service, 'drawTokenWeight');

//         service.placeToken({ character: 'A', weight: 0, isBlankLetter: false }, { line: 20, column: 20 });
//         expect(drawTokenSpy).toHaveBeenCalled();
//         expect(drawCharacterSpy).toHaveBeenCalled();
//         expect(drawTokenWeight).toHaveBeenCalled();
//     });

//     it('should draw token background', () => {
//         const fillSpy = spyOn(service.gridContext, 'fillRect');
//         service['drawTokenBackground']({ x: 20, y: 20 });
//         expect(fillSpy).toHaveBeenCalled();
//     });

//     describe('decreaseTokenFontsize', () => {
//         const CHAR_FONT_INCREASER = 0.02;
//         const WEIGHT_FONT_INCREASER = 0.005;

//         it('should decrease token font size', () => {
//             const drawContentSpy = spyOn(service, 'drawBoardContent');
//             service.charProportion = 1;
//             service.weightProportion = 1;

//             service.decreaseTokenFontsize();
//             expect(service.charProportion).toEqual(1 - CHAR_FONT_INCREASER);
//             expect(service.weightProportion).toEqual(1 - WEIGHT_FONT_INCREASER);
//             expect(drawContentSpy).toHaveBeenCalled();
//         });

//         it('should not decrease token font size if font too small', () => {
//             const drawContentSpy = spyOn(service, 'drawBoardContent');
//             service.charProportion = 0.01;
//             service.weightProportion = 0.01;

//             service.decreaseTokenFontsize();
//             expect(drawContentSpy).not.toHaveBeenCalled();
//         });

//         it('should increase token font size', () => {
//             const drawContentSpy = spyOn(service, 'drawBoardContent');
//             service.charProportion = 0.1;
//             service.weightProportion = 0.1;

//             service.increaseTokenFontsize();
//             expect(service.charProportion).toEqual(0.1 + CHAR_FONT_INCREASER);
//             expect(service.weightProportion).toEqual(0.1 + WEIGHT_FONT_INCREASER);
//             expect(drawContentSpy).toHaveBeenCalled();
//         });

//         it('should not increase token font size if font too big', () => {
//             const drawContentSpy = spyOn(service, 'drawBoardContent');
//             service.charProportion = 1;
//             service.weightProportion = 1;

//             service.increaseTokenFontsize();
//             expect(drawContentSpy).not.toHaveBeenCalled();
//         });
//     });

//     it('drawInitialBoard should call clearRect', () => {
//         const clearRectSpy = spyOn(service.gridContext, 'clearRect').and.callThrough();
//         service.drawInitialBoard();
//         expect(clearRectSpy).toHaveBeenCalled();
//     });

//     it('drawInitialBoard should call drawBackgroundGrid', () => {
//         const drawBackgroundGridSpy = spyOn<any>(service, 'drawBackgroundGrid');
//         service.drawInitialBoard();
//         expect(drawBackgroundGridSpy).toHaveBeenCalled();
//     });

//     it('drawInitialBoard should call drawColumnsNumber', () => {
//         const drawColumnsNumberSpy = spyOn<any>(service, 'drawColumnsNumber');
//         service.drawInitialBoard();
//         expect(drawColumnsNumberSpy).toHaveBeenCalled();
//     });

//     it('drawInitialBoard should call drawLineLetters', () => {
//         const drawLineLettersSpy = spyOn<any>(service, 'drawLineLetters');
//         service.drawInitialBoard();
//         expect(drawLineLettersSpy).toHaveBeenCalled();
//     });

//     it('drawInitialBoard should call drawBonusSquares', () => {
//         const drawBonusSquaresSpy = spyOn<any>(service, 'drawBonusSquares');
//         service.drawInitialBoard();
//         expect(drawBonusSquaresSpy).toHaveBeenCalled();
//     });

//     it('should draw board content', () => {
//         const initialBoardSpy = spyOn(service, 'drawInitialBoard');
//         const placeTokenSpy = spyOn(service, 'placeToken');

//         service.boardModel.viewBoardMatrix = mockMatrix;

//         service.drawBoardContent();
//         expect(initialBoardSpy).toHaveBeenCalled();
//         expect(placeTokenSpy).toHaveBeenCalled();
//     });

//     it(' drawBackgroundGrid should call moveTo on the canvas', () => {
//         const moveToSpy = spyOn(service.gridContext, 'moveTo').and.callThrough();
//         service['drawBackgroundGrid']();
//         expect(moveToSpy).toHaveBeenCalled();
//     });

//     it(' drawBackgroundGrid should call lineTo on the canvas', () => {
//         const lineToSpy = spyOn(service.gridContext, 'lineTo').and.callThrough();
//         service['drawBackgroundGrid']();
//         expect(lineToSpy).toHaveBeenCalled();
//     });

//     it(' drawBackgroundGrid should call beginPath on the canvas', () => {
//         const beginPathSpy = spyOn(service.gridContext, 'beginPath').and.callThrough();
//         service['drawBackgroundGrid']();
//         expect(beginPathSpy).toHaveBeenCalled();
//     });

//     it(' drawBackgroundGrid should call stroke on the canvas', () => {
//         const strokeSpy = spyOn(service.gridContext, 'stroke').and.callThrough();
//         service['drawBackgroundGrid']();
//         expect(strokeSpy).toHaveBeenCalled();
//     });

//     it('drawBonusSquares should call determineCaseParameter, drawSquareBackground and drawBonusName', () => {
//         const determineCaseParameterSpy = spyOn<any>(service, 'determineCaseParameter').and.returnValue(wordX3ParamMock);
//         const drawSquareBackgroundSpy = spyOn<any>(service, 'drawSquareBackground').and.callThrough();
//         const drawBonusNameSpy = spyOn<any>(service, 'drawBonusName').and.callThrough();
//         service.boardModel.viewBoardMatrix = mockMatrix;
//         service['drawBonusSquares']();
//         service['drawBonusName'](0, 0, { word: 'LETTRE', bonusNumber: 'X2' });
//         expect(drawBonusNameSpy).toHaveBeenCalled();
//         expect(determineCaseParameterSpy).toHaveBeenCalledTimes(9);
//         expect(drawSquareBackgroundSpy).toHaveBeenCalledWith(0, 0, wordX3colorMock);
//         expect(drawBonusNameSpy).toHaveBeenCalledWith(0, 0, wordX3NameMock);
//     });

//     it('determineCaseParameter should return all bonus names and colors', () => {
//         service['boardModel'].viewBoardMatrix = mockMatrix;
//         expect(service['determineCaseParameter'](0, 0)).toEqual({ bonusInfo: { word: '', bonusNumber: '' }, color: '#eceaeb' });
//         expect(service['determineCaseParameter'](0, 1)).toEqual({ bonusInfo: { word: 'LETTRE', bonusNumber: 'x2' }, color: 'rgb(120, 190, 207)' });
//         expect(service['determineCaseParameter'](0, 2)).toEqual({ bonusInfo: { word: 'LETTRE', bonusNumber: 'x3' }, color: 'rgb(101, 194, 149)' });
//         expect(service['determineCaseParameter'](1, 0)).toEqual({ bonusInfo: { word: 'MOT', bonusNumber: 'x2' }, color: 'rgb(231, 157, 167)' });
//         expect(service['determineCaseParameter'](1, 1)).toEqual({ bonusInfo: { word: 'MOT', bonusNumber: 'x3' }, color: 'rgb(199, 118, 91)' });
//         expect(service['determineCaseParameter'](1, 2)).toEqual({ bonusInfo: { word: '', bonusNumber: '' }, color: 'rgb(231, 157, 167)' });
//     });

//     it('drawSquareBackground should call fillRect', () => {
//         const fillRectSpy = spyOn(service.gridContext, 'fillRect').and.callThrough();
//         service['drawSquareBackground'](5, 5, wordX3colorMock);
//         expect(fillRectSpy).toHaveBeenCalled();
//     });

//     it('should draw character', () => {
//         const fillTextSpy = spyOn(service.gridContext, 'fillText');

//         service['drawCharacter']('test', { x: 20, y: 20 }, 3);
//         expect(fillTextSpy).toHaveBeenCalled();
//     });

//     it('should draw token weight', () => {
//         const fillTextSpy = spyOn(service.gridContext, 'fillText');

//         service['drawTokenWeight'](5, { x: 20, y: 20 });
//         expect(fillTextSpy).toHaveBeenCalled();
//     });

//     it('should draw token arrow', () => {
//         const fillTextSpy = spyOn(service.gridContext, 'fillText');
//         service.drawArrow({ line: 0, column: 0 }, Direction.Horizontal);
//         expect(fillTextSpy).toHaveBeenCalled();
//         service.drawArrow({ line: 0, column: 0 }, Direction.Vertical);
//         expect(fillTextSpy).toHaveBeenCalled();
//     });

//     it('should convert', () => {
//         service.initializeGrid(1700);
//         expect(service.convertPositionToCoord(330)).toEqual(2);
//     });
// });
