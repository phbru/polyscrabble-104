/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '@app/classes/user';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class HttpClientService {
    private readonly baseUrl: string = `${environment.serverUrl}/api`;
    // private readonly baseUrl: string = '/api';

    constructor(private readonly http: HttpClient) {}

    register(user: User): Observable<any> {
        return this.http.post<User>(`${this.baseUrl}/auth/signup`, user);
    }

    login(user: User): Observable<any> {
        return this.http.post<User>(`${this.baseUrl}/auth/login`, user);
    }

    updateUsername(user: User, newUsername: string): Observable<any> {
        return this.http.post<any>(`${this.baseUrl}/auth/updateUsername`, { user, newUsername });
    }

    updateAvatar(user: User, avatarUrl: string): Observable<any> {
        return this.http.post<any>(`${this.baseUrl}/auth/updateAvatar`, { user, avatarUrl });
    }

    // private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    //     return () => of(result as T);
    // }
}
