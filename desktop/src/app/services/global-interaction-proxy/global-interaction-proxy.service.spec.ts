import { TestBed } from '@angular/core/testing';

import { GlobalInteractionProxyService } from './global-interaction-proxy.service';

describe('GlobalInteractionProxyService', () => {
    let service: GlobalInteractionProxyService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(GlobalInteractionProxyService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
