/* eslint-disable no-unused-vars -- needed cause we are using @CallServer : calling method on server. */
/* eslint-disable @typescript-eslint/no-empty-function -- needed cause we are using @CallServer : calling method on server. */

import { Injectable } from '@angular/core';
import { CallServer } from '@app/decorators/call-server/call-server';

@Injectable({
    providedIn: 'root',
})
export class GlobalInteractionProxyService {
    @CallServer('GlobalInteractionService') async createFriendRequest(_userId: string): Promise<void> {}
    @CallServer() async acceptInvite(_userId: string): Promise<void> {}
    @CallServer() async removeInvite(_userId: string): Promise<void> {}
    @CallServer() async refuseInvite(_userId: string): Promise<void> {}
    @CallServer() async removeFriend(_userId: string): Promise<void> {}
}
