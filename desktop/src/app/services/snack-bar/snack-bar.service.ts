import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

const SNACK_BAR_DURATION = 3000;

@Injectable({
    providedIn: 'root',
})
export class SnackBarService {
    constructor(private snackBar: MatSnackBar, private zone: NgZone) {}

    open(message: string, action: string, duration = SNACK_BAR_DURATION): void {
        this.zone.run(() => {
            this.snackBar.open(message, action, { duration });
        });
    }
}
