import { Injectable } from '@angular/core';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { ActionMessage } from '@app/types/action-message';
import { Socket } from 'ngx-socket-io';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ActionProxyService {
    @ReflectProp('ActionMessageService') currentAction: ActionMessage;
    currentActionObservable: Subject<ActionMessage> = new Subject<ActionMessage>();

    constructor(private socket: Socket) {
        this.socket.on('propUpdate_ActionMessageService_currentAction', (actionMessage: ActionMessage) => {
            this.currentAction = actionMessage;
            this.currentActionObservable.next(this.currentAction);
        });
    }
}
