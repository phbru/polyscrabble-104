/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable no-unused-vars */
import { Injectable } from '@angular/core';
import { User } from '@app/classes/user';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { GlobalChannelProxyService } from '@app/services/global-channel-proxy/global-channel-proxy.service';
import { ClientChannel } from '@app/types/client-channel';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class ClientChannelProxyService {
    @ReflectProp('ClientChannelService') joinedChannels: ClientChannel[] = [];
    @ReflectProp() globalChannel: ClientChannel = {
        newMessages: false,
        channel: { id: '', name: '', users: [], owner: new User(), searchable: false, directMessage: false, messages: [] },
    };
    @ReflectProp() gameChannel: ClientChannel[] = [];
    currentChannelIndex: number = -1;

    constructor(globalChannelService: GlobalChannelProxyService, socket: Socket) {
        // Always set current channel as read
        const updateChannelRead = () => {
            if (this.current !== undefined) {
                globalChannelService.setChannelAsRead(this.current.channel.id);
            }
        };
        socket.on('propUpdate_ClientChannelService_joinedChannels', updateChannelRead);
        socket.on('propUpdate_ClientChannelService_gameChannel', updateChannelRead);
    }

    // -1 for global channel
    // -2 for game channel
    get current() {
        if (this.currentChannelIndex === -1) {
            return this.globalChannel;
        } else if (this.currentChannelIndex === -2) {
            if (this.gameChannel.length > 0) {
                return this.gameChannel[0];
            }

            // No game channel, reset to global channel
            this.currentChannelIndex = -1;
        }

        if (this.currentChannelIndex < this.joinedChannels.length) {
            return this.joinedChannels[this.currentChannelIndex];
        }

        // If all else failed, return global channel
        return this.globalChannel;
    }

    // Returns index of channel, or -1
    findIndex(channelId: string) {
        for (let i = 0; i < this.joinedChannels.length; ++i) {
            if (this.joinedChannels[i].channel.id === channelId) return i;
        }

        return -1;
    }
}
