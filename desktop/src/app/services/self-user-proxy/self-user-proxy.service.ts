import { Injectable } from '@angular/core';
import { User } from '@app/classes/user';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';

@Injectable({
    providedIn: 'root',
})
export class SelfUserServiceProxy {
    @ReflectProp('SelfUserService') user: User;

    clear() {
        this.user = new User();
    }
}
