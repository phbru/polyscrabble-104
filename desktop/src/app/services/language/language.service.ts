/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root',
})
export class LanguageService {
    selectedLanguage: string = 'fr';
    constructor(private translateService: TranslateService) {
        this.translateService.use(this.selectedLanguage);
    }

    changeLanguage(language: string): void {
        this.selectedLanguage = language;
        this.translateService.use(language);
        (window as any).electronAPI.setLanguage(this.selectedLanguage);
    }
}
