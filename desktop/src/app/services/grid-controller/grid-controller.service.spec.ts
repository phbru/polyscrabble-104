// import { TestBed } from '@angular/core/testing';
// import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
// import { BoardViewService } from '@app/services/board-view/board-view';
// import { Socket } from 'ngx-socket-io';
// import { GridControllerService } from './grid-controller.service';

// describe('GridControllerService', () => {
//     let service: GridControllerService;
//     let boardViewSpy: jasmine.SpyObj<BoardViewService>;
//     let boardModelSpy: jasmine.SpyObj<BoardModelServiceProxy>;
//     let socketSpy: jasmine.SpyObj<Socket>;

//     beforeEach(() => {
//         boardViewSpy = jasmine.createSpyObj('GridService', ['drawBoardContent']);
//         boardModelSpy = jasmine.createSpyObj('BoardModelServiceProxy', ['addWordToView', 'deepCopyBoardMatrix', 'uploadBoardMatrix']);
//         socketSpy = jasmine.createSpyObj('Socket', ['on', 'emit']);
//     });

//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: BoardViewService, useValue: boardViewSpy },
//                 { provide: BoardModelServiceProxy, useValue: boardModelSpy },
//                 { provide: Socket, useValue: socketSpy },
//             ],
//         });
//         service = TestBed.inject(GridControllerService);
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     describe('initDrawingEvents', () => {
//         it('should listen to boardMatrix prop updates', () => {
//             service.initDrawingEvents();
//             expect(socketSpy.on).toHaveBeenCalledWith('propUpdate_BoardModelService_boardMatrix', jasmine.any(Function));
//         });

//         // it('should reset view when boardMatrix is updated', () => {
//         //     const resetSpy = spyOn(service, 'resetView');

//         //     service.initDrawingEvents();
//         //     const callback = socketSpy.on.calls.argsFor(0)[1];
//         //     callback([]);

//         //     expect(resetSpy).toHaveBeenCalled();
//         // });
//     });

//     // it('should add word to view', () => {
//     //     const wordMap = { word: 'test', map: [] };
//     //     service.addWordToView(wordMap);
//     //     expect(boardViewSpy.drawBoardContent).toHaveBeenCalled();
//     //     expect(boardModelSpy.addWordToView).toHaveBeenCalledWith(wordMap);
//     // });

//     it('should reset view board', () => {
//         boardModelSpy.deepCopyBoardMatrix.and.returnValue([]);

//         service.resetView();
//         expect(boardViewSpy.drawBoardContent).toHaveBeenCalled();
//     });

//     it('should save view board', () => {
//         boardModelSpy.viewBoardMatrix = [];
//         boardModelSpy.deepCopyBoardMatrix.and.returnValue([]);

//         service.saveView();
//         expect(boardModelSpy.boardMatrix).toEqual([]);
//         expect(boardModelSpy.uploadBoardMatrix).toHaveBeenCalledWith([]);
//     });
// });
