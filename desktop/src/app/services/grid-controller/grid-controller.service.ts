import { Injectable } from '@angular/core';
import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
import { BoardViewService } from '@app/services/board-view/board-view';
import { GameServiceProxy } from '@app/services/game-proxy/game-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { WordMap } from '@app/types/word-map';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class GridControllerService {
    constructor(
        public boardView: BoardViewService,
        public boardModel: BoardModelServiceProxy,
        private socket: Socket,
        private gameServiceProxy: GameServiceProxy,
        private snackBar: SnackBarService,
        private translateService: TranslateService,
    ) {
        socket.on('addWordToView', (wordMap: WordMap) => {
            this.addWordToView(wordMap);
        });
        socket.on('invalidWord', () => {
            this.snackBar.open(this.translateService.instant('gamePage.forbiddenActions.invalidWord'), 'OK');
        });
        socket.on('resetView', () => {
            this.resetView();
        });
    }

    initDrawingEvents() {
        this.socket.on('propUpdate_BoardModelService_boardMatrix', () => {
            this.resetView();
        });
        this.socket.on('propUpdate_BoardModelService_startingCase', () => {
            this.resetView();
        });
    }

    addWordToView(wordMap: WordMap) {
        this.boardModel.addWordToView(wordMap);
    }

    resetView() {
        // TODO : à quoi sert cette ligne ?
        if (this.gameServiceProxy.validateIsOurTurn()) return;
        this.boardModel.viewBoardMatrix = this.boardModel.deepCopyBoardMatrix(this.boardModel.boardMatrix);
        this.boardView.drawBoardContent();
        if (this.boardModel.startingCase.line >= 0 && this.boardModel.startingCase.column >= 0)
            this.boardView.drawStartingCase(this.boardModel.startingCase);
    }
}
