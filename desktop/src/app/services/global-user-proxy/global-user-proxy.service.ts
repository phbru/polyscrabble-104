/* eslint-disable no-unused-vars */
import { Injectable } from '@angular/core';
import { User } from '@app/classes/user';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';

@Injectable({
    providedIn: 'root',
})
export class GlobalUserProxyService {
    @ReflectProp('GlobalUserService') onlineUsers: User[] = [];

    isUserOnline(userId: string) {
        for (const user of this.onlineUsers) {
            if (user.id === userId) {
                return true;
            }
        }
        return false;
    }
}
