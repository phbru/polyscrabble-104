// /* eslint-disable @typescript-eslint/no-empty-function */
// /* eslint-disable dot-notation -- We explicitly need an empty function*/
// import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
// import { TestBed } from '@angular/core/testing';
// import { Message } from '@app/classes/message';
// import { CommunicationService } from '@app/services/communication/communication.service';

// describe('CommunicationService', () => {
//     let httpMock: HttpTestingController;
//     let service: CommunicationService;
//     let baseUrl: string;

//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             imports: [HttpClientTestingModule],
//         });
//         service = TestBed.inject(CommunicationService);
//         httpMock = TestBed.inject(HttpTestingController);

//         baseUrl = service['baseUrl'];
//     });

//     afterEach(() => {
//         httpMock.verify();
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     it('should return expected message (HttpClient called once)', () => {
//         const expectedMessage: Message = { body: 'Hello', title: 'World' };

//         // check the content of the mocked call
//         service.basicGet().subscribe({
//             next: (response: Message) => {
//                 expect(response.title).toEqual(expectedMessage.title);
//                 expect(response.body).toEqual(expectedMessage.body);
//             },
//             error: fail,
//         });

//         const req = httpMock.expectOne(`${baseUrl}/example`);
//         expect(req.request.method).toBe('GET');
//         // actually send the request
//         req.flush(expectedMessage);
//     });

//     it('should not return any message when sending a POST request (HttpClient called once)', () => {
//         const sentMessage: Message = { body: 'Hello', title: 'World' };
//         // subscribe to the mocked call

//         service.basicPost(sentMessage).subscribe({ next: () => {}, error: fail });
//         const req = httpMock.expectOne(`${baseUrl}/example/send`);
//         expect(req.request.method).toBe('POST');
//         // actually send the request
//         req.flush(sentMessage);
//     });

//     // it('should post new game', () => {
//     //     const game: GameInfo = { creatorName: 'John', roomName: 'testroom1', time: 1 };

//     //     service.addGame(game).subscribe({ next: () => {}, error: fail });
//     // });

//     // it('should get games', () => {
//     //     const expectedGames: GameInfo[] = [
//     //         { creatorName: 'John', roomName: 'testroom1', time: 1 },
//     //         { creatorName: 'Carlito', roomName: 'testroom2', time: 2 },
//     //     ];

//     //     service.getGames().subscribe({
//     //         next: (response: number | GameInfo[]) => {
//     //             expect(response).toEqual(expectedGames);
//     //         },
//     //         error: fail,
//     //     });
//     // });

//     // it('should delete game', () => {
//     //     const roomName = 'Jacob';

//     //     service.deleteGame(roomName).subscribe({ next: () => {}, error: fail });
//     // });
//     it('should handle http error safely', () => {
//         service.basicGet().subscribe({
//             next: (response: Message) => {
//                 expect(response).toBeUndefined();
//             },
//             error: fail,
//         });

//         const req = httpMock.expectOne(`${baseUrl}/example`);
//         expect(req.request.method).toBe('GET');
//         req.error(new ProgressEvent('Random error occurred'));
//     });
// });
