/* eslint-disable no-unused-vars */
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GameInvitationDialogComponent } from '@app/components/dialogs/game-invitation-dialog/game-invitation-dialog.component';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { SelfUserServiceProxy } from '@app/services/self-user-proxy/self-user-proxy.service';
import { SnackBarService } from '@app/services/snack-bar/snack-bar.service';
import { GameInfo } from '@app/types/game-info';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
    providedIn: 'root',
})
export class GlobalGameListProxyService {
    @ReflectProp('GlobalGameListService') games: GameInfo[] = [];

    constructor(
        private matDialog: MatDialog,
        socket: Socket,
        selfUserService: SelfUserServiceProxy,
        snackBarService: SnackBarService,
        translateService: TranslateService,
    ) {
        socket.on('gameInviteReceived', (invitedUserId: string, roomName: string) => {
            if (invitedUserId === selfUserService.user.id) {
                this.matDialog.closeAll();
                this.matDialog.open(GameInvitationDialogComponent, { data: this.getGameInfo(roomName), backdropClass: 'backdrop' });
            }
        });

        socket.on('revokeInvite', (invitedUser: string) => {
            if (selfUserService.user.id === invitedUser) {
                this.matDialog.closeAll();
                snackBarService.open(translateService.instant('waitingRoom.revokedInvite'), 'OK');
            }
        });
    }

    @CallServer()
    async updateGame(_gameInfo: GameInfo) {
        return;
    }

    getRandomGame(): GameInfo {
        return this.games[Math.floor(Math.random() * this.games.length)];
    }

    getGameInfo(gameId: string): GameInfo {
        let tempIndex = -1;
        this.games.forEach((element, index) => {
            if (element.id === gameId) tempIndex = index;
        });
        return this.games[tempIndex];
    }

    isGameExist(gameId: string): boolean {
        for (const game of this.games) {
            if (gameId === game.id) return true;
        }
        return false;
    }
}
