import { Injectable } from '@angular/core';
import { ALL } from '@app/constants/game-filters';
import { Filter } from '@app/types/game-filters';

@Injectable({
    providedIn: 'root',
})
export class GameFiltersService {
    appliedGameFilters: Filter[] = [];

    removeFilter(translatedLabel: string) {
        this.appliedGameFilters.forEach((value, index) => {
            if (value.translatedLabel === translatedLabel) {
                this.appliedGameFilters.splice(index, 1);
                return;
            }
        });
    }

    getFilter(translatedLabel: string): string {
        const foundIndex = this.appliedGameFilters.findIndex((filter) => filter.translatedLabel === translatedLabel);
        if (foundIndex >= 0) return this.appliedGameFilters[foundIndex].value;
        return ALL;
    }
}
