/* eslint-disable no-unused-vars */
import { Injectable } from '@angular/core';
import { User } from '@app/classes/user';
import { CallServer } from '@app/decorators/call-server/call-server';
import { ReflectProp } from '@app/decorators/reflect-prop/reflect-prop';
import { Channel } from '@app/types/channel';
import { Message } from '@app/types/message';

@Injectable({
    providedIn: 'root',
})
export class GlobalChannelProxyService {
    @ReflectProp('GlobalChannelService') channels: Channel[] = [];

    @CallServer()
    async sendMessage(_channelId: string, _message: Message) {
        return;
    }

    @CallServer()
    async createChannel(_name: string): Promise<Channel> {
        return { id: '', name: '', users: [], owner: new User(), searchable: true, directMessage: false, messages: [] };
    }

    @CallServer()
    async createDM(_userId: string): Promise<Channel> {
        return { id: '', name: '', users: [], owner: new User(), searchable: true, directMessage: false, messages: [] };
    }

    @CallServer()
    async deleteChannel(_channelId: string) {
        return;
    }

    @CallServer()
    async joinChannel(_channelId: string) {
        return;
    }

    @CallServer()
    async leaveChannel(_channelId: string) {
        return;
    }

    @CallServer()
    async setChannelAsRead(_channelId: string) {
        return;
    }
}
