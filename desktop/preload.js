const { contextBridge, ipcRenderer } = require('electron');
const { webFrame } = require('electron');

webFrame.setZoomFactor(1);
webFrame.setVisualZoomLevelLimits(1, 1);

window.addEventListener('mouseup', (e) => {
    if (e.button === 3 || e.button === 4) e.preventDefault();
});

contextBridge.exposeInMainWorld('electronAPI', {
    getTheme: () => ipcRenderer.invoke('theme'),
    setTheme: (theme) => ipcRenderer.send('setTheme', theme),
    getLanguage: () => ipcRenderer.invoke('language'),
    setLanguage: (language) => ipcRenderer.send('setLanguage', language),
});
