# Desktop

## @CallServer()

Les interfaces marqués avec `@CallServer` appellerons les fonctions du serveur.

```java
// Sur desktop
export class GameManagerProxy {
    // Il faut spécifier le nom du service dans le premier décorateur
    @CallServer('GameManager') async changeTurn() {}

    // Avec valeur de retour, le 'false' est juste un placeholder
    @CallServer() async addPlayer(_name: string) { return false; }
    @CallServer() async addBot(_name: string) { return false; }
    @CallServer() async placeWord(_word: WordMap) {}
}
```

## @ReflectProp()

Définir des propriétés qui seront synchronisées avec le serveur.

```java
// Sur desktop
export class GameManagerProxy {
    // Il faut spécifier le nom du service dans le premier décorateur
    @ReflectProp('GameService') currentlyPlayingIndex: number;
    @ReflectProp() players: Player[] = [];
    @ReflectProp() isGameEnded: boolean = false;
    @ReflectProp() sumOfLettersLeftByPlayer: number[] = [0, 0];
    @ReflectProp() winner: Player | undefined;
}
```

# Mobile

Pour utiliser les décorateurs, il faut que le service (singleton) dérive de `RoomService`.

## callServer()

Les fonctions `callServer` appellerons les fonctions du serveur.

```dart
// Sur mobile
class GameProxyService extends RoomService {
    // Il faut spécifier le nom du service dans le constructeur de RoomService
    GameProxyService() : super('GameService');

    changeTurn()             => callServer(methodName: 'changeTurn');
    addPlayer(String name)   => callServer(methodName: 'addPlayer', args: [name]);
    placeWord(WordMap word, Pos position) => callServer(methodName: 'placeWord', args: [word, position]);

    // Avec une valeur de retour
    getMyPlayer(String name) => callServer(methodName: 'getMyPlayer', args: [name], returnType: Player());
    getWords() => callServer(methodName: 'getWords', returnType: [WordMap()]); // Retourne une liste
}
```

## reflectProp()

Définir des propriétés qui seront synchronisées avec le serveur.

```dart
// Sur mobile
class GameProxyService extends RoomService {
    // Il faut spécifier la valeur initiale de la propriété entre parenthèses
    get isGameEnded   => reflectProp('isGameEnded', false);
    get isGameStarted => reflectProp('isGameStarted', true);
    get gameName      => reflectProp('gameName', '');
    get players       => reflectProp('players', [Player()]);
    get bots          => reflectProp('bots', [Bot()]);

    // Toujours spécifier le nom du service dans le constructeur de RoomService
    GameProxyService() : super('GameService');
}
```

## onPropUpdate()

Puisque les widget mobile ne sont **pas** updatés automatiquement comparativement aux component Angular, il faut souvent appeler `setState()` lorsqu'il y a une mise-à-jour d'une propriété.

```dart
gameProxyService.onPropUpdate('gameName', () {
    setState(() {
        widgetName = gameProxyService.gameName;
    });
});
```

## JSON pour les objets

**Malheureusement**, en Dart, il est impossible d'automatiquement convertir nos objets en JSON, et vis versa. Ceci veut dire qu'il faut écrire nos propre conversions de JSON pour tout nos types. Ce n'est pas très compliqué; tous les types utilisés avec `reflectProp()` et `callServer()` qui **ne sont pas des primitives** doivent hériter de `JsonObj` et implémenter les fonctions `createFromJson()` et `toJson()`:

```dart
class User extends JsonObj {
    String username = "";
    String email = "";
    String id = "";

    // Retourne un nouvel objet à partir du JSON
    @override
    User createFromJson(dynamic json) {
        var user = User();

        // Le JSON passé en paramètre est un map string:value
        user.username = json['username'];
        user.email = json['email'];
        user.id = json['id'];

        return user;
    }

    // Retourne un JSON à partir de notre objet
    @override
    Map<String, dynamic> toJson() {
        return {
            'username': username,
            'email': email,
            'id': id
        };
    }
}
```

# Serveur

Pour utiliser les décorateurs, il faut que le service dérive de `RoomService`.

## @ProxyCall()

Permet à rendre une fonction accessible aux clients.

```java
// Sur serveur
export class GameManager extends RoomService {
    @ProxyCall()
    changeTurn() {
        this.currentlyPlayingIndex = this.currentlyPlayingIndex === 0 ? 1 : 0;
    }
}
```

## @MirrorProp()

Permet à synchronizer la propriété avec les clients. Lors d'une **redéfinition** (`=`), les clients sont automatiquement updatés. Lors d'une **modification**, il faut updater les clients manuellement.

```java
// Sur serveur
export class GameManager extends RoomService {
    @MirrorProp() isGameEnded: bool   = false;
    @MirrorProp() isGameStarted: bool = false;
    @MirrorProp() gameName: string    = '';
    @MirrorProp() players: Player[]   = [];

    startGame() {
        // Redéfinition: clients updatés automatiquement
        this.isGameStarted = true;
    }

    addPlayer(name: string) {
        // Modification: il faut updater les clients manuellement
        this.players.push(new Player(name));
        this.emitPropUpdate('players');
    }
}
```
