import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeManager {
  bool isDarkTheme = false;

  static final ThemeManager _service = ThemeManager.internal();

  ThemeManager.internal();

  factory ThemeManager() {
    return _service;
  }

  get boardBackground {
    return isDarkTheme
        ? const Color.fromRGBO(34, 34, 34, 1)
        : const Color.fromRGBO(245, 245, 245, 1);
  }

  get boardLineColor {
    return isDarkTheme ? Colors.white : Colors.black;
  }

  setDarkTheme() async {
    isDarkTheme = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isDarkTheme', true);
  }

  setLightTheme() async {
    isDarkTheme = false;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isDarkTheme', false);
  }

  getTheme() {
    return isDarkTheme
        ? ThemeData.dark().copyWith(
            colorScheme: const ColorScheme.light(
                primary: Color.fromRGBO(45, 45, 45, 1),
                secondary: Color.fromRGBO(34, 34, 34, 1),
                primaryContainer: Color.fromRGBO(199, 199, 199, 1),
                secondaryContainer: Color.fromRGBO(190, 190, 190, 1)),
            elevatedButtonTheme: ElevatedButtonThemeData(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        const Color.fromRGBO(30, 30, 30, 1)),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white))),
            appBarTheme: const AppBarTheme(
                titleTextStyle: TextStyle(color: Colors.white)),
            textSelectionTheme:
                const TextSelectionThemeData(cursorColor: Colors.white))
        : ThemeData.light().copyWith(
            colorScheme: const ColorScheme.light(
                primary: Color.fromRGBO(245, 245, 245, 1),
                secondary: Color.fromRGBO(235, 235, 235, 1),
                primaryContainer: Color.fromRGBO(170, 170, 170, 1),
                secondaryContainer: Color.fromRGBO(45, 45, 45, 1)),
            elevatedButtonTheme: ElevatedButtonThemeData(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(const Color.fromRGBO(245, 245, 245, 1)),
                    foregroundColor: MaterialStateProperty.all<Color>(Colors.black))),
            appBarTheme: const AppBarTheme(titleTextStyle: TextStyle(color: Colors.black)),
            textSelectionTheme: const TextSelectionThemeData(cursorColor: Colors.black),
            textButtonTheme: TextButtonThemeData(style: ButtonStyle(foregroundColor: MaterialStateProperty.all<Color>(const Color.fromRGBO(45, 45, 45, 1)))));
  }
}
