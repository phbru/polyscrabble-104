import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/client_friend_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_interaction_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/global_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

import '../classes/channel.dart';

class OtherProfile extends StatefulWidget {
  User user;
  bool isBot = false;
  bool isSelf = false;
  int index;
  final Function sendDMCallback;
  Function(int) setWatchedPlayer;

  OtherProfile(
      {required this.user,
      required this.sendDMCallback,
      super.key,
      this.index = -1,
      required this.setWatchedPlayer});

  @override
  State<OtherProfile> createState() => _OtherProfileState();
}

class _OtherProfileState extends State<OtherProfile> {
  final ClientChannelProxyService _clientChannelProxyService =
      ClientChannelProxyService();

  @override
  Widget build(BuildContext context) {
    if (widget.user.id == "") widget.isBot = true;
    if (widget.user.id == SelfUserProxyService().user.id) widget.isSelf = true;

    return Column(mainAxisSize: MainAxisSize.min, children: [
      SizedBox(
          child: Center(
        child: Column(children: [
          if (widget.index != -1 && // if is in game
              GameServiceProxy().isObserver() &&
              !widget.isBot)
            ...observePlayerButton(),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            avatarDisplay(),
            const SizedBox(width: 20),
            usernameDisplay(),
          ]),
          if (!widget.isBot) ...getStats(),
          getPlayerButtons()
        ]),
      ))
    ]);
  }

  List<Widget> getStats() {
    return [
      const Divider(height: 20),
      widget.user.getStats(),
    ];
  }

  Widget getPlayerButtons() {
    if (widget.isSelf) {
      return const Divider();
    } else if (widget.isBot && GameServiceProxy().isObserver()) {
      return observerToBotButtons();
    } else if (widget.isBot) {
      return const Divider();
    } else {
      return buttons(widget.user.id);
    }
  }

  List<Widget> observePlayerButton() {
    return [
      ElevatedButton(
          onPressed: () {
            widget.setWatchedPlayer(widget.index.toInt());
            Navigator.pop(context);
          },
          style: ElevatedButton.styleFrom(
            shape: const ContinuousRectangleBorder(),
            padding: const EdgeInsets.all(7),
          ),
          child: Text(translate("userInteractions.watchPlayer"))),
      const Divider(height: 10)
    ];
  }

  Widget observerToBotButtons() {
    return SizedBox(
        width: 300,
        height: 100,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          ElevatedButton(
              onPressed: () {
                GameServiceProxy()
                    .takeBotPlace(GameServiceProxy().players[widget.index]);
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate("gamePage.infoPanel.takePlace"))),
          ElevatedButton(
              onPressed: () {
                widget.setWatchedPlayer(widget.index.toInt());
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate("userInteractions.watchPlayer")))
        ]));
  }

  Widget buttons(String userId) {
    return Column(
      children: [
        const Divider(height: 40),
        Row(children: [
          const Spacer(),
          friendshipButton(),
          const Spacer(),
          ElevatedButton(
              onPressed: () async {
                // Switch to new channel
                Channel newChannel =
                    await GlobalChannelProxyService().createDM(widget.user.id);
                _clientChannelProxyService.currentChannelIndex =
                    _clientChannelProxyService.findIndex(newChannel.id);
                if (_clientChannelProxyService.currentChannelIndex != -1)
                  widget.sendDMCallback();
                Navigator.pop(context);
              },
              style: ButtonStyle(
                padding: MaterialStateProperty.all<EdgeInsets>(
                    const EdgeInsets.all(5)),
              ),
              child: const Text('Message')),
          const Spacer(),
        ])
      ],
    );
  }

  Widget avatarDisplay() {
    if (widget.isBot) {
      return widget.user.getAvatar(size: 100);
    }

    return GlobalUserProxyService().getAvatar(widget.user.username, 100);
  }

  Widget usernameDisplay() {
    return SizedBox(
        width: 90,
        child: Text(
          widget.user.username,
          style: const TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
        ));
  }

  Widget friendshipButton() {
    if (ClientFriendServiceProxy().isFriend(widget.user.id)) {
      return ElevatedButton(
          onPressed: () async {
            await GlobalInteractionServiceProxy().removeFriend(widget.user.id);
            setState(() {});
          },
          style: ButtonStyle(
            padding:
                MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(5)),
          ),
          child: Text(translate('friends.remove')));
    } else if (ClientFriendServiceProxy()
        .friendRequestReceived(widget.user.id)) {
      return ElevatedButton(
          onPressed: () async {
            await GlobalInteractionServiceProxy().acceptInvite(widget.user.id);
            setState(() {});
          },
          style: ButtonStyle(
            padding:
                MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(5)),
          ),
          child: Text(translate('friends.accept')));
    } else if (ClientFriendServiceProxy().friendRequestSent(widget.user.id)) {
      return ElevatedButton(
          onPressed: () async {
            await GlobalInteractionServiceProxy().removeInvite(widget.user.id);
            setState(() {});
          },
          style: ButtonStyle(
            padding:
                MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(5)),
          ),
          child: Text(translate('friends.revokeReq')));
    } else {
      return ElevatedButton(
          onPressed: () async {
            await GlobalInteractionServiceProxy()
                .createFriendRequest(widget.user.id);
            setState(() {});
          },
          style: ButtonStyle(
            padding:
                MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(5)),
          ),
          child: Text(translate('friends.add')));
    }
  }
}
