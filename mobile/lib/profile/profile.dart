import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/translate_preferences.dart';
import 'package:poly_scrabble_mobile/register_page/avatar_form.dart';
import 'package:poly_scrabble_mobile/services/communication_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/theme_manager.dart';

import '../socket.dart';

class Profile extends StatefulWidget {
  Function() updateTheme;
  bool allowChange;
  Profile({super.key, required this.updateTheme, this.allowChange = true});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final SelfUserProxyService self = SelfUserProxyService();
  final CommunicationService _communicationService = CommunicationService();
  final Socket socket = Socket();

  Future<bool> isEnglish() async {
    var x = await TranslatePreferences().getPreferredLocale();
    return localeToString(x) == 'en';
  }

  _ProfileState() {
    self.refreshDisplay = refreshDisplay;
  }

  String username = SelfUserProxyService().user.username;
  bool isEditable = false;
  bool submitted = false;

  int avatarResCode = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (widget.allowChange) updatedFieldsRes(),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                ...profile(),
                const SizedBox(width: 270, height: 30, child: Divider()),
                Container(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: const Text('Statistiques',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold))),
                self.user.getStats()
              ],
            ),
            const SizedBox(height: 320, child: VerticalDivider(width: 50)),
            SizedBox(
              width: 250,
              child: Column(
                children: [...settings()],
              ),
            ),
          ],
        ),
      ],
    );
  }

  refreshDisplay() {
    if (!submitted && mounted) {
      setState(() {});
    }
  }

  List<Widget> profile() {
    return [
      Column(
        children: [
          Row(children: [
            avatarDisplay(),
            const SizedBox(width: 5),
            usernameDisplay(),
            const SizedBox(width: 5),
            if (widget.allowChange)
              IconButton(
                icon: const Icon(Icons.edit),
                onPressed: () {
                  setState(() => {isEditable = !isEditable});
                },
              ),
          ]),
          if (isEditable)
            Container(
                width: 200,
                margin: const EdgeInsets.only(left: 100),
                child: Text(translate('profile.username.condition'),
                    style: const TextStyle(fontSize: 12))),
        ],
      )
    ];
  }

  List<bool> isSelectedTheme = [
    ThemeManager().isDarkTheme, // Dark
    !ThemeManager().isDarkTheme, // Light
  ];
  List<bool> isSelectedLang = [true, false];

  List<Widget> settings() {
    return [
      Container(height: 10),
      Row(
        children: [
          Text(translate('common.language')),
          const Spacer(),
          FutureBuilder(
              future: TranslatePreferences().getPreferredLocale(),
              builder: ((context, snapshot) {
                if (snapshot.data.toString() == 'en') {
                  // if english
                  isSelectedLang[0] = false; // fr
                  isSelectedLang[1] = true; // en
                } else {
                  // if french
                  isSelectedLang[0] = true; // fr
                  isSelectedLang[1] = false; // en
                }
                return ToggleButtons(
                  onPressed: (int index) {
                    setState(() {
                      if (index == 0) {
                        changeLocale(context, 'fr');
                      } else {
                        changeLocale(context, 'en');
                      }
                      for (int buttonIndex = 0;
                          buttonIndex < isSelectedLang.length;
                          buttonIndex++) {
                        if (buttonIndex == index) {
                          isSelectedLang[buttonIndex] = true;
                        } else {
                          isSelectedLang[buttonIndex] = false;
                        }
                      }
                    });
                  },
                  color: Theme.of(context).textTheme.bodyLarge?.color!,
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  borderColor: Theme.of(context).colorScheme.primaryContainer,
                  selectedBorderColor:
                      Theme.of(context).colorScheme.primaryContainer,
                  selectedColor: Theme.of(context).textTheme.bodyLarge?.color!,
                  fillColor: Colors.grey[400],
                  constraints: const BoxConstraints(
                    minHeight: 40.0,
                    minWidth: 80.0,
                  ),
                  isSelected: isSelectedLang,
                  children: <Widget>[
                    Text(translate('common.french')),
                    Text(translate('common.english')),
                  ],
                );
              })),
        ],
      ),
      Container(height: 10),
      Row(
        children: [
          Text(translate('common.theme')),
          const Spacer(),
          ToggleButtons(
            onPressed: (int index) async {
              setState(() {
                if (index == 0) {
                  ThemeManager().setDarkTheme();
                } else {
                  ThemeManager().setLightTheme();
                }
                widget.updateTheme();
                for (int buttonIndex = 0;
                    buttonIndex < isSelectedTheme.length;
                    buttonIndex++) {
                  if (buttonIndex == index) {
                    isSelectedTheme[buttonIndex] = true;
                  } else {
                    isSelectedTheme[buttonIndex] = false;
                  }
                }
              });
            },
            color: Theme.of(context).textTheme.bodyLarge?.color!,
            borderRadius: const BorderRadius.all(Radius.circular(8)),
            borderColor: Theme.of(context).colorScheme.primaryContainer,
            selectedBorderColor: Theme.of(context).colorScheme.primaryContainer,
            selectedColor: Theme.of(context).textTheme.bodyLarge?.color!,
            fillColor: Colors.grey[400],
            constraints: const BoxConstraints(
              minHeight: 40.0,
              minWidth: 80.0,
            ),
            isSelected: isSelectedTheme,
            children: <Widget>[
              Text(translate('common.dark')),
              Text(translate('common.light')),
            ],
          ),
        ],
      ),
      Container(height: 160),
      ElevatedButton(
          onPressed: () {
            socket.io.disconnect();
            Navigator.of(context).pop();
            SchedulerBinding.instance.addPostFrameCallback((_) {
              Navigator.pushReplacementNamed(context, '/');
            });
          },
          child: Text(translate('menuSettings.signOut')))
    ];
  }

  Widget updatedFieldsRes() {
    if (!submitted) {
      return updateAvatarRes();
    }

    if (username.isEmpty || username.length > 10) {
      submitted = false;
      return sizedBox(translate('profile.username.length'), Colors.red);
    }

    if (!RegExp(r'^[A-Za-z0-9]+$').hasMatch(username) ||
        username.contains(' ')) {
      submitted = false;
      return sizedBox(translate('profile.username.invalid'), Colors.red);
    }

    return FutureBuilder(
      future: _communicationService.updateUsername(self.user, username),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return sizedBox(translate('common.loading '), Colors.black);
        }

        submitted = false;

        switch (snapshot.data) {
          case 11000:
            return sizedBox(
                translate('profile.username.alreadyUsed'), Colors.red);
          default:
            return sizedBox(
                translate('profile.username.updated'), Colors.green);
        }
      },
    );
  }

  Widget updateAvatarRes() {
    switch (avatarResCode) {
      case 0:
        return Container();
      case 200:
        avatarResCode = 0;
        return sizedBox(translate('profile.avatar.updated'), Colors.green);
      default:
        avatarResCode = 0;
        return sizedBox(translate('profile.avatar.error'), Colors.red);
    }
  }

  Widget sizedBox(String text, Color color) {
    return SizedBox(
        height: 30,
        child: Center(
            child: Text(text, style: TextStyle(color: color, fontSize: 20))));
  }

  Widget avatarDisplay() {
    return ElevatedButton(
        onPressed: () {
          if (widget.allowChange) updateAvatar();
        },
        style: ButtonStyle(
            shape:
                MaterialStateProperty.all<CircleBorder>(const CircleBorder())),
        child: Stack(children: [
          self.user.getAvatar(size: 90),
          if (widget.allowChange)
            Positioned(
              bottom: 1,
              right: 1,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                      width: 3,
                      color: Colors.white,
                    ),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(
                        50,
                      ),
                    ),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(2, 4),
                        color: Colors.black.withOpacity(
                          0.3,
                        ),
                        blurRadius: 3,
                      ),
                    ]),
                child: const Padding(
                  padding: EdgeInsets.all(2.0),
                  child: Icon(Icons.add_a_photo, color: Colors.black),
                ),
              ),
            )
        ]));
  }

  void updateAvatar() {
    String avatarUrl = '';

    void getAvatarUrl(String newAvatarUrl) {
      if (newAvatarUrl.split('.').last == 'svg') {
        avatarUrl = newAvatarUrl;
      } else {
        avatarUrl = 'data:image/jpeg;base64,$newAvatarUrl';
      }
    }

    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text(translate('profile.avatar.modify')),
              content: AvatarForm(
                getAvatarUrl,
                isRegisterPage: false,
              ),
              actions: <Widget>[
                Center(
                    child: Column(children: [
                  const Divider(),
                  ElevatedButton(
                    onPressed: () async {
                      if (avatarUrl == '') return;
                      if (avatarUrl == self.user.avatar) return;

                      int res = await _communicationService.updateAvatar(
                          SelfUserProxyService().user, avatarUrl);
                      if (!mounted) return;

                      avatarResCode = res;

                      Navigator.pop(context);
                    },
                    child: Text(translate('profile.avatar.update')),
                  ),
                ]))
              ],
            ));
  }

  Widget usernameDisplay() {
    return Column(children: [
      SizedBox(
          width: 130,
          child: !isEditable
              ? Text(
                  username,
                  style: const TextStyle(
                      fontSize: 23, fontWeight: FontWeight.bold),
                )
              : TextFormField(
                  style: const TextStyle(fontSize: 23),
                  initialValue: username,
                  textInputAction: TextInputAction.done,
                  onFieldSubmitted: (newUsername) {
                    if (newUsername != SelfUserProxyService().user.username) {
                      setState(() => {
                            submitted = true,
                            isEditable = false,
                            username = newUsername,
                          });
                    }
                  })),
    ]);
  }
}
