enum Color { green, blue, red, black }

int colorToInt(Color color) {
  switch (color) {
    case Color.green:
      return 0;
    case Color.blue:
      return 1;
    case Color.red:
      return 2;
    case Color.black:
      return 3;
  }
}

Color intToColor(int color) {
  switch (color) {
    case 0:
      return Color.green;
    case 1:
      return Color.blue;
    case 2:
      return Color.red;
    case 3:
      return Color.black;
  }

  return Color.black;
}
