// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/profile/other_profile.dart';
import 'package:poly_scrabble_mobile/services/client_friend_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/global_user_proxy_service.dart';

import '../../classes/user.dart';

class OnlineUsersList extends StatefulWidget {
  final Function sendDMCallback;
  bool isOnFriend;
  Function() refreshRequests;
  OnlineUsersList(
      {super.key,
      required this.isOnFriend,
      required this.sendDMCallback,
      required this.refreshRequests});
  @override
  OnlineUsersListState createState() => OnlineUsersListState();
}

class OnlineUsersListState extends State<OnlineUsersList> {
  GlobalUserProxyService globalUserService = GlobalUserProxyService();
  ClientFriendServiceProxy clientFriendService = ClientFriendServiceProxy();
  @override
  void initState() {
    super.initState();
    globalUserService.onPropUpdate('onlineUsers', () {
      if (mounted) {
        setState(() {});
      }
    });

    clientFriendService.onPropUpdate('friends', () {
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    List<User> onlineUsers = widget.isOnFriend
        ? clientFriendService.friends
        : globalUserService.onlineUsers;
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: onlineUsers.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              height: 50,
              margin: const EdgeInsets.all(5.0),
              padding: const EdgeInsets.all(3.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: [
                    ElevatedButton(
                        onPressed: () =>
                            showOnlineUserProfile(onlineUsers[index]),
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<CircleBorder>(
                                const CircleBorder())),
                        child: SizedBox(
                            height: 40,
                            width: 40,
                            child: onlineUsers[index].getAvatar())),
                    const SizedBox(width: 5),
                    Text(onlineUsers[index].username,
                        style: const TextStyle(
                            fontFamily: 'Arial',
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                  ])));
        });
  }

  void showOnlineUserProfile(User user) {
    int fakeInt = 0;
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
                content: OtherProfile(
              user: user,
              sendDMCallback: widget.sendDMCallback,
              setWatchedPlayer: (fakeInt) {},
            )));
  }
}
