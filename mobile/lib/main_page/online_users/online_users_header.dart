import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/client_friend_service_proxy.dart';

class OnlineUsersHeader extends StatelessWidget {
  bool isOnFriend;
  Function(bool) setOnFriend;

  OnlineUsersHeader(
      {super.key, required this.isOnFriend, required this.setOnFriend});

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).colorScheme.secondary,
        height: 60,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                  width: 180,
                  height: 40,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      color: isOnFriend
                          ? Theme.of(context).colorScheme.secondary
                          : Theme.of(context).colorScheme.primary,
                      borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(5),
                          topLeft: Radius.circular(5))),
                  child: TextButton(
                      style: ButtonStyle(
                        shape: MaterialStateProperty.all(
                            const ContinuousRectangleBorder()),
                        padding:
                            MaterialStateProperty.all(const EdgeInsets.all(1)),
                      ),
                      onPressed: () {
                        setOnFriend(false);
                      },
                      child: Text(translate('onlineUsers.onlineUsers'),
                          style: TextStyle(
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyLarge!
                                  .color)))),
              Container(
                  width: 180,
                  height: 40,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      color: isOnFriend
                          ? Theme.of(context).colorScheme.primary
                          : Theme.of(context).colorScheme.secondary,
                      borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(5),
                          topLeft: Radius.circular(5))),
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      TextButton(
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                                const ContinuousRectangleBorder()),
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.all(1)),
                          ),
                          onPressed: () {
                            setOnFriend(true);
                          },
                          child: Text(translate('onlineUsers.friends'),
                              style: TextStyle(
                                  color: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .color))),
                      Positioned(
                          right: 5,
                          top: 5,
                          child: ClientFriendServiceProxy().notificationOn
                              ? Container(
                                  width: 12,
                                  height: 10,
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      shape: BoxShape.circle,
                                      border: Border.all(color: Colors.grey)),
                                )
                              : Container())
                    ],
                  )),
            ]));
  }
}
