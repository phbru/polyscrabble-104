import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/main_page/online_users/online_users_header.dart';
import 'package:poly_scrabble_mobile/main_page/online_users/online_users_list.dart';
import 'package:poly_scrabble_mobile/services/client_friend_service_proxy.dart';

import '../../requests_page.dart/request.dart';

class OnlineUsersDisplay extends StatefulWidget {
  final Function sendDMCallback;
  OnlineUsersDisplay({super.key, required this.sendDMCallback});
  @override
  OnlineUsersDisplayState createState() => OnlineUsersDisplayState();
}

class OnlineUsersDisplayState extends State<OnlineUsersDisplay> {
  bool isOnFriend = false;
  ClientFriendServiceProxy clientFriendService = ClientFriendServiceProxy();
  @override
  void initState() {
    super.initState();

    clientFriendService.onPropUpdate('friends', () {
      if (mounted) setState(() {});
    });

    clientFriendService.onPropUpdate('friendsRequested', () {
      if (mounted) {
        setState(() {});
      }
    });

    clientFriendService.onPropUpdate('receivedInvites', () {
      if (mounted) {
        setState(() {});
      }
    });

    clientFriendService.onPropUpdate('notificationOn', () {
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      OnlineUsersHeader(
        isOnFriend: isOnFriend,
        setOnFriend: setOnFriend,
      ),
      Expanded(
          flex: 9,
          child: OnlineUsersList(
            isOnFriend: isOnFriend,
            sendDMCallback: widget.sendDMCallback,
            refreshRequests: refreshRequests,
          )),
      if (isOnFriend &&
          (ClientFriendServiceProxy().friendsRequested.isNotEmpty ||
              ClientFriendServiceProxy().receivedInvites.isNotEmpty))
        Stack(
          children: [
            SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                    onPressed: () {
                      openRequestDlg();
                      ClientFriendServiceProxy().removeNotification();
                    },
                    child: const Text('Requêtes'))),
            Positioned(
                left: 222,
                top: 10,
                child: ClientFriendServiceProxy().notificationOn
                    ? Container(
                        width: 12,
                        height: 10,
                        decoration: BoxDecoration(
                            color: Colors.red,
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.grey)))
                    : Container())
          ],
        )
    ]);
  }

  setOnFriend(bool isFriend) {
    setState(() {
      isOnFriend = isFriend;
    });
  }

  void openRequestDlg() {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => const AlertDialog(
                content: SizedBox(
              width: 500,
              height: 500,
              child: Requests(),
            )));
  }

  refreshRequests() {
    setState(() {});
  }
}
