import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/enums/game_participant_type.dart';
import 'package:poly_scrabble_mobile/services/global_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class PlayerItem extends StatelessWidget {
  GameParticipantType type;
  String name;
  GlobalUserProxyService globalUserProxyService = GlobalUserProxyService();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  PlayerItem({required this.name, required this.type});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(left: 25),
          decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.grey))),
          height: 65,
          width: 750),
      SizedBox(
          height: 65,
          width: 750,
          child: Row(
            children: [
              Expanded(
                  flex: 9,
                  child: Row(children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: globalUserProxyService.getAvatar(name, 50))),
                    Padding(
                        padding: const EdgeInsets.only(left: 40),
                        child: Text(
                            type == GameParticipantType.CreatorPlayer
                                ? '$name (${translate('mainPage.joinableGames.creator')})'
                                : name,
                            style: const TextStyle(fontSize: 20)))
                  ])),
              waitingRoomService.isSelfGameCreator &&
                      type != GameParticipantType.CreatorPlayer
                  ? SizedBox(
                      width: 100,
                      child: ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                  const ContinuousRectangleBorder()),
                              padding: MaterialStateProperty.all(
                                  const EdgeInsets.all(1)),
                              overlayColor:
                                  MaterialStateProperty.all(Colors.grey[200])),
                          onPressed: () {
                            waitingRoomService.kickUserOut(name);
                          },
                          child: Text(translate('waitingRoom.kickOut'))))
                  : Container()
            ],
          ))
    ]);
  }
}
