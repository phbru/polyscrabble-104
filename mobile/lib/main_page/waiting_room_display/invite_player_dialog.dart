import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/services/client_friend_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/global_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class InvitePlayerDialog extends StatefulWidget {
  GameInfo gameInfo;
  InvitePlayerDialog({super.key, required this.gameInfo});
  @override
  InvitePlayerDialogState createState() => InvitePlayerDialogState();
}

class InvitePlayerDialogState extends State<InvitePlayerDialog> {
  final TextEditingController _textEditingSearchPlayerController =
      TextEditingController();
  GlobalUserProxyService globalUserService = GlobalUserProxyService();
  SelfUserProxyService selfUserService = SelfUserProxyService();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  ClientFriendServiceProxy clientFriendServiceProxy = ClientFriendServiceProxy();
  String nameFilter = "";

  @override
  void initState() {
    super.initState();
    globalUserService.onPropUpdate('onlineUsers', () {
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var filteredUsers = [];
    // Prioritize friends
    for(var user in globalUserService.onlineUsers) {
      if(canInviteUser(user)) {
        if(clientFriendServiceProxy.isFriend(user.id)) {
          filteredUsers.insert(0, user);
        } else {
          filteredUsers.add(user);
        }
      }
    }

    return Builder(
        builder: (context) => Container(
            padding: const EdgeInsets.only(top: 30),
            width: 600,
            child: Column(children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Container(
                    width: 50,
                    height: 50,
                    child: IconButton(
                        onPressed: () {
                          setState(() {
                            nameFilter =
                                _textEditingSearchPlayerController.text;
                          });
                        },
                        icon: const Icon(
                          Icons.search,
                        ))),
                Container(
                    width: 500,
                    child: TextField(
                        controller: _textEditingSearchPlayerController,
                        onSubmitted: (data) {
                          setState(() {
                            nameFilter = data;
                          });
                        },
                        textAlign: TextAlign.start,
                        decoration: InputDecoration(
                          fillColor: Theme.of(context).colorScheme.primary,
                          filled: true,
                          hintText: 'Search',
                        ))),
              ]),
              Container(
                  margin: const EdgeInsets.only(left: 10),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: Theme.of(context)
                                  .colorScheme
                                  .primaryContainer))),
                  height: 20,
                  width: 540),
              Container(
                  height: 400,
                  width: 545,
                  padding: const EdgeInsets.only(left: 10, top: 10),
                  child: ListView.builder(
                      itemCount: filteredUsers.length,
                      itemBuilder: (BuildContext context, int index) {
                          return Container(
                              height: 50,
                              width: 540,
                              margin:
                                  const EdgeInsets.only(top: 5.0, bottom: 5.0),
                              padding: const EdgeInsets.all(3.0),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Align(
                                        alignment: Alignment.centerLeft,
                                        child: Row(children: [
                                          SizedBox(
                                              height: 40,
                                              width: 40,
                                              child: filteredUsers[index]
                                                  .getAvatar()),
                                          const SizedBox(width: 5),
                                          Text(
                                              filteredUsers[index].username,
                                              style: const TextStyle(
                                                  fontFamily: 'Arial',
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold)),
                                        ])),
                                    ElevatedButton(
                                        onPressed: () {
                                          inviteUser(filteredUsers[index]);
                                          Navigator.pop(context);
                                        },
                                        child: Text(
                                            translate('waitingRoom.invite'))),
                                  ]));
                      }))
            ])));
  }

  canInviteUser(User user) {
    if ((selfUserService.user.username == user.username) ||
        (nameFilter.isNotEmpty &&
            !user.username.toLowerCase().contains(nameFilter.toLowerCase()))) {
      return false;
    }
    for (User player in widget.gameInfo.players) {
      if (player.username == user.username) {
        return false;
      }
    }
    for (User observer in widget.gameInfo.observers) {
      if (observer.username == user.username) {
        return false;
      }
    }
    return true;
  }

  inviteUser(User user) {
    waitingRoomService.inviteUser(user.id);
    waitingRoomService.addInvitedUser(user.id);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
      translate('userInteractions.invite'),
      textAlign: TextAlign.center,
    )));
  }
}
