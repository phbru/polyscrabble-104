import 'package:flutter/material.dart';

class AddPlace extends StatelessWidget {
  Function() addPlayer;
  Function() addBot;
  AddPlace({super.key, required this.addPlayer, required this.addBot});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(left: 25),
          decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.grey))),
          height: 65,
          width: 750),
      SizedBox(
        height: 65,
        width: 750,
        child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: ElevatedButton(
                    onPressed: () {
                      addPlayer();
                    },
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all(const CircleBorder()),
                        overlayColor:
                            MaterialStateProperty.all(Colors.grey[200])),
                    child: Container(
                        width: 50,
                        height: 50,
                        decoration: const BoxDecoration(shape: BoxShape.circle),
                        child:
                            const Center(child: Icon(Icons.add, size: 30)))))),
      )
    ]);
  }
}
