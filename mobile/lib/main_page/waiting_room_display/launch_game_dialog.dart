import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class LaunchGameDialog extends StatelessWidget {
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Container(
            padding: const EdgeInsets.only(top: 10),
            width: 500,
            child: Column(children: [
              Container(
                  padding: EdgeInsets.only(bottom: 10),
                  child:
                      Text(translate('waitingRoom.launchGame.launchAnyway'))),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      style: ElevatedButton.styleFrom(
                        shape: const ContinuousRectangleBorder(),
                        padding: const EdgeInsets.all(7),
                      ),
                      child: Text(translate('common.no'))),
                  ElevatedButton(
                      onPressed: () {
                        waitingRoomService.fillEmptySpots();
                        Navigator.pop(context);
                      },
                      style: ElevatedButton.styleFrom(
                        shape: const ContinuousRectangleBorder(),
                        padding: const EdgeInsets.all(7),
                      ),
                      child: Text(translate('common.yes')))
                ],
              ),
            ])));
  }
}
