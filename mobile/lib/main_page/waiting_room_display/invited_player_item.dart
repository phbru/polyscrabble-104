import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

class InvitedPlayerItem extends StatelessWidget {
  User user;
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  Socket socket = Socket();
  InvitedPlayerItem({required this.user});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(left: 25),
          decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.grey))),
          height: 65,
          width: 750),
      SizedBox(
          height: 65,
          width: 750,
          child: Row(
            children: [
              Expanded(
                  flex: 9,
                  child: Row(children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: user.getAvatar(size: 50))),
                    Padding(
                        padding: const EdgeInsets.only(left: 40),
                        child: Text(
                            translate('waitingRoom.invitedPlayer',
                                args: {'name': user.username}),
                            style: const TextStyle(
                                fontSize: 20, color: Colors.grey)))
                  ])),
              waitingRoomService.isSelfGameCreator
                  ? SizedBox(
                      width: 100,
                      child: ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                  const ContinuousRectangleBorder()),
                              padding: MaterialStateProperty.all(
                                  const EdgeInsets.all(1)),
                              overlayColor:
                                  MaterialStateProperty.all(Colors.grey[200])),
                          onPressed: () {
                            revokeInvite(user.id);
                          },
                          child: Text(
                            translate('waitingRoom.revokeInvite'),
                            textAlign: TextAlign.center,
                          )))
                  : Container()
            ],
          ))
    ]);
  }

  revokeInvite(String userIdToKickout) {
    waitingRoomService.removeInvitedUser(userIdToKickout);
    socket.io.emit('revokeInvite', userIdToKickout);
  }
}
