import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/services/global_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class WaitingUserItem extends StatelessWidget {
  Function() refreshDisplay;
  User user;
  Socket socket = Socket();
  GlobalUserProxyService globalUserProxyService = GlobalUserProxyService();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  WaitingUserItem({required this.user, required this.refreshDisplay});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(left: 25),
          decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.grey))),
          height: 65,
          width: 750),
      SizedBox(
          height: 65,
          width: 750,
          child: Row(
            children: [
              Expanded(
                  flex: 9,
                  child: Row(children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: globalUserProxyService.getAvatar(
                                user.username, 50))),
                    Padding(
                        padding: const EdgeInsets.only(left: 40),
                        child: Text(user.username,
                            style: const TextStyle(fontSize: 20)))
                  ])),
              SizedBox(
                  width: 100,
                  child: ElevatedButton(
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(
                              const ContinuousRectangleBorder()),
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.all(1)),
                          overlayColor:
                              MaterialStateProperty.all(Colors.grey[200])),
                      onPressed: () {
                        declineRequest();
                        refreshDisplay();
                      },
                      child: Text(translate('common.decline')))),
              SizedBox(
                  width: 100,
                  child: ElevatedButton(
                      style: ButtonStyle(
                          shape: MaterialStateProperty.all(
                              const ContinuousRectangleBorder()),
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.all(1)),
                          overlayColor:
                              MaterialStateProperty.all(Colors.grey[200])),
                      onPressed: () {
                        acceptRequest();
                        refreshDisplay();
                      },
                      child: Text(translate('common.accept'))))
            ],
          ))
    ]);
  }

  acceptRequest() {
    waitingRoomService.removeWaitingToJoin(user.id);
    socket.io.emit('acceptJoin', user.id);
  }

  declineRequest() {
    waitingRoomService.removeWaitingToJoin(user.id);
    socket.io.emit('declineJoin', user.id);
  }
}
