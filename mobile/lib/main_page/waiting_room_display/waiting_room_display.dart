// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/enums/game_participant_type.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/add-place.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/bot-player.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/invite_player_dialog.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/invited_player_item.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/launch_game_dialog.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/player-item.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/player-spot.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/waiting_user_item.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class WaitingRoomDisplay extends StatefulWidget {
  Function() closeWaitingRoom;
  WaitingRoomDisplay({required this.closeWaitingRoom});
  @override
  WaitingRoomDisplayState createState() => WaitingRoomDisplayState();
}

class WaitingRoomDisplayState extends State<WaitingRoomDisplay> {
  GlobalUserProxyService globalUserService = GlobalUserProxyService();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  SelfUserProxyService self = SelfUserProxyService();
  GameServiceProxy gameService = GameServiceProxy();
  Socket socket = Socket();
  ValueNotifier<bool> isKicked = ValueNotifier(false);
  bool isOnObserver = true;
  bool notificationOn = false;
  @override
  void initState() {
    super.initState();
    waitingRoomService.onPropUpdate('gameInfo', () {
      if (mounted) {
        updateGameToJoin();
      }
    });

    socket.io.on('goToGamePage', (e) {
      if (mounted) {
        goToGamePage();
      }
    });

    socket.io.on('kickUserOut', (userName) {
      if (mounted) {
        if (userName == self.user.username) {
          waitingRoomService.leaveGame();
          waitingRoomService.resetParameters();
          SchedulerBinding.instance.addPostFrameCallback((_) {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    children: [
                      SizedBox(
                          height: 100,
                          width: 100,
                          child: Center(
                              child: Text(
                            translate('waitingRoom.kickedOut'),
                            style: const TextStyle(fontSize: 20),
                            textAlign: TextAlign.center,
                          )))
                    ],
                  );
                });
            widget.closeWaitingRoom();
          });
        }
      }
    });

    socket.io.on('informCreatorJoin', (joinerUser) {
      if (mounted &&
          waitingRoomService.gameInfo.creatorName == self.user.username) {
        setState(() {
          notificationOn = true;
          waitingRoomService.waitingToJoin
              .add(User().createFromJson(joinerUser));
        });
      }
    });

    socket.io.on('informCreatorCancel', (joinerUser) {
      if (mounted &&
          waitingRoomService.gameInfo.creatorName == self.user.username) {
        setState(() {
          waitingRoomService
              .removeWaitingToJoin(User().createFromJson(joinerUser).id);
        });
      }
    });

    socket.io.on('declineInvite', (invitedUser) {
      if (!mounted || !waitingRoomService.isSelfGameCreator) return;
      setState(() {
        waitingRoomService.removeInvitedUser(invitedUser);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    GameInfo gameToBuild = waitingRoomService.gameInfo;
    return Container(
        decoration: const BoxDecoration(
            border: Border(left: BorderSide(color: Colors.grey))),
        child: SingleChildScrollView(
            child: Column(children: [
          SizedBox(
              height: 40,
              width: 800,
              child: Center(
                  child: Text(translate('waitingRoom.waitingRoom'),
                      style: const TextStyle(fontSize: 20)))),
          Stack(children: [
            Container(
                margin: const EdgeInsets.only(left: 25),
                decoration: const BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.black))),
                height: 30,
                width: 750),
            Container(
                margin: const EdgeInsets.only(left: 25),
                width: 750,
                height: 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        margin: const EdgeInsets.only(top: 6),
                        child: Text(translate('waitingRoom.players'),
                            style:
                                const TextStyle(fontWeight: FontWeight.bold))),
                    waitingRoomService.isSelfGameCreator &&
                            gameToBuild.openSpots > 0
                        ? Container(
                            height: 25,
                            margin: const EdgeInsets.only(bottom: 6),
                            child: ElevatedButton(
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return SimpleDialog(
                                          title: Center(
                                              child:
                                                  Text(translate('waitingRoom.inviteDialog'))),
                                          children: [
                                            InvitePlayerDialog(
                                              gameInfo: gameToBuild,
                                            )
                                          ],
                                        );
                                      });
                                },
                                child: Text(translate('waitingRoom.invite'))))
                        : waitingRoomService.isSelfOnObserverSpot &&
                                (gameToBuild.openSpots > 0 ||
                                    gameToBuild.virtualPlayers.isNotEmpty)
                            ? Container(
                                height: 25,
                                margin: const EdgeInsets.only(bottom: 6),
                                child: ElevatedButton(
                                    onPressed: () {
                                      takePlayerSpot();
                                    },
                                    child: Text(
                                        translate('waitingRoom.takePlace'))))
                            : Container()
                  ],
                ))
          ]),
          Container(
              height: 260,
              padding: const EdgeInsets.only(left: 15),
              child: ListView.builder(
                  itemCount: gameToBuild.players.length +
                              gameToBuild.openSpots +
                              gameToBuild.invitedUsers.length +
                              waitingRoomService
                                  .gameInfo.virtualPlayers.length ==
                          4
                      ? 4
                      : (gameToBuild.players.length +
                              gameToBuild.openSpots +
                              gameToBuild.invitedUsers.length +
                              waitingRoomService
                                  .gameInfo.virtualPlayers.length +
                              1)
                          .toInt(),
                  itemBuilder: (BuildContext context, int index) {
                    if (index < gameToBuild.players.length) {
                      return PlayerItem(
                          name: waitingRoomService
                              .gameInfo.players[index].username,
                          type: waitingRoomService
                                      .gameInfo.players[index].username ==
                                  gameToBuild.creatorName
                              ? GameParticipantType.CreatorPlayer
                              : GameParticipantType.RegularPlayer);
                    } else if (index <
                        gameToBuild.players.length +
                            gameToBuild.invitedUsers.length) {
                      return InvitedPlayerItem(
                          user: gameToBuild.invitedUsers[
                              index - gameToBuild.players.length.toInt()]);
                    } else if (index <
                        gameToBuild.players.length +
                            gameToBuild.invitedUsers.length +
                            gameToBuild.virtualPlayers.length) {
                      return BotPlayer(
                        virtualPlayer: gameToBuild.virtualPlayers[index -
                            gameToBuild.players.length.toInt() -
                            gameToBuild.invitedUsers.length.toInt()],
                        removeBot: removeBot,
                      );
                    } else if (index <
                        gameToBuild.players.length +
                            gameToBuild.invitedUsers.length +
                            gameToBuild.virtualPlayers.length +
                            gameToBuild.openSpots) {
                      return PlayerSpot(
                        removePlayerSpot: removePlayerSpot,
                        addBot: addBot,
                      );
                    } else if (waitingRoomService.isSelfGameCreator) {
                      return AddPlace(
                        addPlayer: addPlayerSpot,
                        addBot: addBot,
                      );
                    }
                    return Container();
                  })),
          Container(
              padding: const EdgeInsets.only(top: 50),
              child: Stack(children: [
                Container(
                    margin: const EdgeInsets.only(left: 25),
                    width: 750,
                    height: 30,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        if (gameToBuild.isPrivate &&
                            gameToBuild.creatorName == self.user.username)
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                    width: 180,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.white),
                                        color: isOnObserver
                                            ? Theme.of(context)
                                                .colorScheme
                                                .primary
                                            : Theme.of(context)
                                                .colorScheme
                                                .secondary,
                                        borderRadius: const BorderRadius.only(
                                            topRight: Radius.circular(5),
                                            topLeft: Radius.circular(5))),
                                    child: TextButton(
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all(
                                              const ContinuousRectangleBorder()),
                                          padding: MaterialStateProperty.all(
                                              const EdgeInsets.all(1)),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            if (isOnObserver == false) {
                                              notificationOn = false;
                                            }
                                            isOnObserver = true;
                                          });
                                        },
                                        child: Text(
                                            translate('waitingRoom.observer'),
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyLarge!
                                                    .color)))),
                                Stack(children: [
                                  Container(
                                      width: 180,
                                      height: 40,
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: Colors.white),
                                          color: isOnObserver
                                              ? Theme.of(context)
                                                  .colorScheme
                                                  .secondary
                                              : Theme.of(context)
                                                  .colorScheme
                                                  .primary,
                                          borderRadius: const BorderRadius.only(
                                              topRight: Radius.circular(5),
                                              topLeft: Radius.circular(5))),
                                      child: TextButton(
                                          style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                const ContinuousRectangleBorder()),
                                            padding: MaterialStateProperty.all(
                                                const EdgeInsets.all(1)),
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              isOnObserver = false;
                                            });
                                          },
                                          child: Text(
                                              translate(
                                                  'waitingRoom.waitingToJoin'),
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      .bodyLarge!
                                                      .color)))),
                                  Positioned(
                                      right: 2,
                                      top: 5,
                                      child: notificationOn
                                          ? Container(
                                              width: 12,
                                              height: 10,
                                              decoration: BoxDecoration(
                                                  color: Colors.red,
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                      color: Colors.grey)),
                                            )
                                          : Container())
                                ]),
                              ]),
                        if (!gameToBuild.isPrivate ||
                            (gameToBuild.isPrivate &&
                                gameToBuild.creatorName != self.user.username))
                          Container(
                              margin: const EdgeInsets.only(top: 6),
                              child: Text(translate('waitingRoom.observer'),
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold))),
                        waitingRoomService.isSelfOnPlayerSpot &&
                                !waitingRoomService.isSelfGameCreator
                            ? Container(
                                height: 25,
                                margin: const EdgeInsets.only(bottom: 6),
                                child: ElevatedButton(
                                    onPressed: () {
                                      waitingRoomService.takeObserverSpot();
                                    },
                                    child: Text(
                                        translate('waitingRoom.observeGame'))))
                            : Container()
                      ],
                    )),
                Container(
                    margin: const EdgeInsets.only(left: 25, top: 29),
                    decoration: const BoxDecoration(
                        border:
                            Border(bottom: BorderSide(color: Colors.black))),
                    height: 1,
                    width: 750),
              ])),
          if (!gameToBuild.isPrivate || isOnObserver)
            SingleChildScrollView(
                child: Container(
                    height: 150,
                    padding: const EdgeInsets.only(left: 15),
                    child: ListView.builder(
                        itemCount: gameToBuild.observers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return PlayerItem(
                              name: waitingRoomService
                                  .gameInfo.observers[index].username,
                              type: GameParticipantType.Observer);
                        }))),
          if (gameToBuild.isPrivate && !isOnObserver)
            SingleChildScrollView(
                child: Container(
                    height: 150,
                    padding: const EdgeInsets.only(left: 15),
                    child: ListView.builder(
                        itemCount: waitingRoomService.waitingToJoin.length,
                        itemBuilder: (BuildContext context, int index) {
                          return WaitingUserItem(
                            user: waitingRoomService.waitingToJoin[index],
                            refreshDisplay: refreshDisplay,
                          );
                        }))),
          Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                      width: 150,
                      child: ElevatedButton(
                          onPressed: () {
                            if (waitingRoomService.isSelfGameCreator) {
                              waitingRoomService.cancelGame();
                            } else {
                              waitingRoomService.leaveGame();
                            }

                            widget.closeWaitingRoom();
                          },
                          child: Text(waitingRoomService.isSelfGameCreator
                              ? translate('waitingRoom.cancelGame')
                              : translate('waitingRoom.leaveGame')))),
                  waitingRoomService.isSelfGameCreator
                      ? SizedBox(
                          width: 150,
                          child: ElevatedButton(
                              onPressed: () {
                                if (gameToBuild.openSpots > 0) {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return SimpleDialog(
                                          title: gameToBuild.openSpots == 1
                                              ? Text(translate(
                                                  "waitingRoom.launchGame.singleRemainingSpot"))
                                              : Text(translate(
                                                  "waitingRoom.launchGame.remainingSpots",
                                                  args: {
                                                      "openSpot":
                                                          gameToBuild.openSpots
                                                    })),
                                          children: [LaunchGameDialog()],
                                        );
                                      });
                                } else {
                                  waitingRoomService.launchGame();
                                }
                              },
                              child: Text(translate('waitingRoom.launch'))))
                      : Container()
                ],
              ))
        ])));
  }

  addPlayerSpot() {
    setState(() {
      waitingRoomService.addSpot();
    });
  }

  removePlayerSpot() {
    setState(() {
      waitingRoomService.removeSpot();
    });
  }

  addBot() {
    setState(() {
      waitingRoomService.addVirtualPlayer();
    });
  }

  removeBot(String name) {
    setState(() {
      waitingRoomService.removeVirtualPlayer(name);
    });
  }

  updateGameToJoin() {
    setState(() {});
  }

  refreshDisplay() {
    setState(() {});
  }

  goToGamePage() {
    gameService.setWatchedPlayer(0);
    gameService.resetRackViewToModel();
    ClientChannelProxyService().currentChannelIndex = -2;
    Navigator.pushReplacementNamed(context, '/gamePage');
  }

  takePlayerSpot() {
    if (waitingRoomService.gameInfo.openSpots > 0) {
      waitingRoomService.takePlayerSpot();
    } else if (waitingRoomService.gameInfo.virtualPlayers.isNotEmpty) {
      waitingRoomService.takeBotSpot();
    }
  }
}
