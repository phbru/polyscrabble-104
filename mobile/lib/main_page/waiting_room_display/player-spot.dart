import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class PlayerSpot extends StatelessWidget {
  Function() removePlayerSpot;
  Function() addBot;
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  PlayerSpot({required this.removePlayerSpot, required this.addBot});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(left: 25),
          decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.grey))),
          height: 65,
          width: 750),
      SizedBox(
          height: 65,
          width: 750,
          child: Row(
            children: [
              Expanded(
                  flex: 9,
                  child: Row(children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: ElevatedButton(
                                onPressed: () {
                                  if (waitingRoomService.isSelfGameCreator) {
                                    if (waitingRoomService
                                                .gameInfo.players.length ==
                                            1 &&
                                        waitingRoomService.gameInfo.openSpots ==
                                            1) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                              content: Text(
                                        translate('waitingRoom.cantRemoveSpot'),
                                        textAlign: TextAlign.center,
                                      )));
                                    } else {
                                      removePlayerSpot();
                                    }
                                  }
                                },
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                      const CircleBorder()),
                                ),
                                child: Container(
                                    width: 50,
                                    height: 50,
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle),
                                    child: waitingRoomService.isSelfGameCreator
                                        ? const Center(
                                            child: Icon(Icons.close, size: 30))
                                        : Container())))),
                    Padding(
                        padding: const EdgeInsets.only(left: 25),
                        child: Text(translate('waitingRoom.empty'),
                            style: const TextStyle(
                                fontSize: 20, color: Colors.grey)))
                  ])),
              SizedBox(
                  width: 100,
                  child: waitingRoomService.isSelfGameCreator
                      ? ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                  const ContinuousRectangleBorder()),
                              padding: MaterialStateProperty.all(
                                  const EdgeInsets.all(1)),
                              overlayColor:
                                  MaterialStateProperty.all(Colors.grey[200])),
                          onPressed: () {
                            addBot();
                          },
                          child: Text(translate('addPlace.VP')))
                      : Container())
            ],
          ))
    ]);
  }
}
