import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class BotPlayer extends StatelessWidget {
  User virtualPlayer;
  Function(String) removeBot;
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  BotPlayer({super.key, required this.virtualPlayer, required this.removeBot});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(left: 25),
          decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.grey))),
          height: 65,
          width: 750),
      Container(
          height: 65,
          width: 750,
          child: Row(
            children: [
              Expanded(
                  flex: 9,
                  child: Row(children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: ElevatedButton(
                                onPressed: () {},
                                style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                        const CircleBorder()),
                                    overlayColor: MaterialStateProperty.all(
                                        Colors.grey[200])),
                                child: Container(
                                    width: 50,
                                    height: 50,
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle),
                                    child: const Center(
                                        child: Image(
                                      image: AssetImage(
                                          'assets/images/avatars/bot.png'),
                                    )))))),
                    Padding(
                        padding: const EdgeInsets.only(left: 25),
                        child: Text(virtualPlayer.username,
                            style: const TextStyle(fontSize: 20)))
                  ])),
              waitingRoomService.isSelfGameCreator
                  ? SizedBox(
                      width: 100,
                      child: ElevatedButton(
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                  const ContinuousRectangleBorder()),
                              padding: MaterialStateProperty.all(
                                  const EdgeInsets.all(1)),
                              overlayColor:
                                  MaterialStateProperty.all(Colors.grey[200])),
                          onPressed: () {
                            removeBot(virtualPlayer.username);
                          },
                          child: Text(translate('waitingRoom.kickOut'))))
                  : Container()
            ],
          ))
    ]);
  }
}

Widget getAvatar(String avatar) {
  String ext = avatar.split('.').last;

  if (ext == 'svg') {
    return SvgPicture.asset(avatar, width: 120);
  }

  String avatarBase64 = avatar.split(',').last;
  dynamic bytes = const Base64Decoder().convert(avatarBase64);

  return ClipOval(child: Image.memory(bytes, width: 120));
}
