import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/join-game-dialog.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class JoinGamePasswordDialog extends StatefulWidget {
  GameInfo game;
  Function() openWaitingRoom;
  JoinGamePasswordDialog(
      {super.key, required this.game, required this.openWaitingRoom});

  JoinGamePasswordDialogState createState() => JoinGamePasswordDialogState();
}

class JoinGamePasswordDialogState extends State<JoinGamePasswordDialog> {
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  TextEditingController textEditingPasswordController = TextEditingController();
  bool isPressed = false;

  String? get errorTextPassword {
    final text = textEditingPasswordController.value.text;
    if (isPressed) {
      if (text != widget.game.password) {
        return translate('waitingRoom.joinGame.invalidPassword');
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 300,
        height: 150,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: 300,
              child: TextField(
                controller: textEditingPasswordController,
                obscureText: true,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Theme.of(context).colorScheme.primary,
                    hintText: translate('loginPage.password'),
                    errorText: errorTextPassword),
              ),
            ),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                    shape: const ContinuousRectangleBorder(),
                    padding: const EdgeInsets.all(7),
                  ),
                  child: Text(translate('common.cancel'))),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      isPressed = true;
                      if (textEditingPasswordController.text ==
                          widget.game.password) {
                        isPressed = false;
                        Navigator.of(context).pop();
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return SimpleDialog(
                                title: Center(
                                    child: Text(translate(
                                        "waitingRoom.joinGame.join"))),
                                children: [
                                  JoinGameDialog(
                                      game: widget.game,
                                      openWaitingRoom: widget.openWaitingRoom)
                                ],
                              );
                            });
                      }
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    shape: const ContinuousRectangleBorder(),
                    padding: const EdgeInsets.all(7),
                  ),
                  child: Text(translate('common.confirm')))
            ])
          ],
        ));
  }
}
