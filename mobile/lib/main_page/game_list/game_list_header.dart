// ignore_for_file: sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class GameListHeader extends StatelessWidget {
  GameListHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
          margin: const EdgeInsets.only(left: 25),
          decoration: const BoxDecoration(
              border: Border(top: BorderSide(color: Colors.grey))),
          height: 55,
          width: 750),
      Container(
          height: 55,
          margin:
              const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Container(
                width: 75,
                child: Text(translate('mainPage.joinableGames.creator'),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontWeight: FontWeight.bold))),
            Container(
                width: 75,
                child: Text(translate('mainPage.joinableGames.accessType'),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontWeight: FontWeight.bold))),
            Container(
                width: 75,
                child: Text(translate('mainPage.joinableGames.timer'),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontWeight: FontWeight.bold))),
            Container(
                width: 100,
                child: Text(translate('mainPage.joinableGames.dictionary'),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontWeight: FontWeight.bold))),
            Container(width: 30, child: const Icon(Icons.person)),
            Container(width: 20, child: const Icon(Icons.computer)),
            Container(width: 20, child: const Icon(Icons.visibility)),
          ]))
    ]);
  }
}
