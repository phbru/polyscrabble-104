// ignore_for_file: sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/filter-games-dialog.dart';

class GameListFilter extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey;
  Function(String) setSearchString;
  Function() refreshDisplay;
  GameListFilter(
      {super.key,
      required this.setSearchString,
      required this.scaffoldKey,
      required this.refreshDisplay});

  @override
  GameListFilterState createState() => GameListFilterState();
}

class GameListFilterState extends State<GameListFilter> {
  final TextEditingController _textEditingSearchController =
      TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 60,
        width: 800,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Container(
            height: 50,
            width: 800,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  width: 50,
                  height: 50,
                  child: IconButton(
                      onPressed: () {
                        widget
                            .setSearchString(_textEditingSearchController.text);
                      },
                      icon: const Icon(
                        Icons.search,
                      ))),
              Container(
                  width: 500,
                  child: TextField(
                      controller: _textEditingSearchController,
                      onSubmitted: (data) {
                        widget.setSearchString(data);
                      },
                      textAlign: TextAlign.start,
                      decoration: InputDecoration(
                        fillColor: Theme.of(context).colorScheme.primary,
                        filled: true,
                        hintText: 'Search',
                      ))),
              IconButton(
                icon: Icon(Icons.settings),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return SimpleDialog(
                          title: Center(child: Text('Filtrer les parties')),
                          children: [
                            FilterGamesDialog(
                              scaffoldKey: widget.scaffoldKey,
                              refreshDisplay: widget.refreshDisplay,
                            )
                          ],
                        );
                      });
                },
              )
            ]),
          ),
        ));
  }
}
