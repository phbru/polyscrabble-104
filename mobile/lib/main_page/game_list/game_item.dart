// ignore_for_file: sized_box_for_whitespace, must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/join-game-dialog.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/join_game_password_dialog.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/waiting_creator_approval_dialog.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/timer_service_proxy.dart';
import 'package:poly_scrabble_mobile/socket.dart';

class GameItem extends StatelessWidget {
  GameInfo game;
  Function() openWaitingRoom;
  Socket socket = Socket();
  SelfUserProxyService selfUserService = SelfUserProxyService();

  GameItem(this.game, this.openWaitingRoom, {super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
            shape: MaterialStateProperty.all(const ContinuousRectangleBorder()),
            padding: MaterialStateProperty.all(const EdgeInsets.all(1)),
            overlayColor: MaterialStateProperty.all(Colors.grey[200])),
        onPressed: () {
          joinGame(context);
        },
        child: Container(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                      width: 75,
                      child: Text(game.creatorName,
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontWeight: FontWeight.bold))),
                  Container(
                      width: 75,
                      child: Text(
                          game.isPrivate
                              ? translate('mainPage.joinableGames.private')
                              : translate('mainPage.joinableGames.public'),
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontWeight: FontWeight.bold))),
                  Container(
                      width: 75,
                      child: Text(TimerServiceProxy().getTimeString(game.time),
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontWeight: FontWeight.bold))),
                  Container(
                      width: 100,
                      child: Text(game.dictionary,
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontWeight: FontWeight.bold))),
                  Container(
                      width: 30,
                      child: Text(
                          '${game.players.length}/${game.players.length + game.openSpots}',
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontWeight: FontWeight.bold))),
                  Container(
                      width: 20,
                      child: Text(game.virtualPlayers.length.toString(),
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontWeight: FontWeight.bold))),
                  Container(
                      width: 20,
                      child: Text(game.observers.length.toString(),
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontWeight: FontWeight.bold))),
                ])));
  }

  joinGame(BuildContext context) {
    if (game.isPrivate) {
      socket.io.emit('askToJoin', {selfUserService.user, game.id});

      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Center(
                  child:
                      Text(translate("mainPage.joinableGames.waitingCreator"))),
              children: [
                WaitingCreatorApprovalDialog(
                  gameId: game.id,
                )
              ],
            );
          });
    } else if (game.password.isNotEmpty) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Center(
                  child: Text(
                      translate('waitingRoom.joinGame.passwordProtected'))),
              children: [
                JoinGamePasswordDialog(
                    game: game, openWaitingRoom: openWaitingRoom)
              ],
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title:
                  Center(child: Text(translate("waitingRoom.joinGame.join"))),
              children: [
                JoinGameDialog(game: game, openWaitingRoom: openWaitingRoom)
              ],
            );
          });
    }
  }
}
