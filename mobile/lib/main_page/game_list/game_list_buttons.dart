import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/create_game_dialog.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/picker_data_timer.dart';

class GameListButton extends StatelessWidget {
  GlobalKey<ScaffoldState> scaffoldKey;
  Function() openWaitingRoom;

  GameListButton(
      {super.key, required this.scaffoldKey, required this.openWaitingRoom});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 75,
        child: Stack(children: [
          Container(
              margin: const EdgeInsets.only(left: 25),
              decoration: const BoxDecoration(
                  border: Border(top: BorderSide(color: Colors.grey))),
              height: 55,
              width: 750),
          SizedBox(
              height: 90,
              child: Row(
                children: [
                  const Spacer(),
                  ElevatedButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return SimpleDialog(
                                title: Center(
                                    child: Text(translate(
                                        'mainPage.createGame.createGame'))),
                                children: [
                                  CreateGameDialog(
                                    scaffoldKey: scaffoldKey,
                                    openWaitingRoom: openWaitingRoom,
                                  )
                                ],
                              );
                            });
                      },
                      child: Text(translate('mainPage.createGame.createGame'))),
                  const Spacer(),
                ],
              ))
        ]));
  }

  showPicker(BuildContext context) async {
    Picker picker = Picker(
        adapter: PickerDataAdapter<String>(
            pickerdata: const JsonDecoder().convert(PickerData)),
        changeToFirst: false,
        textAlign: TextAlign.left,
        textStyle: const TextStyle(
          color: Colors.blue,
        ),
        selectedTextStyle: const TextStyle(color: Colors.red),
        columnPadding: const EdgeInsets.all(8.0),
        onConfirm: (Picker picker, List value) {
          print(value.toString());
          print(picker.getSelectedValues());
        });
    picker.showBottomSheet(context);
  }
}
