// ignore_for_file: avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/game_item.dart';
import 'package:poly_scrabble_mobile/services/filter_games_service.dart';
import 'package:poly_scrabble_mobile/services/global_game_list_proxy_service.dart';

class GameList extends StatefulWidget {
  Function() openWaitingRoom;
  String searchString;
  GameList(
      {super.key, required this.openWaitingRoom, required this.searchString});
  @override
  GameListState createState() => GameListState();
}

class GameListState extends State<GameList> {
  GlobalGameListProxyService gamesListService = GlobalGameListProxyService();
  FilterGamesService filterGamesService = FilterGamesService();
  @override
  void initState() {
    super.initState();
    gamesListService.onPropUpdate('games', () {
      if (!mounted) return;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
          height: 460,
          child: ListView.builder(
              padding: const EdgeInsets.only(top: 8, bottom: 8),
              itemCount: gamesListService.games.length,
              itemBuilder: (BuildContext context, int index) {
                if (containsSearchString(gamesListService.games[index]) &&
                    containsFilter(gamesListService.games[index]) &&
                    !gamesListService.games[index].isGameStarted) {
                  return Container(
                    height: 70,
                    margin: const EdgeInsets.only(
                        left: 10, right: 10, top: 10, bottom: 10),
                    child: GameItem(
                        gamesListService.games[index], widget.openWaitingRoom),
                  );
                }
                return const SizedBox();
              })),
    ]);
  }

  containsSearchString(GameInfo game) {
    return widget.searchString.isEmpty ||
        game.creatorName.contains(widget.searchString);
    ;
  }

  containsFilter(GameInfo game) {
    bool dictionary = filterGamesService.dictionaryFilter == 'All' ||
        game.dictionary == filterGamesService.dictionaryFilter;
    bool playersNum = filterGamesService.playersNumFilter == 'All' ||
        game.openSpots.toString() == filterGamesService.playersNumFilter;
    bool timer = filterGamesService.timerFilter == -1 ||
        game.time == filterGamesService.timerFilter;
    return dictionary && playersNum && timer;
  }
}
