import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/dictionary_name.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/services/dictionary_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_game_list_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

import 'picker_data_timer.dart';

class CreateGameDialog extends StatefulWidget {
  Function openWaitingRoom;
  GlobalKey<ScaffoldState> scaffoldKey;
  CreateGameDialog({required this.scaffoldKey, required this.openWaitingRoom});
  @override
  CreateGameDialogState createState() => CreateGameDialogState();
}

class CreateGameDialogState extends State<CreateGameDialog> {
  List<String> playersNum = <String>['2', '3', '4'];
  List<String> accessType = <String>[
    translate('mainPage.createGame.publicAccess'),
    translate('mainPage.createGame.privateAccess')
  ];
  String dropdownPlayerNumValue = "";
  String dropdownAccessTypeValue = "";
  String dropdownDictionaryValue = "";
  final TextEditingController _textEditingPasswordController =
      TextEditingController();
  SelfUserProxyService selfUserService = SelfUserProxyService();
  GlobalGameListProxyService gameListService = GlobalGameListProxyService();
  Socket socket = Socket();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  DictionaryServiceProxy dictionaryService = DictionaryServiceProxy();
  String timer = '1:00';
  num timerNum = 1.00;
  bool isPressed = false;

  @override
  void initState() {
    super.initState();
    dropdownDictionaryValue = dictionaryService.dictionaryList.first.title;
    dropdownPlayerNumValue = playersNum.first;
    dropdownAccessTypeValue = accessType.first;
  }

  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Container(
            padding: const EdgeInsets.only(top: 30),
            width: 600,
            child: Column(children: [
              Container(
                  width: 500,
                  margin: const EdgeInsets.only(bottom: 20),
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1.5, color: Colors.grey))),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 500,
                            child: Text(
                              translate('mainPage.createGame.numberPlayers'),
                              textAlign: TextAlign.start,
                            )),
                        Container(
                            color: Theme.of(context).colorScheme.primary,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownPlayerNumValue,
                              icon: const Icon(
                                Icons.arrow_downward,
                              ),
                              elevation: 16,
                              underline: Container(),
                              onChanged: (String? value) {
                                setState(() {
                                  dropdownPlayerNumValue = value!;
                                });
                              },
                              items: playersNum.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Text(value)),
                                );
                              }).toList(),
                            ))
                      ])),
              Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  width: 500,
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1.5, color: Colors.grey))),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 500,
                            child: Text(
                              translate('mainPage.createGame.dictionary'),
                              textAlign: TextAlign.start,
                            )),
                        Container(
                            color: Theme.of(context).colorScheme.primary,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownDictionaryValue,
                              icon: const Icon(
                                Icons.arrow_downward,
                              ),
                              elevation: 16,
                              underline: Container(),
                              onChanged: (String? value) {
                                setState(() {
                                  dropdownDictionaryValue = value!;
                                });
                              },
                              items: dictionaryService.dictionaryList
                                  .map<DropdownMenuItem<String>>(
                                      (DictionaryName value) {
                                return DropdownMenuItem<String>(
                                  value: value.title,
                                  child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Text(value.title)),
                                );
                              }).toList(),
                            ))
                      ])),
              Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  width: 500,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 500,
                            child: Text(
                              translate('mainPage.createGame.timer'),
                              textAlign: TextAlign.start,
                            )),
                        Container(
                            width: 500,
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.primary,
                                border: const Border(
                                    bottom: BorderSide(color: Colors.grey))),
                            child: TextButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                      const ContinuousRectangleBorder()),
                                  padding: MaterialStateProperty.all(
                                      const EdgeInsets.all(1))),
                              onPressed: () {
                                showPicker(widget.scaffoldKey);
                              },
                              child: Container(
                                  padding: const EdgeInsets.only(left: 10),
                                  width: 500,
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(timer,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyLarge!
                                                    .color)),
                                        const Icon(
                                          Icons.arrow_downward,
                                        ),
                                      ])),
                            ))
                      ])),
              Container(
                  width: 500,
                  margin: const EdgeInsets.only(bottom: 20),
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1.5, color: Colors.grey))),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 500,
                            child: Text(
                              translate('mainPage.createGame.accessType'),
                              textAlign: TextAlign.start,
                            )),
                        Container(
                            color: Theme.of(context).colorScheme.primary,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownAccessTypeValue,
                              icon: const Icon(
                                Icons.arrow_downward,
                              ),
                              elevation: 16,
                              underline: Container(),
                              onChanged: (String? value) {
                                setState(() {
                                  dropdownAccessTypeValue = value!;
                                });
                              },
                              items: accessType.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Text(value)),
                                );
                              }).toList(),
                            ))
                      ])),
              if (dropdownAccessTypeValue ==
                  translate('mainPage.createGame.publicAccess'))
                Container(
                  width: 500,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 500,
                            child: Text(
                              translate('mainPage.createGame.password'),
                              textAlign: TextAlign.start,
                            )),
                        Container(
                          margin: const EdgeInsets.only(bottom: 20),
                          width: 500,
                          child: TextField(
                            obscureText: true,
                            controller: _textEditingPasswordController,
                            decoration: InputDecoration(
                              fillColor: Theme.of(context).colorScheme.primary,
                              filled: true,
                            ),
                          ),
                        ),
                      ]),
                ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      isPressed = true;
                    });

                    GameInfo gameInfo = GameInfo();
                    socket.io.emitWithAck(
                        'joinGame', {selfUserService.user.username, ''},
                        ack: (response) {
                      gameInfo.creatorName = selfUserService.user.username;
                      gameInfo.openSpots = int.parse(dropdownPlayerNumValue);
                      gameInfo.id = response;
                      gameInfo.time = timerNum;
                      gameInfo.password = _textEditingPasswordController.text;
                      gameInfo.dictionary = dropdownDictionaryValue;
                      gameInfo.isPrivate = dropdownAccessTypeValue ==
                              translate('mainPage.createGame.publicAccess')
                          ? false
                          : true;
                      waitingRoomService.createWaitingRoom(
                          gameInfo, selfUserService.user.id);
                      widget.openWaitingRoom();
                      Navigator.pop(context);
                    });
                  },
                  child:
                      Text(translate('mainPage.createGame.confirmCreation'))),
            ])));
  }

  showPicker(GlobalKey<ScaffoldState> scaffoldKey) async {
    Picker picker = Picker(
        adapter: PickerDataAdapter<String>(
            pickerdata: const JsonDecoder().convert(PickerData)),
        changeToFirst: false,
        textAlign: TextAlign.left,
        textStyle: const TextStyle(
          color: Colors.grey,
        ),
        cancelText: 'Annuler',
        confirmText: 'Confirmer',
        cancelTextStyle:
            TextStyle(color: Theme.of(context).textTheme.bodySmall?.color),
        confirmTextStyle:
            TextStyle(color: Theme.of(context).textTheme.bodySmall?.color),
        selectedTextStyle: const TextStyle(color: Colors.blue),
        columnPadding: const EdgeInsets.all(8.0),
        onConfirm: (Picker picker, List value) {
          List<String> values = picker.getSelectedValues() as List<String>;
          setState(() {
            timer = values[0] + ':' + values[1];
            timerNum = int.parse(values[0]) + int.parse(values[1]) / 60;
          });
        });
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return picker.makePicker();
        });
  }
}
