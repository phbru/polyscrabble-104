import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/dictionary_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/filter_games_service.dart';
import 'package:poly_scrabble_mobile/services/global_game_list_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/timer_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

import 'picker_data_timer.dart';

class FilterGamesDialog extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey;
  Function() refreshDisplay;
  FilterGamesDialog(
      {super.key, required this.scaffoldKey, required this.refreshDisplay});
  @override
  FilterGamesDialogState createState() => FilterGamesDialogState();
}

class FilterGamesDialogState extends State<FilterGamesDialog> {
  List<String> accessType = <String>[
    'Tous',
    'Public',
    'Privé',
  ];
  List<String> playersNum = <String>['2', '3', '4'];
  String dropdownPlayerNumValue = "";
  String dropdownAccessTypeValue = "";
  String dropdownDictionaryValue = "";
  SelfUserProxyService selfUserService = SelfUserProxyService();
  GlobalGameListProxyService gameListService = GlobalGameListProxyService();
  Socket socket = Socket();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  DictionaryServiceProxy dictionaryService = DictionaryServiceProxy();
  FilterGamesService filterGamesService = FilterGamesService();
  String timer = '1:00';
  num timerNum = 1.00;
  bool isCheckedTimer = false;
  bool isCheckedPlayers = false;

  @override
  void initState() {
    super.initState();
    dictionaryService.onPropUpdate('dictionaryList', (dictionaryList) {
      if (mounted) {
        setState(() {});
      }
    });
    dropdownDictionaryValue = filterGamesService.dictionaryFilter == 'All'
        ? 'Tous'
        : filterGamesService.dictionaryFilter;
    dropdownAccessTypeValue = filterGamesService.accessFilter == 'All'
        ? 'Tous'
        : filterGamesService.accessFilter;
    if (filterGamesService.timerFilter == -1) {
      isCheckedTimer = true;
    } else {
      timer = TimerServiceProxy().getTimeString(filterGamesService.timerFilter);
    }
    if (filterGamesService.playersNumFilter == 'All') {
      isCheckedPlayers = true;
      dropdownPlayerNumValue = playersNum.first;
    } else {
      dropdownPlayerNumValue = filterGamesService.playersNumFilter;
    }
  }

  @override
  Widget build(BuildContext context) {
    List<String> dictionaryList = ['Tous'];
    dictionaryList.addAll(dictionaryService.dictionaryList.map((e) => e.title));
    return Builder(
        builder: (context) => Container(
            padding: const EdgeInsets.only(top: 30),
            width: 600,
            child: Column(children: [
              Container(
                  width: 500,
                  margin: const EdgeInsets.only(bottom: 20),
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1.5, color: Colors.grey))),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const SizedBox(
                            width: 500,
                            child: Text(
                              'Accès',
                              textAlign: TextAlign.start,
                            )),
                        Container(
                            color: Theme.of(context).colorScheme.primary,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownAccessTypeValue,
                              icon: const Icon(
                                Icons.arrow_downward,
                              ),
                              elevation: 16,
                              underline: Container(),
                              onChanged: (String? value) {
                                setState(() {
                                  dropdownAccessTypeValue = value!;
                                });
                              },
                              items: accessType.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Text(value)),
                                );
                              }).toList(),
                            ))
                      ])),
              Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  width: 500,
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(width: 1.5, color: Colors.grey))),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 500,
                            child: Text(
                              translate('mainPage.createGame.dictionary'),
                              textAlign: TextAlign.start,
                            )),
                        Container(
                            color: Theme.of(context).colorScheme.primary,
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownDictionaryValue,
                              icon: const Icon(
                                Icons.arrow_downward,
                              ),
                              elevation: 16,
                              underline: Container(),
                              onChanged: (String? value) {
                                setState(() {
                                  dropdownDictionaryValue = value!;
                                });
                              },
                              items: dictionaryList
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Container(
                                      margin: const EdgeInsets.only(left: 10),
                                      child: Text(value)),
                                );
                              }).toList(),
                            ))
                      ])),
              Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  width: 500,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 500,
                            height: 20,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                      child: Text(
                                    translate('mainPage.createGame.timer'),
                                    textAlign: TextAlign.start,
                                  )),
                                  Container(
                                      padding: const EdgeInsets.only(left: 10),
                                      height: 35,
                                      width: 35,
                                      child: Checkbox(
                                          value: isCheckedTimer,
                                          onChanged: (value) {
                                            setState(() {
                                              isCheckedTimer = value!;
                                            });
                                          })),
                                  const SizedBox(
                                      child: Text(
                                    'Tous',
                                    textAlign: TextAlign.start,
                                  )),
                                ])),
                        Container(
                            margin: const EdgeInsets.only(bottom: 20),
                            width: 500,
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.primary,
                                border: const Border(
                                    bottom: BorderSide(color: Colors.grey))),
                            child: TextButton(
                              style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                      const ContinuousRectangleBorder()),
                                  padding: MaterialStateProperty.all(
                                      const EdgeInsets.all(1))),
                              onPressed: isCheckedTimer
                                  ? null
                                  : () {
                                      showPicker(widget.scaffoldKey);
                                    },
                              child: Container(
                                  padding: const EdgeInsets.only(left: 10),
                                  width: 500,
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(timer,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                color: isCheckedTimer
                                                    ? Theme.of(context)
                                                        .colorScheme
                                                        .primaryContainer
                                                    : Theme.of(context)
                                                        .textTheme
                                                        .bodyLarge!
                                                        .color)),
                                        Icon(
                                          Icons.arrow_downward,
                                          color: isCheckedTimer
                                              ? Theme.of(context)
                                                  .colorScheme
                                                  .primaryContainer
                                              : null,
                                        ),
                                      ])),
                            )),
                        Container(
                            width: 500,
                            decoration: const BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        width: 1.5, color: Colors.grey))),
                            child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SizedBox(
                                      width: 500,
                                      height: 20,
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                                child: Text(
                                              translate(
                                                  'mainPage.createGame.numberPlayers'),
                                              textAlign: TextAlign.start,
                                            )),
                                            Container(
                                                padding: const EdgeInsets.only(
                                                    left: 10),
                                                height: 35,
                                                width: 35,
                                                child: Checkbox(
                                                    value: isCheckedPlayers,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        isCheckedPlayers =
                                                            value!;
                                                      });
                                                    })),
                                            const SizedBox(
                                                child: Text(
                                              'Tous',
                                              textAlign: TextAlign.start,
                                            )),
                                          ])),
                                  Container(
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                      child: DropdownButton<String>(
                                        isExpanded: true,
                                        value: dropdownPlayerNumValue,
                                        hint: Container(
                                            margin:
                                                const EdgeInsets.only(left: 10),
                                            child: const Text('Two')),
                                        icon: const Icon(
                                          Icons.arrow_downward,
                                        ),
                                        elevation: 16,
                                        underline: Container(),
                                        onChanged: (String? value) {
                                          setState(() {
                                            dropdownPlayerNumValue = value!;
                                          });
                                        },
                                        items: isCheckedPlayers
                                            ? null
                                            : playersNum
                                                .map<DropdownMenuItem<String>>(
                                                    (String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Container(
                                                      margin:
                                                          const EdgeInsets.only(
                                                              left: 10),
                                                      child: Text(
                                                        value,
                                                      )),
                                                );
                                              }).toList(),
                                      ))
                                ]))
                      ])),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(translate('Annuler'))),
                ElevatedButton(
                    onPressed: () {
                      filterGamesService.accessFilter =
                          dropdownAccessTypeValue == 'Tous'
                              ? 'All'
                              : dropdownAccessTypeValue;
                      filterGamesService.dictionaryFilter =
                          dropdownDictionaryValue == 'Tous'
                              ? 'All'
                              : dropdownDictionaryValue;
                      filterGamesService.timerFilter =
                          isCheckedTimer ? -1 : timerNum;
                      filterGamesService.playersNumFilter =
                          isCheckedPlayers ? 'All' : dropdownPlayerNumValue;
                      widget.refreshDisplay();
                      Navigator.pop(context);
                    },
                    child: Text(translate('Appliquer'))),
              ])
            ])));
  }

  showPicker(GlobalKey<ScaffoldState> scaffoldKey) async {
    Picker picker = Picker(
        adapter: PickerDataAdapter<String>(
            pickerdata: const JsonDecoder().convert(PickerData)),
        changeToFirst: false,
        textAlign: TextAlign.left,
        textStyle: const TextStyle(
          color: Colors.grey,
        ),
        cancelText: 'Annuler',
        confirmText: 'Confirmer',
        cancelTextStyle:
            TextStyle(color: Theme.of(context).textTheme.bodySmall?.color),
        confirmTextStyle:
            TextStyle(color: Theme.of(context).textTheme.bodySmall?.color),
        selectedTextStyle: const TextStyle(color: Colors.blue),
        columnPadding: const EdgeInsets.all(8.0),
        onConfirm: (Picker picker, List value) {
          List<String> values = picker.getSelectedValues() as List<String>;
          setState(() {
            timer = '${values[0]}:${values[1]}';
            timerNum = int.parse(values[0]) + int.parse(values[1]) / 60;
          });
        });
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return picker.makePicker();
        });
  }
}
