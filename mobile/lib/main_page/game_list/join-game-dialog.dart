import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/enums/game_participant_type.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class JoinGameDialog extends StatelessWidget {
  GameInfo game;
  Function() openWaitingRoom;
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();

  JoinGameDialog(
      {super.key, required this.game, required this.openWaitingRoom});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 300,
        height: 100,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          ElevatedButton(
              onPressed: () {
                if (game.openSpots == 0) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return SimpleDialog(
                          children: [
                            SizedBox(
                                height: 100,
                                width: 100,
                                child: Center(
                                    child: Text(
                                  translate('waitingRoom.joinGame.noPlace'),
                                  style: const TextStyle(fontSize: 20),
                                  textAlign: TextAlign.center,
                                )))
                          ],
                        );
                      });
                } else {
                  waitingRoomService.joinGame(
                      game, GameParticipantType.RegularPlayer);
                  openWaitingRoom();
                  Navigator.pop(context);
                }
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate('waitingRoom.joinGame.player'))),
          ElevatedButton(
              onPressed: () {
                waitingRoomService.joinGame(game, GameParticipantType.Observer);
                openWaitingRoom();
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate('waitingRoom.joinGame.observer')))
        ]));
  }
}
