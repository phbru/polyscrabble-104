import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

class WaitingCreatorApprovalDialog extends StatelessWidget {
  Socket socket = Socket();
  SelfUserProxyService selfUserService = SelfUserProxyService();
  String gameId;
  WaitingCreatorApprovalDialog({super.key, required this.gameId});
  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Container(
            padding: const EdgeInsets.only(top: 10),
            height: 150,
            width: 500,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const SizedBox(
                  height: 60,
                  width: 60,
                  child: CircularProgressIndicator(
                    color: Colors.blue,
                    strokeWidth: 6,
                  ),
                ),
                ElevatedButton(
                    onPressed: () {
                      socket.io
                          .emit('cancelJoin', {selfUserService.user, gameId});
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      shape: const ContinuousRectangleBorder(),
                      padding: const EdgeInsets.all(7),
                    ),
                    child: Text(translate('common.cancel')))
              ],
            )));
  }
}
