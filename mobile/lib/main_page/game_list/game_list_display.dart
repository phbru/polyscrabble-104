import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/game_list.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/game_list_buttons.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/join-game-dialog.dart';
import 'package:poly_scrabble_mobile/services/global_game_list_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

import 'game_list_filter.dart';
import 'game_list_header.dart';

class GameListDisplay extends StatefulWidget {
  Function() openWaitingRoom;
  GlobalKey<ScaffoldState> scaffoldKey;
  GameListDisplay(
      {super.key, required this.openWaitingRoom, required this.scaffoldKey});
  @override
  GameListDisplayState createState() => GameListDisplayState();
}

class GameListDisplayState extends State<GameListDisplay> {
  String searchString = '';
  Socket socket = Socket();
  SelfUserProxyService selfUserService = SelfUserProxyService();

  @override
  void initState() {
    socket.io.on('acceptJoin', (invitedInfo) {
      if (mounted) {
        if (invitedInfo[0] == selfUserService.user.id) {
          if (Navigator.canPop(context)) {
            Navigator.of(context).pop();
          }

          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  title: Center(
                      child: Text(translate("waitingRoom.joinGame.join"))),
                  children: [
                    JoinGameDialog(
                        game: GlobalGameListProxyService()
                            .getGameInfo(invitedInfo[1]),
                        openWaitingRoom: widget.openWaitingRoom)
                  ],
                );
              });
          setState(() {});
        }
      }
    });

    socket.io.on('declineJoin', (invitedUserId) {
      if (mounted && invitedUserId == selfUserService.user.id) {
        if (Navigator.canPop(context)) {
          Navigator.of(context).pop();
        }
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
          translate('waitingRoom.joinGame.declineRequest'),
          textAlign: TextAlign.center,
        )));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
            decoration: const BoxDecoration(
                border: Border(
                    left: BorderSide(color: Colors.grey),
                    right: BorderSide(color: Colors.grey))),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  GameListFilter(
                    setSearchString: setSearchString,
                    scaffoldKey: widget.scaffoldKey,
                    refreshDisplay: refreshDisplay,
                  ),
                  GameListHeader(),
                  GameList(
                      openWaitingRoom: widget.openWaitingRoom,
                      searchString: searchString),
                  GameListButton(
                    scaffoldKey: widget.scaffoldKey,
                    openWaitingRoom: widget.openWaitingRoom,
                  ),
                ])));
  }

  setSearchString(String search) {
    setState(() {
      searchString = search;
    });
  }

  refreshDisplay() {
    setState(() {});
  }
}
