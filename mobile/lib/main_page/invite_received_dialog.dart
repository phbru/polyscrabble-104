import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/enums/game_participant_type.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

class InviteReceivedDialog extends StatelessWidget {
  GameInfo invitedGameInfo;
  Function() openWaitingRoom;
  bool inGamePage;
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  Socket socket = Socket();

  InviteReceivedDialog(
      {super.key,
      required this.invitedGameInfo,
      required this.openWaitingRoom,
      required this.inGamePage});

  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Container(
            padding: const EdgeInsets.only(top: 10),
            width: 500,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                    onPressed: () {
                      declineInvitation();
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      shape: const ContinuousRectangleBorder(),
                      padding: const EdgeInsets.all(7),
                    ),
                    child: Text(translate('common.decline'))),
                ElevatedButton(
                    onPressed: () {
                      acceptInvitation();
                      Navigator.pop(context);
                      if (!inGamePage) {
                        openWaitingRoom();
                      } else {
                        WaitingRoomProxyService().inWaitingRoom = true;
                        Navigator.of(context).pushReplacementNamed('/mainPage');
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      shape: const ContinuousRectangleBorder(),
                      padding: const EdgeInsets.all(7),
                    ),
                    child: Text(translate('common.accept')))
              ],
            )));
  }

  acceptInvitation() async {
    await waitingRoomService.joinGame(
        invitedGameInfo, GameParticipantType.RegularPlayer);
  }

  declineInvitation() {
    socket.io.emit(
        'declineInvite', {SelfUserProxyService().user.id, invitedGameInfo.id});
  }
}
