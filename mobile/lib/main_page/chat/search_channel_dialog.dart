import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/global_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

import '../../classes/channel.dart';

class SearchChannelDialog extends StatefulWidget {
  const SearchChannelDialog({super.key});

  @override
  SearchChannelDialogState createState() => SearchChannelDialogState();
}

class SearchChannelDialogState extends State<SearchChannelDialog> {
  final GlobalChannelProxyService _globalChannelService =
      GlobalChannelProxyService();
  final SelfUserProxyService _selfUserProxyService = SelfUserProxyService();
  final TextEditingController _channelNameField = TextEditingController();
  List<Channel> _filteredChannels = [];

  @override
  void initState() {
    super.initState();

    // Initial
    _filteredChannels = _globalChannelService.channels
      .where((Channel channel) =>
        channel.searchable &&
        channel.name
            .toLowerCase()
            .contains(_channelNameField.value.text.toLowerCase()))
      .toList();

    void setStateFunction() {
      if (!mounted) return;
      setState(() {
        _filteredChannels = _globalChannelService.channels
            .where((Channel channel) =>
                channel.searchable &&
                channel.name
                    .toLowerCase()
                    .contains(_channelNameField.value.text.toLowerCase()))
            .toList();
      });
    }

    _channelNameField.addListener(setStateFunction);
    _globalChannelService.onPropUpdate('channels', setStateFunction);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 600,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
            padding: const EdgeInsets.only(right: 20),
            width: 300,
            child: TextField(
              controller: _channelNameField,
              decoration: InputDecoration(
                fillColor: Theme.of(context).colorScheme.primary,
                filled: true,
                hintText: translate('channels.name'),
              ),
            ),
          ),
          Container(
              padding: const EdgeInsets.only(top: 20),
              height: 500,
              child: ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: _filteredChannels.length,
                  itemBuilder: (BuildContext context, int index) {
                    Channel channel = _filteredChannels[index];
                    return Container(
                        height: 45,
                        padding: const EdgeInsets.only(left: 150, right: 150),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(channel.name,
                                  style: const TextStyle(fontSize: 25)),
                              () {
                                bool alreadyJoined = false;
                                for (var user in channel.users) {
                                  if (user.id ==
                                      _selfUserProxyService.user.id) {
                                    alreadyJoined = true;
                                  }
                                }
                                if (!alreadyJoined) {
                                  return ElevatedButton(
                                      onPressed: () {
                                        // Pressed the button
                                        _globalChannelService
                                            .joinChannel(channel.id);
                                        setState(() {});
                                      },
                                      style: ElevatedButton.styleFrom(
                                        shape:
                                            const ContinuousRectangleBorder(),
                                        padding: const EdgeInsets.all(7),
                                      ),
                                      child: Text(
                                          translate('channels.joinChannel')));
                                }

                                return const SizedBox();
                              }()
                            ]));
                  }))
        ]));
  }
}
