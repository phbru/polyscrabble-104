// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/message.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

import '../../classes/user.dart';
import '../../profile/other_profile.dart';

class ChatChannelDisplay extends StatefulWidget {
  // This constructor must NOT be const (needs to be rebuild on parent setState).
  // Don't listen to IDE!
  final Function sendDMCallback;
  ChatChannelDisplay({super.key, required this.sendDMCallback});
  @override
  ChatChannelDisplayState createState() => ChatChannelDisplayState();
}

class ChatChannelDisplayState extends State<ChatChannelDisplay> {
  final ClientChannelProxyService _clientChannelService =
      ClientChannelProxyService();
  final ScrollController _scrollController = ScrollController();

  ChatChannelDisplayState() {
    _clientChannelService.onPropUpdate('joinedChannels', () {
      if (mounted) {
        setState(() {
          _clientChannelService.current.channel.messages = List.from(
              _clientChannelService.current.channel.messages.reversed);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Message> currentChannelChat =
        List.from(_clientChannelService.current.channel.messages.reversed);
    return SizedBox(
        height: 510,
        child: ListView.builder(
            reverse: true,
            itemCount: currentChannelChat.length,
            controller: _scrollController,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
                  decoration: BoxDecoration(
                      border:
                          Border(top: BorderSide(color: (Colors.grey[200])!))),
                  child: Row(children: [
                    ElevatedButton(
                        onPressed: () => showOnlineUserProfile(
                            currentChannelChat[index].user),
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<CircleBorder>(
                                const CircleBorder())),
                        child: SizedBox(
                            height: 40,
                            width: 40,
                            child: currentChannelChat[index].user.getAvatar())),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                              height: 30,
                              child: Align(
                                  child: Row(children: [
                                Container(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                        currentChannelChat[index].user.username,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold))),
                                Container(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                        currentChannelChat[index].timestamp,
                                        style: const TextStyle(
                                            color: Colors.grey)))
                              ]))),
                          Container(
                              padding: const EdgeInsets.only(left: 10),
                              width: WaitingRoomProxyService()
                                      .gameInfo
                                      .isGameStarted
                                  ? 200
                                  : 700,
                              child: Text(
                                currentChannelChat[index].output,
                                textAlign: TextAlign.left,
                              ))
                        ])
                  ]));
            }));
  }

  void showOnlineUserProfile(User user) {
    int fakeInt = 0;
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
                content: OtherProfile(
              user: user,
              sendDMCallback: widget.sendDMCallback,
              setWatchedPlayer: (fakeInt) {},
            )));
  }
}
