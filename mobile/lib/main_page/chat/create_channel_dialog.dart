import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/global_channel_proxy_service.dart';

class CreateChannelDialog extends StatelessWidget {
  final GlobalChannelProxyService _globalChannelService =
      GlobalChannelProxyService();
  final TextEditingController _channelNameField = TextEditingController();

  submit(BuildContext context, String value) {
    if (value.isEmpty) return;

    _globalChannelService
        .createChannel(_channelNameField.value.text)
        .then((newChannel) {
      _globalChannelService.joinChannel(newChannel.id);
    });

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 600,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
            padding: const EdgeInsets.only(right: 20),
            width: 300,
            child: TextField(
              controller: _channelNameField,
              onSubmitted: (String value) {
                if(value.length >= 25) {
                  showAlert(context);
                } else {
                  submit(context, value);
                }
              },
              decoration: InputDecoration(
                fillColor: Theme.of(context).colorScheme.primary,
                filled: true,
                hintText: translate('channels.name'),
              ),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                if(_channelNameField.value.text.length >= 25) {
                  showAlert(context);
                } else {
                  submit(context, _channelNameField.value.text);
                }
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(0),
              ),
              child: SizedBox(
                width: 100,
                height: 50,
                child: Center(
                    child: Text(translate('chat.send'),
                        style: const TextStyle(fontSize: 10))),
              ))
        ]));
  }

  void showAlert(context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text(translate('common.confirm')),
      onPressed: () { Navigator.of(context).pop(); },
    );

    AlertDialog alert = AlertDialog(
      title: Text(translate('channels.invalidNameTitle')),
      content: Text(translate('channels.invalidName')),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
