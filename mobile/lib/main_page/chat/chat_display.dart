// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/main_page/chat/chat_channel_display.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';

import 'chat_input.dart';

class ChatDisplay extends StatefulWidget {
  final Function sendDMCallback;
  Function() toggleGame;
  bool displayBack;
  ChatDisplay(
      {super.key,
      required this.sendDMCallback,
      required this.toggleGame,
      this.displayBack = true});
  @override
  ChatDisplayState createState() => ChatDisplayState();
}

class ChatDisplayState extends State<ChatDisplay> {
  final ClientChannelProxyService _clientChannelProxyService =
      ClientChannelProxyService();

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            border: Border(
                left: BorderSide(
                    color: Theme.of(context).colorScheme.primaryContainer),
                right: BorderSide(
                    color: Theme.of(context).colorScheme.primaryContainer))),
        child: Column(children: [
          Container(
              color: Theme.of(context).colorScheme.secondary,
              child: Row(
                children: [
                  if (widget.displayBack)
                    TextButton(
                      onPressed: (() {
                        widget.toggleGame();
                        ClientChannelProxyService().currentChannelIndex = -10;
                        widget.toggleGame();
                      }),
                      child: Transform.scale(
                        scaleX: -1,
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Theme.of(context).colorScheme.primaryContainer,
                        ),
                      ),
                    ),
                  const Spacer(),
                  SizedBox(
                      height: 60,
                      child: Center(
                          child: _clientChannelProxyService
                                  .current.channel.directMessage
                              ? Text(
                                  style: const TextStyle(fontSize: 25),
                                  _clientChannelProxyService
                                      .current.channel.name,
                                  textAlign: TextAlign.center)
                              : Container(
                                  padding: const EdgeInsets.only(
                                      top: 5.0, bottom: 5.0),
                                  child: Column(children: [
                                    Text(translate('channels.channelText'),
                                        style: TextStyle(
                                            color: Theme.of(context).hintColor,
                                            fontSize: 17),
                                        textAlign: TextAlign.center),
                                    Text(
                                        style: const TextStyle(fontSize: 25),
                                        translate(_clientChannelProxyService
                                            .current.channel.name),
                                        textAlign: TextAlign.center)
                                  ])))),
                  const Spacer(),
                ],
              )),
          Expanded(
              flex: 9,
              child: Container(
                  child: SingleChildScrollView(
                      child: Column(mainAxisSize: MainAxisSize.max, children: [
                ChatChannelDisplay(sendDMCallback: widget.sendDMCallback),
                ChatInput()
              ]))))
        ]));
  }
}
