// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

import '../../classes/message.dart';

class ChatInput extends StatelessWidget {
  final TextEditingController _textEditingController = TextEditingController();
  final GlobalChannelProxyService _globalChannelProxyService =
      GlobalChannelProxyService();
  final ClientChannelProxyService _clientChannelProxyService =
      ClientChannelProxyService();
  final SelfUserProxyService _selfUserProxyService = SelfUserProxyService();
  final focusNode = FocusNode();

  ChatInput({super.key}) {
    initializeDateFormatting();
  }

  @override
  Widget build(BuildContext context) {
    var focusNode = FocusNode();
    var childText = TextField(
      focusNode: focusNode,
      controller: _textEditingController,
      onSubmitted: (String value) {
        DateTime time = DateTime.now();
        if (value.isNotEmpty && value.replaceAll(" ", '').isNotEmpty) {
          Message msg = Message();
          msg.output = value;
          msg.timestamp = DateFormat("HH:mm:ss").format(time).toString();
          msg.user = _selfUserProxyService.user;

          _globalChannelProxyService.sendMessage(
              _clientChannelProxyService.current.channel.id, msg);
          _textEditingController.clear();
          focusNode.requestFocus();
        }
      },
      decoration: InputDecoration(
        filled: true,
        fillColor: Theme.of(context).colorScheme.primary,
        border: const OutlineInputBorder(),
        hintText: translate('chat.input'),
      ),
    );

    return Container(
        color: Theme.of(context).colorScheme.tertiary,
        height: 100,
        child: Center(
            child: SizedBox(
                width: 600,
                child: TextField(
                  keyboardType: TextInputType.text,
                  focusNode: focusNode,
                  controller: _textEditingController,
                  onSubmitted: (String value) {
                    DateTime time = DateTime.now();
                    if (value.isNotEmpty &&
                        value.replaceAll(" ", '').isNotEmpty) {
                      Message msg = Message();
                      msg.output = value;
                      msg.timestamp =
                          DateFormat("HH:mm:ss").format(time).toString();
                      msg.user = _selfUserProxyService.user;

                      _globalChannelProxyService.sendMessage(
                          _clientChannelProxyService.current.channel.id, msg);
                      _textEditingController.clear();
                      focusNode.requestFocus();
                    }
                  },
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Theme.of(context).colorScheme.secondary,
                    border: const OutlineInputBorder(),
                    hintText: translate('chat.input'),
                  ),
                ))));
  }
}
