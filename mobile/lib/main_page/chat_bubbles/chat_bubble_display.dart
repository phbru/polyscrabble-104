// ignore_for_file: avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/client_channel.dart';
import 'package:poly_scrabble_mobile/main_page/chat/create_channel_dialog.dart';
import 'package:poly_scrabble_mobile/main_page/chat_bubbles/add_chat_bubble.dart';
import 'package:poly_scrabble_mobile/main_page/chat_bubbles/chat_bubble.dart';
import 'package:poly_scrabble_mobile/main_page/chat_bubbles/search_chat_bubble.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_channel_proxy_service.dart';

import '../chat/search_channel_dialog.dart';

class ChatBubbleDisplay extends StatefulWidget {
  final Function toggleFuncChat;
  const ChatBubbleDisplay(this.toggleFuncChat, {super.key});

  @override
  ChatBubbleDisplayState createState() => ChatBubbleDisplayState();
}

class ChatBubbleDisplayState extends State<ChatBubbleDisplay> {
  final ClientChannelProxyService _clientChannelService =
      ClientChannelProxyService();
  final GlobalChannelProxyService _globalChannelService =
      GlobalChannelProxyService();

  ChatBubbleDisplayState() {
    // Always set current channel as read
    void callback() {
      if (mounted) setState(() => {});
      _globalChannelService
          .setChannelAsRead(_clientChannelService.current.channel.id);
    }

    _clientChannelService.onPropUpdate('joinedChannels', callback);
    _clientChannelService.onPropUpdate('gameChannel', callback);
    _clientChannelService.onPropUpdate('globalChannel', callback);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> chatBubbles = [];

    // Add channels
    for (int i = 0; i < _clientChannelService.joinedChannels.length; ++i) {
      ClientChannel channel = _clientChannelService.joinedChannels[i];
      chatBubbles.add(ChatBubble(
        clientChannel: channel,
        index: i,
        toggleFuncChat: widget.toggleFuncChat,
      ));
    }

    return SingleChildScrollView(
        child: SizedBox(
            height: 668,
            child: Column(children: [
              Container(
                margin: const EdgeInsets.only(top: 10, bottom: 10),
                child: ChatBubble(
                  clientChannel: _clientChannelService.globalChannel,
                  index: -1,
                  toggleFuncChat: widget.toggleFuncChat,
                  isGlobalChannel: true,
                ),
              ),
              const Divider(height: 0),
              Expanded(
                  flex: 6,
                  child: ListView.builder(
                      padding: const EdgeInsets.only(
                        top: 8.0,
                      ),
                      itemCount: chatBubbles.length,
                      itemBuilder: (BuildContext c, int i) => Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child: chatBubbles[i]))),
              const Divider(height: 0),
              addAndSearchBubbles(),
              const Divider(height: 0),
              if (_clientChannelService.gameChannel.length != 0)
                Container(
                    margin: const EdgeInsets.only(top: 8, bottom: 8),
                    child: ChatBubble(
                      clientChannel: _clientChannelService.gameChannel[0],
                      index: -2,
                      toggleFuncChat: widget.toggleFuncChat,
                      isGameChannel: true,
                    )),
            ])));
  }

  // Add search and add channel buttons
  addAndSearchBubbles() {
    return Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Container(
          margin: const EdgeInsets.only(top: 8),
          child: AddChatBubble(addChatFunc: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    title: Center(
                        child: Text(translate('channels.createChannel'))),
                    children: [CreateChannelDialog()],
                  );
                });
          })),
      Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: SearchChatBubble(searchChatFunc: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    title: Center(
                        child: Text(translate('channels.searchChannel'))),
                    children: const [SearchChannelDialog()],
                  );
                });
          })),
    ]);
  }
}
