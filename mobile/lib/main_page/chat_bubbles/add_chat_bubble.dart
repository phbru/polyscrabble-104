// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class AddChatBubble extends StatelessWidget {
  final Function addChatFunc;

  AddChatBubble({super.key, required this.addChatFunc});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        addChatFunc();
      },
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        padding: const EdgeInsets.all(0),
      ),
      child: Container(
          width: 60,
          height: 60,
          decoration: const BoxDecoration(shape: BoxShape.circle),
          child: const Center(child: Icon(Icons.add, size: 50))),
    );
  }
}
