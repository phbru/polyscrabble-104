// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class SearchChatBubble extends StatelessWidget {
  final Function searchChatFunc;

  const SearchChatBubble({super.key, required this.searchChatFunc});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        searchChatFunc();
      },
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        padding: const EdgeInsets.all(0),
      ),
      child: Container(
          width: 60,
          height: 60,
          decoration: const BoxDecoration(shape: BoxShape.circle),
          child: const Center(child: Icon(Icons.search, size: 50))),
    );
  }
}
