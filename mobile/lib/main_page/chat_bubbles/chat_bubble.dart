// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/client_channel.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

import '../../classes/user.dart';

class ChatBubble extends StatelessWidget {
  final ClientChannel clientChannel;
  final int index;
  final Function toggleFuncChat;
  final ClientChannelProxyService _clientChannelService =
      ClientChannelProxyService();
  final GlobalChannelProxyService _globalChannelService =
      GlobalChannelProxyService();
  final SelfUserProxyService _selfUserProxyService = SelfUserProxyService();
  final bool isGlobalChannel;
  final bool isGameChannel;
  late User dmUser;
  late bool selected;

  ChatBubble(
      {super.key,
      required this.clientChannel,
      required this.index,
      required this.toggleFuncChat,
      this.isGlobalChannel = false,
      this.isGameChannel = false}) {
    selected = _clientChannelService.currentChannelIndex == index;

    if (clientChannel.channel.directMessage) {
      if (clientChannel.channel.users.length == 2) {
        if (clientChannel.channel.users[0].id ==
            _selfUserProxyService.user.id) {
          // We are the first user, show second user avatar
          dmUser = clientChannel.channel.users[1];
        } else {
          // We are the second user, show first user avatar
          dmUser = clientChannel.channel.users[0];
        }
      } else {
        dmUser = User();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: () {
          _clientChannelService.currentChannelIndex = index;
          _globalChannelService
              .setChannelAsRead(_clientChannelService.current.channel.id);
          toggleFuncChat();
        },
        style: ElevatedButton.styleFrom(
          shape: CircleBorder(
            side: selected
                ? BorderSide(width: 3, color: Theme.of(context).primaryColor)
                : BorderSide.none,
          ),
        ),
        child: Stack(children: [
          Container(
            width: 60,
            height: 60,
            decoration: const BoxDecoration(shape: BoxShape.circle),
            child: Center(child: getBubbleContents()),
          ),
          if (isThereNewMessage())
            Positioned(
                left: 40,
                child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.grey))))
        ]));
  }

  Widget getBubbleContents() {
    if (isGlobalChannel) {
      return const Icon(Icons.groups, size: 35);
    }

    if (isGameChannel) {
      return const Icon(Icons.sports_esports, size: 50);
    }

    if (clientChannel.channel.directMessage) {
      return dmUser.getAvatar();
    }

    if (clientChannel.channel.name.isNotEmpty) {
      return Text(clientChannel.channel.name[0].toUpperCase(),
          style: const TextStyle(fontSize: 40));
    } else {
      return const Text('');
    }
  }

  bool isThereNewMessage() {
    if (clientChannel.newMessages &&
        _clientChannelService.currentChannelIndex != index) {
      return true;
    }
    return false;
  }
}
