import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/main_page/chat/chat_display.dart';
import 'package:poly_scrabble_mobile/main_page/chat_bubbles/chat_bubble_display.dart';
import 'package:poly_scrabble_mobile/main_page/game_list/game_list_display.dart';
import 'package:poly_scrabble_mobile/main_page/invite_received_dialog.dart';
import 'package:poly_scrabble_mobile/main_page/waiting_room_display/waiting_room_display.dart';
import 'package:poly_scrabble_mobile/profile/profile.dart';
import 'package:poly_scrabble_mobile/services/global_game_list_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

import 'online_users/online_users_display.dart';

class MainPageDisplay extends StatefulWidget {
  Function() updateTheme;
  MainPageDisplay({super.key, required this.updateTheme});
  @override
  MainPageDisplayState createState() => MainPageDisplayState();
}

class MainPageDisplayState extends State<MainPageDisplay> {
  final Socket socket = Socket();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final SelfUserProxyService _selfUserProxyService = SelfUserProxyService();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  bool showingDMs = false;
  bool toggle = true;
  @override
  initState() {
    super.initState();

    _selfUserProxyService.onPropUpdate('user', () {
      if (mounted) setState(() {});
    });

    socket.io.on('canceledGame', (i) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Text(
                translate('Game canceled'),
                textAlign: TextAlign.center,
              ),
              children: const [SizedBox(height: 10)],
            );
          });

      WaitingRoomProxyService().leaveGame();
      refresh();
    });

    socket.io.on('gameInviteReceived', (invite) {
      if (mounted) {
        if (invite[0] == _selfUserProxyService.user.id) {
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }
          GameInfo invitedGameInfo =
              GlobalGameListProxyService().getGameInfo(invite[1]);
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  title: Text(
                    translate('waitingRoom.gameInvite.invitationReceived',
                        args: {'name': invitedGameInfo.creatorName}),
                    textAlign: TextAlign.center,
                  ),
                  children: [
                    InviteReceivedDialog(
                      invitedGameInfo: invitedGameInfo,
                      openWaitingRoom: refresh,
                      inGamePage: false,
                    )
                  ],
                );
              });
        }
      }
    });

    socket.io.on('revokeInvite', (invitedUser) {
      if (_selfUserProxyService.user.id == invitedUser) {
        if (Navigator.canPop(context)) {
          Navigator.pop(context);
        }

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
          translate('waitingRoom.revokedInvite'),
          textAlign: TextAlign.center,
        )));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
              const Text("Scrabble"),
              ElevatedButton(
                onPressed: () {
                  showProfile();
                },
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<CircleBorder>(
                        const CircleBorder())),
                child: _selfUserProxyService.user.getAvatar(size: 60),
              ),
            ])),
        body: Row(children: [
          Expanded(
              flex: 3, child: OnlineUsersDisplay(sendDMCallback: toggleChat)),
          Expanded(
              flex: 7,
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Expanded(
                  flex: 9,
                  child: AnimatedSwitcher(
                    duration: const Duration(milliseconds: 300),
                    switchInCurve: Curves.easeIn,
                    switchOutCurve: Curves.fastOutSlowIn,
                    child: checkDisplay(context),
                    transitionBuilder: (child, animation) {
                      return SlideTransition(
                        position: Tween<Offset>(
                                begin: const Offset(1, 0),
                                end: const Offset(0, 0))
                            .animate(animation),
                        child: child,
                      );
                    },
                  ),
                ),
                Expanded(flex: 1, child: ChatBubbleDisplay(toggleChat))
              ]))
        ]));
  }

  void showProfile() {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Column(children: [
                Text(
                  translate('menuSettings.myProfile'),
                  style: const TextStyle(
                      fontSize: 25, fontWeight: FontWeight.bold),
                ),
                Container(height: 10),
                const Divider(height: 0),
              ]),
              content: Profile(updateTheme: widget.updateTheme),
            ));
  }

  void toggleGame() {
    setState(() {
      toggle = true;
    });
  }

  void toggleChat() {
    setState(() {
      toggle = false;
    });
  }

  switchChat() {
    setState(() {
      showingDMs = !showingDMs;
    });
  }

  Widget checkDisplay(BuildContext context) {
    if (toggle) {
      if (WaitingRoomProxyService().inWaitingRoom) {
        return WaitingRoomDisplay(
          closeWaitingRoom: refresh,
        );
      }
      return GameListDisplay(
          scaffoldKey: _scaffoldKey, openWaitingRoom: refresh);
    }
    return ChatDisplay(
        sendDMCallback: toggleChat, toggleGame: () => toggleGame());
  }

  refresh() {
    setState(() {});
  }
}
