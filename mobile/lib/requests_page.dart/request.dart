import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/client_friend_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/global_interaction_service_proxy.dart';

import '../classes/user.dart';

class Requests extends StatefulWidget {
  const Requests({super.key});

  @override
  State<Requests> createState() => _RequestsState();
}

class _RequestsState extends State<Requests> {
  ClientFriendServiceProxy clientFriendService = ClientFriendServiceProxy();
  @override
  void initState() {
    super.initState();

    clientFriendService.onPropUpdate('friends', () {
      if (mounted) setState(() {});
    });

    clientFriendService.onPropUpdate('friendsRequested', () {
      if (mounted) {
        setState(() {});
      }
    });

    clientFriendService.onPropUpdate('receivedInvites', () {
      if (mounted) {
        setState(() {});
      }
    });

    clientFriendService.onPropUpdate('notificationOn', () {
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        translate('requests.pending'),
        style: const TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.w500,
        ),
      ),
      Container(height: 20),
      requests(ClientFriendServiceProxy().receivedInvites, true),
      const Divider(height: 50),
      Text(
        translate('requests.sent'),
        textAlign: TextAlign.left,
        style: const TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
      ),
      Container(height: 20),
      requests(ClientFriendServiceProxy().friendsRequested, false),
    ]);
  }

  Widget requests(List<User> users, bool received) {
    return Flexible(
        child: ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: users.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          height: 50,
          margin: const EdgeInsets.all(5.0),
          padding: const EdgeInsets.all(3),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                ElevatedButton(
                    // TODO PLUGGER LE PROFIL
                    onPressed: (() => print('pressed')),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<CircleBorder>(
                            const CircleBorder())),
                    child: SizedBox(
                        height: 60,
                        width: 50,
                        child: users[index].getAvatar())),
                Text(users[index].username,
                    style: const TextStyle(
                        fontFamily: 'Arial',
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
                const Spacer(),
                if (received) receivedActions(users[index].id),
                if (received == false) requestedActions(users[index].id),
              ],
            ),
          ),
        );
      },
    ));
  }

  Widget receivedActions(String userId) {
    return Row(
      children: [
        ElevatedButton(
          onPressed: (() {
            GlobalInteractionServiceProxy().acceptInvite(userId);
            setState(() {});
          }),
          child: Text(translate('common.accept')),
        ),
        const SizedBox(width: 40),
        ElevatedButton(
            onPressed: (() {
              GlobalInteractionServiceProxy().refuseInvite(userId);
              setState(() {});
            }),
            child: Text(translate('common.decline'))),
      ],
    );
  }

  Widget requestedActions(String userId) {
    return Row(
      children: [
        ElevatedButton(
          onPressed: (() {
            GlobalInteractionServiceProxy().removeInvite(userId);
            setState(() {});
          }),
          child: Text(translate('common.cancel')),
        ),
      ],
    );
  }
}
