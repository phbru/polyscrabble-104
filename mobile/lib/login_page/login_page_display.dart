// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';
import 'package:poly_scrabble_mobile/services/communication_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';
import 'package:poly_scrabble_mobile/theme_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPageDisplay extends StatefulWidget {
  Function() updateTheme;
  LoginPageDisplay({super.key, required this.updateTheme});
  @override
  LoginPageDisplayState createState() => LoginPageDisplayState();
}

class LoginPageDisplayState extends State<LoginPageDisplay> {
  final TextEditingController _textEditingUsernameController =
      TextEditingController();
  final TextEditingController _textEditingPasswordController =
      TextEditingController();
  final CommunicationService _communicationService = CommunicationService();
  ThemeManager themeManager = ThemeManager();
  String username = "";
  String password = "";
  bool isPressed = false;
  bool isValid = false;
  final Socket socket = Socket();

  String? get _errorTextUsername {
    final text = _textEditingUsernameController.value.text;
    if (isPressed) {
      if (text.isEmpty) {
        return translate('loginPage.notNullUsername');
      }
      if (text.length < 3) {
        return translate('loginPage.tooShortUsername');
      }
    }
    return null;
  }

  String? get _errorTextPassword {
    final text = _textEditingPasswordController.value.text;
    if (isPressed) {
      if (text.isEmpty) {
        return translate("loginPage.notNullPassword");
      }
    }
    return null;
  }

  void loadTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (!prefs.containsKey('isDarkTheme')) {
      prefs.setBool('isDarkTheme', false);
    } else {
      ThemeManager().isDarkTheme = prefs.getBool('isDarkTheme')!;
      widget.updateTheme();
    }
  }

  @override
  initState() {
    super.initState();
    RoomService.init();
    loadTheme();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
              Text(
                translate('loginPage.login'),
                style: const TextStyle(fontSize: 20),
              ),
            ])),
        body: SingleChildScrollView(
            child: Column(children: [
          SizedBox(
              height: 200,
              child: Center(
                  child: Image(width: 400, height: 200, image: getLogo()))),
          Container(
              width: 700,
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.secondary,
                  borderRadius: const BorderRadius.all(Radius.circular(5))),
              child: Center(
                  child: Column(children: [
                () {
                  if (isValid) {
                    isValid = false;
                    return FutureBuilder<List>(
                        future: _communicationService.login(username, password),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return SizedBox(
                                height: 30,
                                child: Center(
                                    child:
                                        Text(translate("loginPage.loading"))));
                          } else {
                            if (snapshot.data == null) {
                              return SizedBox(
                                  height: 30,
                                  child: Center(
                                      child: Text(
                                          translate("loginPage.loading"))));
                            }
                            switch (snapshot.data![0]) {
                              case "Pass":
                                {
                                  socket.io.connect();
                                  socket.io.emit(
                                      'login', snapshot.data![1] as String);

                                  SchedulerBinding.instance
                                      .addPostFrameCallback((_) {
                                    Navigator.pushReplacementNamed(
                                        context, '/mainPage');
                                  });
                                  return Container(height: 30);
                                }
                              default:
                                return SizedBox(
                                    height: 30,
                                    child: Center(
                                        child: Text(
                                            translate(snapshot.data![0]),
                                            style: const TextStyle(
                                                color: Colors.red))));
                            }
                          }
                        });
                  } else {
                    return Container(height: 30);
                  }
                }(),
                Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(bottom: 10),
                        width: 500,
                        child: TextField(
                          controller: _textEditingUsernameController,
                          decoration: InputDecoration(
                              fillColor: Theme.of(context).colorScheme.primary,
                              filled: true,
                              hintText: translate('loginPage.username'),
                              errorText: _errorTextUsername),
                        ),
                      ),
                      SizedBox(
                        width: 500,
                        child: TextField(
                          controller: _textEditingPasswordController,
                          obscureText: true,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Theme.of(context).colorScheme.primary,
                              hintText: translate('loginPage.password'),
                              errorText: _errorTextPassword),
                        ),
                      ),
                      Container(
                          padding: const EdgeInsets.only(top: 20, bottom: 10),
                          width: 500,
                          child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  isPressed = true;
                                  if (_errorTextUsername != null ||
                                      _errorTextPassword != null) {
                                    return;
                                  }

                                  username =
                                      _textEditingUsernameController.value.text;
                                  password =
                                      _textEditingPasswordController.value.text;
                                  isValid = true;
                                });
                              },
                              child: Text(translate('loginPage.login')))),
                      const Divider(
                        thickness: 0.3,
                        indent: 100,
                        endIndent: 100,
                        color: Colors.black,
                      ),
                      Container(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          width: 500,
                          child: ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacementNamed(
                                    context, '/registerPage');
                              },
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color?>(
                                          Colors.blue[300])),
                              child: Text(translate('loginPage.signUp'))))
                    ])
              ])))
        ])));
  }

  AssetImage getLogo() {
    return themeManager.isDarkTheme
        ? const AssetImage('assets/logo_light.png')
        : const AssetImage('assets/logo_dark.png');
  }
}
