// ignore_for_file: unnecessary_this

import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/enums/game_participant_type.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_game_list_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

import '../Socket.dart';

class WaitingRoomProxyService extends RoomService {
  static final WaitingRoomProxyService _service =
      WaitingRoomProxyService.internal();
  Socket socket = Socket();
  GameInfo get gameInfo => reflectProp('gameInfo', GameInfo());
  bool inWaitingRoom = false;
  bool isSelfOnPlayerSpot = false;
  bool isSelfOnObserverSpot = false;
  bool isSelfGameCreator = false;
  bool isKicked = false;
  bool notificationOn = false;
  List<User> waitingToJoin = [];
  SelfUserProxyService selfUserService = SelfUserProxyService();
  GlobalGameListProxyService gameListService = GlobalGameListProxyService();

  WaitingRoomProxyService.internal() : super('WaitingRoomService') {
    socket.io.on('kickUserOut', (userName) {
      if (userName == selfUserService.user.username) {
        leaveGame();
        //this.dialog.open(KickedOutDialogComponent, { backdropClass: 'backdrop' });
      }
    });
  }

  factory WaitingRoomProxyService() {
    return _service;
  }

  createWaitingRoom(GameInfo gameInfo, String creatorId) {
    initiateWaitingRoom(gameInfo, creatorId);
    inWaitingRoom = true;
    isSelfOnPlayerSpot = true;
    isSelfOnObserverSpot = false;
    isSelfGameCreator = true;
  }

  initiateWaitingRoom(GameInfo _gameInfo, String _creatorId) => callServer(
      methodName: 'initiateWaitingRoom', args: [_gameInfo, _creatorId]);

  joinAsPlayer() => callServer(methodName: 'joinAsPlayer');

  launchGame() => callServer(methodName: 'launchGame', args: []);

  joinAsObserver() => callServer(methodName: 'joinAsObserver');

  addVirtualPlayer() => callServer(methodName: 'addVirtualPlayer');

  removeVirtualPlayer(String _username) =>
      callServer(methodName: 'removeVirtualPlayer', args: [_username]);

  addSpot() => callServer(methodName: 'addSpot', args: []);

  removeSpot() => callServer(methodName: 'removeSpot', args: []);

  checkIsUserInGame(String _username) =>
      callServer(methodName: 'checkIsUserInGame', args: [_username]);

  removeUser(String _userIdToKickout) =>
      callServer(methodName: 'removeUser', args: [_userIdToKickout]);

  addInvitedUser(String _userId) =>
      callServer(methodName: 'addInvitedUser', args: [_userId]);

  removeInvitedUser(String _userId) =>
      callServer(methodName: 'removeInvitedUser', args: [_userId]);

  joinGame(GameInfo game, GameParticipantType playerType) {
    inWaitingRoom = true;
    resetParameters();
    switch (playerType) {
      case GameParticipantType.RegularPlayer:
        {
          socket.io.emit('joinGame', {selfUserService.user.username, game.id});
          joinAsPlayer();
          isSelfOnPlayerSpot = true;
          isSelfOnObserverSpot = false;
          break;
        }
      case GameParticipantType.Observer:
        {
          GameServiceProxy().ourIndex = -1;
          socket.io.emit('joinGame', {selfUserService.user.username, game.id});
          joinAsObserver();
          isSelfOnObserverSpot = true;
          isSelfOnPlayerSpot = false;
          break;
        }
      default:
    }
  }

  leaveGame() {
    inWaitingRoom = false;
    removeUser(selfUserService.user.id);
    socket.io.emit('leaveGame', selfUserService.user);
    resetParameters();
  }

  resetParameters() {
    isSelfGameCreator = false;
    isSelfOnPlayerSpot = false;
    isSelfOnObserverSpot = false;
  }

  cancelGame() {
    inWaitingRoom = false;
    socket.io.emit('cancelGame', selfUserService.user);
    resetParameters();
  }

  kickUserOut(String userIdToKickout) {
    removeUser(userIdToKickout);
    socket.io.emit('kickUserOut', userIdToKickout);
  }

  getKicked() {
    if (isKicked == true) {
      inWaitingRoom = false;
      isKicked = !isKicked;
    }
    return isKicked;
  }

  takeBotSpot() {
    if (gameInfo.virtualPlayers.isEmpty) return;
    removeVirtualPlayer(this.gameInfo.virtualPlayers[0].username);
    isSelfOnPlayerSpot = true;
    isSelfOnObserverSpot = false;
    joinAsPlayer();
  }

  takePlayerSpot() {
    isSelfOnPlayerSpot = true;
    isSelfOnObserverSpot = false;
    joinAsPlayer();
  }

  takeObserverSpot() {
    isSelfOnObserverSpot = true;
    isSelfOnPlayerSpot = false;
    joinAsObserver();
  }

  fillEmptySpots() {
    for (int i = 0; i < gameInfo.openSpots; i++) {
      addVirtualPlayer();
    }
    launchGame();
  }

  removeWaitingToJoin(String userId) {
    for (User user in waitingToJoin) {
      if (user.id == userId) {
        waitingToJoin.remove(user);
        return;
      }
    }
  }

  inviteUser(String userId) {
    socket.io.emit('inviteUser', userId);
  }
}
