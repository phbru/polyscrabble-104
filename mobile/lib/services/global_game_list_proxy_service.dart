import 'package:poly_scrabble_mobile/classes/game_info.dart';

import '../room_service/room_service.dart';

class GlobalGameListProxyService extends RoomService {
  static final GlobalGameListProxyService _gamesListProxyService =
      GlobalGameListProxyService.internal();

  List<GameInfo> get games => reflectProp('games', [GameInfo()]);

  factory GlobalGameListProxyService() {
    return _gamesListProxyService;
  }

  addGame(GameInfo _gameInfo) => callServer(methodName: 'addGame', args: [_gameInfo]);

  updateGame(GameInfo _gameInfo) => callServer(methodName: 'updateGame', args: [_gameInfo]);

  getGameInfo(String gameId) {
    for (GameInfo game in games) {
      if (game.id == gameId) {
        return game;
      }
    }
  }

  GlobalGameListProxyService.internal() : super('GlobalGameListService');
}
