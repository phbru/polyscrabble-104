import 'package:poly_scrabble_mobile/classes/Case.dart';
import 'package:poly_scrabble_mobile/classes/matrix_coordinates.dart';
import 'package:poly_scrabble_mobile/classes/word_map.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

class BoardModelServiceProxy extends RoomService {
  static final BoardModelServiceProxy _boardModelServiceProxy =
      BoardModelServiceProxy.internal();
  Socket socket = Socket();
  List<List<Case>> get boardMatrix => reflectProp('boardMatrix', [
        [Case()]
      ]);
  MatrixCoordinates get startingCase =>
      reflectProp('startingCase', MatrixCoordinates());
  MatrixCoordinates startingCaseView = MatrixCoordinates();
  late Function() refreshDisplay;
  List<List<Case>> viewBoardMatrix =
      []; // Board state displayed by BoardViewService
  List<WordMap> wordsOnGrid = [];

  factory BoardModelServiceProxy() {
    return _boardModelServiceProxy;
  }

  BoardModelServiceProxy.internal() : super('BoardModelService') {
    deepCopyBoardMatrix(boardMatrix);
    socket.io.on('propUpdate_BoardModelService_boardMatrix', (newMatrix) {
      List<Case> boardMatrixLine = [];
      viewBoardMatrix = [];
      for (var row in newMatrix) {
        boardMatrixLine.clear();
        for (var caseToCopy in row) {
          boardMatrixLine.add(Case().createFromJson(caseToCopy));
        }
        viewBoardMatrix.add(List.from(boardMatrixLine));
      }
      refreshDisplay();
    });
  }

  isBoardEmpty() async =>
      callServer(methodName: 'isBoardEmpty', args: [], returnType: false);
  validateEntryTouchesPlacedLetters(WordMap _wordMap) => callServer(
      methodName: 'validateEntryTouchesPlacedLetters',
      args: [_wordMap],
      returnType: false);
  setSelectedCaseArrow(MatrixCoordinates _coords) async =>
      callServer(methodName: 'setSelectedCaseArrow', args: [_coords]);

  deepCopyBoardMatrix(dynamic matrixToCopy) {
    List<Case> boardMatrixLine = [];
    viewBoardMatrix = [];
    for (var row in matrixToCopy) {
      boardMatrixLine.clear();
      for (var caseToCopy in row) {
        boardMatrixLine.add(caseToCopy);
      }
      viewBoardMatrix.add(List.from(boardMatrixLine));
    }
  }

  deepCopyCase(Case sourceCase, Case destCase) {
    destCase.bonus = sourceCase.bonus;
    destCase.letter.character = sourceCase.letter.character;
    destCase.letter.weight = sourceCase.letter.weight;
    destCase.letter.isBlankLetter = sourceCase.letter.isBlankLetter;
  }
}
