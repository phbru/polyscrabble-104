import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

class ClientFriendServiceProxy extends RoomService {
  static final ClientFriendServiceProxy _service =
      ClientFriendServiceProxy.internal();
  SelfUserProxyService selfUserService = SelfUserProxyService();

  List<User> get friends => reflectProp('friends', [User()]);
  List<User> get friendsRequested => reflectProp('friendsRequested', [User()]);
  List<User> get receivedInvites => reflectProp('receivedInvites', [User()]);
  bool get notificationOn => reflectProp('notificationOn', false);

  removeNotification() => callServer(methodName: 'removeNotification');

  late bool viewNotificationOn;

  factory ClientFriendServiceProxy() {
    return _service;
  }

  ClientFriendServiceProxy.internal() : super('ClientFriendService') {
    viewNotificationOn = notificationOn;
  }

  checkIfRequestPossible(String userId) {
    if (userId == selfUserService.user.id) return false;

    if (isFriend(userId)) return false;

    if (friendRequestSent(userId)) return false;

    return true;
  }

  bool isFriend(String userId) {
    for (User friend in friends) {
      if (userId == friend.id) {
        return true;
      }
    }
    return false;
  }

  bool friendRequestSent(String userId) {
    for (User friend in friendsRequested) {
      if (userId == friend.id) {
        return true;
      }
    }
    return false;
  }

  bool friendRequestReceived(String userId) {
    for (User friend in receivedInvites) {
      if (userId == friend.id) {
        return true;
      }
    }
    return false;
  }
}
