import '../classes/channel.dart';
import '../classes/message.dart';
import '../room_service/room_service.dart';

class GlobalChannelProxyService extends RoomService {
  static final GlobalChannelProxyService _service
  = GlobalChannelProxyService.internal();
  GlobalChannelProxyService.internal() : super('GlobalChannelService');

  factory GlobalChannelProxyService() {
    return _service;
  }

  get channels => reflectProp('channels', [Channel()]);
  sendMessage(String channelId, Message message)
  => callServer(methodName: 'sendMessage', args: [channelId, message]);
  createChannel(String name) => callServer(methodName: 'createChannel', args: [name], returnType: Channel());
  createDM(String userId) => callServer(methodName: 'createDM', args: [userId], returnType: Channel());
  deleteChannel(String channelId) => callServer(methodName: 'deleteChannel', args: [channelId]);
  joinChannel(String channelId) => callServer(methodName: 'joinChannel', args: [channelId]);
  leaveChannel(String channelId) => callServer(methodName: 'leaveChannel', args: [channelId]);
  setChannelAsRead(String channelId) => callServer(methodName: 'setChannelAsRead', args: [channelId]);
}
