import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/classes/dictionary_name.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class DictionaryServiceProxy extends RoomService {
  static final DictionaryServiceProxy _service =
      DictionaryServiceProxy.internal();

  Socket socket = Socket();

  List<DictionaryName> get dictionaryList => reflectProp('dictionaryList', [DictionaryName()]);

  factory DictionaryServiceProxy() {
    return _service;
  }

  DictionaryServiceProxy.internal() : super('GlobalDictionaryService') {
    socket.io.on('propUpdate_GlobalDictionaryService_dictionaryList',
        (dictionaries) => {});
  }
}
