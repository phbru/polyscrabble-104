// ignore_for_file: file_names

import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class GlobalUserProxyService extends RoomService {
  static final GlobalUserProxyService _service =
      GlobalUserProxyService.internal();
  get onlineUsers => reflectProp('onlineUsers', [User()]);

  factory GlobalUserProxyService() {
    return _service;
  }

  getAvatar(String username, double size) {
    for (User user in onlineUsers) {
      if (user.username == username) {
        return user.getAvatar(size: size);
      }
    }
  }

  GlobalUserProxyService.internal() : super('GlobalUserService');
}
