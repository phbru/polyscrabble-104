import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_translate/flutter_translate.dart';

class NotificationService {
  int id = 0;
  bool showingNotifications = false;
  bool showingNewMessageNotification = false;

  static final NotificationService _notificationService =
    NotificationService._internal();
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  Future<void> init() async {
    const InitializationSettings initializationSettings =
    InitializationSettings(
        android: AndroidInitializationSettings('app_icon'),
        iOS: null,
        macOS: null);

    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  void enableNotifications() {
    showingNotifications = true;
  }

  void disableNotifications() {
    flutterLocalNotificationsPlugin.cancelAll();
    showingNewMessageNotification = false;
    showingNotifications = false;
  }

  // Only shows one new message notification at a time
  void notifyNewMessages() {
    if(!showingNotifications || showingNewMessageNotification) return;
    showingNewMessageNotification = true;

    const AndroidNotificationDetails androidDetails =
    AndroidNotificationDetails(
        'ChanID',
        'ChanName',
        channelDescription: 'MyChanDescription',
        importance: Importance(3),
        priority: Priority(0),
        playSound: true
    );

    flutterLocalNotificationsPlugin.show(
        id,
        translate('notifications.newMessageTitle'),
        translate('notifications.newMessageBody'),
        const NotificationDetails(android: androidDetails)
    );
    ++id;
  }

  // For other notifications
  void notify(String title, String text) {
    if(!showingNotifications) return;

    const AndroidNotificationDetails androidDetails =
      AndroidNotificationDetails(
          'ChanID',
          'ChanName',
          channelDescription: 'MyChanDescription',
          importance: Importance(3),
          priority: Priority(0),
          playSound: true
      );

    flutterLocalNotificationsPlugin.show(
        id,
        title,
        text,
        const NotificationDetails(android: androidDetails)
    );
    ++id;
  }
}
