import 'dart:convert';

import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/classes/letter.dart';
import 'package:poly_scrabble_mobile/classes/player.dart';
import 'package:poly_scrabble_mobile/classes/word_map.dart';
import 'package:poly_scrabble_mobile/enums/game_participant_type.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/timer_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';

class GameServiceProxy extends RoomService {
  static final GameServiceProxy _service = GameServiceProxy.internal();

  Socket socket = Socket();
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  TimerServiceProxy timer = TimerServiceProxy();
  SelfUserProxyService selfUserService = SelfUserProxyService();
  num get currentlyPlayingIndex => reflectProp(
      'currentlyPlayingIndex', 0); // index of player whose turn it is
  List<Player> get players => reflectProp('players', [Player()]);
  bool get isGameEnded => reflectProp('isGameEnded', false);
  List<num> get sumOfLettersLeftByPlayer =>
      reflectProp('sumOfLettersLeftByPlayer', [0, 0]);
  Player get winner => reflectProp('winner', Player());
  GameInfo get partialGameInfo => reflectProp('partialGameInfo', GameInfo());
  num ourIndex = 0;
  num watchedPlayer = 0;
  List<Letter> lettersView = [];
  List<String> copyOfPlayersForRematch = [];

  List<dynamic> avatarBytes = [];

  factory GameServiceProxy() {
    return _service;
  }

  GameServiceProxy.internal() : super('GameService') {
    socket.io.on('initiateClientPlayer', (e) {
      setOurIndexFromPlayerName();
      setAvatarBytes();
    });
  }

  setAvatarBytes() {
    avatarBytes.clear();
    for (Player player in players) {
      if (player.isBotPlayer) {
        avatarBytes.add(null);
      } else {
        String ext = player.user.avatar.split('.').last;

        if (ext == 'svg' || ext == 'png') {
          avatarBytes.add(null);
        } else {
          String avatarBase64 = player.user.avatar.split(',').last;
          dynamic bytes = const Base64Decoder().convert(avatarBase64);
          avatarBytes.add(bytes);
        }
      }
    }
  }

  joinWaitingRoom(String creatorName, num time, num openSpots) async {
    // Leave any games we had previously joined
    // this.socket.emit('leaveGame');

    socket.io
        .emitWithAck('joinGame', {'creatorName': creatorName, 'roomName': ''},
            ack: (response) {
      GameInfo gameInfo = GameInfo();
      gameInfo.creatorName = response.creatorName;

      gameInfo.id = response.id;
      gameInfo.time = response.time;
      gameInfo.openSpots = response.openSpots;
      gameInfo.isPrivate = response.isPrivate;
      gameInfo.players = response.players;
      gameInfo.observers = response.observers;
      gameInfo.virtualPlayers = response.virtualPlayers;

      waitingRoomService.joinGame(gameInfo, GameParticipantType.CreatorPlayer);
      socket.io.emit('addGame', gameInfo);
    });
  }

  getCurrentPlayerName() {
    return players[currentlyPlayingIndex.toInt()].user.username;
  }

  getOurPlayer() {
    return ourIndex >= players.length ? Player() : players[ourIndex.toInt()];
  }

  getOpponentIndex() {
    return ourIndex == 0 ? 1 : 0;
  }

  getOpponentPlayer() {
    return getOpponentIndex() >= players.length
        ? Player()
        : players[getOpponentIndex()];
  }

  updatePlayerLetters(num _playerIndex, List<Letter> _letters) => callServer(
      methodName: 'updatePlayerLetters', args: [_playerIndex, _letters]);
  executePass() => callServer(methodName: 'executePass', args: []);
  executeExchange(List<String> _lettersToExchange) =>
      callServer(methodName: 'executeExchange', args: [_lettersToExchange]);
  executePlace(WordMap _wordMapToPlace) =>
      callServer(methodName: 'executePlace', args: [_wordMapToPlace]);
  abandonGame(String _leavingPlayerId) =>
      callServer(methodName: 'abandonGame', args: [_leavingPlayerId]);
  terminateGame() => callServer(methodName: 'terminateGame', args: []);
  takeBotPlace(Player _botPlayer) =>
      callServer(methodName: 'takeBotPlace', args: [_botPlayer]);

  resetRackViewToModel() {
    if (ourIndex < 0 || players.length < ourIndex) return;
    List<Letter> letters = players[ourIndex.toInt()].currentLetters;
    if (letters != []) {
      lettersView = List.from(letters);
    }
  }

  setOurIndexFromPlayerName() {
    for (int i = 0; i < players.length; i++) {
      if (players[i].user.username == selfUserService.user.username) {
        ourIndex = i;
      }
    }
  }

  addLetter(Letter letter) {
    lettersView.add(letter);
  }

  removeLetter(String letter) {
    for (Letter token in lettersView) {
      if (token.character == letter) {
        lettersView.remove(token);
        return;
      }
    }
  }

  validateIsOurTurn() {
    return currentlyPlayingIndex == ourIndex;
  }

  isObserver() {
    for (Player player in players) {
      if (player.user.id == selfUserService.user.id) {
        return false;
      }
    }
    return true;
  }

  confirmAbandon() async {
    await abandonGame(players[ourIndex.toInt()].user.id);
  }

  setWatchedPlayer(num playerIndex) {
    watchedPlayer = playerIndex;
    updateObserverRack();
  }

  updateObserverRack() {
    lettersView = this.players[watchedPlayer.toInt()].currentLetters;
  }

  copyPlayersRematch() {
    for (Player player in players) {
      if (!player.isBotPlayer && player.user.id != selfUserService.user.id) {
        copyOfPlayersForRematch.add(player.user.id);
      }
    }
  }

  isPlaying() {
    return ourIndex == currentlyPlayingIndex;
  }

  copyPlayersForRematch() {
    copyOfPlayersForRematch = [];
    for (Player player in players) {
      if (!player.isBotPlayer && player.user.id != selfUserService.user.id) {
        copyOfPlayersForRematch.add(player.user.id);
      }
    }
  }
}
