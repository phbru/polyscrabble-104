import '../classes/channel.dart';
import '../classes/client_channel.dart';
import '../room_service/room_service.dart';

class ClientChannelProxyService extends RoomService {
  int currentChannelIndex = -10; // No channel selected

  static final ClientChannelProxyService _service =
      ClientChannelProxyService.internal();
  ClientChannelProxyService.internal() : super('ClientChannelService');

  factory ClientChannelProxyService() {
    return _service;
  }

  get joinedChannels => reflectProp('joinedChannels', [ClientChannel()]);
  get globalChannel => reflectProp('globalChannel', ClientChannel());
  get gameChannel => reflectProp('gameChannel', [ClientChannel()]);

  ClientChannel get current {
    if (currentChannelIndex == -1) return globalChannel;
    if (currentChannelIndex == -2) {
      if(gameChannel.length > 0) return gameChannel[0];

      // Reset to global channel
      currentChannelIndex = 1;
    }

    if(currentChannelIndex < 0) return ClientChannel();
    if (currentChannelIndex < joinedChannels.length) {
      return joinedChannels[currentChannelIndex];
    }
    return ClientChannel();
  }

  // Returns index of channel, or -1
  int findIndex(String channelId) {
    for(int i = 0; i < joinedChannels.length; ++i) {
      if(joinedChannels[i].channel.id == channelId) return i;
    }

    return -1;
  }
}
