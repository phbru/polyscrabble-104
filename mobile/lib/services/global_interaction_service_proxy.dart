// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class GlobalInteractionServiceProxy extends RoomService {
  GlobalInteractionServiceProxy.internal() : super('GlobalInteractionService');

  static final GlobalInteractionServiceProxy _service =
      GlobalInteractionServiceProxy.internal();

  factory GlobalInteractionServiceProxy() {
    return _service;
  }

  createFriendRequest(String _userId) => callServer(methodName: 'createFriendRequest', args: [_userId]);
  acceptInvite(String _userId) => callServer(methodName: 'acceptInvite', args: [_userId]);
  removeInvite(String _userId) => callServer(methodName: 'removeInvite', args: [_userId]);
  refuseInvite(String _userId) => callServer(methodName: 'refuseInvite', args: [_userId]);
  removeFriend(String _userId) => callServer(methodName: 'removeFriend', args: [_userId]);
}
