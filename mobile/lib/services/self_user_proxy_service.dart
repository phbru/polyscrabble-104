import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

import '../classes/user.dart';

class SelfUserProxyService extends RoomService {
  static final SelfUserProxyService _service = SelfUserProxyService.internal();
  User get user => reflectProp('user', User());
  Function() refreshDisplay = () {};
  Socket socket = Socket();

  factory SelfUserProxyService() {
    return _service;
  }

  SelfUserProxyService.internal() : super('SelfUserService') {
    socket.io.on('propUpdate_SelfUserService_user', (user) {
      refreshDisplay();
    });
  }
}
