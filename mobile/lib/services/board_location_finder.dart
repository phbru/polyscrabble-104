import 'dart:ui';

import 'package:poly_scrabble_mobile/classes/character_position.dart';
import 'package:poly_scrabble_mobile/classes/letter.dart';
import 'package:poly_scrabble_mobile/classes/widget_token.dart';
import 'package:poly_scrabble_mobile/classes/word_map.dart';
import 'package:poly_scrabble_mobile/enums/line.dart';
import 'package:poly_scrabble_mobile/services/board_model_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/token_service.dart';

class BoardLocationService {
  static final BoardLocationService _service = BoardLocationService.internal();
  BoardLocationService.internal();
  TokenService tokenService = TokenService();
  BoardModelServiceProxy boardModel = BoardModelServiceProxy();
  GameServiceProxy gameService = GameServiceProxy();
  List<CharacterPosition> currentWordPos = [];
  WordMap currentWordMap = WordMap('', []);

  factory BoardLocationService() {
    return _service;
  }

  getLine(num line) {
    switch (line) {
      case 1:
        return 'A';
      case 2:
        return 'B';
      case 3:
        return 'C';
      case 4:
        return 'D';
      case 5:
        return 'E';
      case 6:
        return 'F';
      case 7:
        return 'G';
      case 8:
        return 'H';
      case 9:
        return 'I';
      case 10:
        return 'J';
      case 11:
        return 'K';
      case 12:
        return 'L';
      case 13:
        return 'M';
      case 14:
        return 'N';
      case 15:
        return 'O';
      default:
        return 'Invalid';
    }
  }

  getCanvasPos(Offset offset) {
    Offset centerOffset = Offset(offset.dx, offset.dy);
    num canvasWidth = 700;
    num spacerWidth = ((canvasWidth + 40) / (17)).floor();
    num boardStarterPositionX = 150;
    num boardStarterPositionY = -40;
    return Offset(
        ((centerOffset.dx - boardStarterPositionX) / spacerWidth)
            .floor()
            .toDouble(),
        ((centerOffset.dy - boardStarterPositionY) / spacerWidth)
            .floor()
            .toDouble());
  }

  isInCanvas(Offset offset) {
    Offset pos = getCanvasPos(offset);
    return pos.dx > 0 && pos.dx < 16 && pos.dy > 0 && pos.dy < 16;
  }

  Offset getTokenWidgetPos(Offset offset) {
    Offset pos = getCanvasPos(offset);
    num spacerWidth = 43;
    return Offset(
        (pos.dx - 1) * spacerWidth + 23.5, (pos.dy - 1) * spacerWidth + 16);
  }

  getTokenWidget(Letter letter, Offset offset) {
    return WidgetToken(letter: letter, pos: getTokenWidgetPos(offset));
  }

  addToCurrentWord(Letter letter, Offset offset) {
    Offset pos = getCanvasPos(offset);
    CharacterPosition charPos =
        CharacterPosition(letter, getLine(pos.dy), pos.dx);
    currentWordPos.add(charPos);
  }

  updateCurrentWord(int index, Offset offset) {
    Offset pos = getCanvasPos(offset);
    currentWordPos[index].line = getLine(pos.dy);
    currentWordPos[index].column = pos.dx;
  }

  removeFromCurrentWord(int index) {
    currentWordPos.removeAt(index);
  }

  isValidPlace() async {
    if (currentWordPos.isEmpty) {
      return false;
    }
    if (currentWordPos.length == 1) {
      currentWordMap =
          WordMap(currentWordPos[0].letter.character, currentWordPos);
      return await validateEmptyBoardPlacement() &&
          await boardModel.validateEntryTouchesPlacedLetters(currentWordMap);
    }

    String firstLine = currentWordPos[0].line;
    String secondLine = currentWordPos[1].line;
    num column = currentWordPos[0].column;
    List<num> columns = [];
    List<num> lines = [];
    String word = '';

    if (firstLine == secondLine) {
      currentWordPos.sort((a, b) => a.column.compareTo(b.column));

      for (int i = 0; i < currentWordPos.length; i++) {
        word += currentWordPos[i].letter.character;
        if (currentWordPos[i].line != firstLine ||
            (i != currentWordPos.length - 1 &&
                currentWordPos[i].column + 1 != currentWordPos[i + 1].column &&
                boardModel
                        .viewBoardMatrix[Line.values.byName(firstLine).index]
                            [currentWordPos[i].column.toInt()]
                        .letter
                        .character ==
                    '?')) {
          return false;
        }
      }
    } else {
      currentWordPos.sort((a, b) => Line.values
          .byName(a.line)
          .index
          .compareTo(Line.values.byName(b.line).index));

      for (int i = 0; i < currentWordPos.length; i++) {
        word += currentWordPos[i].letter.character;
        if (currentWordPos[i].column != column ||
            (i != currentWordPos.length - 1 &&
                Line.values.byName(currentWordPos[i].line).index + 1 !=
                    Line.values.byName(currentWordPos[i + 1].line).index &&
                boardModel
                        .viewBoardMatrix[
                            Line.values.byName(currentWordPos[i].line).index +
                                1][column.toInt() - 1]
                        .letter
                        .character ==
                    '?')) {
          return false;
        }
      }
    }
    currentWordMap = WordMap(word, currentWordPos);
    return await validateEmptyBoardPlacement() &&
        await boardModel.validateEntryTouchesPlacedLetters(currentWordMap);
  }

  validateEmptyBoardPlacement() async {
    if (await boardModel.isBoardEmpty()) {
      if (currentWordPos.length < 2) return false;
      for (CharacterPosition letter in currentWordPos) {
        if (letter.column == 8 && letter.line == 'H') {
          return true;
        }
      }
      return false;
    }

    return true;
  }

  reset() {
    currentWordPos.clear();
  }
}
