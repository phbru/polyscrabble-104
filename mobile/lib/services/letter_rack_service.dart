import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';

class LetterRackService {
  static final LetterRackService _service = LetterRackService.internal();
  GameServiceProxy gameService = GameServiceProxy();

  factory LetterRackService() {
    return _service;
  }

  LetterRackService.internal();

  List<int> exchangeIndexes = [];

  exchange() async {
    List<dynamic> playerLetters = gameService.getOurPlayer().currentLetters;
    List<String> exchangeLetters = [];

    for (int index in exchangeIndexes) {
      exchangeLetters.add(playerLetters[index].character);
    }

    await gameService.executeExchange(exchangeLetters);
    gameService.resetRackViewToModel();

    exchangeIndexes.clear();
  }

  addToExchange(int index) {
    if (exchangeIndexes.contains(index)) {
      exchangeIndexes.remove(index);
    } else {
      exchangeIndexes.add(index);
    }
  }

  exchangeEmpty() {
    return exchangeIndexes.isEmpty;
  }
}
