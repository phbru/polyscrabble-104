import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/matrix_coordinates.dart';
import 'package:poly_scrabble_mobile/classes/widget_token.dart';
import 'package:poly_scrabble_mobile/services/board_model_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';

class TokenService {
  List<WidgetToken> tokens = [];
  static final TokenService _service = TokenService.internal();
  GameServiceProxy gameService = GameServiceProxy();
  BoardModelServiceProxy boardModelService = BoardModelServiceProxy();
  factory TokenService() {
    return _service;
  }

  TokenService.internal();

  addToken(WidgetToken token, Offset canvasPos) {
    if (tokens.isEmpty) {
      MatrixCoordinates posToSend = MatrixCoordinates();
      posToSend.line = canvasPos.dy - 1;
      posToSend.column = canvasPos.dx - 1;
      boardModelService.setSelectedCaseArrow(posToSend);
    }
    for (int i = 0; i < tokens.length; i++) {
      if (tokens[i].pos == token.pos) {
        gameService.addLetter(tokens[i].letter);
        tokens.removeAt(i);
        tokens.add(token);
        return;
      }
    }
    tokens.add(token);
  }

  updateToken(int index, Offset pos, Offset canvasPos) {
    if (index == 0) {
      boardModelService.setSelectedCaseArrow(MatrixCoordinates()
        ..line = canvasPos.dy - 1
        ..column = canvasPos.dx - 1);
    }
    for (int i = 0; i < tokens.length; i++) {
      if (tokens[i].pos == pos && i != index) {
        tokens[index].pos = pos;
        gameService.addLetter(tokens[i].letter);
        tokens.removeAt(i);
        return;
      }
    }
    tokens[index].pos = pos;
  }

  resetPass() {
    for (WidgetToken token in tokens) {
      gameService.lettersView.add(token.letter);
    }
    tokens.clear();
  }

  resetPlace() {
    tokens.clear();
  }
}
