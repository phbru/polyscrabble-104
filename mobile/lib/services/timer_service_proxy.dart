import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/classes/time.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class TimerServiceProxy extends RoomService {
  Socket socket = Socket();
  Time get timerLength => reflectProp('timerLength', Time(minutes: 0, seconds: 0));
  Time get mTime => reflectProp('mTime', Time(minutes: 0, seconds: 0));

  static final TimerServiceProxy _service = TimerServiceProxy.internal();

  factory TimerServiceProxy() {
    return _service;
  }

  TimerServiceProxy.internal() : super('TimerService');

  getTimeString(num timerValue) {
    if (timerValue % 1 == 0) return '${(timerValue).truncate()}:00';
    return '${(timerValue).truncate()}:30';
  }
}
