// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:poly_scrabble_mobile/classes/statistics.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

class CommunicationService {
  String
      baseUrl = "ec2-35-183-18-181.ca-central-1.compute.amazonaws.com:3000";


  setIPAdress(IPAdress) {}

  Future<List> register(String username, String avatar, String email,
      String password, String confirmedPassword) async {
    var dio = Dio();
    var url = Uri.http(baseUrl, "api/auth/signup");
    try {
      Response response = await dio.post(url.toString(), data: {
        'username': username,
        'avatar': avatar,
        'email': email,
        'password': password,
        'confirmPassword': confirmedPassword,
        'statistics': Statistics()
      });
      return ["Pass", response.data];
    } on DioError catch (e) {
      if (e.response?.data == 11000) {
        return ["Duplicate"];
      } else {
        return [e.response?.data];
      }
    }
  }

  Future<List> login(String username, String password) async {
    var url = Uri.http(baseUrl, "api/auth/login");
    var response = await http
        .post(url, body: {'username': username, 'password': password});

    switch (response.statusCode) {
      case 200:
        var id = await json.decode(response.body)["id"];
        return ["Pass", id];
      default:
        return [await json.decode(response.body)];
    }
  }

  Future<dynamic> updateUsername(User user, String newUsername) async {
    var dio = Dio();
    var url = Uri.http(baseUrl, "api/auth/updateUsername");

    try {
      Response response = await dio.post(url.toString(), data: {
        'user': SelfUserProxyService().user,
        'newUsername': newUsername
      });
      return response.data;
    } on DioError catch (e) {
      return e.response?.data;
    }
  }

  Future<dynamic> updateAvatar(User user, String avatarUrl) async {
    var dio = Dio();
    var url = Uri.http(baseUrl, "api/auth/updateAvatar");

    try {
      Response res = await dio.post(url.toString(), data: {
        'user': SelfUserProxyService().user,
        'avatarUrl': avatarUrl,
      });
      return res.statusCode;
    } on DioError catch (e) {
      return e.response?.data;
    }
  }
}
