import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/bonus_info.dart';
import 'package:poly_scrabble_mobile/classes/case_parameters.dart';
import 'package:poly_scrabble_mobile/classes/matrix_coordinates.dart';
import 'package:poly_scrabble_mobile/enums/bonus.dart';
import 'package:poly_scrabble_mobile/services/board_model_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/theme_manager.dart';

import '../classes/letter.dart';

class SizeUtils {
  static Size canvasSize = const Size(700, 700);

  static get width {
    return canvasSize.width;
  }

  static get height {
    return canvasSize.height;
  }
}

class GameBoard extends CustomPainter {
  static const AMOUNT_OF_COLUMNS = 16;
  static const AMOUNT_OF_LINES = 16;
  num canvasWidth = 0;
  num spacerWidth = 0;
  num boardStarterPosition = 0;
  num tokenWidth = 0;
  num coordinateCharSize = 0;
  num charProportion = 0.68;
  num weightProportion = 0.28;
  BoardModelServiceProxy boardModel = BoardModelServiceProxy();
  ThemeManager themeManager = ThemeManager();
  late Canvas _canvas;

  GameBoard({required Listenable repaint}) : super(repaint: repaint);
  initializeGrid() {
    canvasWidth = SizeUtils.width;
    spacerWidth = ((canvasWidth + 40) / (AMOUNT_OF_LINES + 1)).floor();
    boardStarterPosition = (0.55 * spacerWidth).floor();
    tokenWidth = 0.92 * spacerWidth;
    coordinateCharSize = 0.4 * spacerWidth;
  }

  placeToken(Letter letter, MatrixCoordinates coords) {
    Offset position = Offset(convertCoordToPosition(coords.column).toDouble(),
        convertCoordToPosition(coords.line).toDouble());
    num charSize = charProportion * spacerWidth;
    drawTokenBackground(position);
    drawCharacter(letter.character, position, charSize);
    drawTokenWeight(letter.weight, position);
  }

  drawCharacter(String character, Offset position, num charSize) {
    var textStyle = TextStyle(
      color: Colors.black,
      fontSize: charSize as double,
    );
    var textSpan = TextSpan(
      text: character,
      style: textStyle,
    );
    var textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: 50,
    );
    num charXCentralOffset = (spacerWidth - charSize) / 2;
    num charYCentralOffset = 0.1 * spacerWidth;
    Offset offset = Offset(
        position.dx + charXCentralOffset, position.dy + charYCentralOffset);
    textPainter.paint(_canvas, offset);
  }

  drawTokenWeight(num weight, Offset position) {
    num weightSize = weightProportion * spacerWidth;

    var textStyle = TextStyle(
      color: Colors.black,
      fontSize: weightSize as double,
    );
    var textSpan = TextSpan(
      text: weight.toString(),
      style: textStyle,
    );
    var textPainter = TextPainter(
      textAlign: TextAlign.right,
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: 50,
    );
    num weightXOffset = 0.7 * spacerWidth;
    num weightYOffset = 0.55 * spacerWidth;
    Offset offset =
        Offset(position.dx + weightXOffset, position.dy + weightYOffset);
    textPainter.paint(_canvas, offset);
  }

  drawInitialBoard() {
    final paint = Paint()
      ..color = themeManager.boardBackground
      ..style = PaintingStyle.fill;
    num rectangleTotalSize = spacerWidth * AMOUNT_OF_COLUMNS - 6;
    _canvas.drawRect(
        Rect.fromCenter(
            center: Offset(SizeUtils.height / 2.03, SizeUtils.width / 2.03),
            width: rectangleTotalSize.toDouble(),
            height: rectangleTotalSize.toDouble()),
        paint);
    drawBackgroundGrid();
    drawBonusSquares();
  }

  drawBackgroundGrid() {
    final paint = Paint()
      ..strokeWidth = 2
      ..color = themeManager.boardLineColor
      ..style = PaintingStyle.stroke;

    Path path = Path();

    for (var i = 0; i < AMOUNT_OF_LINES; i++) {
      path.moveTo(boardStarterPosition.toDouble(),
          convertCoordToPosition(i).toDouble());
      path.lineTo(convertCoordUnitAboveToPosition(AMOUNT_OF_LINES).toDouble(),
          convertCoordToPosition(i).toDouble());
    }

    for (var i = 0; i < AMOUNT_OF_COLUMNS; i++) {
      path.moveTo(convertCoordToPosition(i).toDouble(),
          boardStarterPosition.toDouble());
      path.lineTo(convertCoordToPosition(i).toDouble(),
          convertCoordUnitAboveToPosition(AMOUNT_OF_COLUMNS).toDouble());
    }
    _canvas.drawPath(path, paint);
  }

  drawColumnsNumber() {
    for (var i = 1; i < AMOUNT_OF_COLUMNS; i++) {
      var character = i.toString();
      var positionX = convertCoordUnitAboveToPosition(i);
      var positionY = 0;
      var textStyle = TextStyle(
        color: themeManager.boardLineColor,
        fontSize: coordinateCharSize.toDouble(),
      );
      var textSpan = TextSpan(
        text: character,
        style: textStyle,
      );
      final textPainter = TextPainter(
        text: textSpan,
        textDirection: TextDirection.ltr,
      );
      textPainter.layout(
        minWidth: 0,
        maxWidth: 20,
      );
      var charXCentralOffset = (spacerWidth - coordinateCharSize) / 2;
      var charYCentralOffset = 0.5 * spacerWidth;
      Offset offset = Offset(
          positionX + charXCentralOffset, positionY + charYCentralOffset);
      textPainter.paint(_canvas, offset);
    }
  }

  drawLineLetters() {
    for (var i = 0; i < AMOUNT_OF_LINES - 1; i++) {
      final character = String.fromCharCode(65 + i); // 65 is ascii for A
      const positionX = 10;
      final positionY = convertCoordToPosition(i);
      var textStyle = TextStyle(
        color: themeManager.boardLineColor,
        fontSize: coordinateCharSize.toDouble(),
      );
      var textSpan = TextSpan(
        text: character,
        style: textStyle,
      );
      final textPainter = TextPainter(
        text: textSpan,
        textDirection: TextDirection.ltr,
      );
      textPainter.layout(
        minWidth: 0,
        maxWidth: 20,
      );

      final charXCentralOffset = 0.3 * spacerWidth;
      const charYCentralOffset = 14;

      Offset offset = Offset(positionX + charXCentralOffset,
          positionY + charYCentralOffset.toDouble());
      textPainter.paint(_canvas, offset);
    }
  }

  drawBonusSquares() {
    for (var i = 0; i < boardModel.viewBoardMatrix.length; i++) {
      for (var j = 0; j < boardModel.viewBoardMatrix[i].length; j++) {
        CaseParameters caseParameter = determineCaseParameter(i, j);
        drawSquareBackground(j, i, caseParameter.color);
        drawBonusName(j, i, caseParameter.bonusInfo);
      }
    }
    drawCentralCase();
  }

  CaseParameters determineCaseParameter(num xCoordinate, num yCoordinate) {
    Bonus currentCaseBonus = boardModel
        .viewBoardMatrix[xCoordinate.toInt()][yCoordinate.toInt()].bonus;

    switch (currentCaseBonus) {
      case Bonus.LetterX2:
        return CaseParameters(
            BonusInfo('LETTRE', 'x2'), const Color.fromRGBO(120, 190, 207, 1));
      case Bonus.LetterX3:
        return CaseParameters(
            BonusInfo('LETTRE', 'x3'), const Color.fromRGBO(101, 194, 149, 1));
      case Bonus.WordX2:
        return CaseParameters(
            BonusInfo('MOT', 'x2'), const Color.fromRGBO(231, 157, 167, 1));
      case Bonus.WordX3:
        return CaseParameters(
            BonusInfo('MOT', 'x3'), const Color.fromRGBO(199, 118, 91, 1));
      case Bonus.Star:
        return CaseParameters(
            BonusInfo('', ''), const Color.fromRGBO(231, 157, 167, 1));
      default:
        return CaseParameters(
            BonusInfo('', ''),
            themeManager.isDarkTheme
                ? Color.fromRGBO(34, 34, 34, 1)
                : Color.fromRGBO(236, 234, 235, 1));
    }
  }

  drawSquareBackground(num line, num column, Color color) {
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;
    const PROPORTION_OF_SPACER_WIDTH = 0.95;
    var xValue = convertCoordToPosition(line.toDouble());
    var yValue = convertCoordUnitAboveToPosition(column.toDouble());
    Offset position = Offset(xValue, yValue);
    num xOffset = (spacerWidth - PROPORTION_OF_SPACER_WIDTH * spacerWidth) / 2;
    num yOffset = spacerWidth +
        (spacerWidth - PROPORTION_OF_SPACER_WIDTH * spacerWidth) / 2;
    _canvas.drawRect(
        Rect.fromLTWH(position.dx + xOffset, position.dy + yOffset,
            spacerWidth * 0.95, spacerWidth * 0.95),
        paint);
  }

  drawBonusName(num xCoordinate, num yCoordinate, BonusInfo bonusName) {
    var bonusFontSize = 0.27 * this.spacerWidth;
    var textStyle = TextStyle(
      color: Colors.black,
      fontSize: bonusFontSize.toDouble(),
    );

    num xOffsetProportion = 1;
    if (bonusName.word == 'LETTRE') {
      xOffsetProportion = 0.05;
    } else {
      xOffsetProportion = 0.18;
    }

    num xLetterOffset = xOffsetProportion * spacerWidth;
    num yLetterOffset = 0.2 * spacerWidth;
    var textSpan = TextSpan(
      text: bonusName.word,
      style: textStyle,
    );
    var textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: 50,
    );

    Offset offset = Offset(
      convertCoordToPosition(xCoordinate) + xLetterOffset,
      convertCoordToPosition(yCoordinate) + yLetterOffset,
    );
    textPainter.paint(_canvas, offset);

    num xNumberOffset = 0.35 * this.spacerWidth;
    num yNumberOffset = 0.5 * this.spacerWidth;
    offset = Offset(
      convertCoordToPosition(xCoordinate) + xNumberOffset,
      convertCoordToPosition(yCoordinate) + yNumberOffset,
    );

    textSpan = TextSpan(
      text: bonusName.bonusNumber,
      style: textStyle,
    );
    textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: 50,
    );
    textPainter.paint(_canvas, offset);
  }

  drawCentralCase() {
    const CENTRAL_COORD = 7;
    final xLetterOffset = 0.3 * spacerWidth;
    final yLetterOffset = 0.3 * spacerWidth - 8;
    final bonusFontSize = spacerWidth;

    var textStyle = TextStyle(
      color: const Color.fromRGBO(5, 5, 5, 1),
      fontSize: bonusFontSize.toDouble(),
    );
    var textSpan = TextSpan(
      text: '*',
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: 20,
    );
    Offset offset = Offset(
      convertCoordToPosition(CENTRAL_COORD).toDouble() + xLetterOffset,
      convertCoordToPosition(CENTRAL_COORD).toDouble() + yLetterOffset,
    );
    textPainter.paint(_canvas, offset);
  }

  drawTokenBackground(Offset position) {
    final paint = Paint()
      ..color = const Color.fromRGBO(117, 97, 63, 1)
      ..style = PaintingStyle.fill;
    final tokenCentralOffset = (spacerWidth - tokenWidth) / 2;
    final tokenYOffset = 0.05 * spacerWidth;
    _canvas.drawRect(
        Rect.fromLTWH(
            position.dx + tokenCentralOffset,
            position.dy + tokenYOffset,
            tokenWidth.toDouble(),
            tokenWidth.toDouble()),
        paint);

    paint.color = const Color.fromRGBO(235, 218, 171, 1);
    final tokenInteriorWidth = 0.9 * tokenWidth;
    final tokenInteriorXOffset = (spacerWidth - tokenInteriorWidth) / 2;
    final tokenInteriorYOffset = 1.1 * tokenYOffset;
    _canvas.drawRect(
        Rect.fromLTWH(
            position.dx + tokenInteriorXOffset,
            position.dy + tokenInteriorYOffset,
            tokenInteriorWidth,
            tokenInteriorWidth),
        paint);
  }

  convertCoordToPosition(num coord) {
    return coord * spacerWidth + boardStarterPosition;
  }

  // This function is for the various cases where a coord is happens to be one unit over what it represents
  convertCoordUnitAboveToPosition(num coord) {
    return (coord - 1) * spacerWidth + boardStarterPosition;
  }

  drawBoardContent() {
    drawInitialBoard(); // Draws empty board
    for (var i = 0; i < boardModel.viewBoardMatrix.length; i++) {
      for (var j = 0; j < boardModel.viewBoardMatrix[i].length; j++) {
        if (boardModel.viewBoardMatrix[j][i].letter.character != '?') {
          {
            placeToken(
                boardModel.viewBoardMatrix[j][i].letter,
                MatrixCoordinates()
                  ..line = j
                  ..column = i);
          }
        }
      }
    }
  }

  drawStartingCase(MatrixCoordinates coords) {
    drawSquareBackground(
        coords.column, coords.line, const Color.fromRGBO(238, 130, 238, 1));
    final bonusFontSize = 0.65 * spacerWidth;
    final xNumberOffset = 0.15 * spacerWidth;
    final yNumberOffset = 0.02 * spacerWidth;

    var textStyle = TextStyle(
      color: const Color.fromRGBO(5, 5, 5, 1),
      fontSize: bonusFontSize.toDouble(),
    );
    var textSpan = TextSpan(
      text: '✏️',
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: 20,
    );
    Offset offset = Offset(
      convertCoordToPosition(coords.column).toDouble() + xNumberOffset,
      convertCoordToPosition(coords.line).toDouble() + yNumberOffset,
    );
    textPainter.paint(_canvas, offset);
  }

  @override
  void paint(Canvas canvas, Size size) {
    initializeGrid();
    _canvas = canvas;

    drawBoardContent();
    if (boardModel.startingCaseView.column >= 0 &&
        boardModel.startingCaseView.line >= 0 &&
        !GameServiceProxy().validateIsOurTurn() &&
        !GameServiceProxy().isGameEnded) {
      drawStartingCase(boardModel.startingCaseView);
    }
  }

  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
