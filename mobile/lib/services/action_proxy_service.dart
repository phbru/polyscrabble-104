import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/classes/action_message.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class ActionServiceProxy extends RoomService {
  Socket socket = Socket();
  ActionMessage get currentAction => reflectProp('currentAction', ActionMessage());

  static final ActionServiceProxy _service = ActionServiceProxy.internal();

  factory ActionServiceProxy() {
    return _service;
  }

  ActionServiceProxy.internal() : super('ActionMessageService');
}
