import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/Socket.dart';
import 'package:poly_scrabble_mobile/classes/translate_preferences.dart';
import 'package:poly_scrabble_mobile/login_page/login_page_display.dart';
import 'package:poly_scrabble_mobile/profile/profile.dart';
import 'package:poly_scrabble_mobile/register_page/register_page_display.dart';
import 'package:poly_scrabble_mobile/services/client_channel_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/notification_service.dart';
import 'package:poly_scrabble_mobile/theme_manager.dart';

import 'game_page/game_page_display.dart';
import 'main_page/main_page_display.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);

  var delegate = await LocalizationDelegate.create(
      preferences: TranslatePreferences(),
      basePath: 'assets/i18n/',
      fallbackLocale: 'fr',
      supportedLocales: ['en', 'fr']);

  await NotificationService().init();
  runApp(LocalizedApp(delegate, const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> with WidgetsBindingObserver {
  List<String> onlineUsers = [];
  ThemeManager themeManager = ThemeManager();
  ClientChannelProxyService clientChannelProxyService
    = ClientChannelProxyService();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      if(state == AppLifecycleState.resumed) {
        NotificationService().disableNotifications();
      }
      else if(state == AppLifecycleState.inactive
          || state == AppLifecycleState.paused) {
        NotificationService().enableNotifications();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    clientChannelProxyService.onPropUpdate('joinedChannels', () {
      if (mounted) {
        NotificationService().notifyNewMessages();
      }
    });

    Socket().io.on('gameInviteReceived', (invite) {
      if (mounted) {
        NotificationService().notify(translate('notifications.newInviteTitle'),
            translate('notifications.newInviteBody'));
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LocalizationProvider(
        state: LocalizationProvider.of(context).state,
        child: MaterialApp(
          theme: themeManager.getTheme(),
          initialRoute: '/',
          routes: {
            '/': (context) => LoginPageDisplay(updateTheme: updateTheme),
            '/gamePage': (context) => GamePageDisplay(updateTheme: updateTheme),
            '/registerPage': (context) => const RegisterPageDisplay(),
            '/mainPage': (context) => MainPageDisplay(updateTheme: updateTheme),
            '/profile': (context) => Profile(updateTheme: updateTheme),
          },
        ));
  }

  updateTheme() => setState(() {});
}
