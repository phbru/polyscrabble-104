import 'dart:async';

import 'package:poly_scrabble_mobile/classes/Case.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';
import 'package:stack_trace/stack_trace.dart';

abstract class JsonObj {
  Map<String, dynamic> toJson();
  JsonObj createFromJson(dynamic json);

  JsonObj createFromJsonSafe(dynamic json) {
    JsonObj obj = createFromJson(json);
    assert(obj != this,
        'JsonObj createFromJson() should return a new object! Ask Carl for more info.');
    return obj;
  }
}

class RoomService {
  static const String _propUpdatePrefix = 'propUpdate';
  static const String _proxyCallPrefix = 'proxyCall';
  static const List _primitives = [num, int, double, String, bool];
  static final Map<String, dynamic> _socketEventMap = {};
  static final Socket _socket = Socket();
  static bool _initialized = false;

  final String _serverName;

  static init() {
    // Make sure we only do this once
    if (_initialized) return;

    // Register all received events in a static map; individual RoomServices will
    // access their relevant propUpdates from this map.
    // This even is guaranteed to run after all other events according to:
    // https://github.com/rikulo/socket_io_common/blob/6f18885c9a4a60f25b2f6eb38486dd2c77c919c6/lib/src/util/event_emitter.dart#L68
    _socket.io.onAny((String event, dynamic data) {
      _socketEventMap[event] = data;
    });
    _initialized = true;
  }

  RoomService(this._serverName) {
    init(); // Make sure we called this
  }

  // defaultValue is modified if it is a list
  static T _jsonToType<T>(T defaultValue, dynamic jsonData) {
    // Primitive
    if (_primitives.contains(T) || defaultValue is bool) return jsonData;

    if (defaultValue is List<List>) {
      // Case
      assert(defaultValue.isNotEmpty,
          "Provided defaultValue should not be an empty list! Ask Carl for more info.");

      List temp = [];
      for (var jsonE in jsonData) {
        temp.clear();
        for (var jsonV in jsonE) {
          temp.add((defaultValue[0][0] as JsonObj).createFromJsonSafe(jsonV));
        }
        defaultValue.add(List<Case>.from(temp));
      }
      defaultValue.removeAt(0);
      return defaultValue;
    } else if (defaultValue is List) {
      // Other lists
      assert(defaultValue.isNotEmpty,
          "Provided defaultValue should not be an empty list! Ask Carl for more info.");

      // Check to make sure we received valid list
      if (jsonData is List) {
        // This is ridiculous, modify defaultValue because templates suck
        for (var jsonE in jsonData) {
          defaultValue.add(_jsonToType(defaultValue[0], jsonE));
        }
      }

      defaultValue.removeAt(0);
      return defaultValue;
    }

    // Object
    assert(defaultValue is JsonObj,
        "Type must be primitive or JsonObj! Ask Carl for more info.");
    return (defaultValue as JsonObj).createFromJsonSafe(jsonData) as T;
  }

  void onPropUpdate(String propName, dynamic callback) {
    String propUpdateEvent = _getPropUpdateEvent(propName);
    _socket.io.on(propUpdateEvent, (dynamic data) {
      // Update _socketEventMap, since socket.onAny ALWAYS triggers after all
      // other events. We need to update our _socketEventMap before calling the client's
      // callback, so we need to do this here.
      _socketEventMap[propUpdateEvent] = data;
      callback();
    });
  }

  T reflectProp<T>(String propName, T defaultValue) {
    dynamic propJsonData = _getPropJsonData(propName);
    if (propJsonData == false) return defaultValue;

    return _jsonToType(defaultValue, propJsonData);
  }

  Future<T> callServer<T>({required String methodName, List<dynamic>? args, T? returnType}) {
    var completer = Completer<T>();

    ackFunc(jsonReturnedValue) {
      // Don't parse returned data if we don't expect any return value.
      if (returnType == null) {
        completer.complete();
      } else {
        assert(jsonReturnedValue != null,
            "CallServer error for '$methodName()': expected return value from server, but got null.");
        completer.complete(_jsonToType(returnType, jsonReturnedValue));
      }
    }

    args = args ?? [];
    _callServerBase(methodName, args, ackFunc);
    return completer.future;
  }

  String _getPropUpdateEvent(String propName) {
    return '${_propUpdatePrefix}_${_serverName}_$propName';
  }

  // Returns prop data (as JSON), or false if we never received
  // a propUpdated for specified prop.
  dynamic _getPropJsonData(String propName) {
    return _socketEventMap[_getPropUpdateEvent(propName)] ?? false;
  }

  void _callServerBase(
      String methodName, List<dynamic> argList, Function ackFunc) {
    String message = '${_proxyCallPrefix}_${_serverName}_$methodName';
    User ourUser = SelfUserProxyService().user;
    argList.insert(0, ourUser.id);

    if (methodName == '') throw 'CallServer error: could not get method name!';
    _socket.io.emitWithAck(message, argList, ack: ackFunc);
  }
}
