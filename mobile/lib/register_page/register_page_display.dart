// ignore_for_file: must_be_immutable

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/register_page/avatar_form.dart';
import 'package:poly_scrabble_mobile/services/communication_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

class RegisterPageDisplay extends StatefulWidget {
  const RegisterPageDisplay({super.key});
  @override
  RegisterPageDisplayState createState() => RegisterPageDisplayState();
}

class RegisterPageDisplayState extends State<RegisterPageDisplay> {
  final TextEditingController _textEditingUsernameController =
      TextEditingController();
  final TextEditingController _textEditingEmailController =
      TextEditingController();
  final TextEditingController _textEditingPasswordController =
      TextEditingController();
  final TextEditingController _textEditingConfirmedPasswordController =
      TextEditingController();
  final CommunicationService _communicationService = CommunicationService();
  String username = "";
  String email = "";
  String password = "";
  String confirmedPassword = "";
  bool isPressed = false;
  bool isValid = false;
  final Socket socket = Socket();
  String avatarUrl = '';

  String? get _errorTextUsername {
    final text = _textEditingUsernameController.value.text;
    if (isPressed) {
      if (text.isEmpty || text.length < 3 || text.length > 10) {
        return translate('registerPage.usernameRequirements');
      }
      if (!RegExp(r'^[A-Za-z0-9]+$').hasMatch(text) || username.contains(' ')) {
        return translate('registerPage.usernameRequirements');
      }
    }
    return null;
  }

  String? get _errorTextEmail {
    final text = _textEditingEmailController.value.text;
    if (isPressed) {
      if (text.isEmpty) {
        return translate("registerPage.emptyEmail");
      }
      if (!EmailValidator.validate(text)) {
        return translate("registerPage.invalidEmail");
      }
    }
    return null;
  }

  String? get _errorTextPassword {
    final text = _textEditingPasswordController.value.text;
    if (isPressed) {
      if (text.isEmpty) {
        return translate("registerPage.emptyPassword");
      }
      if (text.length < 8) {
        return translate('registerPage.passwordRequirements');
      }
      if (!RegExp(r'^[A-Za-z0-9]+$').hasMatch(text) || username.contains(' ')) {
        return translate('registerPage.passwordRequirements');
      }
    }

    return null;
  }

  String? get _errorTextConfirmedPassword {
    final pwd = _textEditingPasswordController.value.text;
    final confirmedPwd = _textEditingConfirmedPasswordController.value.text;
    if (isPressed) {
      if (confirmedPwd.isEmpty) {
        return translate('registerPage.emptyConfirmPassword');
      }
      if (pwd != confirmedPwd) {
        return translate('registerPage.differentPassword');
      }
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
              Text(
                translate('registerPage.register'),
                style: const TextStyle(fontSize: 20),
              ),
            ])),
        body: Center(
            child: SingleChildScrollView(
                child: Container(
                    width: 700,
                    decoration: BoxDecoration(
                        color: Theme.of(context).colorScheme.tertiary,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5))),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              margin: const EdgeInsets.only(
                                  top: 30, left: 25, right: 25),
                              width: 50,
                              height: 50,
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    shape:
                                        MaterialStateProperty.all<CircleBorder>(
                                            const CircleBorder())),
                                child: const Icon(Icons.arrow_back),
                                onPressed: () {
                                  SchedulerBinding.instance
                                      .addPostFrameCallback((_) {
                                    Navigator.pushReplacementNamed(
                                        context, '/');
                                  });
                                },
                              )),
                          Column(children: [
                            () {
                              if (isValid) {
                                isValid = false;
                                return FutureBuilder<List>(
                                    future: _communicationService.register(
                                        username,
                                        avatarUrl,
                                        email,
                                        password,
                                        confirmedPassword),
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.waiting) {
                                        return SizedBox(
                                            height: 30,
                                            child: Text(translate(
                                                'loginPage.loading')));
                                      }
                                      if (snapshot.data == null) {
                                        return SizedBox(
                                            height: 30,
                                            child: Center(
                                                child: Text(translate(
                                                    "loginPage.loading"))));
                                      }
                                      switch (snapshot.data![0]) {
                                        case "Duplicate":
                                          return SizedBox(
                                              height: 30,
                                              child: Center(
                                                  child: Text(
                                                      translate(
                                                          'registerPage.alreadyUsedUsername'),
                                                      style: const TextStyle(
                                                          color: Colors.red))));

                                        case "Pass":
                                          {
                                            socket.io.connect();
                                            socket.io.emit(
                                                'login', snapshot.data![1]);
                                            SchedulerBinding.instance
                                                .addPostFrameCallback((_) {
                                              Navigator.pushReplacementNamed(
                                                  context, '/mainPage');
                                            });
                                            return SizedBox(
                                                height: 30,
                                                child: Center(
                                                    child: Text(translate(
                                                        'registerPage.successfulRegistration'))));
                                          }
                                        default:
                                          return SizedBox(
                                              height: 30,
                                              child: Center(
                                                  child: Text(
                                                      translate(snapshot
                                                              .data![0]
                                                          ['errors'][0]['msg']),
                                                      style: const TextStyle(
                                                          color: Colors.red))));
                                      }
                                    });
                              } else {
                                return Container(height: 30);
                              }
                            }(),
                            Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    width: 500,
                                    child: TextField(
                                      controller:
                                          _textEditingUsernameController,
                                      decoration: InputDecoration(
                                          fillColor: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          filled: true,
                                          hintText: translate(
                                              'registerPage.username'),
                                          errorText: _errorTextUsername),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    width: 500,
                                    child: TextField(
                                      controller: _textEditingEmailController,
                                      decoration: InputDecoration(
                                          fillColor: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          filled: true,
                                          hintText:
                                              translate('registerPage.email'),
                                          errorText: _errorTextEmail),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    width: 500,
                                    child: TextField(
                                      controller:
                                          _textEditingPasswordController,
                                      obscureText: true,
                                      decoration: InputDecoration(
                                          fillColor: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          filled: true,
                                          hintText: translate(
                                              'registerPage.password'),
                                          errorText: _errorTextPassword),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    width: 500,
                                    child: TextField(
                                      controller:
                                          _textEditingConfirmedPasswordController,
                                      obscureText: true,
                                      decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Theme.of(context)
                                              .colorScheme
                                              .primary,
                                          hintText: translate(
                                              'registerPage.passwordConfirmation'),
                                          errorText:
                                              _errorTextConfirmedPassword),
                                    ),
                                  ),
                                  AvatarForm(setAvatarUrl),
                                  Container(
                                      padding: const EdgeInsets.only(
                                          top: 5, bottom: 10),
                                      width: 500,
                                      child: ElevatedButton(
                                          onPressed: () {
                                            setState(() {
                                              isPressed = true;
                                              if (_errorTextUsername != null ||
                                                  _errorTextEmail != null ||
                                                  _errorTextPassword != null ||
                                                  _errorTextConfirmedPassword !=
                                                      null) {
                                                return;
                                              }

                                              username =
                                                  _textEditingUsernameController
                                                      .value.text;
                                              email =
                                                  _textEditingEmailController
                                                      .value.text;
                                              password =
                                                  _textEditingPasswordController
                                                      .value.text;
                                              confirmedPassword =
                                                  _textEditingConfirmedPasswordController
                                                      .value.text;
                                              isValid = true;
                                            });
                                          },
                                          child: Text(translate(
                                              'registerPage.signUp')))),
                                ])
                          ])
                        ])))));
  }

  setAvatarUrl(String url) {
    if (url.split('.').last == 'svg') {
      avatarUrl = url;
    } else {
      avatarUrl = 'data:image/jpeg;base64,$url';
    }
  }
}
