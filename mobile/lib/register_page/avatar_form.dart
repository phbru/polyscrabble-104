import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:image_picker/image_picker.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';

class AvatarForm extends StatefulWidget {
  Function setAvatarUrl;
  bool isRegisterPage;
  AvatarForm(this.setAvatarUrl, {this.isRegisterPage = true, super.key});
  @override
  AvatarFormState createState() => AvatarFormState();
}

class AvatarFormState extends State<AvatarForm> {
  late File avatarImage;
  int chosenAvatar = -1;

  final SelfUserProxyService _selfUserProxyService = SelfUserProxyService();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 250,
        width: 500,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
                _getChosenAvatar(),
                SizedBox(
                    width: 290,
                    height: 160,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        chosenAvatar = 1;
                                      });
                                    },
                                    style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                CircleBorder>(
                                            const CircleBorder())),
                                    child: SvgPicture.asset(
                                        'assets/images/avatars/avatar-1.svg')),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        chosenAvatar = 2;
                                      });
                                    },
                                    style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                CircleBorder>(
                                            const CircleBorder())),
                                    child: SvgPicture.asset(
                                        'assets/images/avatars/avatar-2.svg')),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        chosenAvatar = 3;
                                      });
                                    },
                                    style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                CircleBorder>(
                                            const CircleBorder())),
                                    child: SvgPicture.asset(
                                        'assets/images/avatars/avatar-3.svg')),
                              ]),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        chosenAvatar = 4;
                                      });
                                    },
                                    style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                CircleBorder>(
                                            const CircleBorder())),
                                    child: SvgPicture.asset(
                                        'assets/images/avatars/avatar-4.svg')),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        chosenAvatar = 5;
                                      });
                                    },
                                    style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                CircleBorder>(
                                            const CircleBorder())),
                                    child: SvgPicture.asset(
                                        'assets/images/avatars/avatar-5.svg')),
                                ElevatedButton(
                                    onPressed: () {
                                      setState(() {
                                        chosenAvatar = 6;
                                      });
                                    },
                                    style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                CircleBorder>(
                                            const CircleBorder())),
                                    child: SvgPicture.asset(
                                        'assets/images/avatars/avatar-6.svg')),
                              ])
                        ]))
              ]),
              SizedBox(
                  //color: Colors.black,
                  height: 50,
                  width: 500,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 220,
                            child: ElevatedButton(
                                onPressed: () {
                                  _getFromGallery();
                                },
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        translate('registerPage.uploadAvatar'),
                                        textAlign: TextAlign.center,
                                      ),
                                      Image(
                                          width: 30,
                                          height: 30,
                                          image: getUploadIcon(context))
                                    ]))),
                        SizedBox(
                            width: 220,
                            child: ElevatedButton(
                                onPressed: () {
                                  _getFromCamera();
                                },
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        translate('registerPage.takePicture'),
                                        textAlign: TextAlign.center,
                                      ),
                                      const Icon(Icons.photo_camera)
                                    ])))
                      ]))
            ]));
  }

  AssetImage getUploadIcon(BuildContext context) {
    return Theme.of(context).colorScheme.primary ==
            const Color.fromRGBO(245, 245, 245, 1)
        ? const AssetImage('assets/images/avatars/upload_icon_light.png')
        : const AssetImage('assets/images/avatars/upload_icon_dark.png');
  }

  /// Get from gallery
  _getFromGallery() async {
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      maxWidth: 120,
      maxHeight: 120,
      imageQuality: 45,
    );
    if (pickedFile != null) {
      List<int> imageBytes = await pickedFile.readAsBytes();
      widget.setAvatarUrl(base64Encode(imageBytes));
      setState(() {
        chosenAvatar = 0;
        avatarImage = File(pickedFile.path);
      });
    }
  }

  /// Get from Camera
  _getFromCamera() async {
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 120,
      maxHeight: 120,
      imageQuality: 45,
    );
    if (pickedFile != null) {
      List<int> imageBytes = await pickedFile.readAsBytes();
      widget.setAvatarUrl(base64Encode(imageBytes));
      setState(() {
        chosenAvatar = 0;
        avatarImage = File(pickedFile.path);
      });
    }
  }

  _getChosenAvatar() {
    const double size = 90;
    if (!widget.isRegisterPage && chosenAvatar == -1) {
      return _selfUserProxyService.user.getAvatar(size: 90);
    } else if (widget.isRegisterPage && chosenAvatar == -1) {
      chosenAvatar = 1;
    }
    switch (chosenAvatar) {
      case 0:
        return Container(
            width: size,
            height: size,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: FileImage(avatarImage), fit: BoxFit.fill)));
      default:
        widget.setAvatarUrl('assets/images/avatars/avatar-$chosenAvatar.svg');
        return SvgPicture.asset(
          'assets/images/avatars/avatar-$chosenAvatar.svg',
          width: size,
          height: size,
        );
    }
  }
}
