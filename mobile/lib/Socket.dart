// ignore_for_file: avoid_print

import 'package:socket_io_client/socket_io_client.dart' as socket_io;

// Singleton for managing SocketIO connection
class Socket {
  static final Socket _socket = Socket.internal();
  static const String _addr =
      'http://ec2-35-183-18-181.ca-central-1.compute.amazonaws.com:3000';
  late socket_io.Socket io;

  factory Socket() {
    return _socket;
  }

  reconnect() {
    io = socket_io.io(
        _addr, socket_io.OptionBuilder().setTransports(['websocket']).build());
    io.connect();
    io.onConnect((_) {
      print('Connected to server.');
    });
  }

  Socket.internal() {
    io = socket_io.io(
        _addr, socket_io.OptionBuilder().setTransports(['websocket']).build());
    io.connect();
    io.onConnect((_) {
      print('Connected to server.');
    });
  }
}
