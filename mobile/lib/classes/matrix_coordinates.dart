import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class MatrixCoordinates extends JsonObj {
  num line = -1;
  num column = -1;
  MatrixCoordinates();

  @override
  Map<String, dynamic> toJson() {
    return {'line': line, 'column': column};
  }

  @override
  MatrixCoordinates createFromJson(dynamic json) {
    var matrixCoord = MatrixCoordinates();
    matrixCoord.line = json['line'];
    matrixCoord.column = json['column'];
    return matrixCoord;
  }
}
