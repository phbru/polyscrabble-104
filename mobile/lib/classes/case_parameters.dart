import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/bonus_info.dart';

class CaseParameters {
  BonusInfo bonusInfo;
  Color color;
  CaseParameters(this.bonusInfo, this.color);
}
