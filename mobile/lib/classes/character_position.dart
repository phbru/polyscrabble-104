import 'package:poly_scrabble_mobile/classes/letter.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class CharacterPosition extends JsonObj {
  Letter letter;
  String line;
  num column;
  CharacterPosition(this.letter, this.line, this.column);

  @override
  Map<String, dynamic> toJson() {
    return {
      'letter': letter,
      'line': line,
      'column': column,
    };
  }

  @override
  CharacterPosition createFromJson(dynamic json) {
    return CharacterPosition(
        Letter().createFromJson(json['letter']), json['line'], json['column']);
  }
}
