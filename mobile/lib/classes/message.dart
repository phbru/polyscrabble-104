import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class Message extends JsonObj {
  User user = User();
  String output = '';
  String timestamp = '';

  @override
  Map<String, dynamic> toJson() {
    return {'user': user.toJson(), 'output': output, 'timestamp': timestamp};
  }

  @override
  Message createFromJson(dynamic json) {
    Message msg = Message();

    msg.user = User().createFromJson(json['user']);
    msg.output = json['output'];
    msg.timestamp = json['timestamp'];

    return msg;
  }
}
