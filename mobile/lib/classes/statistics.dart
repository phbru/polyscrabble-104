import 'package:poly_scrabble_mobile/classes/time.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class Statistics extends JsonObj {
  num gamesPlayed = 0;
  num gamesWon = 0;
  num gamesLost = 0;
  num elo = 0;
  num rank = 0;
  num averagePointByGame = 0;
  Time averageTimePerGame = Time();

  @override
  Map<String, dynamic> toJson() {
    return {
      'gamesPlayed': gamesPlayed,
      'gamesWon': gamesWon,
      'gamesLost': gamesLost,
      'elo': elo,
      'rank': rank,
      'averagePointByGame': averagePointByGame,
      'averageTimePerGame': averageTimePerGame.toJson()
    };
  }

  @override
  Statistics createFromJson(dynamic json) {
    Statistics statistics = Statistics();
    if(json == null) return statistics;

    statistics.gamesPlayed = json['gamesPlayed'];
    statistics.gamesWon = json['gamesWon'];
    statistics.gamesLost = json['gamesLost'];
    statistics.elo = json['elo'];
    statistics.rank = json['rank'];
    statistics.averagePointByGame = json['averagePointByGame'];
    statistics.averageTimePerGame =
        Time(minutes: 0, seconds: 0).createFromJson(json['averageTimePerGame']);
    return statistics;
  }
}
