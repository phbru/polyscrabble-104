import 'package:poly_scrabble_mobile/classes/channel.dart';
import '../room_service/room_service.dart';

class ClientChannel extends JsonObj {
  bool newMessages = false;
  Channel channel = Channel();

  @override
  Map<String, dynamic> toJson() {
    return {
      'newMessages': newMessages,
      'channel': channel.toJson()
    };
  }

  @override
  ClientChannel createFromJson(dynamic json) {
    ClientChannel clientChannel = ClientChannel();
    clientChannel.newMessages = json['newMessages'];
    clientChannel.channel = Channel().createFromJson(json['channel']);
    return clientChannel;
  }
}
