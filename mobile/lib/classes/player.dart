import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

import 'letter.dart';

class Player extends JsonObj {
  User user = User();
  List<Letter> currentLetters = [];
  int points = 0;
  bool isBotPlayer = false;

  @override
  Map<String, dynamic> toJson() {
    List<dynamic> jsonLetters = [];
    for (var letter in currentLetters) {
      jsonLetters.add(letter.toJson());
    }

    return {
      'user': user.toJson(),
      'currentLetters': jsonLetters,
      'points': points,
      'isBotPlayer': isBotPlayer
    };
  }

  @override
  Player createFromJson(dynamic json) {
    var player = Player();
    player.user = User().createFromJson(json['user']);
    player.points = json['points'] as int;

    for (var jsonLetter in json['currentLetters']) {
      player.currentLetters.add(Letter().createFromJson(jsonLetter));
    }
    player.isBotPlayer = json['isBotPlayer'];

    return player;
  }
}
