import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class DictionaryName extends JsonObj {
  String title = '';
  String description = '';

  @override
  Map<String, dynamic> toJson() {
    return {'title': title, 'description': description};
  }

  @override
  DictionaryName createFromJson(dynamic json) {
    DictionaryName dictionary = DictionaryName();
    dictionary.title = json['title'];
    dictionary.description = json['description'];
    return dictionary;
  }
}
