import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class GameInfo extends JsonObj {
  String id = '';
  String creatorName = '';
  num time = 0;
  String dictionary = '';
  num openSpots = 0;
  String password = '';
  bool isPrivate = false;
  List<User> players = [];
  List<User> observers = [];
  List<User> virtualPlayers = [];
  List<User> invitedUsers = [];
  bool isGameStarted = false;

  @override
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'creatorName': creatorName,
      'time': time,
      'dictionary': dictionary,
      'openSpots': openSpots,
      'password': password,
      'isPrivate': isPrivate,
      'players': players,
      'observers': observers,
      'virtualPlayers': virtualPlayers,
      'invitedUsers': invitedUsers,
      'isGameStarted': isGameStarted
    };
  }

  @override
  GameInfo createFromJson(dynamic json) {
    var gameCreated = GameInfo();
    gameCreated.id = json['id'];
    gameCreated.creatorName = json['creatorName'];
    gameCreated.time = json['time'];
    gameCreated.isPrivate = json['isPrivate'];
    gameCreated.dictionary = json['dictionary'];
    gameCreated.openSpots = json['openSpots'];
    gameCreated.openSpots = json['openSpots'];
    gameCreated.password = json['password'];
    gameCreated.isGameStarted = json['isGameStarted'];

    for (var player in json['players']) {
      gameCreated.players.add(User().createFromJson(player));
    }
    for (var observer in json['observers']) {
      gameCreated.observers.add(User().createFromJson(observer));
    }
    for (var virtualPlayer in json['virtualPlayers']) {
      gameCreated.virtualPlayers.add(User().createFromJson(virtualPlayer));
    }
    for (var invitedUser in json['invitedUsers']) {
      gameCreated.invitedUsers.add(User().createFromJson(invitedUser));
    }

    return gameCreated;
  }
}
