import 'package:poly_scrabble_mobile/classes/user.dart';

import '../room_service/room_service.dart';
import 'message.dart';

class Channel extends JsonObj {
  String id = '';
  String name = '';
  List<User> users = [];
  User owner = User();
  bool searchable = true;
  bool directMessage = false;
  List<Message> messages = [];

  @override
  Map<String, dynamic> toJson() {
    List<dynamic> jsonUsers = [];
    for(User user in users) {
      jsonUsers.add(user.toJson());
    }

    List<dynamic> jsonMsgs = [];
    for(Message msg in messages) {
      jsonMsgs.add(msg.toJson());
    }

    return {
      'id': id,
      'name': name,
      'users': jsonUsers,
      'owner': owner.toJson(),
      'searchable': searchable,
      'directMessage': directMessage,
      'messages': jsonMsgs
    };
  }

  @override
  Channel createFromJson(dynamic json) {
    Channel channel = Channel();

    channel.id = json['id'];
    channel.name = json['name'];
    channel.owner = User().createFromJson(json['owner']);
    channel.searchable = json['searchable'];
    channel.directMessage = json['directMessage'];

    for(var jsonUser in json['users']) {
      channel.users.add(User().createFromJson(jsonUser));
    }
    for(var jsonMsg in json['messages']) {
      channel.messages.add(Message().createFromJson(jsonMsg));
    }

    return channel;
  }
}
