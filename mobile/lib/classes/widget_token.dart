import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/letter.dart';

class WidgetToken {
  Letter letter;
  Offset pos;

  WidgetToken({required this.letter, required this.pos});
}
