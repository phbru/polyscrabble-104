import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class Time extends JsonObj {
  num minutes;
  num seconds;

  Time({this.minutes = 0, this.seconds = 0});

  @override
  String toString() {
    String sec = seconds < 10 ? "0$seconds" : "$seconds";

    return '$minutes:$sec';
  }

  @override
  Map<String, dynamic> toJson() {
    return {'minutes': minutes, 'seconds': seconds};
  }

  @override
  Time createFromJson(dynamic json) {
    return Time(minutes: json['minutes'], seconds: json['seconds']);
  }
}
