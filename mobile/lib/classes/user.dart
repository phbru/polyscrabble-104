import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/statistics.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class User extends JsonObj {
  String username = '';
  String avatar = '';
  String email = '';
  String password = '';
  String confirmPassword = '';
  String id = '';
  Statistics statistics = Statistics();

  @override
  Map<String, dynamic> toJson() {
    return {
      'username': username,
      'avatar': avatar,
      'email': email,
      'password': password,
      'confirmPassword': confirmPassword,
      'id': id,
      'statistics': statistics
    };
  }

  @override
  User createFromJson(dynamic json) {
    User user = User();
    user.username = json['username'];
    user.avatar = json['avatar'];
    user.email = json['email'];
    user.password = json['password'];
    user.confirmPassword = json['confirmPassword'];
    user.id = json['id'];
    user.statistics = Statistics().createFromJson(json['statistics']);

    return user;
  }

  Widget getAvatar({double size = 120}) {
    String ext = avatar.split('.').last;
    if (avatar.isEmpty) return const SizedBox.shrink(); // Empty widget

    if (ext == 'svg') {
      return SvgPicture.asset(
        avatar,
        width: size,
        height: size,
      );
    } else if (ext == 'png') {
      return Image.asset(
        avatar,
        width: size,
        height: size,
      );
    }

    String avatarBase64 = avatar.split(',').last;
    dynamic bytes = const Base64Decoder().convert(avatarBase64);

    return ClipOval(
        child:
            Image.memory(bytes, width: size, height: size, fit: BoxFit.fill));
  }

  getStats() {
    return SizedBox(
        width: 250,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(translate('profile.statistics.gamePlayedMultiple')),
                Text(statistics.gamesPlayed.toString()),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(translate('profile.statistics.gameWonMultiple')),
                Text(statistics.gamesWon.toString()),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(translate('profile.statistics.gameLostMultiple')),
                Text(statistics.gamesLost.toString()),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(translate('profile.statistics.elo')),
                Text(statistics.elo.toString()),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(translate('profile.statistics.rank')),
                Text(statistics.rank.toString()),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(translate('profile.statistics.avgPoint')),
                Text(statistics.averagePointByGame.toStringAsFixed(2)),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(translate('profile.statistics.avgTime')),
                statistics.averageTimePerGame.toString().length >= 4
                    ? Text(statistics.averageTimePerGame
                        .toString()
                        .substring(0, 4))
                    : const Text('0:00')
              ],
            ),
          ],
        ));
  }
}
