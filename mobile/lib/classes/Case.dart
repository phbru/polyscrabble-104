import 'package:poly_scrabble_mobile/classes/letter.dart';
import 'package:poly_scrabble_mobile/enums/bonus.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class Case extends JsonObj {
  Letter letter = Letter();
  Bonus bonus = Bonus.Blank;

  Case();

  @override
  Map<String, dynamic> toJson() {
    return {'letter': letter.toJson(), 'bonus': bonus};
  }

  @override
  Case createFromJson(dynamic json) {
    var caseCreated = Case();
    caseCreated.letter = Letter().createFromJson(json['letter']);
    caseCreated.bonus = Bonus.values[json['bonus']];
    return caseCreated;
  }
}
