import 'package:poly_scrabble_mobile/classes/character_position.dart';

class WordMap {
  String word;
  List<CharacterPosition> map;

  WordMap(this.word, this.map);

  Map<String, dynamic> toJson() {
    return {
      'word': word,
      'map': map,
    };
  }
}
