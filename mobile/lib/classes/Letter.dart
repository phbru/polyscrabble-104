import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class Letter extends JsonObj {
  String character = '';
  int weight = 0;
  bool isBlankLetter = true;

  @override
  Map<String, dynamic> toJson() {
    return {
      'character': character,
      'weight': weight,
      'isBlankLetter': isBlankLetter,
    };
  }

  @override
  Letter createFromJson(dynamic json) {
    var letter = Letter();
    letter.character = json['character'];
    letter.weight = json['weight'];
    letter.isBlankLetter = json['isBlankLetter'];
    return letter;
  }
}
