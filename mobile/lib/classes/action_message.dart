import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/enums/action_code.dart';
import 'package:poly_scrabble_mobile/room_service/room_service.dart';

class ActionMessage extends JsonObj {
  User player = User();
  Enum actionCode = ActionCode.Pass;
  String action = '';
  String? param = '';

  ActionMessage();

  @override
  Map<String, dynamic> toJson() {
    return {
      'player': player.toJson(),
      'actionCode': actionCode,
      'action': action,
      'param': param
    };
  }

  @override
  ActionMessage createFromJson(dynamic json) {
    ActionMessage actionMessage = ActionMessage();

    actionMessage.player = User().createFromJson(json['player']);
    actionMessage.actionCode = ActionCode.values[json['actionCode']];
    actionMessage.action = json['action'];
    actionMessage.param = json['param'];

    return actionMessage;
  }
}
