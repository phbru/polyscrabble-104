import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/action_message.dart';
import 'package:poly_scrabble_mobile/classes/player.dart';
import 'package:poly_scrabble_mobile/classes/user.dart';
import 'package:poly_scrabble_mobile/enums/action_code.dart';
import 'package:poly_scrabble_mobile/game_page/abandon_dialog.dart';
import 'package:poly_scrabble_mobile/profile/other_profile.dart';
import 'package:poly_scrabble_mobile/services/action_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/timer_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';
import 'package:speech_bubble/speech_bubble.dart';

class PlayersDisplay extends StatefulWidget {
  Function(int) setWatchedPlayer;
  PlayersDisplay({super.key, required this.setWatchedPlayer});

  @override
  PlayersDisplayState createState() => PlayersDisplayState();
}

class PlayersDisplayState extends State<PlayersDisplay> {
  TimerServiceProxy timerServiceProxy = TimerServiceProxy();
  GameServiceProxy gameService = GameServiceProxy();
  Socket socket = Socket();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    timerServiceProxy.onPropUpdate('mTime', () {
      if (!mounted) return;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (gameService.isGameEnded)
          Container(
              margin: const EdgeInsets.only(top: 42, bottom: 10),
              child: Text(translate("gamePage.endGame.gameOver"))),
        if (!gameService.isGameEnded)
          Container(
              margin: const EdgeInsets.only(top: 42, bottom: 10),
              child: Text(timerServiceProxy.mTime.toString(),
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 30))),
        const SizedBox(
          width: 120,
          height: 20,
          child: Divider(),
        ),
        for (int i = 0; i < gameService.players.length; i++)
          displayPlayer(gameService.players[i], context, i),
        const Spacer(),
        Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: ElevatedButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return SimpleDialog(
                          title: Center(
                              child: Text(
                                  translate('gamePage.infoPanel.abandonGame'))),
                          children: [
                            AbandonDialog(
                              confirmAbandon: () async {
                                if (!gameService.isGameEnded) {
                                  if (!gameService.isObserver()) {
                                    await gameService.confirmAbandon();
                                  }

                                  socket.io.emit('leaveGame');
                                }

                                SchedulerBinding.instance
                                    .addPostFrameCallback((_) {
                                  WaitingRoomProxyService().inWaitingRoom =
                                      false;
                                  Navigator.pop(context);
                                  Navigator.pushReplacementNamed(
                                      context, '/mainPage');
                                });
                              },
                            )
                          ],
                        );
                      });
                },
                child: Text(translate(
                    gameService.isObserver() || gameService.isGameEnded
                        ? 'gamePage.infoPanel.leave'
                        : 'gamePage.infoPanel.abandon')))),
      ],
    );
  }

  Widget displayPlayer(Player player, BuildContext context, int index) {
    FontWeight weight = FontWeight.w400;
    if (gameService.currentlyPlayingIndex != -1 &&
        gameService.players[gameService.currentlyPlayingIndex.toInt()].user
                .username ==
            player.user.username) weight = FontWeight.w600;

    return Stack(
      alignment: Alignment.center,
      children: [
        getProfile(context, player, weight, index),
        if (ActionServiceProxy().currentAction.player.username ==
                player.user.username &&
            !gameService.isGameEnded)
          Positioned(
              bottom: 100,
              child: SpeechBubble(
                color: Theme.of(context).colorScheme.secondary,
                child: getAction(),
              )),
        if (gameService.currentlyPlayingIndex == index)
          Positioned(
            left: 10,
            top: 35,
            child: Transform.scale(
              scaleX: -1,
              child: Icon(
                Icons.arrow_back_ios,
                color: Theme.of(context).colorScheme.primaryContainer,
              ),
            ),
          )
      ],
    );
  }

  getProfile(
      BuildContext context, Player player, FontWeight weight, int index) {
    return Container(
        padding: const EdgeInsets.all(20),
        width: 200,
        height: 130,
        child: Column(
          children: [
            ElevatedButton(
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<CircleBorder>(
                        const CircleBorder())),
                onPressed: () => showProfile(player.user, context, index),
                child: getAvatar(index, 50)),
            Text(player.user.username, style: TextStyle(fontWeight: weight)),
            Text("${player.points} points",
                style: TextStyle(fontWeight: weight)),
          ],
        ));
  }

  void showProfile(User user, BuildContext context, int index) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              content: OtherProfile(
                user: user,
                sendDMCallback: () {},
                index: index,
                setWatchedPlayer: widget.setWatchedPlayer,
              ),
            ));
  }

  Widget getAvatar(int index, double size) {
    String ext = GameServiceProxy().players[index].user.avatar.split('.').last;

    if (ext == 'svg') {
      return SvgPicture.asset(
        GameServiceProxy().players[index].user.avatar,
        width: size,
        height: size,
      );
    } else if (ext == "png") {
      return Image.asset(
        GameServiceProxy().players[index].user.avatar,
        width: size,
        height: size,
      );
    }

    return ClipOval(
        child: Image.memory(
      GameServiceProxy().avatarBytes[index],
      width: size,
      height: size,
      fit: BoxFit.fill,
    ));
  }

  Text getAction() {
    ActionMessage ac = ActionServiceProxy().currentAction;
    switch (ac.actionCode) {
      case ActionCode.Pass:
        return Text(translate(ac.action));
      case ActionCode.Place:
        return Text(translate(ac.action, args: {'placedLetters': ac.param}));
      case ActionCode.Exchange:
        return Text(translate(ac.action, args: {'numberLetters': ac.param}));
    }
    return const Text("");
  }
}
