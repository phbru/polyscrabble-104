// ignore_for_file: sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:poly_scrabble_mobile/classes/letter.dart';
import 'package:poly_scrabble_mobile/classes/widget_token.dart';
import 'package:poly_scrabble_mobile/game_page/draggable_token.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/letter_rack_service.dart';
import 'package:poly_scrabble_mobile/services/token_service.dart';

class LetterRack extends StatefulWidget {
  Function(WidgetToken, Offset) addTokenToBoard;
  Function(Letter) addLetterToRack;
  Function(int) removeTokenFromBoard;
  Function() displayExchange;
  GlobalKey<ScaffoldState> scaffoldKey;

  LetterRack(
      {super.key,
      required this.addTokenToBoard,
      required this.scaffoldKey,
      required this.removeTokenFromBoard,
      required this.addLetterToRack,
      required this.displayExchange});

  @override
  LetterRackState createState() => LetterRackState();
}

class LetterRackState extends State<LetterRack> {
  GameServiceProxy gameService = GameServiceProxy();
  TokenService tokenService = TokenService();
  @override
  void initState() {
    super.initState();
    gameService.onPropUpdate('players', () {
      if (!mounted) return;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Theme.of(context).colorScheme.primary,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.black)),
        child: Container(
            child: ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemCount: gameService.lettersView.length,
                itemBuilder: (BuildContext context, int index) {
                  return Align(
                      alignment: Alignment.center,
                      child: Container(
                          alignment: Alignment.topRight,
                          width: 40,
                          height: 40,
                          margin:
                              const EdgeInsets.only(left: 12.5, right: 12.5),
                          child: GestureDetector(
                              onTap: () {
                                if (gameService.validateIsOurTurn()) {
                                  setState(() {
                                    widget.displayExchange();
                                    addToExchange(index);
                                  });
                                }
                              },
                              child: DraggableToken(
                                  letter:
                                      gameService.lettersView[index].character,
                                  weight: gameService.lettersView[index].weight,
                                  isBlankLetter: gameService
                                      .lettersView[index].isBlankLetter,
                                  addTokenToBoard: widget.addTokenToBoard,
                                  updateTokenFromBoard: ((p0, p1, p2) {}),
                                  removeTokenFromBoard:
                                      widget.removeTokenFromBoard,
                                  addTokenToRack: widget.addLetterToRack,
                                  removeTokenFromRack: removeLetterFromRack,
                                  size: 45,
                                  index: -1,
                                  scaffoldKey: widget.scaffoldKey,
                                  color: getColor(index)))));
                })));
  }

  addToExchange(int index) {
    LetterRackService().addToExchange(index);
  }

  Color getColor(int currentIndex) {
    for (int index in LetterRackService().exchangeIndexes) {
      if (index == currentIndex) return Colors.red;
    }

    return Colors.black;
  }

  removeLetterFromRack(String letter) {
    setState(() {
      gameService.removeLetter(letter);
    });
  }
}
