import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/player.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';

class PlayerOptionDialog extends StatelessWidget {
  GameServiceProxy gameService = GameServiceProxy();
  Player player;
  Function(int) setWatchedPlayer;
  num index;
  PlayerOptionDialog(
      {super.key,
      required this.player,
      required this.setWatchedPlayer,
      required this.index});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 300,
        height: 100,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          ElevatedButton(
              onPressed: () {
                gameService.takeBotPlace(player);
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate("gamePage.infoPanel.takePlace"))),
          ElevatedButton(
              onPressed: () {
                setWatchedPlayer(index.toInt());
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate("userInterations.watchPlayer")))
        ]));
  }
}
