import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/waiting_room_proxy_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

class EndGameDialog extends StatelessWidget {
  WaitingRoomProxyService waitingRoomService = WaitingRoomProxyService();
  String winner;
  Socket socket = Socket();
  SelfUserProxyService selfUserService = SelfUserProxyService();
  GameServiceProxy gameServiceProxy = GameServiceProxy();

  EndGameDialog({super.key, required this.winner});

  @override
  Widget build(BuildContext context) {
    return Builder(
        builder: (context) => Container(
            padding: const EdgeInsets.only(top: 10),
            width: 500,
            child: Column(children: [
              Container(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text(translate(
                      winner == ''
                          ? 'gamePage.endGame.tie'
                          : 'gamePage.endGame.oneWinner',
                      args: winner == '' ? null : {'name': winner}))),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                if (!gameServiceProxy.isObserver())
                  ElevatedButton(
                      onPressed: () {
                        createRematch();
                        Navigator.pop(context);
                        Navigator.of(context).pushReplacementNamed('/mainPage');
                      },
                      style: ElevatedButton.styleFrom(
                        shape: const ContinuousRectangleBorder(),
                        padding: const EdgeInsets.all(7),
                      ),
                      child: Text(translate('gamePage.infoPanel.rematch'))),
                ElevatedButton(
                    onPressed: () {
                      quitGame();
                      Navigator.pop(context);
                      Navigator.of(context).pushReplacementNamed('/mainPage');
                    },
                    style: ElevatedButton.styleFrom(
                      shape: const ContinuousRectangleBorder(),
                      padding: const EdgeInsets.all(7),
                    ),
                    child: Text(translate('gamePage.infoPanel.leave')))
              ]),
            ])));
  }

  createRematch() async {
    String creatorName = selfUserService.user.username;
    num time = gameServiceProxy.partialGameInfo.time;
    num openSpots = gameServiceProxy.players.length;
    String dictionary = gameServiceProxy.partialGameInfo.dictionary;
    String password = gameServiceProxy.partialGameInfo.password;

    socket.io.emit('leaveGame');
    await joinWaitingRoom(creatorName, time, openSpots, dictionary, password,
        gameServiceProxy.copyOfPlayersForRematch);
  }

  joinWaitingRoom(
    String creatorName,
    num time,
    num openSpots,
    String dictionary,
    String password,
    List<String> playerIdsToInvite,
  ) async {
    socket.io.emitWithAck('joinGame', {creatorName, ''}, ack: (gameId) async {
      GameInfo gameInfo = GameInfo();
      gameInfo.id = gameId;
      gameInfo.creatorName = creatorName;
      gameInfo.time = time;
      gameInfo.dictionary = dictionary;
      gameInfo.openSpots = openSpots;
      gameInfo.password = password;
      gameInfo.players = [];
      gameInfo.observers = [];
      gameInfo.virtualPlayers = [];
      gameInfo.invitedUsers = [];
      gameInfo.isGameStarted = false;
      gameInfo.isPrivate = false;
      await waitingRoomService.createWaitingRoom(
          gameInfo, selfUserService.user.id);
      for (String playerId in playerIdsToInvite) {
        waitingRoomService.addInvitedUser(playerId);
        socket.io.emit('inviteUser', playerId);
      }
    });
  }

  quitGame() {
    if (!gameServiceProxy.isObserver()) {
      gameServiceProxy.confirmAbandon();

      socket.io.emit('leaveGame');
    } else {
      socket.io.emit('leaveGame');
    }
  }
}
