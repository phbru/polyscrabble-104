import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/letter_rack_service.dart';

class ButtonsLeft extends StatelessWidget {
  GameServiceProxy gameService = GameServiceProxy();

  ButtonsLeft({super.key});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 100,
        height: 90,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                  height: 40,
                  child: ElevatedButton(
                      onPressed: () {
                        if (gameService.validateIsOurTurn()) {
                          gameService.executePass();
                          LetterRackService().exchangeIndexes.clear();
                        }
                      },
                      child: Text(translate('gamePage.letterRack.pass'),
                          style: const TextStyle(fontSize: 13)))),
              SizedBox(
                  height: 40,
                  child: ElevatedButton(
                      onPressed: () {
                        if (gameService.validateIsOurTurn()) {
                          LetterRackService().exchange();
                        }
                      },
                      child: Text(translate('gamePage.letterRack.exchange'),
                          style: const TextStyle(fontSize: 13))))
            ]));
  }
}
