import 'package:flutter/material.dart';

class Token extends StatelessWidget {
  double size;
  String letter;
  num weight;
  Color color;

  Token(
      {super.key,
      required this.letter,
      required this.weight,
      required this.size,
      this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
            border: Border.all(color: color),
            color: const Color.fromRGBO(235, 218, 171, 1)),
        child: Material(
          type: MaterialType.transparency,
          child: Stack(children: [
            Positioned(
                top: 0,
                left: 0,
                child: Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Text(letter,
                        style:
                            TextStyle(fontSize: size / 2 + 5, color: color)))),
            Positioned(
                bottom: 0,
                right: 2,
                child: Text(weight.toString(),
                    style: TextStyle(fontSize: size / 3, color: color)))
          ]),
        ));
  }
}
