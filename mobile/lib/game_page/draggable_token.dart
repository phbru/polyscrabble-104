import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/letter.dart';
import 'package:poly_scrabble_mobile/classes/widget_token.dart';
import 'package:poly_scrabble_mobile/game_page/star_letter_dialog.dart';
import 'package:poly_scrabble_mobile/game_page/token.dart';
import 'package:poly_scrabble_mobile/services/board_location_finder.dart';
import 'package:poly_scrabble_mobile/services/board_model_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/letter_rack_service.dart';
import 'package:poly_scrabble_mobile/services/token_service.dart';

class DraggableToken extends StatelessWidget {
  String letter;
  int weight;
  bool isBlankLetter;
  Function(WidgetToken, Offset) addTokenToBoard;
  Function(int, Offset, Offset) updateTokenFromBoard;
  Function(int) removeTokenFromBoard;
  Function(String) removeTokenFromRack;
  Function(Letter) addTokenToRack;
  GameServiceProxy gameService = GameServiceProxy();
  double size;
  int index;
  GlobalKey<ScaffoldState> scaffoldKey;
  Color color;

  TokenService tokenService = TokenService();
  BoardLocationService boardLocationService = BoardLocationService();
  BoardModelServiceProxy boardModelService = BoardModelServiceProxy();

  DraggableToken(
      {super.key,
      required this.letter,
      required this.weight,
      required this.isBlankLetter,
      required this.addTokenToBoard,
      required this.updateTokenFromBoard,
      required this.removeTokenFromBoard,
      required this.addTokenToRack,
      required this.removeTokenFromRack,
      required this.size,
      required this.index,
      required this.scaffoldKey,
      this.color = Colors.black});
  @override
  Widget build(BuildContext context) {
    return Draggable(
        onDragEnd: (dragDetails) {
          if (gameService.validateIsOurTurn() &&
              LetterRackService().exchangeIndexes.isEmpty) {
            if (index != -1) {
              if (boardLocationService.isInCanvas(dragDetails.offset)) {
                Offset pos =
                    boardLocationService.getCanvasPos(dragDetails.offset);
                if (boardModelService
                        .viewBoardMatrix[pos.dy.toInt() - 1][pos.dx.toInt() - 1]
                        .letter
                        .character ==
                    '?') {
                  updateTokenFromBoard(
                      index,
                      boardLocationService
                          .getTokenWidgetPos(dragDetails.offset),
                      boardLocationService.getCanvasPos(dragDetails.offset));
                  boardLocationService.updateCurrentWord(
                      index, dragDetails.offset);
                } else {
                  removeTokenFromBoard(index);
                  addTokenToRack(Letter()
                    ..character = letter
                    ..weight = weight
                    ..isBlankLetter = isBlankLetter);
                }
              } else {
                if (isBlankLetter) {
                  letter = '*';
                  weight = 0;
                }
                removeTokenFromBoard(index);
                addTokenToRack(Letter()
                  ..character = letter
                  ..weight = weight
                  ..isBlankLetter = isBlankLetter);
                boardLocationService.removeFromCurrentWord(index);
              }
            } else {
              if (boardLocationService.isInCanvas(dragDetails.offset)) {
                Offset pos =
                    boardLocationService.getCanvasPos(dragDetails.offset);
                if (boardModelService
                        .viewBoardMatrix[pos.dy.toInt() - 1][pos.dx.toInt() - 1]
                        .letter
                        .character ==
                    '?') {
                  removeTokenFromRack(letter);

                  Letter letterObj = Letter()
                    ..character = letter
                    ..weight = weight
                    ..isBlankLetter = isBlankLetter;
                  addTokenToBoard(
                      boardLocationService.getTokenWidget(
                          letterObj, dragDetails.offset),
                      boardLocationService.getCanvasPos(dragDetails.offset));

                  if (letter == '*') {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return SimpleDialog(
                            title: Center(
                                child: Text(
                                    translate('gamePage.letterRack.star'))),
                            children: [
                              StarLetterDialog(
                                scaffoldKey: scaffoldKey,
                                setLetter: setLetter,
                              )
                            ],
                          );
                        }).then(
                      (value) {
                        removeTokenFromBoard(tokenService.tokens.length - 1);
                        if (letter != '*') {
                          Letter letterObj = Letter()
                            ..character = letter
                            ..weight = weight
                            ..isBlankLetter = isBlankLetter;
                          addTokenToBoard(
                              boardLocationService.getTokenWidget(
                                  letterObj, dragDetails.offset),
                              boardLocationService
                                  .getCanvasPos(dragDetails.offset));
                          boardLocationService.addToCurrentWord(
                              letterObj, dragDetails.offset);
                        } else {
                          addTokenToRack(Letter()
                            ..character = letter
                            ..weight = weight
                            ..isBlankLetter);
                        }
                      },
                    );
                  } else {
                    boardLocationService.addToCurrentWord(
                        letterObj, dragDetails.offset);
                  }
                }
              }
            }
          }
        },
        feedback: Token(
          letter: letter,
          weight: weight,
          size: size,
          color: color,
        ),
        childWhenDragging: SizedBox(height: size, width: size),
        child: Token(
          letter: letter,
          weight: weight,
          size: size,
          color: color,
        ));
  }

  setLetter(String newLetter) {
    letter = newLetter;
  }
}
