import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_translate/flutter_translate.dart';

class StarLetterDialog extends StatefulWidget {
  GlobalKey<ScaffoldState> scaffoldKey;
  Function(String) setLetter;
  StarLetterDialog(
      {super.key, required this.scaffoldKey, required this.setLetter});

  @override
  StarLetterDialogState createState() => StarLetterDialogState();
}

class StarLetterDialogState extends State<StarLetterDialog> {
  TextEditingController starLetterController = TextEditingController(text: 'A');

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 300,
        height: 150,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Container(
            padding: const EdgeInsets.only(bottom: 10),
            width: 150,
            child: TextField(
              controller: starLetterController,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                LengthLimitingTextInputFormatter(1)
              ],
              decoration: InputDecoration(
                fillColor: Theme.of(context).colorScheme.primary,
                filled: true,
                hintText: translate('loginPage.username'),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(translate('common.cancel'))),
              ElevatedButton(
                  onPressed: () {
                    widget.setLetter(starLetterController.text.toUpperCase());
                    Navigator.pop(context);
                  },
                  child: Text(translate('common.confirm')))
            ],
          )
        ]));
  }
}
