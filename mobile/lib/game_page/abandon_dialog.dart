import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class AbandonDialog extends StatelessWidget {
  Function() confirmAbandon;

  AbandonDialog({super.key, required this.confirmAbandon});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 300,
        height: 100,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate("common.no"))),
          ElevatedButton(
              onPressed: () {
                confirmAbandon();
              },
              style: ElevatedButton.styleFrom(
                shape: const ContinuousRectangleBorder(),
                padding: const EdgeInsets.all(7),
              ),
              child: Text(translate("common.yes")))
        ]));
  }
}
