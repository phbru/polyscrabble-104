// ignore_for_file: sized_box_for_whitespace

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:poly_scrabble_mobile/classes/game_info.dart';
import 'package:poly_scrabble_mobile/classes/matrix_coordinates.dart';
import 'package:poly_scrabble_mobile/classes/widget_token.dart';
import 'package:poly_scrabble_mobile/game_page/draggable_token.dart';
import 'package:poly_scrabble_mobile/game_page/end_game_dialog.dart';
import 'package:poly_scrabble_mobile/game_page/letter_rack.dart';
import 'package:poly_scrabble_mobile/game_page/players_display.dart';
import 'package:poly_scrabble_mobile/main_page/chat/chat_display.dart';
import 'package:poly_scrabble_mobile/main_page/chat_bubbles/chat_bubble_display.dart';
import 'package:poly_scrabble_mobile/main_page/invite_received_dialog.dart';
import 'package:poly_scrabble_mobile/profile/profile.dart';
import 'package:poly_scrabble_mobile/services/action_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/board_location_finder.dart';
import 'package:poly_scrabble_mobile/services/board_model_service.dart';
import 'package:poly_scrabble_mobile/services/game_board_drawing_service.dart';
import 'package:poly_scrabble_mobile/services/game_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/global_game_list_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/letter_rack_service.dart';
import 'package:poly_scrabble_mobile/services/self_user_proxy_service.dart';
import 'package:poly_scrabble_mobile/services/timer_service_proxy.dart';
import 'package:poly_scrabble_mobile/services/token_service.dart';
import 'package:poly_scrabble_mobile/socket.dart';

import '../classes/letter.dart';

class GamePageDisplay extends StatefulWidget {
  Function() updateTheme;
  GamePageDisplay({super.key, required this.updateTheme});

  @override
  GamePageDisplayState createState() => GamePageDisplayState();
}

class GamePageDisplayState extends State<GamePageDisplay> {
  Socket socket = Socket();
  BoardModelServiceProxy boardModelServiceProxy = BoardModelServiceProxy();
  final TimerServiceProxy _timerServiceProxy = TimerServiceProxy();
  GameServiceProxy gameService = GameServiceProxy();
  TokenService tokenService = TokenService();
  ActionServiceProxy actionService = ActionServiceProxy();
  BoardLocationService boardLocationService = BoardLocationService();
  var repaint = ValueNotifier<bool>(false);
  bool isRightDisplayChat = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool invalidWord = false;
  dynamic bytes;

  @override
  void initState() {
    super.initState();

    actionService.onPropUpdate('currentAction', () {
      if (!mounted) return;
      setState(() {});
    });

    boardModelServiceProxy.refreshDisplay = refreshDisplay;

    socket.io.on("invalidWord", (e) {
      if (!mounted) return;
      setState(() {
        invalidWord = true;
      });
    });

    socket.io.on("resetView", (e) {
      if (!mounted) return;
      setState(() {});
    });

    gameService.onPropUpdate('players', () {
      if (!mounted) return;
      setState(() {
        gameService.setAvatarBytes();
        if (gameService.players.length > 1 && gameService.isObserver()) {
          gameService.updateObserverRack();
        }
        gameService.resetRackViewToModel();
      });
    });

    socket.io.on('takeIndex', (index) {
      if (!mounted) return;
      setState(() {
        gameService.ourIndex = index;
        gameService.resetRackViewToModel();
      });
    });

    gameService.onPropUpdate('currentlyPlayingIndex', () {
      if (!mounted) return;
      if (tokenService.tokens.isNotEmpty && !gameService.validateIsOurTurn()) {
        resetBoard();
      }
    });

    boardModelServiceProxy.onPropUpdate('startingCase', () async {
      if (!mounted) return;
      if (!(await gameService.validateIsOurTurn())) {
        boardModelServiceProxy.startingCaseView =
            boardModelServiceProxy.startingCase;
        refreshDisplay();
      } else {
        boardModelServiceProxy.startingCaseView = MatrixCoordinates();
      }
    });

    socket.io.on('gameEnded', (winner) {
      if (!mounted) return;
      gameService.copyPlayersForRematch();
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Text(
                translate('gamePage.endGame.gameOver'),
                textAlign: TextAlign.center,
              ),
              children: [EndGameDialog(winner: winner)],
            );
          });
      socket.io.emit('leaveGame');
    });

    socket.io.on('gameInviteReceived', (invite) {
      if (mounted) {
        if (invite[0] == SelfUserProxyService().user.id) {
          if (Navigator.canPop(context)) {
            Navigator.pop(context);
          }
          GameInfo invitedGameInfo =
              GlobalGameListProxyService().getGameInfo(invite[1]);

          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  title: Text(
                    translate('waitingRoom.gameInvite.invitationReceived',
                        args: {'name': invitedGameInfo.creatorName}),
                    textAlign: TextAlign.center,
                  ),
                  children: [
                    InviteReceivedDialog(
                      invitedGameInfo: invitedGameInfo,
                      openWaitingRoom: () {},
                      inGamePage: true,
                    )
                  ],
                );
              });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          SingleChildScrollView(
              child: Container(
                  width: 200,
                  height: 750,
                  child: PlayersDisplay(
                    setWatchedPlayer: setWatchedPlayer,
                  ))),
          SingleChildScrollView(
              child: Container(
                  width: 690,
                  height: 750,
                  child: Column(children: [
                    Container(
                        width: 690,
                        height: 690,
                        child: Stack(children: [
                          DragTarget<int>(builder: (BuildContext context,
                              List<int?> data, List<dynamic> rejects) {
                            return CustomPaint(
                                painter: GameBoard(repaint: repaint));
                          }),
                          for (int i = 0; i < tokenService.tokens.length; i++)
                            Positioned(
                                top: tokenService.tokens[i].pos.dy + 8,
                                left: tokenService.tokens[i].pos.dx,
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                        height: 42,
                                        child: DraggableToken(
                                            letter: tokenService
                                                .tokens[i].letter.character,
                                            weight: tokenService
                                                .tokens[i].letter.weight,
                                            isBlankLetter: tokenService
                                                .tokens[i].letter.isBlankLetter,
                                            addTokenToBoard: ((p0, p1) {}),
                                            updateTokenFromBoard:
                                                updateTokenFromBoard,
                                            removeTokenFromBoard:
                                                removeTokenFromBoard,
                                            addTokenToRack: addLetterToRack,
                                            removeTokenFromRack: ((p0) {}),
                                            size: 42,
                                            scaffoldKey: _scaffoldKey,
                                            index: i))))
                        ])),
                    Center(
                        child: Container(
                            height: 60,
                            color: Theme.of(context).colorScheme.secondary,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  if (gameService.isPlaying())
                                    SizedBox(
                                        height: 40,
                                        width: 90,
                                        child: ElevatedButton(
                                            onPressed: () {
                                              if (gameService
                                                  .validateIsOurTurn()) {
                                                gameService.executePass();
                                                LetterRackService()
                                                    .exchangeIndexes
                                                    .clear();
                                              }
                                            },
                                            child: Text(
                                                translate(
                                                    'gamePage.letterRack.pass'),
                                                style: const TextStyle(
                                                    fontSize: 13))))
                                  else if (gameService.isObserver())
                                    SizedBox(
                                        height: 60,
                                        width: 140,
                                        child: Center(
                                            child: Text(
                                          translate(
                                              'userInteractions.currentlyWatching',
                                              args: {
                                                'name': gameService
                                                    .players[gameService
                                                        .watchedPlayer
                                                        .toInt()]
                                                    .user
                                                    .username
                                              }),
                                          style: const TextStyle(
                                              fontSize: 13,
                                              fontWeight: FontWeight.bold),
                                          textAlign: TextAlign.center,
                                        )))
                                  else
                                    SizedBox(height: 40, width: 90),
                                  Container(
                                      margin: const EdgeInsets.only(
                                          left: 10, right: 10),
                                      height: 50,
                                      width: 460,
                                      child: LetterRack(
                                        addTokenToBoard: addTokenToBoard,
                                        scaffoldKey: _scaffoldKey,
                                        removeTokenFromBoard:
                                            removeTokenFromBoard,
                                        addLetterToRack: addLetterToRack,
                                        displayExchange: displayExchange,
                                      )),
                                  if (gameService.isPlaying() &&
                                      LetterRackService().exchangeEmpty() &&
                                      tokenService.tokens.isNotEmpty)
                                    SizedBox(
                                        height: 40,
                                        width: 90,
                                        child: ElevatedButton(
                                            onPressed: () async => await play(),
                                            child: Text(
                                                translate(
                                                    'gamePage.letterRack.play'),
                                                style: const TextStyle(
                                                    fontSize: 13))))
                                  else if (!LetterRackService()
                                          .exchangeEmpty() &&
                                      gameService.isPlaying())
                                    SizedBox(
                                        height: 40,
                                        width: 90,
                                        child: ElevatedButton(
                                            onPressed: () async {
                                              await LetterRackService()
                                                  .exchange();
                                              setState(() {});
                                            },
                                            child: Text(
                                                translate(
                                                    'gamePage.letterRack.exchange'),
                                                style: const TextStyle(
                                                    fontSize: 13))))
                                  else if (gameService.isObserver())
                                    SizedBox()
                                  else
                                    SizedBox(height: 40, width: 90),
                                ])))
                  ]))),
          Expanded(
              flex: 4,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 24),
                    height: 60,
                    width: 390,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: ElevatedButton(
                        onPressed: () {
                          showProfile();
                        },
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<CircleBorder>(
                                const CircleBorder())),
                        child: getAvatar(),
                      ),
                    ),
                  ),
                  const Divider(height: 0),
                  Expanded(
                      flex: 8,
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 8,
                              child: ChatDisplay(
                                toggleGame: () {},
                                sendDMCallback: toggleChat,
                                displayBack: false,
                              ),
                            ),
                            Expanded(
                                flex: 2,
                                child: ChatBubbleDisplay(
                                  toggleChat,
                                )),
                          ])),
                ],
              ))
        ]));
  }

  Widget getAvatar() {
    String avatar = SelfUserProxyService().user.avatar;
    double size = 60;

    String ext = avatar.split('.').last;
    if (avatar.isEmpty) return const SizedBox.shrink(); // Empty widget

    if (ext == 'svg') {
      return SvgPicture.asset(
        avatar,
        width: size,
        height: size,
      );
    } else if (ext == 'png') {
      return Image.asset(
        avatar,
        width: size,
        height: size,
      );
    }
    if (bytes == null) {
      String avatarBase64 = avatar.split(',').last;
      bytes = const Base64Decoder().convert(avatarBase64);
    }

    return ClipOval(
        child:
            Image.memory(bytes, width: size, height: size, fit: BoxFit.fill));
  }

  refreshDisplay() {
    repaint.value = !repaint.value;
  }

  toggleChat() {
    if (!mounted) return;
    setState(() {
      isRightDisplayChat = true;
    });
  }

  addTokenToBoard(WidgetToken token, Offset canvasPos) {
    if (!mounted) return;
    setState(() {
      tokenService.addToken(token, canvasPos);
    });
  }

  updateTokenFromBoard(int index, Offset pos, Offset canvasPos) {
    if (!mounted) return;
    setState(() {
      tokenService.updateToken(index, pos, canvasPos);
    });
  }

  removeTokenFromBoard(int index) {
    if (!mounted) return;
    setState(() {
      tokenService.tokens.removeAt(index);
    });
  }

  addLetterToRack(Letter letter) {
    if (!mounted) return;
    setState(() {
      gameService.addLetter(letter);
    });
  }

  Widget getScrore(int score) {
    return Text(score.toString());
  }

  Widget getUsername(String username) {
    return Text(username);
  }

  getRightDisplay() {
    if (!isRightDisplayChat) {
    } else {
      return;
    }
  }

  void showProfile() {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Column(children: [
                Text(
                  translate('menuSettings.myProfile'),
                  style: const TextStyle(
                      fontSize: 25, fontWeight: FontWeight.bold),
                ),
                Container(height: 10),
                const Divider(height: 0),
              ]),
              content: Profile(
                updateTheme: widget.updateTheme,
                allowChange: false,
              ),
            ));
  }

  setWatchedPlayer(int index) {
    if (!mounted) return;
    setState(() {
      gameService.setWatchedPlayer(index);
    });
  }

  resetBoard() {
    if (!mounted) return;
    setState(() {
      gameService.resetRackViewToModel();
      tokenService.tokens.clear();
    });
  }

  displayExchange() {
    if (!mounted) return;
    setState(() {
      if (tokenService.tokens.isNotEmpty) {
        gameService.resetRackViewToModel();
        tokenService.tokens.clear();
      }
    });
  }

  play() async {
    boardModelServiceProxy.setSelectedCaseArrow(MatrixCoordinates());

    if (await boardLocationService.isValidPlace()) {
      await gameService.executePlace(boardLocationService.currentWordMap);
      gameService.resetRackViewToModel();
      boardLocationService.reset();
      tokenService.resetPlace();
    } else {
      tokenService.resetPass();
      boardLocationService.reset();

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
        translate('mainPage.snackBar.invalidPlacement'),
        textAlign: TextAlign.center,
      )));
    }

    if (invalidWord && mounted) {
      tokenService.resetPass();
      boardLocationService.reset();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
        translate('mainPage.snackBar.invalidWord'),
        textAlign: TextAlign.center,
      )));
      invalidWord = false;
      gameService.executePass();
    }

    setState(() {});
  }
}
