import { Bonus } from '@app/enums/bonus';
import { Letter } from './letter';

export type Case = {
    letter: Letter;
    bonus: Bonus;
};
