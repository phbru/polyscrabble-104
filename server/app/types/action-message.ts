/* eslint-disable prettier/prettier */
import { User } from '@app/classes/user';
import { ActionCode } from '@app/enums/action-code';

export type ActionMessage = {
    player: User;
    actionCode: ActionCode;
    action: string;
    param?: string;
};
