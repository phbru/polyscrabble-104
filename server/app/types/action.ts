import { ActionCode } from '@app/enums/action-code';
import { WordMap } from './word-map';

export type Action = {
    actionType: ActionCode;
    parameter: undefined | WordMap | string[];
};
