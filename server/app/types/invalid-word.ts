export type InvalidWord = {
    word: string;
    line: string;
    column: number;
};
