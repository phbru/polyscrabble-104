export type Dictionary = {
    title: string;
    description: string;
    isDefault: boolean;
    words: string[];
};

export type DictionaryName = { title: string; description: string };
