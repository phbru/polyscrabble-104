export type MatrixCoordinates = {
    line: number;
    column: number;
};
