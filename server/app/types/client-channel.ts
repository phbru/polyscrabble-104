import { Channel } from '@app/classes/channel/channel';

export type ClientChannel = {
    newMessages: boolean;
    channel: Channel;
};
