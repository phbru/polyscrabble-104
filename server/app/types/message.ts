import { User } from '@app/classes/user';

export type Message = {
    user: User;
    output: string;
    timestamp: string;
};
