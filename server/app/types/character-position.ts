import { Letter } from './letter';

export type CharacterPosition = {
    letter: Letter;
    line: string;
    column: number;
};
