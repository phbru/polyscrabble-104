export type RankingIdPair = {
    id: string;
    elo: number;
};
