export type Objective = {
    id: number;
    objectiveDescription: string;
    rewardDescription: string;
    points: number;
    factor: number;
    isClosed: boolean;
};
