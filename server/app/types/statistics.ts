import { Time } from './time';

export type Statistics = {
    gamesPlayed: number;
    gamesWon: number;
    gamesLost: number;
    elo: number;
    rank: number;
    averagePointByGame: number;
    averageTimePerGame: Time;
};
