import { User } from '@app/classes/user';
import { ClientChannelService } from '@app/services/client-channel/client-channel.service';
import { Message } from '@app/types/message';

export class Channel {
    id: string = '';
    name: string = '';
    users: User[] = [];
    owner: User = new User('');
    searchable: boolean = true;
    directMessage: boolean = false;
    messages: Message[] = [];

    shallowCopy() {
        const newChannel = new Channel();
        newChannel.id = this.id;
        newChannel.name = this.name;
        newChannel.users = this.users;
        newChannel.owner = this.owner;
        newChannel.searchable = this.searchable;
        newChannel.directMessage = this.directMessage;
        newChannel.messages = this.messages;
        return newChannel;
    }

    addMessage(message: Message) {
        // Push new message and set channels as unread for all users
        const timestamp = new Date().toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false });
        const messageTimestamp = message;
        messageTimestamp.timestamp = timestamp;
        this.messages.push(messageTimestamp);
        for (const user of this.users) {
            user.getContainer().get(ClientChannelService).setChannelAsUnread(this.id);
        }

        // Just in-case some users didn't get updated through unread messages
        this.updateConnectedClients();
    }

    updateConnectedClients() {
        for (const user of this.users) {
            user.getContainer().get(ClientChannelService).sendChannelUpdates();
        }
    }

    findUser(userId: string): User | undefined {
        for (const user of this.users) {
            if (user.id === userId) return user;
        }

        return undefined;
    }
}
