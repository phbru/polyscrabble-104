/* eslint-disable no-console -- logs on server */
import { Channel } from '@app/classes/channel/channel';
import { User } from '@app/classes/user';
import { ActionMessageService } from '@app/services/action-message-transmitter/action-message.service';
import { BoardModelService } from '@app/services/board-model/board-model.service';
import { BotActionGenerator } from '@app/services/bot-action-generator/bot-action-generator';
import { BotWordGeneratorService } from '@app/services/bot-word-generator/bot-word-generator.service';
import { ClientChannelService } from '@app/services/client-channel/client-channel.service';
import { ClientFriendService } from '@app/services/client-friend/client-friend.service';
import { ExchangeService } from '@app/services/command/exchange-command/exchange-service';
import { PlaceService } from '@app/services/command/place-command/place.service';
import { DictionaryService } from '@app/services/dictionary/dictionary.service';
import { GameService } from '@app/services/game/game.service';
import { GlobalChannelService } from '@app/services/global-channel/global-channel.service';
import { GlobalDictionaryService } from '@app/services/global-dictionary/global-dictionary.service';
import { GlobalGameListService } from '@app/services/global-game-list/global-game-list.service';
import { GlobalInteractionService } from '@app/services/global-interaction/global-interaction.service';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { LetterBankService } from '@app/services/letter-bank/letter-bank.service';
import { ScoreCounterService } from '@app/services/score-counter/score-counter.service';
import { SelfUserService } from '@app/services/self-user/self-user.service';
import { TimerService } from '@app/services/timer/timer.service';
import { WaitingRoomService } from '@app/services/waiting-room/waiting-room.service';
import { WordFinderService } from '@app/services/word-finder/word-finder.service';
import * as http from 'http';
import { Server, Socket } from 'socket.io';
import { Container } from 'typedi';

const GLOBAL_SERVICES = [GlobalUserService, GlobalChannelService, GlobalGameListService, GlobalInteractionService, GlobalDictionaryService];

const CLIENT_SERVICES = [SelfUserService, ClientChannelService, ClientFriendService];

const GAME_SERVICES = [
    WaitingRoomService,
    GameService,
    TimerService,
    LetterBankService,
    DictionaryService,
    BoardModelService,
    WordFinderService,
    ScoreCounterService,
    ExchangeService,
    PlaceService,
    BotActionGenerator,
    BotWordGeneratorService,
    ActionMessageService,
];

export class SocketManager {
    private sio: Server;
    private currentRoomIndex = 0;

    constructor(server: http.Server) {
        this.sio = new Server(server, { cors: { credentials: false, methods: ['GET', 'POST'] } });
        Container.get(GlobalUserService).setupUsersFromRepo();
    }

    handleSockets(): void {
        this.sio.on('connection', (socket: Socket) => {
            console.log(`Receiving socket connection with id: ${socket.id}`);
            this.setEventListeners(socket);
        });
    }

    private setEventListeners(socket: Socket) {
        socket.on('login', (userId: string) => {
            const globalRoom = 'global';
            socket.join(globalRoom);
            console.log(`User '${userId}' logged in successfully.`);

            // Set user socket
            const globalUserService = Container.get(GlobalUserService);
            const user = globalUserService.getUser(userId);
            if (user === undefined) {
                console.log(`Error: Can't login, user '${userId}' does not exist!`);
                return;
            }
            console.log(`User '${user.username} successfully logged in.`);
            user.setSocket(socket);

            // Init connection to all global services
            for (const service of GLOBAL_SERVICES) {
                Container.of('').get(service).initConnectionToRoom(this.sio, user, globalRoom);
            }

            // Setup other services
            this.setupClientServices(user);
            this.setupGameServices(user);

            // Disconnect handling
            socket.on('disconnect', (reason: string) => {
                // Leave client services running (don't reset client container).
                // This is useful for reconnection.
                globalUserService.setUserOffline(userId);
                console.log(`User '${userId}' disconnected.`);
                console.log(`Reason: ${reason}`);
            });

            socket.on('askToJoin', (joinerUser: User, gameId: string) => {
                socket.to(gameId).emit('informCreatorJoin', joinerUser);
            });

            socket.on('cancelJoin', (joinerUser: User, gameId: string) => {
                socket.to(gameId).emit('informCreatorCancel', joinerUser);
            });

            socket.on('declineInvite', (invitedUserId: string, gameId: string) => {
                socket.to(gameId).emit('declineInvite', invitedUserId);
            });
        });
    }

    private setupClientServices(user: User) {
        // Join self room
        const roomName = user.id;
        const container = user.getContainer();

        // Init connection to all client services
        for (const service of CLIENT_SERVICES) {
            container.get(service).initConnectionToRoom(this.sio, user, roomName);
        }

        // Set self user
        container.get(SelfUserService).user = user;
    }

    private setupGameServices(user: User) {
        const NO_GAME_ID = '0xdeadbeef';
        let lastGameId = NO_GAME_ID;
        const socket = user.getSocket();
        if (socket === undefined) return;

        // If roomName is empty string, will generate and return unique roomName
        socket.on('joinGame', (playerName: string, gameId: string, acknowledgement?: (gameId: string) => void) => {
            if (lastGameId === gameId) {
                // Already in a game, joining same game
                console.log(`Warning: user ${user.username} trying to join the same game twice!`);
                return;
            }
            if (lastGameId !== NO_GAME_ID) {
                // Already in a game, joining different game.
                // Leave previous game before continuing.
                const channel = Container.of(lastGameId).get(GameService).channel ?? new Channel();
                console.log(`Warning: user ${user.username} trying to join 2 games at once!`);
                this.leaveGameRoom(user, lastGameId, channel);
                this.resetGameRoomIfEmpty(lastGameId, channel);
                lastGameId = NO_GAME_ID;
            }
            if (gameId === '') {
                gameId = (this.currentRoomIndex++).toString();
            }

            const container = Container.of(gameId);
            socket.join(gameId);
            console.log(`Le joueur '${playerName}' a rejoint la salle de jeu '${gameId}'.`);

            // Init connection to all game services
            for (const service of GAME_SERVICES) {
                container.get(service).initConnectionToRoom(this.sio, user, gameId);
            }

            // Create game channel if it doesn't exist.
            // This probably needs refactoring.
            const globalChannelService = Container.get(GlobalChannelService);
            const gameService = container.get(GameService);
            if (gameService.channel === undefined) {
                gameService.channel = globalChannelService.createGameChannel(user);
            }

            // Join game channel
            const gameChannel = gameService.channel ?? new Channel();
            globalChannelService.addUserToChannel(user, gameChannel.id, true);

            socket.on('inviteUser', (invitedUserId: string) => {
                socket.to('global').emit('gameInviteReceived', invitedUserId, gameId);
            });

            socket.on('acceptJoin', (userId: string) => {
                socket.to('global').emit('acceptJoin', userId, gameId);
            });

            socket.on('declineJoin', (userId: string) => {
                socket.to('global').emit('declineJoin', userId);
            });

            socket.on('revokeInvite', (invitedUserId: string) => {
                socket.to('global').emit('revokeInvite', invitedUserId);
            });

            socket.on('kickUserOut', (userIdToKickout: string) => {
                socket.to(gameId).emit('kickUserOut', userIdToKickout);
            });

            socket.on('leaveGame', () => {
                this.leaveGameRoom(user, gameId, gameChannel);
                this.resetGameRoomIfEmpty(gameId, gameChannel);
                lastGameId = NO_GAME_ID;
            });

            socket.on('cancelGame', () => {
                socket.to(gameId).emit('canceledGame');
                this.resetGameRoom(gameId, gameChannel); // TODO : remove because done in leaveGame
                lastGameId = NO_GAME_ID;
            });

            socket.on('disconnect', () => {
                container.get(GameService).abandonGame(user.id);
                this.resetGameRoomIfEmpty(gameId, gameChannel);
            });

            if (acknowledgement !== undefined) acknowledgement(gameId); // Return roomName for client
            lastGameId = gameId;
        });
    }

    private leaveGameRoom(user: User, gameId: string, gameChannel: Channel) {
        // Remove user from relevant global services
        Container.get(GlobalChannelService).spoofCaller(user).leaveChannel(gameChannel.id);
        Container.get(GlobalGameListService).removeUser(user.id, gameId);

        // Disconnect from all game services
        for (const service of GAME_SERVICES) {
            Container.of(gameId).get(service).disconnectFromRoom(user);
        }

        // Clean manual socket events
        user.getSocket()?.removeAllListeners('inviteUser');
        user.getSocket()?.removeAllListeners('acceptJoin');
        user.getSocket()?.removeAllListeners('declineJoin');
        user.getSocket()?.removeAllListeners('revokeInvite');
        user.getSocket()?.removeAllListeners('kickUserOut');
        user.getSocket()?.removeAllListeners('leaveGame');
        user.getSocket()?.removeAllListeners('cancelGame');

        // Leave socket room
        user.getSocket()?.leave(gameId);
        console.log(`Le joueur '${user.username}' a quitter la salle de jeu '${gameId}'.`);
    }

    private resetGameRoom(gameId: string, gameChannel: Channel) {
        // Remove all users from room and reset room
        const globalUserService = Container.get(GlobalUserService);
        const roomSockets = this.sio.sockets.adapter.rooms.get(gameId);

        if (roomSockets !== undefined) {
            // Remove any remaining users from the room
            for (const socket of roomSockets) {
                const user = globalUserService.getUserFromSocket(socket);
                if (user !== undefined) {
                    // Cleanly remove user from room
                    this.leaveGameRoom(user, gameId, gameChannel);
                }
            }
        }

        this.resetGameRoomIfEmpty(gameId, gameChannel);
    }

    private resetGameRoomIfEmpty(gameId: string, gameChannel: Channel) {
        const container = Container.of(gameId);

        if (this.sio.sockets.adapter.rooms.get(gameId) === undefined) {
            container.get(GlobalGameListService).removeGame(gameId); // Remove game from game list
            container.get(GlobalChannelService).deleteChannel(gameChannel.id); // Delete channel
            container.get(GameService).killGame();
            Container.of(gameId).reset();
        }
    }
}
