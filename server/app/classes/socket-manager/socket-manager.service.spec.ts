// /* eslint-disable no-unused-expressions -- ESLint gets confused with Chai syntax*/
// /* eslint-disable @typescript-eslint/no-unused-expressions  -- ESLint gets confused with Chai syntax */

// import { GameManagerService } from '@app/services/game-manager/game-manager.service';
// import { GameTerminatorService } from '@app/services/game-terminator/game-terminator.service';
// import { expect } from 'chai';
// import * as express from 'express';
// import * as http from 'http';
// import * as Sinon from 'sinon';
// import { Server, Socket } from 'socket.io';
// import { Container, ContainerInstance } from 'typedi';
// import { SocketManager } from './socket-manager.service';

// describe('SocketManager', () => {
//     let expressApp: Express.Application;
//     let manager: SocketManager;
//     // eslint-disable-next-line @typescript-eslint/no-explicit-any
//     let socketServerStub: any;

//     beforeEach(async () => {
//         expressApp = express();
//         manager = new SocketManager(http.createServer(expressApp));

//         socketServerStub = Sinon.createStubInstance(Server);
//         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Replace private property
//         Sinon.replace(manager as any, 'sio', socketServerStub);
//         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Stub adapter.rooms
//         Object.defineProperty(socketServerStub, 'sockets', { value: { adapter: { rooms: new Map() } } } as any);
//     });

//     it('should be created', () => {
//         expect(manager).to.exist;
//     });

//     describe('handleSockets', () => {
//         beforeEach(() => {
//             manager.handleSockets();
//         });

//         it('should define connection callback', () => {
//             expect(socketServerStub.on.calledWith('connection')).to.be.true;
//         });

//         describe('connection callback', () => {
//             // eslint-disable-next-line @typescript-eslint/no-explicit-any
//             let socketStub: any;

//             beforeEach(() => {
//                 socketStub = Sinon.createStubInstance(Socket);
//                 socketServerStub.on.getCall(0).args[1](socketStub); // Call connection callback
//             });

//             it('should define joinGame callback', () => {
//                 expect(socketStub.on.calledWith('joinGame')).to.be.true;
//             });

//             describe('joinGame callback', () => {
//                 let joinGameCallback: (playerName: string, roomName: string, callback?: (roomName: string) => void) => void;
//                 let playerName: string;
//                 let roomName: string;
//                 // let container: ContainerInstance;
//                 // let gameTerminator: GameTerminatorService;
//                 // let game: GameManagerService;

//                 beforeEach(() => {
//                     playerName = 'John';
//                     roomName = 'Carlito';

//                     joinGameCallback = socketStub.on.getCalls().find((call: Sinon.SinonSpyCall) => {
//                         return call.args[0] === 'joinGame';
//                     })?.args[1] as (playerName: string, roomName: string, callback?: (roomName: string) => void) => void;

//                     container = Container.of(roomName);
//                     // gameTerminator = container.get(GameTerminatorService);
//                     // game = container.get(GameManagerService);
//                 });

//                 it('should join room', () => {
//                     // eslint-disable-next-line @typescript-eslint/no-empty-function -- Give empty acknowledgement
//                     joinGameCallback(playerName, roomName, () => {});
//                     expect(socketStub.join.calledWith(roomName)).to.be.true;
//                 });

//                 it('should acknowledge unique roomName', (done) => {
//                     joinGameCallback(playerName, '', (room: string) => {
//                         expect(room).to.equal('0');
//                         done();
//                     });
//                 });

//                 // it('should not return roomName if no acknowledgement', () => {
//                 //     expect(() => {
//                 //         joinGameCallback(playerName, '');
//                 //     }).not.to.throw();
//                 // });

//                 // describe('disconnect callback', () => {
//                 //     let disconnectCallback: (reason: string) => void;
//                 //     // let resetStub: Sinon.SinonStub;
//                 //     let clock: Sinon.SinonFakeTimers;

//                 //     before(() => {
//                 //         clock = Sinon.useFakeTimers();
//                 //     });

//                 //     after(() => {
//                 //         clock.restore();
//                 //     });

//                 //     beforeEach(() => {
//                 //         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Stub private method
//                 //         // let resetStub = Sinon.stub(manager as any, 'resetRoomIfEmpty');
//                 //         // eslint-disable-next-line @typescript-eslint/no-empty-function -- Give empty acknowledgement
//                 //         joinGameCallback(playerName, roomName, () => {});

//                 //         disconnectCallback = socketStub.on.getCalls().find((call: Sinon.SinonSpyCall) => {
//                 //             return call.args[0] === 'disconnect';
//                 //         })?.args[1] as (reason: string) => void;
//                 //     });

//                 //     // it('should abandon game', () => {
//                 //     //     const ABANDON_TIMEOUT = 5000;

//                 //     //     Object.defineProperty(game, 'players', { value: [new Player({ playerName, color: Color.Green }, [])] });
//                 //     //     const abandonStub = Sinon.stub(gameTerminator, 'abandonGame');

//                 //     //     disconnectCallback('');
//                 //     //     expect(resetStub.called).to.be.true;

//                 //     //     clock.tick(ABANDON_TIMEOUT);
//                 //     //     expect(abandonStub.called).to.be.true;
//                 //     // });

//                 //     it('should do nothing if game is already ended', () => {
//                 //         const ABANDON_TIMEOUT = 5000;
//                 //         const abandonStub = Sinon.stub(gameTerminator, 'abandonGame');
//                 //         Object.defineProperty(game, 'isGameEnded', { value: true });

//                 //         disconnectCallback('');
//                 //         clock.tick(ABANDON_TIMEOUT);
//                 //         expect(abandonStub.notCalled).to.be.true;
//                 //     });
//                 // });

//                 // describe('leaveGame callback', () => {
//                 //     beforeEach(() => {
//                 //         // eslint-disable-next-line @typescript-eslint/no-empty-function -- Give empty acknowledgement
//                 //         joinGameCallback(playerName, roomName, () => {});

//                 //         const callback = socketStub.on.getCalls().find((call: Sinon.SinonSpyCall) => {
//                 //             return call.args[0] === 'leaveGame';
//                 //         })?.args[1];
//                 //         (callback as () => void)();
//                 //     });

//                 //     it('should join room', () => {
//                 //         expect(socketStub.leave.calledWith()).to.be.true;
//                 //     });
//                 // });
//             });
//         });
//     });

//     describe('resetRoomIfEmpty', () => {
//         it('should handle non-empty room', () => {
//             socketServerStub.sockets.adapter.rooms.set('TestRoom', new Set('user'));
//             expect(() => {
//                 // eslint-disable-next-line dot-notation -- Test private method
//                 manager['resetRoomIfEmpty']('TestRoom');
//             }).not.to.throw();
//         });
//     });

//     afterEach(() => {
//         Container.reset();
//     });
// });
