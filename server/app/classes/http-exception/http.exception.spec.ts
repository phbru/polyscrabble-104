import { HttpException } from '@app/classes/http-exception/http.exception';
import { expect } from 'chai';
import { StatusCodes } from 'http-status-codes';

describe('HttpException', () => {
    it('should set message and status', () => {
        const ex = new HttpException('error message', StatusCodes.NOT_FOUND);
        expect(ex.status).to.be.equal(StatusCodes.NOT_FOUND);
        expect(ex.message).to.be.equal('error message');
    });
});
