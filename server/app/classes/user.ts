/* eslint-disable max-classes-per-file */
import { Statistics } from '@app/types/statistics';
import { Time } from '@app/types/time';
import { Socket } from 'socket.io';
// import * as eloLib from 'elo-rating'; //TODO : not workign cause implicit any type
import { Container, ContainerInstance } from 'typedi';

// Define local properties here.
// These will not be serialized.
class LocalUserProps {
    socket: Socket | undefined;
    container: ContainerInstance;
}

export class User {
    // Hack to avoid some properties from being serialized
    private static localPropsMap: Map<string, LocalUserProps> = new Map();
    username: string = '';
    avatar: string = '';
    email: string = '';
    password: string = '';
    confirmPassword: string = '';
    statistics: Statistics = {
        gamesPlayed: 0,
        gamesWon: 0,
        gamesLost: 0,
        elo: 0,
        rank: 0,
        averagePointByGame: 0,
        averageTimePerGame: { minutes: 0, seconds: 0 },
    };
    readonly id: string = '';

    constructor(id: string) {
        if (id === 'bot') return;
        this.id = id;
        this.setContainer(Container.of(id));
    }

    static makeBot(name: string): User {
        const botUser: User = new User('bot');
        botUser.username = name;
        botUser.avatar = 'assets/images/avatars/bot.png';
        return botUser;
    }

    static clone(user: User): User {
        const clone: User = new User(user.id);
        clone.username = user.username;
        clone.avatar = user.avatar;
        clone.email = user.email;
        clone.password = user.password;

        return clone;
    }

    // Useful to avoid sending avatars over the network
    static cloneNoAvatar(user: User) {
        const clone: User = User.clone(user);
        clone.avatar = '';
        return clone;
    }

    static registered(username: string, avatar: string, email: string, password: string, id: string, stats: Statistics): User {
        const user: User = new User(id);
        user.username = username;
        user.avatar = avatar;
        user.email = email;
        user.password = password;
        user.statistics = stats;

        return user;
    }

    private static getLocalProps(userId: string): LocalUserProps {
        if (!User.localPropsMap.has(userId)) User.localPropsMap.set(userId, new LocalUserProps());
        const localProps = User.localPropsMap.get(userId);
        if (localProps !== undefined) return localProps;

        return new LocalUserProps(); // We will never get here
    }

    adjustUserStats(isGameWon: boolean, cumulativeGameTime: number, averageLosingElo: number, points?: number): void {
        this.statistics.averageTimePerGame = this.numberToTime(
            (this.timeToNumber(this.statistics.averageTimePerGame) * this.statistics.gamesPlayed + cumulativeGameTime) /
                (this.statistics.gamesPlayed + 1),
        );
        if (points !== undefined) {
            this.statistics.averagePointByGame = Math.round(
                (this.statistics.averagePointByGame * this.statistics.gamesPlayed + points) / (this.statistics.gamesPlayed + 1),
            );
        }
        // eslint-disable-next-line @typescript-eslint/no-var-requires, @typescript-eslint/no-require-imports
        const eloRating = require('elo-rating');
        this.statistics.elo = eloRating.calculate(this.statistics.elo, averageLosingElo, isGameWon).playerRating;

        if (isGameWon) {
            this.statistics.gamesWon++;
        } else {
            this.statistics.gamesLost++;
        }
        this.statistics.gamesPlayed++;
    }

    numberToTime(time: number): Time {
        const minutes = Math.floor(time);
        const secondsInMinute = 60;
        const seconds = (time - minutes) * secondsInMinute;
        return { minutes, seconds };
    }

    timeToNumber(time: Time): number {
        const secondsInMinute = 60;
        return time.minutes + time.seconds / secondsInMinute;
    }

    getContainer(): ContainerInstance {
        return User.getLocalProps(this.id).container;
    }

    setSocket(socket: Socket | undefined) {
        User.getLocalProps(this.id).socket = socket;
        socket?.join(this.id);
    }

    getSocket(): Socket | undefined {
        return User.getLocalProps(this.id).socket;
    }

    private setContainer(container: ContainerInstance) {
        User.getLocalProps(this.id).container = container;
    }
}
