import { Player } from '@app/classes/player/player';
import * as constant from '@app/constants/bot-player';

export class BotPlayer extends Player {
    startActionTime: number = 0;
    isBotPlayer = true;
    computeRemainingDelay() {
        const elapsedTime = Date.now() - this.startActionTime;
        let remainingDelay = constant.MAX_BOT_TIME - elapsedTime;
        if (remainingDelay <= 0) remainingDelay = 0;
        return remainingDelay;
    }
}
