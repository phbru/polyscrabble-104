import { User } from '@app/classes/user';
import { Letter } from '@app/types/letter';

export class Player {
    user: User;
    currentLetters: Letter[];
    points: number = 0;
    isBotPlayer: boolean = false; // Even if we have polymorphism for bot player, we need this attribute to send it to client

    constructor(user: User, currentLetters: Letter[], points?: number) {
        this.user = user;
        this.currentLetters = currentLetters;
        if (points !== undefined) {
            this.points = points;
        }
    }

    removeLettersFromPlayer(lettersToRemove: Letter[]) {
        for (const letterToRemove of lettersToRemove) {
            const INVALID_INDEX = -1;
            const currentLetters = this.currentLetters;
            const letterToRemoveIndex: number = letterToRemove.isBlankLetter
                ? currentLetters.findIndex((curLetter: Letter) => curLetter.isBlankLetter)
                : currentLetters.findIndex((curLetter: Letter) => curLetter.character === letterToRemove.character);

            if (letterToRemoveIndex > INVALID_INDEX) {
                this.currentLetters.splice(letterToRemoveIndex, 1);
            }
        }
    }
    addPoints(newPoints: number) {
        this.points += newPoints;
    }

    verifyPlayerHasLetters(lettersToVerify: Letter[]): boolean {
        const invalidIndex = -1;

        const currentLetters = Object.assign([], this.currentLetters);
        let indexOfLetter;
        for (const letter of lettersToVerify) {
            if (letter.isBlankLetter) {
                indexOfLetter = currentLetters.findIndex((currentLetter: Letter) => currentLetter.isBlankLetter);
            } else {
                indexOfLetter = currentLetters.findIndex((currentLetter: Letter) => currentLetter.character === letter.character);
            }

            if (indexOfLetter === invalidIndex) {
                return false;
            } else {
                currentLetters.splice(indexOfLetter, 1);
            }
        }
        return true;
    }
}
