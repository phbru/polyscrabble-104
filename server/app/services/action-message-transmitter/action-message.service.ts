import { User } from '@app/classes/user';
import { ActionCode } from '@app/enums/action-code';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { RoomService } from '@app/services/room-service/room-service';
import { ActionMessage } from '@app/types/action-message';

import { Service } from 'typedi';

@Service()
export class ActionMessageService extends RoomService {
    @MirrorProp() currentAction: ActionMessage;

    emitAction(player: User, actionCode: ActionCode, action: string, quantity?: string): void {
        this.currentAction = { player, actionCode, action, param: quantity };
        this.emitPropUpdate('currentAction');
    }
}
