import { Channel } from '@app/classes/channel/channel';
import { User } from '@app/classes/user';
import { ClientChannelService } from '@app/services/client-channel/client-channel.service';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { Message } from '@app/types/message';
import { Service } from 'typedi';

@Service({ global: true })
export class GlobalChannelService extends RoomService {
    @MirrorProp() channels: Channel[] = [];
    private globalChannel: Channel;
    private channelId: number = 0; // Todo: get ID from MongoDB

    constructor(private globalUserService: GlobalUserService) {
        super();

        // Global channel is the first channel
        this.globalChannel = new Channel();
        this.globalChannel.id = ++this.channelId + '';
        this.globalChannel.name = 'Global';
        this.globalChannel.owner = new User('');
        this.globalChannel.searchable = false;

        // Set global channel for all users
        for (const user of globalUserService.users) {
            this.globalChannel.users.push(User.cloneNoAvatar(user));
            user.getContainer()?.get(ClientChannelService).setGlobalChannel(this.globalChannel);
        }

        this.channels = [this.globalChannel];
    }

    @ProxyCall()
    sendMessage(channelId: string, message: Message) {
        this.findChannel(channelId)?.addMessage(message);
    }

    @ProxyCall()
    createChannel(name: string) {
        const newChannel = new Channel();
        newChannel.id = ++this.channelId + '';
        newChannel.name = name;
        newChannel.owner = this.caller;

        this.channels.push(newChannel);
        this.emitPropUpdate('channels');
        return newChannel;
    }

    // Creates and joins DM channel
    @ProxyCall()
    createDM(userId: string): Channel {
        const user = this.globalUserService.getUser(userId);
        if (user === undefined) {
            // eslint-disable-next-line no-console
            console.log(`Error: cannot create DM: user '${userId}' does not exist!`);
            return new Channel();
        }

        // Make sure it doesn't already exist
        let alreadyExisting: Channel | boolean = false;
        this.channels.forEach((c) => {
            if (c.directMessage) {
                let containsUs = true;
                c.users.forEach((u) => {
                    if (u.id !== userId && u.id !== this.caller.id) {
                        containsUs = false;
                    }
                });

                if (containsUs) alreadyExisting = c;
            }
        });
        if (alreadyExisting !== false) return alreadyExisting;

        const newChannel = this.createChannel('DM');
        newChannel.directMessage = true;
        newChannel.searchable = false;

        // Add users to channel; needs to be done before adding channel to client services
        newChannel.users.push(this.caller);
        newChannel.users.push(user);

        // Add channels to client services
        this.caller.getContainer()?.get(ClientChannelService).addChannel(newChannel, false);
        user.getContainer()?.get(ClientChannelService).addChannel(newChannel, false);

        // Update users
        newChannel.updateConnectedClients();
        this.emitPropUpdate('channels');
        return newChannel;
    }

    @ProxyCall()
    deleteChannel(channelId: string) {
        const channel = this.findChannel(channelId);
        if (channel !== undefined) {
            // Kick all users from channel
            for (const user of channel.users) {
                this.spoofCaller(user).leaveChannel(channelId);
            }

            // Delete channel from global service
            this.channels.forEach((c, i) => {
                if (c.id === channelId) this.channels.splice(i, 1);
            });
            this.emitPropUpdate('channels');
        }
    }

    @ProxyCall()
    joinChannel(channelId: string) {
        this.addUserToChannel(this.caller, channelId, false);
    }

    @ProxyCall()
    leaveChannel(channelId: string) {
        const channel = this.findChannel(channelId);

        if (channel !== undefined) {
            // Remove caller from channel, and remove channel from ClientChannelService
            channel.users.forEach((user, index) => {
                if (user.id === this.caller.id) channel.users.splice(index, 1);
            });
            this.caller.getContainer()?.get(ClientChannelService).removeChannel(channel.id);
            channel.updateConnectedClients();
        }
    }

    @ProxyCall()
    setChannelAsRead(channelId: string) {
        const channel = this.findChannel(channelId);

        if (channel !== undefined) {
            this.caller.getContainer()?.get(ClientChannelService).setChannelAsRead(channel.id);
        }
    }

    addUserToChannel(user: User, channelId: string, isGameChannel: boolean) {
        const channel = this.findChannel(channelId);

        if (channel !== undefined) {
            const clientChannelService = user.getContainer()?.get(ClientChannelService);
            if (clientChannelService === undefined) {
                return;
            }

            // Make sure we are not already in this channel
            if (channel.findUser(user.id) !== undefined) {
                return;
            }

            // If we are already in a game channel, leave it first.
            // We can only be in once game channel at once.
            if (isGameChannel && clientChannelService.gameChannel.length > 0) {
                this.spoofCaller(user).leaveChannel(clientChannelService.gameChannel[0].channel.id);
            }

            // Add user to channel
            channel.users.push(user);
            // Add channel to caller's ClientChannelService
            clientChannelService.addChannel(channel, isGameChannel);
            // Update users
            channel.updateConnectedClients();
            this.emitPropUpdate('channels');
        }
    }

    createGameChannel(creator: User) {
        const newChannel = this.spoofCaller(creator).createChannel('channels.gameChannel');
        newChannel.searchable = false;
        this.emitPropUpdate('channels');
        return newChannel;
    }

    private findChannel(channelId: string): Channel | undefined {
        for (const channel of this.channels) {
            if (channel.id === channelId) {
                return channel;
            }
        }

        return undefined;
    }
}
