import { Direction } from '@app/enums/direction';
import { BoardModelService } from '@app/services/board-model/board-model.service';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { Case } from '@app/types/case';
import { CharacterPosition } from '@app/types/character-position';
import { Letter } from '@app/types/letter';
import { MatrixCoordinates } from '@app/types/matrix-coordinates';
import { WordMap } from '@app/types/word-map';
import { Service } from 'typedi';

@Service()
export class WordFinderService extends RoomService {
    constructor(private boardModel: BoardModelService) {
        super();
    }

    static coordsToCharPos(letter: Letter, coords: MatrixCoordinates): CharacterPosition {
        return { letter, line: String.fromCharCode('A'.charCodeAt(0) + coords.line), column: coords.column + 1 };
    }

    static charPosToCoords(charPos: CharacterPosition): MatrixCoordinates {
        return { line: charPos.line.charCodeAt(0) - 'A'.charCodeAt(0), column: charPos.column - 1 };
    }

    @ProxyCall() getNewWords(wordMap: WordMap): WordMap[] {
        if (wordMap.map.length === 0) return [];

        const newWords: WordMap[] = [];
        const tempGrid = this.cloneBoard(this.boardModel.boardMatrix);
        this.addWordToGrid(tempGrid, wordMap);
        let mainWordDirection: Direction | undefined;
        if (wordMap.map.length === 1) {
            mainWordDirection = this.getAWordDirectionForUniqueLetterPlacement(wordMap, tempGrid);
        } else {
            mainWordDirection = this.getWordDirection(wordMap);
        }

        if (mainWordDirection === undefined) return []; // TODO : throw exception ? ...

        const perpendicularDirection = mainWordDirection === Direction.Horizontal ? Direction.Vertical : Direction.Horizontal;
        for (const charPos of wordMap.map) {
            const wordAtCoord = this.getWordAtCoords(tempGrid, WordFinderService.charPosToCoords(charPos), perpendicularDirection);
            if (wordAtCoord.map.length > 1 && !this.includesWord(this.boardModel.wordsOnGrid, wordAtCoord)) newWords.push(wordAtCoord);
        }

        // Push our own word
        newWords.push(this.getWordAtCoords(tempGrid, WordFinderService.charPosToCoords(wordMap.map[0]), mainWordDirection));
        return newWords;
    }

    @ProxyCall() getWordAtCoords(grid: Case[][], coords: MatrixCoordinates, direction: Direction): WordMap {
        const word: WordMap = { word: '', map: [] };
        const currentCoord = this.getFirstLetterCoord(grid, coords, direction);
        let currentLetter = grid[currentCoord.line]?.[currentCoord.column]?.letter;

        while (currentLetter !== undefined && currentLetter.character !== '?') {
            word.word += currentLetter.character;
            word.map.push(WordFinderService.coordsToCharPos(currentLetter, currentCoord));

            if (direction === Direction.Horizontal) ++currentCoord.column;
            else ++currentCoord.line;
            currentLetter = grid[currentCoord.line]?.[currentCoord.column]?.letter;
        }

        return word;
    }

    getAWordDirectionForUniqueLetterPlacement(wordMap: WordMap, grid: Case[][]) {
        const coords = WordFinderService.charPosToCoords(wordMap.map[0]);
        const upCaseLetter = grid[coords.line - 1]?.[coords.column]?.letter;
        const downCaseLetter = grid[coords.line + 1]?.[coords.column]?.letter;
        const leftCaseLetter = grid[coords.line]?.[coords.column - 1]?.letter;
        const rightCaseLetter = grid[coords.line]?.[coords.column + 1]?.letter;

        if (this.doesCaseContainLetter(upCaseLetter) || this.doesCaseContainLetter(downCaseLetter)) return Direction.Vertical;
        if (this.doesCaseContainLetter(leftCaseLetter) || this.doesCaseContainLetter(rightCaseLetter)) return Direction.Horizontal;

        return undefined;
    }

    doesCaseContainLetter(caseLetter: Letter) {
        return caseLetter !== undefined && caseLetter.character !== '?';
    }
    // Word must have a minimum of 2 letters
    getWordDirection(wordMap: WordMap): Direction | undefined {
        if (wordMap.map.length >= 2) {
            return wordMap.map[0].line === wordMap.map[1].line ? Direction.Horizontal : Direction.Vertical;
        }

        return undefined;
    }

    private getFirstLetterCoord(grid: Case[][], coords: MatrixCoordinates, direction: Direction): MatrixCoordinates {
        const nextCoords: MatrixCoordinates =
            direction === Direction.Horizontal ? { line: coords.line, column: coords.column - 1 } : { line: coords.line - 1, column: coords.column };
        const nextLetter: Letter = grid[nextCoords.line]?.[nextCoords.column]?.letter;

        if (nextLetter === undefined || nextLetter.character === '?') return { line: coords.line, column: coords.column };

        return this.getFirstLetterCoord(grid, nextCoords, direction);
    }

    private cloneBoard(boardToClone: Case[][]): Case[][] {
        const clonedBoard: Case[][] = [];

        for (let i = 0; i < boardToClone.length; i++) {
            clonedBoard.push([]);
            for (const boardCase of boardToClone[i]) {
                clonedBoard[i].push(this.cloneCase(boardCase));
            }
        }

        return clonedBoard;
    }

    private cloneCase(sourceCase: Case): Case {
        const clonedCase = { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: 0 };
        clonedCase.bonus = sourceCase.bonus;
        clonedCase.letter.character = sourceCase.letter.character;
        clonedCase.letter.weight = sourceCase.letter.weight;
        clonedCase.letter.isBlankLetter = sourceCase.letter.isBlankLetter;

        return clonedCase;
    }

    private addWordToGrid(grid: Case[][], wordMap: WordMap) {
        for (const charPos of wordMap.map) {
            const coord = WordFinderService.charPosToCoords(charPos);
            grid[coord.line][coord.column].letter = charPos.letter;
        }
    }

    private includesWord(wordTable: WordMap[], wordToCheck: WordMap) {
        if (wordTable.length === 0) return false;

        for (const potentialWord of wordTable) {
            if (potentialWord.map.length !== wordToCheck.map.length) continue;

            let includesWord = true;
            for (let i = 0; i < potentialWord.map.length; i++) {
                if (!this.areEquivalentCharacters(potentialWord.map[i], wordToCheck.map[i])) {
                    includesWord = false;
                    break;
                }
            }
            if (includesWord) return true;
        }
        return false;
    }

    private areEquivalentCharacters(character1: CharacterPosition, character2: CharacterPosition) {
        return (
            character1.line === character2.line &&
            character1.column === character2.column &&
            character1.letter.character === character2.letter.character
        );
    }
}
