// /* eslint-disable @typescript-eslint/no-unused-expressions -- eslint struggles to understand chai syntax */
// /* eslint-disable no-unused-expressions -- eslint struggles to understand chai syntax */
// import { Bonus } from '@app/enums/bonus';
// import { Direction } from '@app/enums/direction';
// import { BoardModelService } from '@app/services/board-model/board-model.service';
// import { Case } from '@app/types/case';
// import { CharacterPosition } from '@app/types/character-position';
// import { Letter } from '@app/types/letter';
// import { MatrixCoordinates } from '@app/types/matrix-coordinates';
// import { WordMap } from '@app/types/word-map';
// import { expect } from 'chai';
// import * as Sinon from 'sinon';
// import { WordFinderService } from './word-finder.service';

// describe('Static WordFinderServer', () => {
//     it('should convert matrix coords to character position', () => {
//         const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//         const matrixCoord: MatrixCoordinates = { line: 1, column: 1 };
//         const characterPosition: CharacterPosition = { letter, line: 'B', column: 2 };

//         // eslint-disable-next-line dot-notation -- Test private function
//         expect(WordFinderService['coordsToCharPos'](letter, matrixCoord)).to.eql(characterPosition);
//     });

//     it('should convert character position to matrix coords', () => {
//         const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//         const characterPosition: CharacterPosition = { letter, line: 'B', column: 2 };
//         const matrixCoord: MatrixCoordinates = { line: 1, column: 1 };

//         // eslint-disable-next-line dot-notation -- Test private function
//         expect(WordFinderService['charPosToCoords'](characterPosition)).to.eql(matrixCoord);
//     });
// });

// describe('WordFinderService', () => {
//     let service: WordFinderService;
//     let boardModelStub: Sinon.SinonStubbedInstance<BoardModelService>;

//     let mockValidWord1: WordMap;
//     let mockValidWord2: WordMap;
//     let mockBlankGrid: Case[][];
//     let mockGridData: Case[][];

//     beforeEach(() => {
//         boardModelStub = Sinon.createStubInstance(BoardModelService);
//         boardModelStub.wordsOnGrid = [];
//         boardModelStub.boardMatrix = [];

//         service = new WordFinderService(boardModelStub as unknown as BoardModelService);

//         mockValidWord1 = {
//             word: 'famille',
//             map: [
//                 { letter: { character: 'f', weight: 4, isBlankLetter: false }, line: 'A', column: 1 },
//                 { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 2 },
//                 { letter: { character: 'm', weight: 2, isBlankLetter: false }, line: 'A', column: 3 },
//                 { letter: { character: 'i', weight: 1, isBlankLetter: false }, line: 'A', column: 4 },
//                 { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 5 },
//                 { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 6 },
//                 { letter: { character: 'e', weight: 1, isBlankLetter: false }, line: 'A', column: 7 },
//             ],
//         };
//         mockValidWord2 = {
//             word: 'loup',
//             map: [
//                 { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 5 },
//                 { letter: { character: 'o', weight: 1, isBlankLetter: false }, line: 'B', column: 5 },
//                 { letter: { character: 'u', weight: 1, isBlankLetter: false }, line: 'C', column: 5 },
//                 { letter: { character: 'p', weight: 3, isBlankLetter: false }, line: 'D', column: 5 },
//             ],
//         };

//         mockBlankGrid = [
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//         ];

//         mockGridData = [
//             [
//                 { letter: { character: 'f', weight: 4, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: 'a', weight: 1, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: 'm', weight: 2, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: 'i', weight: 1, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: 'l', weight: 1, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: 'l', weight: 1, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: 'e', weight: 1, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: 'o', weight: 1, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: 'u', weight: 1, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: 'p', weight: 3, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//         ];
//     });

//     it('should be created', () => {
//         expect(service).to.exist;
//     });

//     // describe('getCompleteWord', () => {
//     //     it('should return completed word', () => {
//     //         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Stub private method
//     //         Sinon.stub(service as any, 'getWordAtCoords').returns(mockValidWord1);
//     //         // eslint-disable-next-line dot-notation -- Mock private property
//     //         service['boardModel'].boardMatrix = mockGridData;
//     //         expect(service.getCompleteWord(mockValidWord1, { line: 0, column: 3 }, Direction.Horizontal)).to.eql(mockValidWord1);
//     //     });
//     // });

//     describe('getNewWords', () => {
//         it('should return new words with vertical input', () => {
//             // eslint-disable-next-line dot-notation -- Mock private property
//             service['boardModel'].boardMatrix = mockGridData;

//             const expectedWords: WordMap[] = [mockValidWord1, mockValidWord2];
//             expect(service.getNewWords(mockValidWord2)).to.eql(expectedWords);
//         });

//         it('should return new words with horizontal input', () => {
//             // eslint-disable-next-line dot-notation -- Mock private property
//             service['boardModel'].boardMatrix = mockGridData;

//             const expectedWords: WordMap[] = [mockValidWord2, mockValidWord1];
//             expect(service.getNewWords(mockValidWord1)).to.eql(expectedWords);
//         });

//         it('should not return any words with empty wordmap', () => {
//             expect(service.getNewWords({ word: '', map: [] })).to.eql([]);
//         });
//     });

//     describe('getWordDirection', () => {
//         it('should find horizontal direction', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['getWordDirection'](mockValidWord1)).to.eql(Direction.Horizontal);
//         });

//         it('should find vertical direction', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['getWordDirection'](mockValidWord2)).to.eql(Direction.Vertical);
//         });

//         // it('should find horizontal direction if invalid word', () => {
//         //     // eslint-disable-next-line dot-notation -- Test private method
//         //     expect(service['getWordDirection']({ word: '', map: [] })).to.eql(Direction.Horizontal);
//         // });
//     });

//     describe('getFirstLetterCoord', () => {
//         it('should coord with horizontal word', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['getFirstLetterCoord'](mockGridData, { line: 0, column: 3 }, Direction.Horizontal)).to.eql({ line: 0, column: 0 });
//         });

//         it('should coord with vertical word', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['getFirstLetterCoord'](mockGridData, { line: 0, column: 4 }, Direction.Vertical)).to.eql({ line: 0, column: 4 });
//         });
//     });

//     describe('getWordAtCoords', () => {
//         it('should return horizontal word', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['getWordAtCoords'](mockGridData, { line: 0, column: 3 }, Direction.Horizontal)).to.eql(mockValidWord1);
//         });

//         it('should return vertical word', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['getWordAtCoords'](mockGridData, { line: 0, column: 4 }, Direction.Vertical)).to.eql(mockValidWord2);
//         });

//         it('should return empty word', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['getWordAtCoords']([], { line: 0, column: 4 }, Direction.Vertical)).to.eql({ word: '', map: [] });
//         });
//     });

//     describe('cloneBoard', () => {
//         it('should return equivalent board', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             const clonedBoard = service['cloneBoard'](mockGridData);
//             expect(clonedBoard).to.eql(mockGridData);
//         });

//         it('should deep copy', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             const clonedBoard = service['cloneBoard'](mockGridData);
//             clonedBoard[0][0].letter.character = 'W';
//             expect(mockGridData[0][0].letter.character).not.to.equal('W');
//         });
//     });

//     describe('addWordToGrid', () => {
//         it('should add words to grid', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             service['addWordToGrid'](mockBlankGrid, mockValidWord1);
//             // eslint-disable-next-line dot-notation -- Test private method
//             service['addWordToGrid'](mockBlankGrid, mockValidWord2);
//             expect(mockBlankGrid).to.eql(mockGridData);
//         });
//     });

//     describe('includesWord', () => {
//         it('returns true if it includes word', () => {
//             const mockWordTable: WordMap[] = [mockValidWord2];
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['includesWord'](mockWordTable, mockValidWord2)).to.be.true;
//         });

//         it('returns false if it does not include word', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['includesWord']([mockValidWord1], mockValidWord2)).to.be.false;
//         });

//         it('returns false if it does not include similar word', () => {
//             const word1: WordMap = { word: 'A', map: [{ letter: { character: 'A', weight: 0, isBlankLetter: false }, line: 'A', column: 1 }] };
//             const word2: WordMap = { word: 'B', map: [{ letter: { character: 'B', weight: 0, isBlankLetter: false }, line: 'A', column: 1 }] };

//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['includesWord']([word1], word2)).to.be.false;
//         });

//         it('returns false if it mockWordTable is empty', () => {
//             // eslint-disable-next-line dot-notation -- Test private method
//             expect(service['includesWord']([], mockValidWord2)).to.be.false;
//         });
//     });
// });
