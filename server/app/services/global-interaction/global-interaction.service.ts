import { User } from '@app/classes/user';
import { ClientFriendService } from '@app/services/client-friend/client-friend.service';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { Service } from 'typedi';

@Service({ global: true })
export class GlobalInteractionService extends RoomService {
    constructor(private globalUserService: GlobalUserService) {
        super();
    }
    @ProxyCall() createFriendRequest(userId: string): void {
        const requestorUser: User | undefined = this.globalUserService.getUser(this.caller.id);
        const requestedUser: User | undefined = this.globalUserService.getUser(userId);

        if (requestedUser !== undefined && requestorUser !== undefined) {
            requestorUser.getContainer().get(ClientFriendService).addToFriendsRequested(requestedUser);
            requestedUser.getContainer().get(ClientFriendService).addToReceivedInvites(requestorUser);
            requestedUser.getContainer().get(ClientFriendService).notifyOfInvite();
        }
    }

    @ProxyCall() removeFriend(userId: string): void {
        const selfUser: User | undefined = this.globalUserService.getUser(this.caller.id);
        const friend: User | undefined = this.globalUserService.getUser(userId);

        if (selfUser !== undefined && friend !== undefined) {
            selfUser.getContainer().get(ClientFriendService).removeFriend(userId);
            friend.getContainer().get(ClientFriendService).removeFriend(this.caller.id);
        }
    }

    @ProxyCall() acceptInvite(userId: string): void {
        const acceptorUser: User | undefined = this.globalUserService.getUser(this.caller.id);
        const requestorUser: User | undefined = this.globalUserService.getUser(userId);

        if (acceptorUser !== undefined && requestorUser !== undefined) {
            acceptorUser.getContainer().get(ClientFriendService).addToFriends(requestorUser);
            requestorUser.getContainer().get(ClientFriendService).addToFriends(acceptorUser);

            this.removeFriendRequest(requestorUser, acceptorUser);
        }
    }

    @ProxyCall() removeInvite(userId: string): void {
        const requestorUser: User | undefined = this.globalUserService.getUser(this.caller.id);
        const requestedUser: User | undefined = this.globalUserService.getUser(userId);

        if (requestorUser !== undefined && requestedUser !== undefined) {
            this.removeFriendRequest(requestorUser, requestedUser);
            requestedUser.getContainer().get(ClientFriendService).removeNotification();
        }
    }

    @ProxyCall() refuseInvite(refusedUserId: string): void {
        const refusingUser: User | undefined = this.globalUserService.getUser(this.caller.id);
        const refusedUser: User | undefined = this.globalUserService.getUser(refusedUserId);

        if (refusingUser !== undefined && refusedUser !== undefined) {
            this.removeFriendRequest(refusedUser, refusingUser);
        }
    }

    private removeFriendRequest(requestorUser: User, requestedUser: User) {
        requestorUser.getContainer().get(ClientFriendService).removeFromFriendsRequested(requestedUser);
        requestedUser.getContainer().get(ClientFriendService).removeFromReceivedInvites(requestorUser);
    }
}
