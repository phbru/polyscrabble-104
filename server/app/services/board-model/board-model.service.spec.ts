/* eslint-disable max-len */
// /* eslint-disable no-unused-expressions -- eslint confused with chai */
// /* eslint-disable @typescript-eslint/no-unused-expressions -- eslint confused with chai */
// import * as gridData from '@app/data/grid-data.json';
// import { Bonus } from '@app/enums/bonus';
// import { BoardModelService } from '@app/services/board-model/board-model.service';

// import { Case } from '@app/types/case';
// import { CharacterPosition } from '@app/types/character-position';
// import { Letter } from '@app/types/letter';
// import { WordMap } from '@app/types/word-map';
// import { expect } from 'chai';
// import * as Sinon from 'sinon';

// describe('BoardModelService', () => {
//     let service: BoardModelService;

//     let mockGridData: Case[][];
//     let mockBlankGrid: Case[][];
//     let mockWord: WordMap;

//     beforeEach(() => {
//         mockWord = {
//             word: 'ri',
//             map: [
//                 { letter: { character: 'r', weight: 1, isBlankLetter: false }, line: 'B', column: 4 },
//                 { letter: { character: 'i', weight: 1, isBlankLetter: false }, line: 'B', column: 5 },
//             ],
//         };
//         mockGridData = [
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: 'r', weight: 1, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//         ];
//         mockBlankGrid = [
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//             ],
//         ];
//     });

//     it('should be created', () => {
//         expect(service).to.exist;
//     });
//     describe('initializeMatrix', () => {
//         it('should correctly initialize matrix', () => {
//             const mockBoardMatrix: Case[][] = [];
//             let rowIndex = 0;
//             for (const row of gridData.grid) {
//                 mockBoardMatrix.push([]);

//                 for (const unit of row.line) {
//                     const letter: Letter = { character: '?', weight: 0, isBlankLetter: false };
//                     const unitCase: Case = { letter, bonus: unit.bonus };
//                     mockBoardMatrix[rowIndex].push(unitCase);
//                 }
//                 rowIndex++;
//             }
//             service.initializeMatrix();
//             expect(service.boardMatrix).to.eql(mockBoardMatrix);
//         });
//     });

//     describe('shuffle matrix', () => {
//         it('should return all bonuses', () => {
//             const expectedArray: Bonus[] = [
//                 // eslint-disable-next-line @typescript-eslint/no-magic-numbers -- need to create hard-coded expected array.
//                 4, 1, 4, 1, 4, 3, 2, 2, 3, 3, 1, 1, 3, 1, 3, 1, 3, 1, 3, 3, 2, 2, 2, 2, 1, 1, 1, 1, 4, 1, 1, 4, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 1, 3, 1,
//                 // eslint-disable-next-line @typescript-eslint/no-magic-numbers -- need to create hard-coded expected array.
//                 3, 1, 3, 1, 1, 3, 3, 2, 2, 3, 4, 1, 4, 1, 4,
//             ];
//             service.boardMatrix = mockGridData;
//             // eslint-disable-next-line dot-notation -- access private method
//             expect(service['getAllBonuses']()).to.eql(expectedArray);
//         });
//         it('shuffle should put a bonus to all bonus cases', () => {
//             service.shuffleBonuses();
//             let i = 0;
//             let j = 0;
//             for (const row of gridData.grid) {
//                 for (const unit of row.line) {
//                     if (unit.bonus !== Bonus.Blank) {
//                         expect(service.boardMatrix[i][j].bonus).to.not.equal(Bonus.Blank);
//                     }
//                 }
//             }
//             i++;
//             j++;
//         });
//     });

//     // describe('uploadBoardMatrix', () => {
//     //     it('should upload matrix', () => {
//     //         service.uploadBoardMatrix(mockGridData);
//     //         expect(service.boardMatrix).to.eql(mockGridData);
//     //     });
//     // });

//     describe('uploadWords', () => {
//         it('should upload words', () => {
//             service.wordsOnGrid = [];
//             service.uploadWords([mockWord]);
//             expect(service.wordsOnGrid).to.eql([mockWord]);
//         });
//     });

//     describe('returnNecessaryLetters', () => {
//         it('should return good letters', () => {
//             const expectedWord: CharacterPosition[] = [{ letter: { character: 'i', weight: 1, isBlankLetter: false }, line: 'B', column: 5 }];
//             service.boardMatrix = mockGridData;
//             expect(service.returnNecessaryLetters(mockWord)).to.eql(expectedWord);
//         });
//     });

//     describe('checkIfOverwritesWord', () => {
//         it('should return true if overwrites', () => {
//             const overwriteWord = {
//                 word: 'es',
//                 map: [
//                     { letter: { character: 'e', weight: 1, isBlankLetter: false }, line: 'B', column: 4 },
//                     { letter: { character: 's', weight: 1, isBlankLetter: false }, line: 'B', column: 5 },
//                 ],
//             };

//             service.boardMatrix = mockGridData;
//             expect(service.checkIfOverwritesWord(overwriteWord)).to.be.true;
//         });

//         it('should return false if no overwrite', () => {
//             service.boardMatrix = mockGridData;
//             expect(service.checkIfOverwritesWord(mockWord)).to.be.false;
//         });
//     });

//     describe('isBoardEmpty', () => {
//         it('should return true if grid is empty', () => {
//             service.boardMatrix = mockBlankGrid;
//             expect(service.isBoardEmpty()).to.be.true;
//         });

//         it('should return false if grid has data', () => {
//             service.boardMatrix = mockGridData;
//             expect(service.isBoardEmpty()).to.be.false;
//         });
//     });

//     // describe('validateEntryTouchesLetter', () => {
//     //     it('should return true if it word overlaps letter', () => {
//     //         service.boardMatrix = mockGridData;
//     //         expect(service.validateEntryTouchesPlacedLetters(mockWord)).to.be.true;
//     //     });

//     //     it('should return true if it word is on right of letter', () => {
//     //         mockWord.map[0].column = 5;
//     //         service.boardMatrix = mockGridData;
//     //         expect(service.validateEntryTouchesPlacedLetters(mockWord)).to.be.true;
//     //     });

//     //     it('should return true if it word is on left of letter', () => {
//     //         mockWord.map[0].column = 3;
//     //         service.boardMatrix = mockGridData;
//     //         expect(service.validateEntryTouchesPlacedLetters(mockWord)).to.be.true;
//     //     });

//         // it('should return true if it word is on top of letter', () => {
//         //     mockWord.map[0].line = 'A';
//         //     service.boardMatrix = mockGridData;
//         //     expect(service.validateEntryTouchesPlacedLetters(mockWord)).to.be.true;
//         // });

//         // it('should return true if it word is on bottom of letter', () => {
//         //     mockWord.map[0].line = 'C';
//         //     service.boardMatrix = mockGridData;
//         //     expect(service.validateEntryTouchesPlacedLetters(mockWord)).to.be.true;
//         // });

//         // it('should return false if it does not touch letter', () => {
//         //     service.boardMatrix = mockGridData;
//         //     const mockWrongWord: WordMap = {
//         //         word: 'ri',
//         //         map: [
//         //             { letter: { character: 'r', weight: 1, isBlankLetter: false }, line: 'A', column: 6 },
//         //             { letter: { character: 'i', weight: 1, isBlankLetter: false }, line: 'A', column: 7 },
//         //         ],
//         //     };
//         //     expect(service.validateEntryTouchesPlacedLetters(mockWrongWord)).to.be.false;
//         // });

//         // it('should return true with empty grid', () => {
//         //     service.boardMatrix = mockGridData;
//         //     Sinon.stub(service, 'isBoardEmpty').returns(true);
//         //     expect(service.validateEntryTouchesPlacedLetters(mockWord)).to.be.true;
//         // });
//     // });
// });
