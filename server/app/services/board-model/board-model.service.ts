import * as gridData from '@app/data/grid-data.json';
import { Line } from '@app/enums/line';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { Case } from '@app/types/case';
import { CharacterPosition } from '@app/types/character-position';
import { Letter } from '@app/types/letter';
import { MatrixCoordinates } from '@app/types/matrix-coordinates';
import { WordMap } from '@app/types/word-map';
import { Service } from 'typedi';

@Service()
export class BoardModelService extends RoomService {
    @MirrorProp() boardMatrix: Case[][] = [[]]; // "Real" board state
    @MirrorProp() wordsOnGrid: WordMap[] = [];
    @MirrorProp() startingCase: MatrixCoordinates = { line: -1, column: -1 };

    constructor() {
        super();
        this.initializeMatrix();
    }
    @ProxyCall()
    validateEntryTouchesPlacedLetters(wordMap: WordMap) {
        if (this.isBoardEmpty()) {
            return true;
        }
        for (const letter of wordMap.map) {
            const line = Line[letter.line as keyof typeof Line];
            const column = letter.column - 1;

            if (this.validateTouchesPlacedLetter(letter, line, column)) return true;
        }
        return false;
    }
    // Returns true if overwrites word

    @ProxyCall()
    isBoardEmpty() {
        for (const rows of this.boardMatrix) {
            for (const boardCase of rows) {
                if (boardCase.letter.character !== '?') {
                    return false;
                }
            }
        }
        return true;
    }

    @ProxyCall()
    setSelectedCaseArrow(coords: MatrixCoordinates) {
        this.startingCase = coords;
        this.emitPropUpdate('startingCase');
    }

    uploadWords(words: WordMap[]) {
        this.wordsOnGrid.push(...words);
        this.emitPropUpdate('wordsOnGrid');
    }

    returnNecessaryLetters(wordMap: WordMap): CharacterPosition[] {
        const usedLetter: CharacterPosition[] = [];

        for (const letterMap of wordMap.map) {
            const column = letterMap.column - 1;
            const line = Line[letterMap.line as keyof typeof Line];
            if (this.boardMatrix[line][column].letter.character === '?') {
                usedLetter.push(letterMap);
            }
        }
        return usedLetter;
    }

    checkIfOverwritesWord(wordMap: WordMap): boolean {
        for (const letterMap of wordMap.map) {
            const column = letterMap.column - 1;
            const line = Line[letterMap.line as keyof typeof Line];
            if (
                this.boardMatrix[line][column].letter.character !== '?' &&
                this.boardMatrix[line][column].letter.character !== letterMap.letter.character
            ) {
                return true;
            }
        }
        return false;
    }
    validateBounds(wordMap: WordMap): boolean {
        const firstCharacterPos = wordMap.map[0];
        const lastCharacterPos = wordMap.map[wordMap.map.length - 1];
        const lowerColumnBound = 0;
        const upperColumnBound = 14;
        const lowerLineBound: Line = Line.A;
        const upperLineBound: number = Line.O;
        return (
            firstCharacterPos.column > lowerColumnBound &&
            firstCharacterPos.column < upperColumnBound &&
            Line[firstCharacterPos.line as keyof typeof Line] > lowerLineBound &&
            Line[firstCharacterPos.line as keyof typeof Line] < upperLineBound &&
            lastCharacterPos.column > lowerColumnBound &&
            lastCharacterPos.column < upperColumnBound &&
            Line[lastCharacterPos.line as keyof typeof Line] > lowerLineBound &&
            Line[lastCharacterPos.line as keyof typeof Line] < upperLineBound
        );
    }

    addLettersToRealBoard(wordMap: WordMap) {
        for (const letter of wordMap.map) {
            const column = letter.column - 1;
            const line = Line[letter.line as keyof typeof Line];
            this.boardMatrix[line][column].letter = letter.letter;
        }
        this.emitPropUpdate('boardMatrix');
    }

    initializeMatrix() {
        this.boardMatrix = [];
        let rowIndex = 0;

        for (const row of gridData.grid) {
            this.boardMatrix.push([]);

            for (const unit of row.line) {
                const letter: Letter = { character: '?', weight: 0, isBlankLetter: false };
                const unitCase: Case = { letter, bonus: unit.bonus };

                this.boardMatrix[rowIndex].push(unitCase);
            }
            rowIndex++;
        }
        this.emitPropUpdate('boardMatrix');
    }

    // private getAllBonuses(): Bonus[] { // TODO : remove ?
    //     const allBonuses: Bonus[] = [];
    //     for (const row of gridData.grid) {
    //         for (const unit of row.line) {
    //             if (unit.bonus !== Bonus.Blank && unit.bonus !== Bonus.Star) allBonuses.push(unit.bonus);
    //         }
    //     }
    //     return allBonuses;
    // }

    private validateTouchesPlacedLetter(letter: CharacterPosition, line: number, column: number) {
        return (
            this.touchesOurSquare(letter, line, column) ||
            this.touchesOnLeft(letter, line, column) ||
            this.touchesOnRight(letter, line, column) ||
            this.touchesOnTop(letter, line, column) ||
            this.touchesOnBottom(letter, line, column)
        );
    }

    private touchesOnTop(char: CharacterPosition, line: number, column: number) {
        return column < this.boardMatrix[0].length - 1 && this.boardMatrix[line][column + 1].letter.character !== '?';
    }

    private touchesOnBottom(char: CharacterPosition, line: number, column: number) {
        return column > 0 && this.boardMatrix[line][column - 1].letter.character !== '?';
    }

    private touchesOnRight(char: CharacterPosition, line: number, column: number) {
        return line < this.boardMatrix.length - 1 && this.boardMatrix[line + 1][column].letter.character !== '?';
    }

    private touchesOnLeft(char: CharacterPosition, line: number, column: number) {
        return line > 0 && this.boardMatrix[line - 1][column].letter.character !== '?';
    }

    private touchesOurSquare(char: CharacterPosition, line: number, column: number) {
        return this.boardMatrix[line][column].letter.character !== '?';
    }
}
