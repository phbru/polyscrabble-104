import { Channel } from '@app/classes/channel/channel';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { RoomService } from '@app/services/room-service/room-service';
import { SelfUserService } from '@app/services/self-user/self-user.service';
import { ClientChannel } from '@app/types/client-channel';
import { Service } from 'typedi';

@Service()
export class ClientChannelService extends RoomService {
    @MirrorProp() joinedChannels: ClientChannel[] = []; // Does not hold global or game channels
    @MirrorProp() globalChannel: ClientChannel = { newMessages: false, channel: new Channel() };
    @MirrorProp() gameChannel: ClientChannel[] = []; // Will hold 0 or 1 channel

    constructor(private selfUserService: SelfUserService) {
        super();
    }

    // This should be called once at server start
    setGlobalChannel(channel: Channel) {
        this.globalChannel = { newMessages: false, channel };
    }

    // You shouldn't call this directly, use GlobalChannelService instead.
    // Guarantees channel uniqueness.
    // Must have 2 users if it is a direct message.
    addChannel(channel: Channel, isGameChannel: boolean) {
        // If it is a direct message, each user has a different channel name (name of other username)
        if (channel.directMessage) {
            channel = channel.shallowCopy();

            if (channel.users.length < 2) {
                // eslint-disable-next-line no-console
                console.log(`Error: cannot add DM channel ${channel.name} to user; channel does not have 2 users!`);
                return;
            }

            // Set channel name as other username
            if (this.selfUserService.user.id === channel.users[0].id) {
                // We are the first user
                channel.name = channel.users[1].username;
            } else {
                // We are the second user
                channel.name = channel.users[0].username;
            }
        }

        // Make sure we do not already have this channel
        this.joinedChannels.forEach((c, i) => {
            if (c.channel.id === channel.id) this.joinedChannels.splice(i, 1);
        });

        if (isGameChannel) {
            // Only one game channel at once
            this.gameChannel = [{ newMessages: false, channel }];
        } else {
            this.joinedChannels.unshift({ newMessages: false, channel });
            this.emitPropUpdate('joinedChannels');
        }
    }

    // You shouldn't call this directly, use GlobalChannelService instead.
    removeChannel(channelId: string) {
        this.joinedChannels.forEach((c, i) => {
            if (c.channel.id === channelId) this.joinedChannels.splice(i, 1);
        });
        this.emitPropUpdate('joinedChannels');

        // Remove if it was our game channel
        if (this.gameChannel.length > 0 && this.gameChannel[0].channel.id === channelId) {
            this.gameChannel = [];
        }
    }

    // You shouldn't call this directly, use GlobalChannelService instead.
    setChannelAsRead(channelId: string) {
        this.joinedChannels.forEach((c) => {
            if (c.channel.id === channelId && c.newMessages) {
                c.newMessages = false;
                this.emitPropUpdate('joinedChannels');
            }
        });

        if (this.globalChannel.channel.id === channelId && this.globalChannel.newMessages) {
            // Global channel
            this.globalChannel.newMessages = false;
            this.emitPropUpdate('globalChannel');
        } else if (this.gameChannel.length > 0 && this.gameChannel[0].channel.id === channelId && this.gameChannel[0].newMessages) {
            // Game channel
            this.gameChannel[0].newMessages = false;
            this.emitPropUpdate('gameChannel');
        }
    }

    setChannelAsUnread(channelId: string) {
        this.joinedChannels.forEach((c) => {
            if (c.channel.id === channelId && !c.newMessages) {
                c.newMessages = true;
                this.emitPropUpdate('joinedChannels');
            }
        });

        if (this.globalChannel.channel.id === channelId) {
            // Global channel
            this.globalChannel.newMessages = true;
            this.emitPropUpdate('globalChannel');
        } else if (this.gameChannel.length > 0 && this.gameChannel[0].channel.id === channelId) {
            // Game channel
            this.gameChannel[0].newMessages = true;
            this.emitPropUpdate('gameChannel');
        }
    }

    sendChannelUpdates() {
        this.emitPropUpdate('joinedChannels');
        this.emitPropUpdate('globalChannel');
        this.emitPropUpdate('gameChannel');
    }
}
