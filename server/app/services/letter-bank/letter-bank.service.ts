import * as letterData from '@app/data/letter-data.json';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { Letter } from '@app/types/letter';
import { Service } from 'typedi';

@Service()
export class LetterBankService extends RoomService {
    @MirrorProp() letterBank: Letter[] = [];

    constructor() {
        super();
        this.letterBank = this.getLettersFromData(letterData);
    }

    // Converts char to Letter
    static getLetterFromCharacter(character: string): Letter {
        // TODO : Retirer
        const matchRegex = new RegExp(character, 'i'); // case insensitive

        for (const letterEntry of letterData.letters) {
            if (letterEntry.character.match(matchRegex) !== null) {
                // Found character in letter entry!
                const isBlankLetter = character === character.toUpperCase() ? true : false;
                const letter: Letter = {
                    character: letterEntry.character,
                    weight: letterEntry.weight,
                    isBlankLetter,
                };
                if (isBlankLetter) {
                    letter.weight = 0;
                }

                return letter;
            }
        }
        return { character: '', weight: 0, isBlankLetter: false };
    }

    @ProxyCall()
    getInitialLetterBank() {
        this.emitPropUpdate('letterBank');
    }

    giveBackLetterToBank(letter: Letter) {
        this.letterBank.push(letter);
        this.emitPropUpdate('letterBank');
    }

    reinitializeLetterBank() {
        this.letterBank = this.getLettersFromData(letterData);
    }

    retrieveRandomLetters(numLetters: number): Letter[] {
        const retrievedLetters: Letter[] = [];

        for (let i = 0; i < numLetters; ++i) {
            const randomIndex = Math.floor(Math.random() * this.letterBank.length);
            const removedLetterArray = this.letterBank.splice(randomIndex, 1);

            if (removedLetterArray.length > 0) {
                retrievedLetters.push(removedLetterArray[0]);
            }
        }

        this.emitPropUpdate('letterBank');
        return retrievedLetters;
    }

    private getLettersFromData(data: typeof letterData): Letter[] {
        const letters: Letter[] = [];

        for (const letterEntry of data.letters) {
            const letterCopy: Letter = {
                character: letterEntry.character,
                weight: letterEntry.weight,
                isBlankLetter: letterEntry.character === '*' ? true : false,
            };

            for (let i = 0; i < letterEntry.amount; ++i) {
                letters.push(letterCopy);
            }
        }

        return letters;
    }
}
