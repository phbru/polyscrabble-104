// /* eslint-disable no-unused-expressions -- eslint confused with chai */
// /* eslint-disable @typescript-eslint/no-unused-expressions -- eslint confused with chai*/
// import * as letterData from '@app/data/letter-data.json';
// import { LetterBankService } from '@app/services/letter-bank/letter-bank.service';
// import { Letter } from '@app/types/letter';
// import { expect } from 'chai';

// const mockLetters = [
//     { character: 'A', weight: 2, amount: 1 },
//     { character: 'B', weight: 3, amount: 1 },
//     { character: 'C', weight: 4, amount: 1 },
//     { character: '*', weight: 4, amount: 2 },
// ];
// const NUM_LETTERS = 5;

// describe('Static LetterBankService', () => {
//     beforeEach(() => {
//         // Replace JSON with mock data
//         // https://stackoverflow.com/questions/59945197/karma-property-does-not-have-access-type-get
//         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- replace JSON with mock data
//         (letterData as any).letters = mockLetters;
//     });

//     it('should get letter from character', () => {
//         expect(LetterBankService.getLetterFromCharacter('a')).to.eql({ character: 'A', weight: 2, isBlankLetter: false });
//     });

//     it('should get blank letter from upper-case character', () => {
//         expect(LetterBankService.getLetterFromCharacter('A')).to.eql({ character: 'A', weight: 0, isBlankLetter: true });
//     });

//     it('should return empty character if not found', () => {
//         expect(LetterBankService.getLetterFromCharacter('w')).to.eql({ character: '', weight: 0, isBlankLetter: false });
//     });
// });

// describe('LetterBankService', () => {
//     let service: LetterBankService;

//     beforeEach(() => {
//         // Replace JSON with mock data
//         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- replace JSON with mock data
//         (letterData as any).letters = mockLetters;
//     });

//     beforeEach(() => {
//         service = new LetterBankService();
//     });

//     it('should be created', () => {
//         expect(service).to.exist;
//     });

//     describe('giveBackLetterToBank', () => {
//         it('should give letter to bank', () => {
//             const letter: Letter = { character: 'A', weight: 0, isBlankLetter: false };
//             service.letterBank = [];
//             service.giveBackLetterToBank(letter);
//             expect(service.letterBank[0]).to.eql(letter);
//         });
//     });

//     describe('reinitializeLetterBank', () => {
//         it('should reinitialize correctly', () => {
//             service.letterBank = [];
//             service.reinitializeLetterBank();
//             expect(service.letterBank.length).to.eql(NUM_LETTERS);
//         });
//     });

//     describe('retrieveRandomLetters', () => {
//         it('should retrieve random letters', () => {
//             const nbrLetters = 4;
//             expect(service.retrieveRandomLetters(nbrLetters).length).to.eql(nbrLetters);
//         });

//         it('should not retrieve negative amount of random letters', () => {
//             const nbrLetters = -10;
//             expect(service.retrieveRandomLetters(nbrLetters).length).to.eql(0);
//         });

//         it('should not retrieve more letters than in bank', () => {
//             expect(service.retrieveRandomLetters(NUM_LETTERS + 3).length).to.eql(NUM_LETTERS);
//         });
//     });

//     it('should get letters from letter data', () => {
//         // eslint-disable-next-line dot-notation -- access private method
//         expect(service['getLettersFromData'](letterData).length).to.eql(NUM_LETTERS);
//     });
// });
