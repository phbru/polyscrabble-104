import { Bonus } from '@app/enums/bonus';
import { Line } from '@app/enums/line';
import { BoardModelService } from '@app/services/board-model/board-model.service';
import { RoomService } from '@app/services/room-service/room-service';
import { WordMap } from '@app/types/word-map';
import { Service } from 'typedi';

@Service()
export class ScoreCounterService extends RoomService {
    constructor(public boardModel: BoardModelService) {
        super();
    }

    countAllPoints(validWords: WordMap[]): number {
        let pointCount = 0;
        for (const word of validWords) {
            pointCount += this.countPointOneWord(word);
        }
        return pointCount;
    }

    removeBonus(validWords: WordMap[]) {
        // TODO: Should be somewhere else ?
        for (const word of validWords) {
            for (const letter of word.map) {
                const column = letter.column - 1;
                const line = Line[letter.line as keyof typeof Line];
                this.boardModel.boardMatrix[line][column].bonus = Bonus.Blank;
            }
        }

        this.boardModel.emitPropUpdate('boardMatrix');
    }

    countPointOneWord(word: WordMap): number {
        let wordMultiplicationFactor = 1;
        let wordPoint = 0;
        for (const letter of word.map) {
            const column = letter.column - 1;
            const line = Line[letter.line as keyof typeof Line];
            switch (this.boardModel.boardMatrix[line][column].bonus) {
                case Bonus.LetterX2:
                    wordPoint += letter.letter.weight * 2;
                    break;
                case Bonus.LetterX3:
                    wordPoint += letter.letter.weight * 3;
                    break;
                case Bonus.WordX2:
                    wordPoint += letter.letter.weight;
                    wordMultiplicationFactor *= 2;
                    break;
                case Bonus.WordX3:
                    wordPoint += letter.letter.weight;
                    wordMultiplicationFactor *= 3;
                    break;
                case Bonus.Star:
                    wordPoint += letter.letter.weight;
                    wordMultiplicationFactor *= 2;
                    break;

                default:
                    wordPoint += letter.letter.weight;
            }
        }
        wordPoint *= wordMultiplicationFactor;
        return wordPoint;
    }
}
