/* eslint-disable no-unused-expressions -- to respect chai syntax. */
/* eslint-disable @typescript-eslint/no-unused-expressions -- to respect chai syntax*/
import { Bonus } from '@app/enums/bonus';
import { BoardModelService } from '@app/services/board-model/board-model.service';
import { Case } from '@app/types/case';
import { CharacterPosition } from '@app/types/character-position';
import { WordMap } from '@app/types/word-map';
import { expect } from 'chai';
import * as Sinon from 'sinon';
import { ScoreCounterService } from './score-counter.service';

describe('ScoreCounterService', () => {
    let boardModelStub: Sinon.SinonStubbedInstance<BoardModelService>;
    let service: ScoreCounterService;
    let mockGridData: Case[][] = [];

    beforeEach(() => {
        boardModelStub = Sinon.createStubInstance(BoardModelService);
        service = new ScoreCounterService(boardModelStub as unknown as BoardModelService);
    });

    it('should be created', () => {
        expect(service).to.exist;
    });

    beforeEach(() => {
        mockGridData = [
            [
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
            ],
            [
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
            ],
            [
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
            ],
            [
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Star },
                { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
            ],
        ];
        service.boardModel.boardMatrix = mockGridData;
    });

    describe('countPoints', () => {
        it('should return good amount of points for one word', () => {
            const mockWord = 'famille';
            const mockCharacterPos: CharacterPosition[] = [];
            mockCharacterPos[0] = { letter: { character: 'f', weight: 4, isBlankLetter: false }, line: 'A', column: 1 };
            mockCharacterPos[1] = { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 2 };
            mockCharacterPos[2] = { letter: { character: 'm', weight: 2, isBlankLetter: false }, line: 'A', column: 3 };
            mockCharacterPos[3] = { letter: { character: 'i', weight: 1, isBlankLetter: false }, line: 'A', column: 4 };
            mockCharacterPos[4] = { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 5 };
            mockCharacterPos[5] = { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 6 };
            mockCharacterPos[6] = { letter: { character: 'e', weight: 1, isBlankLetter: false }, line: 'A', column: 7 };
            const mockWordMap: WordMap = { word: mockWord, map: mockCharacterPos };
            const expectedPoints =
                (mockCharacterPos[0].letter.weight +
                    mockCharacterPos[1].letter.weight * 2 +
                    mockCharacterPos[2].letter.weight * 3 +
                    mockCharacterPos[3].letter.weight +
                    mockCharacterPos[4].letter.weight +
                    mockCharacterPos[5].letter.weight +
                    mockCharacterPos[6].letter.weight) *
                2 * // Word x2
                3 * // Word x3
                2; // Star
            // eslint-disable-next-line dot-notation -- Test private method
            expect(service['countPointOneWord'](mockWordMap)).to.equal(expectedPoints);
        });

        it('should return good amount of points for multiple words', () => {
            const mockWords: WordMap[] = [];

            const mockWord1 = 'famille';
            const mockCharacterPos1: CharacterPosition[] = [];
            mockCharacterPos1[0] = { letter: { character: 'f', weight: 4, isBlankLetter: false }, line: 'A', column: 1 };
            mockCharacterPos1[1] = { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 2 };
            mockCharacterPos1[2] = { letter: { character: 'm', weight: 2, isBlankLetter: false }, line: 'A', column: 3 };
            mockCharacterPos1[3] = { letter: { character: 'i', weight: 1, isBlankLetter: false }, line: 'A', column: 4 };
            mockCharacterPos1[4] = { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 5 };
            mockCharacterPos1[5] = { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 6 };
            mockCharacterPos1[6] = { letter: { character: 'e', weight: 1, isBlankLetter: false }, line: 'A', column: 7 };
            const mockWordMap1: WordMap = { word: mockWord1, map: mockCharacterPos1 };
            mockWords.push(mockWordMap1);
            const expectedPoints1 =
                (mockCharacterPos1[0].letter.weight +
                    mockCharacterPos1[1].letter.weight * 2 +
                    mockCharacterPos1[2].letter.weight * 3 +
                    mockCharacterPos1[3].letter.weight +
                    mockCharacterPos1[4].letter.weight +
                    mockCharacterPos1[5].letter.weight +
                    mockCharacterPos1[6].letter.weight) *
                2 * // Word x2
                3 * // Word x3
                2; // Star

            const mockWord2 = 'loup';
            const mockCharacterPos2: CharacterPosition[] = [];
            mockCharacterPos2[0] = { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 5 };
            mockCharacterPos2[1] = { letter: { character: 'o', weight: 1, isBlankLetter: false }, line: 'B', column: 5 };
            mockCharacterPos2[2] = { letter: { character: 'u', weight: 1, isBlankLetter: false }, line: 'C', column: 5 };
            mockCharacterPos2[3] = { letter: { character: 'p', weight: 3, isBlankLetter: false }, line: 'D', column: 5 };
            const mockWordMap2: WordMap = { word: mockWord2, map: mockCharacterPos2 };
            mockWords.push(mockWordMap2);
            const expectedPoints2 =
                (mockCharacterPos2[0].letter.weight +
                    mockCharacterPos2[1].letter.weight +
                    mockCharacterPos2[2].letter.weight +
                    mockCharacterPos2[3].letter.weight) *
                3 *
                3 *
                3 *
                3;

            const expectedTotalPoints = expectedPoints1 + expectedPoints2;
            expect(service.countAllPoints(mockWords)).to.equal(expectedTotalPoints);
        });
    });
    describe('removeBonus', () => {
        it('bonus should be removed', () => {
            const mockWord = 'famille';
            const mockCharacterPos: CharacterPosition[] = [];
            mockCharacterPos[0] = { letter: { character: 'f', weight: 4, isBlankLetter: false }, line: 'A', column: 1 };
            mockCharacterPos[1] = { letter: { character: 'a', weight: 1, isBlankLetter: false }, line: 'A', column: 2 };
            mockCharacterPos[2] = { letter: { character: 'm', weight: 2, isBlankLetter: false }, line: 'A', column: 3 };
            mockCharacterPos[3] = { letter: { character: 'i', weight: 1, isBlankLetter: false }, line: 'A', column: 4 };
            mockCharacterPos[4] = { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 5 };
            mockCharacterPos[5] = { letter: { character: 'l', weight: 1, isBlankLetter: false }, line: 'A', column: 6 };
            mockCharacterPos[6] = { letter: { character: 'e', weight: 1, isBlankLetter: false }, line: 'A', column: 7 };
            const mockWordMap: WordMap = { word: mockWord, map: mockCharacterPos };

            service.removeBonus([mockWordMap]);
            expect(service.boardModel.boardMatrix[0][3].bonus).to.equal(Bonus.Blank);
        });
    });
});
