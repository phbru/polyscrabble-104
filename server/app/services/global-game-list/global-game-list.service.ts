import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { GameInfo } from '@app/types/game-info';
import { Service } from 'typedi';

@Service({ global: true })
export class GlobalGameListService extends RoomService {
    @MirrorProp() games: GameInfo[] = [];

    @ProxyCall()
    addGame(gameInfo: GameInfo) {
        for (const game of this.games) {
            if (gameInfo.id === game.id) return;
        }
        this.games.push(gameInfo);
        this.emitPropUpdate('games');
    }

    @ProxyCall()
    updateGame(gameInfo: GameInfo) {
        this.games.forEach((g, i) => {
            if (g.id === gameInfo.id) this.games[i] = gameInfo;
        });
        this.emitPropUpdate('games');
    }

    removeUser(userId: string, gameId: string): void {
        const gameInfo = this.findGame(gameId);
        if (gameInfo === undefined) return;

        if (gameInfo.players.length > 0) {
            gameInfo.players.forEach((u, i) => {
                if (u.id === userId) gameInfo.players.splice(i, 1);
            });
        }
        if (gameInfo.observers.length > 0) {
            gameInfo.observers.forEach((u, i) => {
                if (u.id === userId) gameInfo.observers.splice(i, 1);
            });
        }

        this.updateGame(gameInfo);
    }

    removeGame(gameId: string) {
        this.games.forEach((g, i) => {
            if (g.id === gameId) this.games.splice(i, 1);
        });
        this.emitPropUpdate('games');
    }

    private findGame(gameId: string): GameInfo | undefined {
        for (const game of this.games) {
            if (game.id === gameId) {
                return game;
            }
        }

        return undefined;
    }
}
