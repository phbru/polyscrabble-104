/*
 * ProxyCall
 * Decorator for allowing method calls from a client-side proxy.
 * Must be used in conjunction with @CallServer on client-side.
 */

import { User } from '@app/classes/user';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { RoomService } from '@app/services/room-service/room-service';
import { Container } from 'typedi';

// eslint-disable-next-line @typescript-eslint/naming-convention -- respecting naming convention for Decorators
export const ProxyCall = () => {
    return (target: RoomService, propertyKey: string) => {
        // eslint-disable-next-line dot-notation -- access private method
        const message = target['getProxyCallMessage'](propertyKey);

        RoomService.addDefaultSocketEventToClass(target, message, async function (this: RoomService, userId: string, ...args: unknown[]) {
            // Client expects acknowledgement for return value, even if there isn't one.
            // Acknowledgement is provided by SocketIO as last argument of event callback.
            type CallbackFunction = (response: unknown) => void;
            const acknowledgement = args.pop() as CallbackFunction;

            // Set the current caller according to the client-provided userId.
            const user = Container.get(GlobalUserService).getUser(userId);
            if (userId === '') {
                // If the userId was empty, silently ignore (happens when clients are too eager during their startup)
                return;
            } else if (user === undefined) {
                // eslint-disable-next-line no-console
                console.log(`ProxyCall Error: user '${userId}' does not exist!`);
                return;
            }
            RoomService.globalCaller = user;

            // Call function
            // eslint-disable-next-line no-invalid-this
            const returnVal = this[propertyKey](...args);
            RoomService.globalCaller = new User(''); // Immediately reset caller
            acknowledgement(await returnVal); // Send acknowledgement. Await in-case we got a promise.
        });
    };
};
