/* eslint-disable no-console */
import { User } from '@app/classes/user';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { RoomService } from '@app/services/room-service/room-service';
import { UserRepository } from '@app/services/user-repository/user-repository';
import { RankingIdPair } from '@app/types/ranking-id-pair';
import { Service } from 'typedi';

@Service({ global: true })
export class GlobalUserService extends RoomService {
    @MirrorProp() private onlineUsers: User[] = [];
    users: User[] = [];
    ranking: RankingIdPair[] = [];

    constructor(private userRepository: UserRepository) {
        super();
    }

    async setupUsersFromRepo() {
        console.log('Downloading users from database...');
        this.users = await this.userRepository.getAllUsers();
        console.log(`All users downloaded from database (${this.users.length} users).`);
    }

    getUserFromSocket(socketId: string): User | undefined {
        // Only online users have sockets
        for (const user of this.onlineUsers) {
            if (user.getSocket()?.id === socketId) {
                return user;
            }
        }

        return undefined;
    }

    adjustGlobalStatistics() {
        for (const userToUpdate of this.users) {
            let position = 1;
            for (const userToCompare of this.users) {
                if (userToUpdate.id !== userToCompare.id && userToUpdate.statistics.elo < userToCompare.statistics.elo) {
                    position++;
                }
            }
            userToUpdate.statistics.rank = position;
        }
        this.emitPropUpdate('onlineUsers');
    }

    getUser(userId: string): User | undefined {
        for (const user of this.users) {
            if (user.id === userId) {
                return user;
            }
        }
        return undefined;
    }

    isUserOnline(userId: string): boolean {
        for (const user of this.onlineUsers) {
            if (user.id === userId) {
                return true;
            }
        }
        return false;
    }

    // User must be a valid user
    addUser(user: User): void {
        if (this.getUser(user.id) !== undefined) {
            // eslint-disable-next-line no-console
            console.log(`Error: cannot add user '${user.id}'; user already exists!`);
            return;
        }
        this.users.push(user);
    }

    setUserOnline(userId: string) {
        const user = this.getUser(userId);
        if (user === undefined) {
            // eslint-disable-next-line no-console
            console.log(`Error: cannot set user '${userId}' online; it does not exist!`);
            return;
        }

        // Make sure user is not already online
        for (const u of this.onlineUsers) {
            if (u.id === user.id) {
                return;
            }
        }
        this.onlineUsers.push(user);
        this.emitPropUpdate('onlineUsers');
    }

    setUserOffline(userId: string) {
        const NONE = -1;
        let index = NONE;
        for (let i = 0; i < this.onlineUsers.length; ++i) {
            if (this.onlineUsers[i].id === userId) {
                index = i;
                break;
            }
        }

        if (index === NONE) {
            // eslint-disable-next-line no-console
            console.log(`Error: cannot set user '${userId}' offline; it does not exist!`);
            return;
        }

        // Invalidate socket
        this.onlineUsers[index].setSocket(undefined);
        this.onlineUsers.splice(index, 1);
        this.emitPropUpdate('onlineUsers');
    }
}
