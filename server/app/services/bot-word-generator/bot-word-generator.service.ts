import { BotPlayer } from '@app/classes/bot-player/bot-player';
import { EMPTY_WORD_MAP } from '@app/constants/bot-player';
import * as letterData from '@app/data/letter-data.json';
import { Alphabet } from '@app/enums/alphabet';
import { Direction } from '@app/enums/direction';
import { Line } from '@app/enums/line';
import { BoardModelService } from '@app/services/board-model/board-model.service';
import { DictionaryService } from '@app/services/dictionary/dictionary.service';
import { RoomService } from '@app/services/room-service/room-service';
import { ScoreCounterService } from '@app/services/score-counter/score-counter.service';
import { WordFinderService } from '@app/services/word-finder/word-finder.service';
import { CharacterPosition } from '@app/types/character-position';
import { Letter } from '@app/types/letter';
import { MatrixCoordinates } from '@app/types/matrix-coordinates';
import { WordMap } from '@app/types/word-map';
import { WordMapWithScore } from '@app/types/word-map-with-score';
import { Service } from 'typedi';

const TIME_LIMIT_FOR_SEARCHING = 6000;
const MAX_WORDS_RETURNED = 3;

@Service()
export class BotWordGeneratorService extends RoomService {
    casesStillConsidered: MatrixCoordinates[] = [];
    generatedWords: WordMapWithScore[] = [];
    wordsBotHasLettersFor: Map<string, string[]>;
    botPlayer: BotPlayer;

    constructor(
        private boardModel: BoardModelService,
        private dictionaryService: DictionaryService,
        private scoreCounterService: ScoreCounterService,
        private wordFinderService: WordFinderService,
    ) {
        super();
        this.wordsBotHasLettersFor = new Map();
    }

    // eslint-disable-next-line no-undef
    async generateWordMapToPlace(botPlayer: BotPlayer): Promise<WordMap | undefined> {
        this.reInitiateGeneratedWords(botPlayer);
        this.setOccupiedCases();
        const startSearchingWordTime: number = Date.now();
        let elapsedTime: number = Date.now() - startSearchingWordTime;

        if (this.boardModel.isBoardEmpty()) {
            await this.generateWordsFirstPlacement();
        } else {
            while (this.validateKeepLooking(elapsedTime)) {
                const randomCaseCoordinates = this.considerRandomCase();

                const generateWordPromise = new Promise<void>((resolve) => {
                    this.generatePossibleWordsForCase(randomCaseCoordinates);
                    resolve();
                });
                await generateWordPromise;
                elapsedTime = Date.now() - startSearchingWordTime;
            }
        }

        return this.generatedWords.length > 0 ? this.generatedWords[0].wordMap : undefined;
    }
    private reInitiateGeneratedWords(botPlayer: BotPlayer) {
        this.generatedWords = [];
        this.wordsBotHasLettersFor.clear();
        this.botPlayer = botPlayer;
    }
    private considerRandomCase(): MatrixCoordinates {
        const randomIndex = Math.floor(Math.random() * (this.casesStillConsidered.length - 1));
        const chosenCaseCoords = { line: this.casesStillConsidered[randomIndex].line, column: this.casesStillConsidered[randomIndex].column };
        this.casesStillConsidered.splice(randomIndex, 1);
        return chosenCaseCoords;
    }

    private setOccupiedCases(): void {
        this.casesStillConsidered = [];
        for (let i = 0; i < this.boardModel.boardMatrix.length; i++) {
            for (let j = 0; j < this.boardModel.boardMatrix[i].length; j++) {
                if (this.boardModel.boardMatrix[i][j].letter.character !== '?') {
                    this.casesStillConsidered.push({ line: i, column: j });
                }
            }
        }
    }

    private async generateWordsFirstPlacement() {
        const NUMBER_OF_DIRECTIONS = 2;
        const direction: Direction = Math.floor(Math.random() * NUMBER_OF_DIRECTIONS) - 1;
        const botLetters: Letter[] = this.botPlayer.currentLetters;
        for (const letter of botLetters) {
            const wordsContainingLetter: string[] = this.dictionaryService.findAllWordsContaining([letter.character.toLowerCase()])[0];

            for (const wordContainingLetter of wordsContainingLetter) {
                if (this.verifyBotHasLetters(this.stringToLetterArray(wordContainingLetter))) {
                    const wordMap = this.firstWordToWordMap(wordContainingLetter, direction);
                    this.orderedPush(wordMap);
                }
            }
        }
    }

    private async generatePossibleWordsForCase(caseCoordinates: MatrixCoordinates) {
        let horizontalWordRoot: string = this.getWordAtCoords(caseCoordinates, Direction.Horizontal);
        let verticalWordRoot: string = this.getWordAtCoords(caseCoordinates, Direction.Vertical);
        horizontalWordRoot = horizontalWordRoot.toLocaleLowerCase();
        verticalWordRoot = verticalWordRoot.toLocaleLowerCase();
        const wordRootsToSearchInDictionary: string[] = [];

        // To not reconsider a same word root:
        if (horizontalWordRoot !== '' && !this.wordsBotHasLettersFor.has(horizontalWordRoot)) {
            wordRootsToSearchInDictionary.push(horizontalWordRoot);
        }
        if (verticalWordRoot !== '' && !this.wordsBotHasLettersFor.has(verticalWordRoot)) {
            wordRootsToSearchInDictionary.push(verticalWordRoot);
        }

        const generateWordPromise = new Promise<string[][]>((resolve) => {
            const wordsContainingRoots = this.dictionaryService.findAllWordsContaining(wordRootsToSearchInDictionary);
            resolve(wordsContainingRoots);
        });
        generateWordPromise.then((wordsContainingRoots) => {
            for (let i = 0; i < wordRootsToSearchInDictionary.length; i++) {
                this.expandWordsBotHasLettersFor(wordRootsToSearchInDictionary[i], wordsContainingRoots[i]);
            }

            this.pushValidWords(horizontalWordRoot, caseCoordinates, Direction.Horizontal);
            this.pushValidWords(verticalWordRoot, caseCoordinates, Direction.Vertical);
        });
    }

    private getWordAtCoords(caseCoordinates: MatrixCoordinates, direction: Direction): string {
        return this.wordFinderService.getWordAtCoords(this.boardModel.boardMatrix, caseCoordinates, direction).word;
    }

    private pushValidWords(wordRoot: string, caseCoordinates: MatrixCoordinates, direction: Direction) {
        let wordsToVerify: string[] | undefined = this.wordsBotHasLettersFor.get(wordRoot);
        wordsToVerify = wordsToVerify === undefined ? [] : wordsToVerify;

        for (const word of wordsToVerify) {
            const wordMap: WordMap = this.stringToWordMapFromCase(word, caseCoordinates, direction);
            if (this.verifyWordFits(wordMap)) {
                const LETTERS_FOR_BINGO = 7;
                const numberOfLettersRequired = word.length - wordRoot.length;
                const isBingo = numberOfLettersRequired === LETTERS_FOR_BINGO;
                this.orderedPush(wordMap, isBingo);
            }
        }
    }

    private expandWordsBotHasLettersFor(wordRoot: string, consideredWords: string[]) {
        if (this.wordsBotHasLettersFor.has(wordRoot)) return;
        this.wordsBotHasLettersFor.set(wordRoot, []);
        for (const word of consideredWords) {
            const missingLetters: string[] = this.findMissingLettersToFormWord(wordRoot.toLowerCase(), word);
            if (this.verifyBotHasLetters(missingLetters)) {
                this.wordsBotHasLettersFor.get(wordRoot)?.push(word);
            }
        }
    }

    private verifyWordFits(consideredWord: WordMap): boolean {
        //
        return (
            !this.isEmptyWordMap(consideredWord) &&
            this.boardModel.validateBounds(consideredWord) &&
            !this.boardModel.checkIfOverwritesWord(consideredWord) &&
            !this.verifyAlreadyOnBoard(consideredWord) &&
            this.validateAllWordsExist(consideredWord)
        );
    }

    private isEmptyWordMap(wordMap: WordMap) {
        return wordMap.word === '' && wordMap.map.length === 0;
    }
    private orderedPush(wordMapToAdd: WordMap, isBingo: boolean = false) {
        const BINGO_BONUS = 50;
        const newWordsOnBoard: WordMap[] = this.wordFinderService.getNewWords(wordMapToAdd);
        let scoreForWordToAdd = this.scoreCounterService.countAllPoints(newWordsOnBoard);
        if (isBingo) scoreForWordToAdd += BINGO_BONUS; // TODO : not sure isBingo is ever trrue...
        let i = 0;
        while (i < this.generatedWords.length) {
            if (scoreForWordToAdd > this.generatedWords[i].score) {
                const newWordMapScore = { wordMap: wordMapToAdd, score: scoreForWordToAdd };
                this.generatedWords.splice(i, 0, newWordMapScore);

                break;
            }
            i++;
        }
        if (i === this.generatedWords.length) {
            const newWordMapScore = { wordMap: wordMapToAdd, score: scoreForWordToAdd };

            this.generatedWords.push(newWordMapScore);
        }
    }

    private firstWordToWordMap(word: string, direction: Direction): WordMap {
        const MIDDLE_COORD = 7;
        const offSet = Math.floor(Math.random() * (word.length - 1));
        const firstLetterCoordinates =
            direction === Direction.Horizontal
                ? { line: MIDDLE_COORD, column: MIDDLE_COORD - offSet }
                : { line: MIDDLE_COORD - offSet, column: MIDDLE_COORD };
        return this.stringToWordMap(word, firstLetterCoordinates, direction);
    }

    private stringToWordMap(word: string, firstLetterCoord: MatrixCoordinates, direction: Direction) {
        let columnCounter = firstLetterCoord.column;
        let lineCounter = firstLetterCoord.line;
        const characterPosition: CharacterPosition[] = [];
        let enumLetter;
        let weight;
        for (const letter of word) {
            enumLetter = Alphabet[letter.toUpperCase() as keyof typeof Alphabet];
            weight = letterData.letters[enumLetter].weight;
            characterPosition.push({
                letter: { character: letter.toUpperCase(), weight, isBlankLetter: false },
                line: Line[lineCounter],
                column: columnCounter + 1,
            });
            if (direction === Direction.Horizontal) {
                columnCounter++;
            } else {
                lineCounter++;
            }
        }
        const wordMap: WordMap = { word, map: characterPosition };
        if (this.boardModel.validateBounds(wordMap)) {
            return wordMap;
        } else {
            return EMPTY_WORD_MAP;
        }
    }

    private stringToWordMapFromCase(word: string, caseCoord: MatrixCoordinates, direction: Direction): WordMap {
        const letterAtCoord = this.boardModel.boardMatrix[caseCoord.line][caseCoord.column].letter.character.toLocaleLowerCase();
        const rootIndex = word.indexOf(letterAtCoord);

        let columnFirstCoord = caseCoord.column;
        let lineFirstCoord = caseCoord.line;
        if (direction === Direction.Horizontal) {
            columnFirstCoord -= rootIndex;
        } else {
            lineFirstCoord -= rootIndex;
        }
        const firstLetterCoordinates: MatrixCoordinates = { line: lineFirstCoord, column: columnFirstCoord };
        return this.stringToWordMap(word, firstLetterCoordinates, direction);
    }

    private stringToLetterArray(word: string): string[] {
        const letterArray: string[] = [];
        for (const letter of word) {
            letterArray.push(letter);
        }
        return letterArray;
    }

    private findMissingLettersToFormWord(wordRoot: string, desiredWord: string): string[] {
        const missingLetterArray: string[] = [];
        const beginningOfRoot = desiredWord.indexOf(wordRoot);
        const endOfRoot = beginningOfRoot + wordRoot.length;
        const firstLettersLength = beginningOfRoot;
        const lastLettersLength = desiredWord.length - endOfRoot;
        const firstLetters = desiredWord.substring(0, firstLettersLength);
        const lastLetters = desiredWord.substring(endOfRoot, endOfRoot + lastLettersLength);
        const allMissingLetters = firstLetters + lastLetters;
        for (const letter of allMissingLetters) {
            missingLetterArray.push(letter);
        }
        return missingLetterArray;
    }
    private verifyBotHasLetters(letters: string[]): boolean {
        const lettersObjArray: Letter[] = [];
        for (const letter of letters) {
            lettersObjArray.push({
                character: letter.toUpperCase(),
                weight: 0,
                isBlankLetter: false,
            });
        }

        return this.botPlayer.verifyPlayerHasLetters(lettersObjArray);
    }

    private validateKeepLooking(elapsedTime: number): boolean {
        if (this.areConsideredCasesEmpty() || elapsedTime >= TIME_LIMIT_FOR_SEARCHING || this.generatedWords.length >= MAX_WORDS_RETURNED) {
            return false;
        }

        return true;
    }
    private areConsideredCasesEmpty() {
        return this.casesStillConsidered.length <= 0;
    }

    private verifyAlreadyOnBoard(wordMap: WordMap): boolean {
        return this.boardModel.returnNecessaryLetters(wordMap).length <= 0;
    }

    private validateAllWordsExist(consideredWord: WordMap): boolean {
        const newWords = this.wordFinderService.getNewWords(consideredWord);

        for (const word of newWords) {
            if (!this.dictionaryService.validateWordExists(word.word)) return false;
        }
        return true;
    }
}
