/* eslint-disable max-lines -- to test private methods*/
/* eslint-disable @typescript-eslint/no-explicit-any -- to test private methods */
/* eslint-disable dot-notation -- to test private methods*/
// import { TestBed } from '@angular/core/testing';
// import { BotPlayer } from '@app/classes/bot-player/bot-player';
// import { Player } from '@app/classes/player';
// import letterData from '@app/data/letter-data.json';
// import { Bonus } from '@app/enums/bonus';
// import { BotLevel } from '@app/enums/bot-level';
// import { Color } from '@app/enums/color';
// import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
// import { BotWordGeneratorService } from '@app/services/bot-word-generator/bot-word-generator.service';
// import { DictionaryServiceProxy } from '@app/services/dictionary-proxy/dictionary-proxy.service';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-manager-proxy.service';
// import { ScoreCounterServiceProxy } from '@app/services/score-counter-proxy/score-counter-proxy.service';
// import { ServiceLocator } from '@app/services/service-locator';
// import { WordFinderServiceProxy } from '@app/services/word-finder-proxy/word-finder-proxy.service';
// import { Case } from '@app/types/case';
// import { Letter } from '@app/types/letter';
// import { PlayerInfo } from '@app/types/player-info';
// import { PointRange } from '@app/types/point-range';
// import { WordMap } from '@app/types/word-map';
// import { Socket } from 'ngx-socket-io';

// const thisMockLetters = [
//     { character: 'A', weight: 1, amount: 9 },
//     { character: 'B', weight: 3, amount: 2 },
//     { character: 'C', weight: 3, amount: 2 },
//     { character: 'D', weight: 2, amount: 3 },
//     { character: 'E', weight: 1, amount: 15 },
//     { character: 'F', weight: 4, amount: 2 },
//     { character: 'G', weight: 2, amount: 2 },
//     { character: 'H', weight: 4, amount: 2 },
//     { character: 'I', weight: 1, amount: 8 },
//     { character: 'J', weight: 8, amount: 1 },
//     { character: 'K', weight: 10, amount: 1 },
//     { character: 'L', weight: 1, amount: 5 },
//     { character: 'M', weight: 2, amount: 3 },
//     { character: 'N', weight: 1, amount: 6 },
//     { character: 'O', weight: 1, amount: 6 },
//     { character: 'P', weight: 3, amount: 2 },
//     { character: 'Q', weight: 8, amount: 1 },
//     { character: 'R', weight: 1, amount: 6 },
//     { character: 'S', weight: 1, amount: 6 },
//     { character: 'T', weight: 1, amount: 6 },
//     { character: 'U', weight: 1, amount: 6 },
//     { character: 'V', weight: 4, amount: 2 },
//     { character: 'W', weight: 10, amount: 1 },
//     { character: 'X', weight: 10, amount: 1 },
//     { character: 'Y', weight: 10, amount: 1 },
//     { character: 'Z', weight: 10, amount: 1 },
//     { character: '*', weight: 0, amount: 2 },
// ];

// import SpyObj = jasmine.SpyObj;

// describe('BotWordGeneratorService', () => {
//     let service: BotWordGeneratorService;
//     let socketSpy: SpyObj<Socket>;
//     let boardModelSpy: SpyObj<BoardModelServiceProxy>;
//     let dictionarySpy: SpyObj<DictionaryServiceProxy>;
//     let gameSpy: SpyObj<GameManagerServiceProxy>;
//     let scoreCounterSpy: SpyObj<ScoreCounterServiceProxy>;
//     let wordFinderServiceSpy: SpyObj<WordFinderServiceProxy>;

//     const mockPlayerInfo: PlayerInfo = { playerName: 'John', color: Color.Green };
//     const mockPlayer: Player = new Player(mockPlayerInfo, []);
//     const mockBotPlayer: BotPlayer = new BotPlayer(mockPlayerInfo, [], BotLevel.Beginner);

//     let mockMatrix: Case[][];
//     const mockWordOnBoard: Letter[] = [
//         { character: 'E', weight: 1, isBlankLetter: false },
//         { character: 'A', weight: 1, isBlankLetter: false },
//         { character: 'U', weight: 1, isBlankLetter: false },
//     ];
//     const mockPlayerLetters: Letter[] = [
//         { character: 'S', weight: 1, isBlankLetter: false },
//         { character: 'S', weight: 1, isBlankLetter: false },
//         { character: 'S', weight: 1, isBlankLetter: false },
//         { character: 'T', weight: 1, isBlankLetter: false },
//         { character: 'T', weight: 1, isBlankLetter: false },
//         { character: 'T', weight: 1, isBlankLetter: false },
//         { character: 'M', weight: 1, isBlankLetter: false },
//     ];

//     const mockPlayerLettersForNotPlaying: Letter[] = [
//         { character: 'Z', weight: 10, isBlankLetter: false },
//         { character: 'X', weight: 10, isBlankLetter: false },
//         { character: 'T', weight: 1, isBlankLetter: false },
//         { character: 'K', weight: 10, isBlankLetter: false },
//         { character: 'Q', weight: 8, isBlankLetter: false },
//         { character: 'B', weight: 3, isBlankLetter: false },
//         { character: 'J', weight: 8, isBlankLetter: false },
//     ];

//     const pointRangeMock: PointRange = { lowerBound: 2, upperBound: 7 };
//     const consideredWordMap: WordMap = {
//         word: 'se',
//         map: [
//             { letter: { character: 'S', weight: 1, isBlankLetter: false }, line: 'B', column: 3 },
//             { letter: { character: 'E', weight: 1, isBlankLetter: false }, line: 'C', column: 3 },
//         ],
//     };

//     beforeEach(() => {
//         socketSpy = jasmine.createSpyObj('Socket', ['emit']);
//         boardModelSpy = jasmine.createSpyObj('BoardModelServiceProxy', [
//             'isBoardEmpty',
//             'checkIfOverwritesWord',
//             'returnNecessaryLetters',
//             'validateBounds',
//         ]);
//         dictionarySpy = jasmine.createSpyObj('DictionaryServiceProxy', [
//             'findAllWordsContaining',
//             'findAllWordsContainingAsAFirstLetter',
//             'validateWordExists',
//         ]);
//         gameSpy = jasmine.createSpyObj('GameManagerServiceProxy', ['validatePlayerHasLetters'], { botPlayer: mockBotPlayer, botIndex: 1 });
//         wordFinderServiceSpy = jasmine.createSpyObj('WordFinderServiceProxy', ['getNewWords', 'getWordAtCoords']);
//         scoreCounterSpy = jasmine.createSpyObj('ScoreCounterServiceProxy', ['countAllPoints']);

//         mockMatrix = [
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//             [
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.Blank },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.LetterX3 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX2 },
//                 { letter: { character: '?', weight: 0, isBlankLetter: false }, bonus: Bonus.WordX3 },
//             ],
//         ];
//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: Socket, useValue: socketSpy },
//                 { provide: BoardModelServiceProxy, useValue: boardModelSpy },
//                 { provide: DictionaryServiceProxy, useValue: dictionarySpy },
//                 { provide: GameManagerServiceProxy, useValue: gameSpy },
//                 { provide: ScoreCounterServiceProxy, useValue: {} },
//                 { provide: WordFinderServiceProxy, useValue: wordFinderServiceSpy },
//                 { provide: ScoreCounterServiceProxy, useValue: scoreCounterSpy },
//             ],
//         });
//         ServiceLocator.injector = { get: TestBed.inject };
//         service = TestBed.inject(BotWordGeneratorService);

//         (letterData as any).letters = thisMockLetters;
//         service['gameService'].currentlyPlayingIndex = 1;
//         wordFinderServiceSpy.getWordAtCoords.and.resolveTo(consideredWordMap);
//     });

//     it('should be created', () => {
//         expect(service).toBeTruthy();
//     });

//     describe('Matrix has one vertical word', () => {
//         beforeEach(() => {
//             mockMatrix[2][2].letter = mockWordOnBoard[0];
//             mockMatrix[3][2].letter = mockWordOnBoard[1];
//             mockMatrix[4][2].letter = mockWordOnBoard[2];
//             service['boardModel'].boardMatrix = mockMatrix;
//             service['gameService'].players = [];
//             service['gameService'].players[0] = mockPlayer;
//             service['gameService'].players[1] = mockBotPlayer;
//             service['gameService'].players[1].currentLetters = mockPlayerLetters;
//         });

//         it('should return three words', async () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([
//                 ['se', 'ma', 'sa', 'su', 'te', 'ta', 'tu', 'me', 'mu'],
//                 ['se', 'ma', 'sa', 'su', 'te', 'ta', 'tu', 'me', 'mu'],
//             ]);

//             boardModelSpy.validateBounds.and.returnValue(true);
//             spyOn<any>(service, 'expandWordsBotHasLettersFor').and.callFake(() => {
//                 return;
//             });
//             spyOn<any>(service, 'verifyWordFits').and.resolveTo(true);
//             spyOn<any>(service, 'verifyAlreadyOnBoard').and.resolveTo(false);
//             spyOn<any>(service, 'verifyScoreInRange').and.returnValue(true);

//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(0);
//         });
//     });

//     describe('Matrix has one horizontal word', () => {
//         beforeEach(() => {
//             mockMatrix[2][2].letter = mockWordOnBoard[0];
//             mockMatrix[2][3].letter = mockWordOnBoard[1];
//             mockMatrix[2][4].letter = mockWordOnBoard[2];
//             service['boardModel'].boardMatrix = mockMatrix;
//             service['gameService'].players = [];
//             service['gameService'].players[0] = mockPlayer;
//             service['gameService'].players[1] = mockBotPlayer;
//             service['gameService'].players[1].currentLetters = mockPlayerLetters;
//         });

//         it('should return three words', async () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['se', 'ma', 'sa', 'su', 'te', 'ta', 'tu', 'me', 'mu'], []]);
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);

//             spyOn<any>(service, 'verifyWordFits').and.resolveTo(true);
//             spyOn<any>(service, 'verifyAlreadyOnBoard').and.resolveTo(false);
//             spyOn<any>(service, 'verifyScoreInRange').and.returnValue(true);

//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(3);
//         });

//         it('should return zero words', async () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([[], []]);
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);

//             spyOn<any>(service, 'verifyWordFits').and.resolveTo(true);

//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(0);
//         });

//         it('should return 0 words, because out of bounds', async () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['se', 'sa', 'su', 'te', 'ta', 'tu', 'ma', 'me', 'mu'], []]);
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             boardModelSpy.validateBounds.and.returnValue(false);
//             spyOn<any>(service, 'verifyAlreadyOnBoard').and.resolveTo(false);

//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(0);
//         });

//         it('should return 0 words, because no new words', async () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['se', 'sa', 'su', 'te', 'ta', 'tu', 'ma', 'me', 'mu'], []]);

//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             boardModelSpy.checkIfOverwritesWord.and.resolveTo(false);
//             boardModelSpy.validateBounds.and.returnValue(true);
//             spyOn<any>(service, 'verifyAlreadyOnBoard').and.resolveTo(false);
//             wordFinderServiceSpy.getNewWords.and.resolveTo([]);
//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(0);
//         });

//         it('should return no word because word is already on board', async () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['se', 'sa', 'su', 'te', 'ta', 'tu', 'ma', 'me', 'mu'], []]);
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);

//             spyOn<any>(service, 'verifyWordFits').and.resolveTo(true);
//             spyOn<any>(service, 'verifyAlreadyOnBoard').and.resolveTo(true);
//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(0);
//         });

//         it('should not find enoughValidWords because bot lacks letters', () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['sa', 'se'], []]);
//             spyOn<any>(service, 'verifyBotHasLetters').and.returnValue(false);
//             service['generatePossibleWordsForCase']({ line: 0, column: 0 });
//             expect(service.generatedWords.length).toEqual(0);
//         });

//         it('should not find enoughValidWords because already on board', () => {
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['sa', 'se'], []]);
//             spyOn<any>(service, 'verifyBotHasLetters').and.returnValue(true);
//             spyOn<any>(service, 'verifyAlreadyOnBoard').and.returnValue(true);
//             spyOn<any>(service, 'verifyWordFits').and.returnValue(false);
//             service['generatePossibleWordsForCase']({ line: 0, column: 0 });
//             expect(service.generatedWords.length).toEqual(0);
//         });
//         it('should verify that word is not already on board', async () => {
//             const charPosition = { letter: { character: 'S', weight: 1, isBlankLetter: false }, line: 'B', column: 3 };
//             boardModelSpy.returnNecessaryLetters.and.resolveTo([charPosition]);
//             const isWordAlreadyOnBoard = await service['verifyAlreadyOnBoard'](consideredWordMap);
//             expect(isWordAlreadyOnBoard).toEqual(false);
//         });
//     });

//     describe('Matrix is empty', () => {
//         beforeEach(() => {
//             service['boardModel'].boardMatrix = mockMatrix;
//             service['gameService'].players = [];
//             service['gameService'].players[0] = mockPlayer;
//             service['gameService'].players[1] = mockBotPlayer;
//             service['gameService'].players[1].currentLetters = mockPlayerLettersForNotPlaying;
//         });

//         it('should return three words', async () => {
//             boardModelSpy.isBoardEmpty.and.resolveTo(true);
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['se', 'te', 'ma'], []]);
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             scoreCounterSpy.countAllPoints.and.resolveTo(2);
//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(3);
//         });

//         it('should return no word because bot dosent have letters', async () => {
//             spyOn<any>(service, 'verifyBotHasLetters').and.returnValue(false);
//             boardModelSpy.isBoardEmpty.and.resolveTo(true);
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['se', 'te', 'ma'], []]);
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             scoreCounterSpy.countAllPoints.and.resolveTo(2);
//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(0);
//         });

//         it('should return no word because out of range', async () => {
//             spyOn<any>(service, 'verifyScoreInRange').and.returnValue(false);
//             boardModelSpy.isBoardEmpty.and.resolveTo(true);
//             dictionarySpy.findAllWordsContaining.and.resolveTo([['se', 'te', 'ma'], []]);
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             scoreCounterSpy.countAllPoints.and.resolveTo(2);
//             const possibleWords: WordMap[] = await service.generateWordsInPointRange(pointRangeMock);
//             expect(possibleWords.length).toEqual(0);
//         });
//     });

//     it('should not expand wordsBotHasLettersFor, because key already there', () => {
//         service.generatedWords = [{ wordMap: consideredWordMap, score: 2 }];
//         service.wordsBotHasLettersFor.set('allo', []);
//         service['expandWordsBotHasLettersFor']('allo', []);
//         expect(service.generatedWords.length).toEqual(1);
//     });

//     it('should not expand wordsBotHasLettersFor, because bot dosent have letters', () => {
//         service.generatedWords = [{ wordMap: consideredWordMap, score: 2 }];
//         spyOn<any>(service, 'verifyBotHasLetters').and.returnValue(false);
//         service['expandWordsBotHasLettersFor']('aimer', ['aimera']);
//         expect(service.generatedWords.length).toEqual(1);
//     });

//     it('should ordred push', async () => {
//         const FORCED_SCORE = 52;
//         service.generatedWords = [{ wordMap: consideredWordMap, score: 2 }];
//         spyOn<any>(service, 'verifyBotHasLetters').and.returnValue(false);

//         const other: WordMap = {
//             word: 'allo',
//             map: [
//                 { letter: { character: 'A', weight: 1, isBlankLetter: false }, line: 'B', column: 4 },
//                 { letter: { character: 'L', weight: 1, isBlankLetter: false }, line: 'C', column: 4 },
//                 { letter: { character: 'L', weight: 1, isBlankLetter: false }, line: 'D', column: 4 },
//                 { letter: { character: 'O', weight: 1, isBlankLetter: false }, line: 'E', column: 4 },
//             ],
//         };
//         scoreCounterSpy.countAllPoints.and.resolveTo(FORCED_SCORE);
//         await service['orderedPush'](other, true);
//         expect(service.generatedWords[0].wordMap.word).toEqual('allo');
//     });

//     describe('validateAllWordsExist', () => {
//         it('should return true on valid entry', async () => {
//             dictionarySpy.validateWordExists.and.resolveTo(true);
//             wordFinderServiceSpy.getNewWords.and.resolveTo([
//                 { word: 'test', map: [{ letter: { character: 'A', weight: 0, isBlankLetter: false }, line: 'A', column: 0 }] },
//             ]);
//             expect(await service['validateAllWordsExist'](consideredWordMap)).toEqual(true);
//         });
//         it('should not validate non-existing word', async () => {
//             dictionarySpy.validateWordExists.and.resolveTo(false);
//             wordFinderServiceSpy.getNewWords.and.resolveTo([
//                 { word: 'test', map: [{ letter: { character: 'A', weight: 0, isBlankLetter: false }, line: 'A', column: 0 }] },
//             ]);
//             expect(await service['validateAllWordsExist'](consideredWordMap)).toEqual(false);
//         });
//     });
// });
