import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { DictionaryName } from '@app/types/dictionary';
import * as fs from 'fs';
import { Service } from 'typedi';

@Service()
export class GlobalDictionaryService extends RoomService {
    @MirrorProp() dictionaryList: DictionaryName[] = [];
    path: string = './app/data/dictionaries/';

    constructor() {
        super();
        this.createDictionaryList();
    }

    @ProxyCall()
    getDictionaryList() {
        this.emitPropUpdate('dictionaryList');
    }

    private createDictionaryList(): void {
        this.dictionaryList = [];
        const directory = fs.readdirSync(this.path);
        for (const file of directory) {
            const data = fs.readFileSync(this.path + file, 'utf8');
            if (JSON.parse(data).title === 'Français')
                this.dictionaryList.unshift({ title: JSON.parse(data).title, description: JSON.parse(data).description });
            else this.dictionaryList.push({ title: JSON.parse(data).title, description: JSON.parse(data).description });
        }
    }
}
