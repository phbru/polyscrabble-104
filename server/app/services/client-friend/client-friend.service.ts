import { User } from '@app/classes/user';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { RoomService } from '@app/services/room-service/room-service';

import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { Service } from 'typedi';

@Service()
export class ClientFriendService extends RoomService {
    @MirrorProp() friends: User[] = [];
    @MirrorProp() friendsRequested: User[] = [];
    @MirrorProp() receivedInvites: User[] = [];
    @MirrorProp() notificationOn: boolean = false;

    @ProxyCall() removeNotification() {
        this.notificationOn = false;
    }

    isUserInList(userList: User[], userToMatch: User) {
        for (const user of userList) {
            if (userToMatch.id === user.id) return true;
        }
        return false;
    }

    addToFriendsRequested(requestedUser: User) {
        if (this.isUserInList(this.friendsRequested, requestedUser)) return;
        this.friendsRequested.push(requestedUser);
        this.emitPropUpdate('friendsRequested');
    }

    // Should not be called directly. Use GlobalInteractionService instead
    addToReceivedInvites(requestSubmitter: User) {
        if (this.isUserInList(this.receivedInvites, requestSubmitter)) return;
        this.receivedInvites.push(requestSubmitter);
        this.emitPropUpdate('receivedInvites');
    }

    removeFriend(userId: string) {
        for (let i = 0; i < this.friends.length; i++) {
            if (userId === this.friends[i].id) {
                this.friends.splice(i, 1);
            }
        }
        this.emitPropUpdate('friends');
    }

    // Should not be called directly. Use GlobalInteractionService instead
    addToFriends(newFriend: User) {
        if (this.isUserInList(this.friends, newFriend)) return;
        this.friends.push(newFriend);
        this.emitPropUpdate('friends');
    }

    // Should not be called directly. Use GlobalInteractionService instead
    removeFromFriendsRequested(requestedToRemove: User) {
        this.friendsRequested = this.friendsRequested.filter((user) => {
            return user !== requestedToRemove;
        });
    }

    // Should not be called directly. Use GlobalInteractionService instead
    removeFromReceivedInvites(inviteToRemove: User) {
        this.receivedInvites = this.receivedInvites.filter((user) => {
            return user !== inviteToRemove;
        });
    }

    notifyOfInvite() {
        this.notificationOn = true;
    }
}
