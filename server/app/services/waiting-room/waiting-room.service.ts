import { User } from '@app/classes/user';
import { GameService } from '@app/services/game/game.service';
import { GlobalGameListService } from '@app/services/global-game-list/global-game-list.service';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { GameInfo } from '@app/types/game-info';
import { Service } from 'typedi';

@Service()
export class WaitingRoomService extends RoomService {
    @MirrorProp() gameInfo: GameInfo = {
        id: '',
        creatorName: '',
        time: 0,
        dictionary: '',
        openSpots: 0,
        password: '',
        players: [],
        observers: [],
        virtualPlayers: [],
        invitedUsers: [],
        isGameStarted: false,
        isPrivate: false,
    };
    constructor(private gameService: GameService, private gameListService: GlobalGameListService, private globalUserService: GlobalUserService) {
        super();
    }

    @ProxyCall()
    launchGame() {
        if (this.gameInfo.isGameStarted) return;
        this.gameInfo.invitedUsers = [];
        this.gameService.startGame(this.gameInfo);
        this.gameInfo.isGameStarted = true;
        this.gameListService.updateGame(this.gameInfo);
        this.emitPropUpdate('gameInfo');
    }

    @ProxyCall()
    initiateWaitingRoom(gameInfo: GameInfo, creatorId: string) {
        const creator = this.globalUserService.getUser(creatorId);
        if (creator === undefined) {
            // eslint-disable-next-line no-console
            console.log(`Error: cannot initiate waiting room; creator '${creatorId}' does not exist!`);
            return;
        }
        this.gameInfo = gameInfo;
        this.gameListService.addGame(gameInfo);
        this.spoofCaller(creator).joinAsPlayer();
    }

    @ProxyCall()
    joinAsPlayer() {
        for (const player of this.gameInfo.players) {
            if (player.id === this.caller.id) return;
        }
        this.gameInfo.observers.forEach((value, index) => {
            if (value.id === this.caller.id) {
                this.gameInfo.observers.splice(index, 1);
            }
        });
        this.gameInfo.invitedUsers.forEach((value, index) => {
            if (value.id === this.caller.id) {
                this.gameInfo.invitedUsers.splice(index, 1);
            }
        });
        this.removeSpot();
        this.gameInfo.players.push(this.caller);
        this.gameListService.updateGame(this.gameInfo);
        this.emitPropUpdate('gameInfo');
    }

    @ProxyCall()
    isGameFull(): boolean {
        return this.gameInfo.openSpots <= 0;
    }

    @ProxyCall()
    joinAsObserver(): void {
        for (const observer of this.gameInfo.observers) {
            if (observer.id === this.caller.id) return;
        }

        this.gameInfo.players.forEach((value, index) => {
            if (value.id === this.caller.id) {
                this.addSpot();
                this.gameInfo.players.splice(index, 1);
            }
        });
        this.gameInfo.observers.push(this.caller);
        this.gameListService.updateGame(this.gameInfo);
        this.emitPropUpdate('gameInfo');
    }
    @ProxyCall()
    addVirtualPlayer(): void {
        const botUser = User.makeBot('Joueur virtuel ' + (this.gameInfo.virtualPlayers.length + 1));
        this.gameInfo.virtualPlayers.push(botUser);
        this.gameListService.updateGame(this.gameInfo);
        this.removeSpot();
        this.emitPropUpdate('gameInfo');
    }

    @ProxyCall()
    removeVirtualPlayer(botName: string): void {
        this.gameInfo.virtualPlayers.forEach((value, index) => {
            if (value.username === botName) {
                this.gameInfo.virtualPlayers.splice(index, 1);
                this.addSpot();
            }
        });
        this.gameListService.updateGame(this.gameInfo);
        this.emitPropUpdate('gameInfo');
    }

    @ProxyCall()
    addSpot(): void {
        this.incrementOpenSpots();
        this.gameListService.updateGame(this.gameInfo);
        this.emitPropUpdate('gameInfo');
    }

    @ProxyCall()
    removeSpot(): void {
        this.decrementOpenSpots();
        this.gameListService.updateGame(this.gameInfo);
        this.emitPropUpdate('gameInfo');
    }

    @ProxyCall()
    removeUser(userIdToRemove: string): void {
        if (this.gameInfo.players.length > 0) {
            this.gameInfo.players.forEach((element, index) => {
                if (element.id === userIdToRemove) {
                    this.gameInfo.players.splice(index, 1);
                    this.addSpot();
                }
            });
        }
        if (this.gameInfo.observers.length > 0) {
            this.gameInfo.observers.forEach((element, index) => {
                if (element.id === userIdToRemove) this.gameInfo.observers.splice(index, 1);
            });
        }
        this.emitPropUpdate('gameInfo');
    }

    @ProxyCall()
    addInvitedUser(invitedUserId: string): void {
        const invitedUser: User | undefined = this.globalUserService.getUser(invitedUserId);
        if (invitedUser !== undefined) {
            this.gameInfo.invitedUsers.push(invitedUser);
            this.removeSpot();
            this.gameListService.updateGame(this.gameInfo);
            this.emitPropUpdate('gameInfo');
        }
    }

    @ProxyCall()
    removeInvitedUser(userId: string): void {
        if (this.gameInfo.invitedUsers.length > 0) {
            this.gameInfo.invitedUsers.forEach((element, index) => {
                if (element.id === userId) {
                    this.gameInfo.invitedUsers.splice(index, 1);
                    this.addSpot();
                }
            });
        }
        this.gameListService.updateGame(this.gameInfo);
        this.emitPropUpdate('gameInfo');
    }

    private decrementOpenSpots() {
        if (this.gameInfo.openSpots > 0) {
            this.gameInfo.openSpots--;
        }
    }

    private incrementOpenSpots() {
        const MAX_PLAYERS = 4;
        if (this.gameInfo.openSpots < MAX_PLAYERS) {
            this.gameInfo.openSpots++;
        }
    }
}
