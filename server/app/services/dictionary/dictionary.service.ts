import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import * as fs from 'fs';
import { Service } from 'typedi';

@Service()
export class DictionaryService extends RoomService {
    dictionary: string[] = [];
    path: string = './app/data/dictionaries/';

    @ProxyCall()
    validateWordExists(word: string) {
        return this.dictionary.includes(word.toLowerCase()) || this.dictionary.includes(word.toUpperCase());
    }
    @ProxyCall()
    findAllWordsContainingAsAFirstLetter(firstLetter: string): string[] {
        const wordsWithFirstLetter: string[] = [];
        for (const word of this.dictionary) {
            if (word[0] === firstLetter) {
                wordsWithFirstLetter.push(word);
            }
        }
        return wordsWithFirstLetter;
    }

    findAllWordsContaining(roots: string[]): string[][] {
        const wordsContainingRoot: Map<string, string[]> = new Map();
        const rootToLookFor: string[] = [];
        const counterByRoot: number[] = [];

        roots.forEach((root) => {
            wordsContainingRoot.set(root, []);
            rootToLookFor.push(root);
            counterByRoot.push(0);
        });
        const MAX_WORDS_PER_ROOT = 10000;
        const MAX_TOTAL_WORDS = 100000;
        const MAX_RANDOM_START_INDEX = 1000;
        const randomStartIndex = Math.floor(Math.random() * MAX_RANDOM_START_INDEX);
        let amountOfWordFound = 0;
        for (let i = randomStartIndex; i < this.dictionary.length; i += 2) {
            const word = this.dictionary[i];
            // Passes one time through the dictionary -> for each word checks if part of a certain given root
            for (let j = 0; j < rootToLookFor.length; j += 1) {
                if (word.includes(rootToLookFor[j])) {
                    wordsContainingRoot.get(rootToLookFor[j])?.push(word);
                    counterByRoot[j]++;
                    amountOfWordFound++;
                    if (counterByRoot[j] >= MAX_WORDS_PER_ROOT) {
                        rootToLookFor.splice(j, 1);
                        counterByRoot.splice(j, 1);
                    }
                }
            }
            if (rootToLookFor.length <= 0 || amountOfWordFound >= MAX_TOTAL_WORDS) break; // TODO : Peut-être ajuster le moment où on break.
        }
        const values: string[][] = Array.from(wordsContainingRoot.values());
        return values;
    }

    setupDictionary(dictionaryName: string) {
        const tempDictionaryFile = JSON.parse(fs.readFileSync(this.path + dictionaryName.toLowerCase() + '.json', 'utf8'));
        this.dictionary = [];

        for (const wordEntry of tempDictionaryFile.words) {
            this.dictionary.push(wordEntry);
        }
    }
}
