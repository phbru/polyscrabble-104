// /* eslint-disable no-unused-expressions -- to deal with chai syntax*/
// /* eslint-disable @typescript-eslint/no-unused-expressions -- to deal with chai syntax*/

// import { DictionaryService } from '@app/services/dictionary/dictionary.service';
// import { expect } from 'chai';

// describe('DictionaryService', () => {
//     let service: DictionaryService;

//     beforeEach(() => {
//         service = new DictionaryService();
//     });

//     it('should be created', () => {
//         expect(service).to.exist;
//     });

//     it('should test validateWordExists() returns true', () => {
//         const dict: string[] = ['manger', 'servir', 'ecouter', 'definir'];
//         const word = 'manger';
//         service.dictionary = dict;
//         const output = service.validateWordExists(word);
//         expect(output).to.be.true;
//     });

//     it('should test validateWordExists() returns false', () => {
//         const dict: string[] = ['manger', 'servir', 'ecouter', 'definir'];
//         const word = 'ecole';
//         service.dictionary = dict;
//         const output = service.validateWordExists(word);
//         expect(output).not.to.be.true;
//     });

//     it('should find all words containing string', () => {
//         const dict: string[] = ['test', 'testing', 'testings', 'allo'];
//         const word = 'test';
//         service.dictionary = dict;

//         expect(service.findAllWordsContaining([word])).to.eql([['test', 'testing', 'testings']]);
//     });

//     it('should find all words starting with string', () => {
//         const dict: string[] = ['atest', 'testing', 'testings', 'allo'];
//         service.dictionary = dict;

//         expect(service.findAllWordsContainingAsAFirstLetter('t')).to.eql(['testing', 'testings']);
//     });
// });
