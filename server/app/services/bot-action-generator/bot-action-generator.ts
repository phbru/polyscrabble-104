import { BotPlayer } from '@app/classes/bot-player/bot-player';
import { ActionCode } from '@app/enums/action-code';
import { BoardModelService } from '@app/services/board-model/board-model.service';
import { BotWordGeneratorService } from '@app/services/bot-word-generator/bot-word-generator.service';
import { LetterBankService } from '@app/services/letter-bank/letter-bank.service';
import { RoomService } from '@app/services/room-service/room-service';
import { Action } from '@app/types/action';
import { Letter } from '@app/types/letter';
import { WordMap } from '@app/types/word-map';
import { Service } from 'typedi';

@Service()
export class BotActionGenerator extends RoomService {
    constructor(
        private letterBankService: LetterBankService, // TODO : might be  able to remove
        private botWordGenerator: BotWordGeneratorService,
        private boardModel: BoardModelService,
    ) {
        super();
    }
    async generateRandomAction(botPlayer: BotPlayer): Promise<Action> {
        const PASS_PROBA = 0.1;
        const EXCHANGE_PROBA = 0.1;
        const randomValue = Math.random();

        if (randomValue <= PASS_PROBA) {
            return { actionType: ActionCode.Pass, parameter: undefined };
        } else if (randomValue > PASS_PROBA && randomValue <= PASS_PROBA + EXCHANGE_PROBA) {
            return this.generateExchangeAction(botPlayer.currentLetters);
        } else {
            return this.generatePlaceAction(botPlayer);
        }
    }

    private generateExchangeAction(botLetters: Letter[], shouldExchangeAll: boolean = false): Action {
        if (this.letterBankService.letterBank.length === 0) {
            // TODO : add a verification for max amount of letters to exchange for endgame. (this way it won,t try to exchange if it knows it can't)
            return { actionType: ActionCode.Pass, parameter: undefined };
        }
        const choseLettersToExchange: string[] = this.choseLettersToExchange(botLetters, shouldExchangeAll);
        return { actionType: ActionCode.Exchange, parameter: choseLettersToExchange };
        // setTimeout(() => {

        //     } else {
        //         let output: string;
        //         if (choseLettersToExchange.length === 1) output = `${choseLettersToExchange.length} lettre`;
        //         else output = `${choseLettersToExchange.length} lettres`;
        //         this.chatHandler.printMessage(`!échanger ${output}`, Emitter.Bot);
        //         this.game.resetTurnsPassed();
        //     }
        // }, remainingDelay);
        // TODO : chat message
    }

    private choseLettersToExchange(botLetters: Letter[], shouldExchangeAll: boolean): string[] {
        const MAX_EXCHANGE_LETTERS = 7;
        const MIN_EXCHANGE_LETTERS = 1;

        let nbrOfLettersToExchange;
        if (shouldExchangeAll) {
            nbrOfLettersToExchange = MIN_EXCHANGE_LETTERS;
        }

        if (this.isLessThanMaxLettersInBank(MAX_EXCHANGE_LETTERS)) {
            nbrOfLettersToExchange = this.letterBankService.letterBank.length;
        } else if (shouldExchangeAll) {
            nbrOfLettersToExchange = MAX_EXCHANGE_LETTERS;
        } else {
            nbrOfLettersToExchange = Math.floor(Math.random() * (MAX_EXCHANGE_LETTERS - MIN_EXCHANGE_LETTERS + 1)) + MIN_EXCHANGE_LETTERS;
        }

        const lettersToExchange: string[] = [];
        const indexes: number[] = [];

        for (let i = 0; i < MAX_EXCHANGE_LETTERS; i++) indexes.push(i); // TODO : kessé ca ...

        for (let i = 0; i < nbrOfLettersToExchange; i++) {
            const index = Math.floor(Math.random() * indexes.length);
            const chosenIndex = indexes[index];
            indexes.splice(index, 1);
            lettersToExchange.push(botLetters[chosenIndex].character);
        }
        return lettersToExchange;
    }

    private isLessThanMaxLettersInBank(maxExchangeLetters: number) {
        return this.letterBankService.letterBank.length < maxExchangeLetters;
    }

    private async generatePlaceAction(botPlayer: BotPlayer): Promise<Action> {
        const chosenWord: WordMap | undefined = await this.botWordGenerator.generateWordMapToPlace(botPlayer);

        if (chosenWord !== undefined) {
            const necessaryLetters = this.boardModel.returnNecessaryLetters(chosenWord);

            let word = '';
            for (const charPos of necessaryLetters) {
                word += charPos.letter.character;
            }

            const necessaryWordMap: WordMap = { word, map: necessaryLetters };

            return { actionType: ActionCode.Place, parameter: necessaryWordMap };
        }
        return this.generateExchangeAction(botPlayer.currentLetters);
    }
}
