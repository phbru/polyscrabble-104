// /* eslint-disable dot-notation -- disabled eslint to test the private methods */
// import { TestBed } from '@angular/core/testing';
// import { Player } from '@app/classes/player';
// import { Color } from '@app/enums/color';
// import { ChatHandlerService } from '@app/services/chat-handler/chat-handler.service';
// import { GameManagerServiceProxy } from '@app/services/game-manager-proxy/game-manager-proxy.service';
// import { LetterBankServiceProxy } from '@app/services/letter-bank-proxy/letter-bank-proxy.service';
// import { Letter } from '@app/types/letter';
// import { ExchangeCommandService } from './exchange-action';
// import SpyObj = jasmine.SpyObj;

// describe('ExchangeCommandService', () => {
//     let service: ExchangeCommandService;
//     let gameSpy: SpyObj<GameManagerServiceProxy>;
//     let chatHandlerSpy: SpyObj<ChatHandlerService>;
//     let letterBankSpy: SpyObj<LetterBankServiceProxy>;
//     let player: Player;

//     beforeEach(() => {
//         player = new Player({ playerName: 'John', color: Color.Green }, []);
//         const playerLetters = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
//         for (const letter of playerLetters) player.currentLetters.push({ character: letter, weight: 0, isBlankLetter: false });

//         gameSpy = jasmine.createSpyObj(
//             'GameManagerServiceProxy',
//             ['giveLettersToPlayer', 'removeLettersFromPlayer', 'changeTurn', 'verifyBotTurn'],
//             {
//                 currentlyPlayingIndex: 0,
//                 currentPlayer: player,
//             },
//         );
//         gameSpy.currentlyPlayingIndex = 0;
//         gameSpy.players = [player];

//         chatHandlerSpy = jasmine.createSpyObj('ChatHandlerServiceProxy', ['printMessage']);
//         letterBankSpy = jasmine.createSpyObj('LetterBankServiceProxy', ['reinitializeLetterBank', 'giveBackLetterToBank']);
//         letterBankSpy.letterBank = [
//             { character: 'A', weight: 0, isBlankLetter: false },
//             { character: 'B', weight: 0, isBlankLetter: false },
//             { character: 'C', weight: 0, isBlankLetter: false },
//             { character: 'D', weight: 0, isBlankLetter: false },
//             { character: 'E', weight: 0, isBlankLetter: false },
//             { character: 'F', weight: 0, isBlankLetter: false },
//             { character: 'G', weight: 0, isBlankLetter: false },
//             { character: 'H', weight: 0, isBlankLetter: false },
//             { character: 'I', weight: 0, isBlankLetter: false },
//         ];
//     });

//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: GameManagerServiceProxy, useValue: gameSpy },
//                 { provide: LetterBankServiceProxy, useValue: letterBankSpy },
//                 { provide: ChatHandlerService, useValue: chatHandlerSpy },
//             ],
//         });
//         service = TestBed.inject(ExchangeCommandService);
//     });

//     it('should create', () => {
//         expect(service).toBeTruthy();
//     });

//     it('should validate player has max letters', () => {
//         expect(service['validatePlayerHasSevenLetters']()).toBeTrue();

//         player.currentLetters.pop();

//         expect(service['validatePlayerHasSevenLetters']()).toBeFalse();

//         player.currentLetters = [];
//         expect(service['validatePlayerHasSevenLetters']()).toBeFalse();
//     });

//     it('should get letters to remove', () => {
//         const lettersToExchange: string[] = ['A', 'B'];
//         const lettersToRemove: Letter[] = [];
//         for (const letter of lettersToExchange) lettersToRemove.push({ character: letter, weight: 0, isBlankLetter: false });

//         expect(service.getLettersToRemove(lettersToExchange)).toEqual(lettersToRemove);
//     });

//     it('should deposit letters to bank', () => {
//         const letters: Letter[] = [];
//         for (const letter of ['a', 'b', 'c']) letters.push({ character: letter, weight: 0, isBlankLetter: false });

//         letterBankSpy.letterBank = [];
//         service['depositLettersToBank'](letters);
//         expect(letterBankSpy.giveBackLetterToBank).toHaveBeenCalledTimes(3);
//     });

//     describe('execute', () => {
//         it('should execute with valid letters', () => {
//             const lettersExchangeString = ['a', 'b', 'c'];
//             const mockLetters = [
//                 { character: 'a', weight: 0, isBlankLetter: false },
//                 { character: 'b', weight: 0, isBlankLetter: false },
//                 { character: 'c', weight: 0, isBlankLetter: false },
//             ];
//             spyOn(service, 'getLettersToRemove').and.returnValue(mockLetters);
//             service.execute(lettersExchangeString);
//             expect(gameSpy.removeLettersFromPlayer).toHaveBeenCalled();
//             expect(gameSpy.giveLettersToPlayer).toHaveBeenCalled();
//         });

//         it("should not execute with letters we don't have", () => {
//             const lettersExchangeString = ['a', 'b', 'x'];

//             service.execute(lettersExchangeString);
//             expect(gameSpy.removeLettersFromPlayer).not.toHaveBeenCalled();
//             expect(gameSpy.giveLettersToPlayer).not.toHaveBeenCalled();
//         });

//         it("should not execute if we don't have 7 letters", () => {
//             const lettersExchangeString = ['a', 'b', 'c'];
//             player.currentLetters.pop();

//             service.execute(lettersExchangeString);
//             expect(gameSpy.removeLettersFromPlayer).not.toHaveBeenCalled();
//             expect(gameSpy.giveLettersToPlayer).not.toHaveBeenCalled();
//         });

//         it('should not execute letter bank has less than 7 letters', () => {
//             gameSpy.currentlyPlayingIndex = 0;
//             letterBankSpy.letterBank = [
//                 { character: 'A', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//             ];

//             service.execute(['a', 'b', 'c']);
//             expect(gameSpy.removeLettersFromPlayer).not.toHaveBeenCalled();
//             expect(gameSpy.giveLettersToPlayer).not.toHaveBeenCalled();
//         });

//         it('should not execute if exchange more letters than letter bank ', () => {
//             const lettersExchangeString = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
//             gameSpy.currentlyPlayingIndex = 0;

//             service.execute(lettersExchangeString);
//             expect(gameSpy.removeLettersFromPlayer).not.toHaveBeenCalled();
//             expect(gameSpy.giveLettersToPlayer).not.toHaveBeenCalled();
//         });
//     });
// });
