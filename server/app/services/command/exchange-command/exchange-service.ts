import { Player } from '@app/classes/player/player';
import * as constant from '@app/constants/command';
import { LetterBankService } from '@app/services/letter-bank/letter-bank.service';
import { RoomService } from '@app/services/room-service/room-service';
import { Letter } from '@app/types/letter';
import { Service } from 'typedi';

@Service()
export class ExchangeService extends RoomService {
    constructor(private letterBankService: LetterBankService) {
        super();
    }

    execute(player: Player, lettersExchangeString: string[]): boolean {
        if (!this.isExchangeValid(player, lettersExchangeString)) return false;
        const lettersToRemoveFromPlayer = this.getLettersToRemove(player, lettersExchangeString);
        player.removeLettersFromPlayer(lettersToRemoveFromPlayer);
        this.giveLettersToPlayer(player, lettersExchangeString.length);
        this.depositLettersToBank(lettersToRemoveFromPlayer);
        return true;
    }

    giveLettersToPlayer(player: Player, nbrLetters: number) {
        const letters: Letter[] = this.letterBankService.retrieveRandomLetters(nbrLetters);
        for (const letter of letters) {
            player.currentLetters.push(letter);
        }
    }

    getLettersToRemove(player: Player, lettersExchangeString: string[]): Letter[] {
        const currentPlayerLetters = player.currentLetters;
        const lettersToRemove: Letter[] = [];
        const currentPlayerCopy = currentPlayerLetters.slice();

        for (const exchangeLetter of lettersExchangeString) {
            for (let i = 0; i < currentPlayerCopy.length; i += 1) {
                if (exchangeLetter.toUpperCase() === currentPlayerCopy[i]?.character) {
                    lettersToRemove.push(currentPlayerCopy[i]);
                    delete currentPlayerCopy[i];
                    break;
                }
            }
        }
        return lettersToRemove;
    }

    private isExchangeValid(player: Player, lettersExchangeString: string[]) {
        // TODO : Deal BOT if (this.letterBankService.letterBank.length < constant.MIN_WORD_BANK && !this.game.verifyBotTurn()) {
        // TODO
        // this.chatHandlerService.printMessage(
        //     "Commande impossible à réaliser : Il n'y a pas assez de lettre dans la banque pour effectuer un échange",
        //     Emitter.Error,
        // );
        //     return false;
        // }

        if (this.letterBankService.letterBank.length < constant.MIN_WORD_BANK) {
            return false;
        }
        if (this.letterBankService.letterBank.length < lettersExchangeString.length) {
            return false;
        }
        if (!this.validatePlayerHasSevenLetters(player)) {
            // this.chatHandlerService.printMessage(
            //     "Commande impossible à réaliser : Vous devez posséder sept lettres avant d'effectuer un échange!",
            //     Emitter.Error,
            // );
            return false;
        }

        return true;
    }

    private validatePlayerHasSevenLetters(player: Player): boolean {
        return player.currentLetters.length === constant.MAX_LETTERS_PLAYER;
    }

    private depositLettersToBank(letters: Letter[]) {
        for (const letter of letters) {
            this.letterBankService.giveBackLetterToBank(letter);
        }
    }
}
