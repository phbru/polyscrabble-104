import { Player } from '@app/classes/player/player';
import * as constant from '@app/constants/command';
import { BoardModelService } from '@app/services/board-model/board-model.service';
import { DictionaryService } from '@app/services/dictionary/dictionary.service';
import { LetterBankService } from '@app/services/letter-bank/letter-bank.service';
import { RoomService } from '@app/services/room-service/room-service';
import { ScoreCounterService } from '@app/services/score-counter/score-counter.service';
import { WordFinderService } from '@app/services/word-finder/word-finder.service';
import { Letter } from '@app/types/letter';
import { WordMap } from '@app/types/word-map';
import { Service } from 'typedi';

@Service()
export class PlaceService extends RoomService {
    validWords: WordMap[];

    constructor(
        private letterBankService: LetterBankService,
        private wordFinder: WordFinderService,
        private scoreCounter: ScoreCounterService,
        private boardModel: BoardModelService,
        private dictionaryService: DictionaryService,
    ) {
        super();
    }
    async execute(player: Player, enteredLetters: WordMap): Promise<boolean> {
        const necessaryLetters: Letter[] = [];
        for (const charPos of enteredLetters.map) {
            necessaryLetters.push(charPos.letter);
        }

        if (this.validateEntries(enteredLetters)) {
            this.boardModel.addLettersToRealBoard(enteredLetters);
            this.actualizeLetters(player, necessaryLetters);
            this.updatePoints(player, this.validWords, necessaryLetters); // TODO : what is the use of these words ... ?
            this.boardModel.uploadWords(this.validWords);
            return true;
        }

        return false;
    }

    private updatePoints(player: Player, validWords: WordMap[], necessaryLetters: Letter[]) {
        let points = this.scoreCounter.countAllPoints(validWords);

        if (necessaryLetters.length === constant.MAX_LETTERS_PLAYER) {
            points += constant.BINGO_BONUS;
        }
        player.addPoints(points);

        this.scoreCounter.removeBonus(validWords);
    }

    private actualizeLetters(player: Player, necessaryLetters: Letter[]) {
        player.removeLettersFromPlayer(necessaryLetters);
        let nbrLetterToRetrieve: number = necessaryLetters.length;

        if (necessaryLetters.length > this.letterBankService.letterBank.length) {
            nbrLetterToRetrieve = this.letterBankService.letterBank.length;
        }

        const newLetters: Letter[] = this.letterBankService.retrieveRandomLetters(nbrLetterToRetrieve);

        for (const letter of newLetters) {
            player.currentLetters.push(letter);
        }
    }

    private validateEntries(wordMap: WordMap): boolean {
        this.validWords = this.wordFinder.getNewWords(wordMap);

        for (const word of this.validWords) {
            if (!this.dictionaryService.validateWordExists(word.word)) {
                return false;
            }
        }

        return true;
    }
}
