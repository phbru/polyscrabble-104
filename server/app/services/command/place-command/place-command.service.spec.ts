// /* eslint-disable max-lines */
// /* eslint-disable @typescript-eslint/no-explicit-any -- eslint disabled to test private methods */
// /* eslint-disable dot-notation -- eslint disabled to test private methods -- lots of tests */

// import { fakeAsync, TestBed, tick } from '@angular/core/testing';
// import { Player } from '@app/classes/player';
// import { Color } from '@app/enums/color';
// import { BoardModelServiceProxy } from '@app/services/board-model-proxy/board-model-proxy.service';
// import { ChatHandlerService } from '@app/services/chat-handler/chat-handler.service';
// import { DictionaryServiceProxy } from '@app/services/dictionary-proxy/dictionary-proxy.service';
// import { GameServiceProxy } from '@app/services/game-manager-proxy/game-manager-proxy.service';
// import { GameTerminatorServiceProxy } from '@app/services/game-terminator-proxy/game-terminator-proxy.service';
// import { GridControllerService } from '@app/services/grid-controller/grid-controller.service';
// import { LetterBankServiceProxy } from '@app/services/letter-bank-proxy/letter-bank-proxy.service';
// import { ScoreCounterServiceProxy } from '@app/services/score-counter-proxy/score-counter-proxy.service';
// import { WordFinderServiceProxy } from '@app/services/word-finder-proxy/word-finder-proxy.service';
// import { Letter } from '@app/types/letter';
// import { WordMap } from '@app/types/word-map';
// import { PlaceCommandService } from './place-command.service';

// describe('PlaceCommandService', () => {
//     let service: PlaceCommandService;
//     let wordMap: WordMap;
//     let gameSpy: jasmine.SpyObj<GameServiceProxy>;
//     let chatHandlerServiceSpy: jasmine.SpyObj<ChatHandlerService>;
//     let letterBankServiceSpy: jasmine.SpyObj<LetterBankServiceProxy>;
//     let dictionaryServiceSpy: jasmine.SpyObj<DictionaryServiceProxy>;
//     let gridControllerServiceSpy: jasmine.SpyObj<GridControllerService>;
//     let scoreCounterSpy: jasmine.SpyObj<ScoreCounterServiceProxy>;
//     let wordFinderServiceSpy: jasmine.SpyObj<WordFinderServiceProxy>;
//     let boardModelSpy: jasmine.SpyObj<BoardModelServiceProxy>;
//     let gameTerminatorSpy: jasmine.SpyObj<GameTerminatorServiceProxy>;
//     beforeEach(() => {
//         const player1 = new Player({ playerName: 'John', color: Color.Green }, []);
//         const player2 = new Player({ playerName: 'Paul', color: Color.Green }, []);

//         gameSpy = jasmine.createSpyObj(
//             'GameServiceProxy',
//             [
//                 'addPoints',
//                 'validatePlayerHasLetters',
//                 'removeLettersFromPlayer',
//                 'addLettersToPlayer',
//                 'changeTurn',
//                 'giveLettersToPlayer',
//                 'removeLettersFromRackView',
//                 'resetRackViewToModel',
//                 'validateIsOurTurn',
//             ],
//             { currentPlayer: player1, botIndex: 1, botPlayer: player2 },
//         );
//         gameSpy.players = [player1, player2];
//         gameSpy.ourIndex = 0;
//         gameSpy.currentlyPlayingIndex = 0;
//         gameSpy.addLettersToPlayer.and.resolveTo();

//         chatHandlerServiceSpy = jasmine.createSpyObj('ChatHandler', ['printMessage']);
//         letterBankServiceSpy = jasmine.createSpyObj('LetterBankService', ['retrieveRandomLetters']);
//         letterBankServiceSpy.retrieveRandomLetters.and.resolveTo([{ character: 'A', weight: 0, isBlankLetter: false }]);
//         letterBankServiceSpy.letterBank = [];

//         dictionaryServiceSpy = jasmine.createSpyObj('DictionaryService', ['validateWordExists']);
//         gridControllerServiceSpy = jasmine.createSpyObj('GridControllerService', ['addWordToView', 'resetView', 'saveView']);
//         scoreCounterSpy = jasmine.createSpyObj('ScoreCounterService', ['countAllPoints', 'removeBonus']);
//         wordFinderServiceSpy = jasmine.createSpyObj('WordFinderService', ['getNewWords']);
//         boardModelSpy = jasmine.createSpyObj('BoardModel', [
//             'countAllPoints',
//             'isBoardEmpty',
//             'validateEntryTouchesPlacedLetters',
//             'addWordToView',
//             'uploadBoardMatrix',
//             'uploadWords',
//             'returnNecessaryLetters',
//             'validateEntries',
//             'checkIfOverwritesWord',
//         ]);
//         boardModelSpy.returnNecessaryLetters.and.resolveTo([]);

//         gameTerminatorSpy = jasmine.createSpyObj('GameTerminatorSpy', ['adjustPointsNoLetterEnding', 'terminateGame']);
//     });

//     beforeEach(() => {
//         wordMap = {
//             word: 'test',
//             map: [
//                 { letter: { character: 'T', weight: 0, isBlankLetter: false }, line: 'I', column: 8 },
//                 { letter: { character: 'E', weight: 0, isBlankLetter: false }, line: 'I', column: 9 },
//                 { letter: { character: 'S', weight: 0, isBlankLetter: false }, line: 'I', column: 10 },
//                 { letter: { character: 'T', weight: 0, isBlankLetter: false }, line: 'I', column: 11 },
//             ],
//         };

//         TestBed.configureTestingModule({
//             providers: [
//                 { provide: GameServiceProxy, useValue: gameSpy },
//                 { provide: ChatHandlerService, useValue: chatHandlerServiceSpy },
//                 { provide: LetterBankServiceProxy, useValue: letterBankServiceSpy },
//                 { provide: DictionaryServiceProxy, useValue: dictionaryServiceSpy },
//                 { provide: GridControllerService, useValue: gridControllerServiceSpy },
//                 { provide: ScoreCounterServiceProxy, useValue: scoreCounterSpy },
//                 { provide: WordFinderServiceProxy, useValue: wordFinderServiceSpy },
//                 { provide: BoardModelServiceProxy, useValue: boardModelSpy },
//                 { provide: GameTerminatorServiceProxy, useValue: gameTerminatorSpy },
//             ],
//         });
//         service = TestBed.inject(PlaceCommandService);
//     });
//     it('should create', () => {
//         expect(service).toBeTruthy();
//     });

//     describe('execute', () => {
//         beforeEach(() => {
//             boardModelSpy.returnNecessaryLetters.and.resolveTo([
//                 { letter: { character: 'A', weight: 0, isBlankLetter: false }, line: 'A', column: 1 },
//             ]);

//             spyOn<any>(service, 'validateEmptyBoardPlacement').and.returnValue(true);
//             spyOn<any>(service, 'validatePlayerHasNecessaryLetters').and.returnValue(true);
//             spyOn<any>(service, 'validateWordIsAttached').and.returnValue(true);
//             spyOn<any>(service, 'validateWordOverlap').and.returnValue(true);
//         });
//         it('should add word to board', async () => {
//             await service.execute(wordMap);
//             expect(gridControllerServiceSpy.addWordToView).toHaveBeenCalled();
//         });

//         it('should execute', async () => {
//             const actualizeSpy = spyOn<any>(service, 'actualizeLetters');
//             const updatePoints = spyOn<any>(service, 'updatePoints');

//             boardModelSpy.wordsOnGrid = [];
//             spyOn<any>(service, 'validateWordExists').and.resolveTo(true);
//             spyOn<any>(service, 'validateEntries').and.resolveTo(true);

//             await service.execute(wordMap);
//             expect(actualizeSpy).toHaveBeenCalled();
//             expect(updatePoints).toHaveBeenCalled();
//             expect(gameSpy.changeTurn).toHaveBeenCalled();
//         });
//         it('should return false if invalid input', async () => {
//             spyOn<any>(service, 'isValid').and.resolveTo(false);
//             expect(await service.execute(wordMap)).toBeFalse();
//         });

//         it('should not change turn if game ended', async () => {
//             gameSpy.isGameEnded = true;
//             boardModelSpy.wordsOnGrid = [];
//             spyOn<any>(service, 'validateWordExists').and.resolveTo(true);
//             spyOn<any>(service, 'validateEntries').and.resolveTo(true);

//             await service.execute(wordMap);
//             expect(gameSpy.changeTurn).not.toHaveBeenCalled();
//         });

//         it("should not change turn if game ended and word doesn't exist", fakeAsync(() => {
//             const DELAY_MS = 3000;
//             spyOn<any>(service, 'validateWordExists').and.resolveTo(false);
//             gameSpy.isGameEnded = true;
//             service.execute(wordMap);
//             tick(DELAY_MS);
//             expect(gameSpy.changeTurn).not.toHaveBeenCalled();
//         }));
//         it('should not execute', fakeAsync(() => {
//             const DELAY_MS = 3000;
//             const actualizeSpy = spyOn<any>(service, 'actualizeLetters');
//             const updatePoints = spyOn<any>(service, 'updatePoints');

//             spyOn<any>(service, 'validateWordExists').and.returnValue(false);

//             service.execute(wordMap);
//             expect(actualizeSpy).not.toHaveBeenCalled();
//             expect(updatePoints).not.toHaveBeenCalled();

//             tick(DELAY_MS);
//             expect(gridControllerServiceSpy.resetView).toHaveBeenCalled();
//         }));
//     });
//     describe('updatePoints', () => {
//         const GRID_POINTS = 3;
//         const LETTER_RACK_SIZE = 7;
//         let necessaryLetters: Letter[];
//         beforeEach(() => {
//             scoreCounterSpy.countAllPoints.and.resolveTo(GRID_POINTS);
//             gameSpy.currentlyPlayingIndex = 0;

//             necessaryLetters = [];
//             for (let i = 0; i < LETTER_RACK_SIZE; ++i) {
//                 necessaryLetters.push({ character: 'A', weight: 0, isBlankLetter: false });
//             }
//         });
//         it('should give player points', async () => {
//             gameSpy.validateIsOurTurn.and.returnValue(true);
//             necessaryLetters.pop();
//             await service['updatePoints']([], necessaryLetters);
//             expect(gameSpy.addPoints).toHaveBeenCalledWith(0, 3);
//         });
//         it('should give player points with bingo', async () => {
//             const BINGO_BONUS = 50;

//             wordMap.word = 'testing';
//             wordMap.map.push({ letter: { character: 'I', weight: 0, isBlankLetter: false }, line: 'I', column: 12 });
//             wordMap.map.push({ letter: { character: 'N', weight: 0, isBlankLetter: false }, line: 'I', column: 13 });
//             wordMap.map.push({ letter: { character: 'G', weight: 0, isBlankLetter: false }, line: 'I', column: 14 });

//             await service['updatePoints']([wordMap], necessaryLetters);
//             expect(gameSpy.addPoints).toHaveBeenCalledWith(0, GRID_POINTS + BINGO_BONUS);
//         });
//         it('should print message with bot points', async () => {
//             gameSpy.currentlyPlayingIndex = 1;
//             await service['updatePoints']([wordMap], necessaryLetters);
//             expect(chatHandlerServiceSpy.printMessage).toHaveBeenCalled();
//         });
//     });
//     describe('validateNewLetters', () => {
//         it('should return true if not empty', () => {
//             const value = service['validateNewLetters']([{ character: 'A', weight: 0, isBlankLetter: false }]);
//             expect(value).toBeTrue();
//         });
//         it('should return false if empty', () => {
//             const value = service['validateNewLetters']([]);
//             expect(value).toBeFalse();
//         });
//     });
//     describe('validateEmptyBoardPlacement', () => {
//         it('should return true if board not empty', async () => {
//             boardModelSpy.isBoardEmpty.and.resolveTo(false);
//             expect(await service['validateEmptyBoardPlacement'](wordMap)).toBeTrue();
//         });
//         it('should return true with center placement', async () => {
//             wordMap.map[0].line = 'H';
//             wordMap.map[0].column = 8;

//             boardModelSpy.isBoardEmpty.and.resolveTo(true);
//             expect(await service['validateEmptyBoardPlacement'](wordMap)).toBeTrue();
//         });
//         it('should return false with placement not at center', async () => {
//             boardModelSpy.isBoardEmpty.and.resolveTo(true);
//             expect(await service['validateEmptyBoardPlacement'](wordMap)).toBeFalse();
//         });
//     });
//     describe('validatePlayerHasNecessaryLetters', () => {
//         it('should return true if has necessary letters', async () => {
//             gameSpy.validatePlayerHasLetters.and.returnValue(true);
//             expect(await service['validatePlayerHasNecessaryLetters']([])).toBeTrue();
//         });
//         it('should return false if does not have necessary letters', () => {
//             gameSpy.validatePlayerHasLetters.and.returnValue(false);
//             expect(service['validatePlayerHasNecessaryLetters']([])).toBeFalse();
//             expect(chatHandlerServiceSpy.printMessage).toHaveBeenCalled();
//         });
//     });
//     describe('actualizeLetters', () => {
//         it('should actualize letters', async () => {
//             const lettersToActualize = [{ character: 'A', weight: 0, isBlankLetter: false }];
//             letterBankServiceSpy.letterBank = [
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//             ];
//             gameSpy.currentlyPlayingIndex = 0;
//             letterBankServiceSpy.retrieveRandomLetters.and.resolveTo(letterBankServiceSpy.letterBank);

//             await service['actualizeLetters'](lettersToActualize);
//             expect(gameSpy.removeLettersFromPlayer).toHaveBeenCalledWith(0, lettersToActualize);
//             expect(letterBankServiceSpy.retrieveRandomLetters).toHaveBeenCalledWith(1);
//             expect(gameSpy.addLettersToPlayer).toHaveBeenCalled();
//         });
//         it('should actualize letters only with remaining letters in bank', async () => {
//             const lettersToActualize = [
//                 { character: 'A', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'C', weight: 0, isBlankLetter: false },
//             ];
//             letterBankServiceSpy.letterBank = [
//                 { character: 'B', weight: 0, isBlankLetter: false },
//                 { character: 'B', weight: 0, isBlankLetter: false },
//             ];
//             gameSpy.currentlyPlayingIndex = 0;
//             letterBankServiceSpy.retrieveRandomLetters.and.resolveTo(letterBankServiceSpy.letterBank);

//             await service['actualizeLetters'](lettersToActualize);
//             expect(gameSpy.removeLettersFromPlayer).toHaveBeenCalledWith(0, lettersToActualize);
//             expect(letterBankServiceSpy.retrieveRandomLetters).toHaveBeenCalledWith(2);
//             expect(gameSpy.addLettersToPlayer).toHaveBeenCalled();
//         });
//         it('should terminate game if player has no more letters', async () => {
//             gameSpy.players[0].currentLetters = [];
//             gameSpy.currentlyPlayingIndex = 0;
//             letterBankServiceSpy.retrieveRandomLetters.and.resolveTo([]);
//             letterBankServiceSpy.letterBank = [];
//             await service['actualizeLetters']([]);
//             expect(gameTerminatorSpy.terminateGame).toHaveBeenCalled();
//         });
//     });
//     it('should validate attached word', async () => {
//         boardModelSpy.validateEntryTouchesPlacedLetters.and.resolveTo(true);
//         expect(await service['validateWordIsAttached'](wordMap)).toBeTrue();
//     });
//     it('should not validate detached word', async () => {
//         boardModelSpy.validateEntryTouchesPlacedLetters.and.resolveTo(false);
//         expect(await service['validateWordIsAttached'](wordMap)).toBeFalse();
//     });
//     describe('validateWordOverlap', () => {
//         it('should return true if no overwrite', async () => {
//             boardModelSpy.checkIfOverwritesWord.and.resolveTo(false);
//             expect(await service['validateWordOverlap'](wordMap)).toBeTrue();
//         });

//         it('should return false if overwrite', async () => {
//             boardModelSpy.checkIfOverwritesWord.and.resolveTo(true);
//             expect(await service['validateWordOverlap'](wordMap)).toBeFalse();
//         });
//     });
//     describe('validateWordExists', () => {
//         it('should return true with existing word', async () => {
//             dictionaryServiceSpy.validateWordExists.and.resolveTo(true);
//             expect(await service['validateWordExists'](wordMap)).toEqual(true);
//         });

//         it('should return false with non-existing word', async () => {
//             dictionaryServiceSpy.validateWordExists.and.resolveTo(false);
//             expect(await service['validateWordExists'](wordMap)).toEqual(false);
//             expect(chatHandlerServiceSpy.printMessage).toHaveBeenCalled();
//         });
//     });
//     describe('validateEntries', () => {
//         it('should return true on valid entry', async () => {
//             dictionaryServiceSpy.validateWordExists.and.resolveTo(true);
//             wordFinderServiceSpy.getNewWords.and.resolveTo([
//                 { word: 'test', map: [{ letter: { character: 'A', weight: 0, isBlankLetter: false }, line: 'A', column: 0 }] },
//             ]);
//             expect(await service['validateEntries'](wordMap)).toEqual(true);
//         });
//         it('should not validate non-existing word', async () => {
//             dictionaryServiceSpy.validateWordExists.and.resolveTo(false);
//             wordFinderServiceSpy.getNewWords.and.resolveTo([
//                 { word: 'test', map: [{ letter: { character: 'A', weight: 0, isBlankLetter: false }, line: 'A', column: 0 }] },
//             ]);
//             expect(await service['validateEntries'](wordMap)).toEqual(false);
//             expect(chatHandlerServiceSpy.printMessage).toHaveBeenCalled();
//         });
//     });
// });
