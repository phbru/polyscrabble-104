import { User } from '@app/classes/user';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { RoomService } from '@app/services/room-service/room-service';
import { Service } from 'typedi';

@Service()
export class SelfUserService extends RoomService {
    @MirrorProp() user: User = new User('');
    constructor(private globalUserService: GlobalUserService) {
        super();
    }

    setUser(user: User) {
        this.user = user;
    }

    updateAvatar(avatarUrl: string) {
        this.user.avatar = avatarUrl;
        this.updateClientsUser();
    }

    updateUsername(username: string) {
        this.user.username = username;
        this.updateClientsUser();
    }

    updateUserStat() {
        this.updateClientsUser();
    }

    updateClientsUser() {
        this.emitPropUpdate('user');
        this.globalUserService.emitPropUpdate('onlineUsers');
    }
}
