/* eslint-disable max-lines */
import { BotPlayer } from '@app/classes/bot-player/bot-player';
import { Channel } from '@app/classes/channel/channel';
import { HumanPlayer } from '@app/classes/human-player/human-player';
import { Player } from '@app/classes/player/player';
import { User } from '@app/classes/user';
import { ActionCode } from '@app/enums/action-code';

import { ActionMessageService } from '@app/services/action-message-transmitter/action-message.service';
import { BotActionGenerator } from '@app/services/bot-action-generator/bot-action-generator';
import { ExchangeService } from '@app/services/command/exchange-command/exchange-service';
import { PlaceService } from '@app/services/command/place-command/place.service';
import { DictionaryService } from '@app/services/dictionary/dictionary.service';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { LetterBankService } from '@app/services/letter-bank/letter-bank.service';
import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { ProxyCall } from '@app/services/room-service/proxy-call/proxy-call';
import { RoomService } from '@app/services/room-service/room-service';
import { SelfUserService } from '@app/services/self-user/self-user.service';
import { TimerService } from '@app/services/timer/timer.service';
import { Action } from '@app/types/action';
import { GameInfo } from '@app/types/game-info';
import { Letter } from '@app/types/letter';
import { WordMap } from '@app/types/word-map';
import { Service } from 'typedi';

const NUM_START_LETTERS = 7;

// TODO : change this ?
const LIMIT_PASS = 2;

@Service()
export class GameService extends RoomService {
    @MirrorProp() currentlyPlayingIndex: number;
    @MirrorProp() isGameEnded: boolean = false;
    @MirrorProp() players: Player[] = [];
    @MirrorProp() sumOfLettersLeftByPlayer: number[] = [0, 0];
    @MirrorProp() winner: Player | undefined;
    @MirrorProp() partialGameInfo: GameInfo = {
        id: '',
        creatorName: '',
        time: 0,
        openSpots: 0,
        dictionary: '',
        password: '',
        players: [],
        observers: [],
        virtualPlayers: [],
        invitedUsers: [],
        isGameStarted: false,
        isPrivate: false,
    };
    observers: User[] = [];
    channel: Channel | undefined;
    cumulativeTime: number = 0;
    isBotGame: boolean = false;
    private gameIsDead: boolean = false;
    private turnsPassedByPlayer: number[] = [];

    constructor(
        private letterBankService: LetterBankService,
        private timerService: TimerService,
        private exchangeService: ExchangeService,
        private botActionGenerator: BotActionGenerator,
        private actionMessageService: ActionMessageService,
        private placeService: PlaceService,
        private globalUserService: GlobalUserService,
        private dictionaryService: DictionaryService,
    ) {
        super();
    }
    @ProxyCall()
    executePass() {
        if (!this.validatePlayerCanAction()) return;
        this.passTurn();
    }

    @ProxyCall()
    async executePlace(wordMapToPlace: WordMap) {
        if (!this.validatePlayerCanAction()) return;
        const caller = this.caller;
        const isValid = await this.placeService.execute(this.getCurrentPlayer(), wordMapToPlace); // executes place as well as validates the placement
        if (isValid) {
            const action = wordMapToPlace.word.length === 1 ? 'gamePage.action.singleLetterPlacement' : 'gamePage.action.multipleLettersPlacement';
            this.actionMessageService.emitAction(this.getCurrentPlayer().user, ActionCode.Place, action, Array.from(wordMapToPlace.word).toString());
            this.resetTurnsPassed();
            this.changeTurn();
        } else {
            this.emitMessageToPlayer(caller, 'invalidWord');
            this.passTurn();
            this.emitMessage('resetView'); // this is necessary to tell the client to redraw the real board if word is not valid
        }
        this.emitPropUpdate('players');
    }

    @ProxyCall()
    executeExchange(lettersToExchange: string[]) {
        if (!this.validatePlayerCanAction()) return;
        if (this.exchangeService.execute(this.getCurrentPlayer(), lettersToExchange)) {
            // At the end of the game, an exchange can be refused -> False
            const action = lettersToExchange.length === 1 ? 'gamePage.action.singleLetterExchange' : 'gamePage.action.multipleLettersExchange';
            this.actionMessageService.emitAction(this.getCurrentPlayer().user, ActionCode.Exchange, action, lettersToExchange.length.toString());
            this.emitPropUpdate('players');
            this.resetTurnsPassed();
            this.changeTurn();
        } else {
            if (this.getCurrentPlayer() instanceof BotPlayer) {
                this.passTurn();
            }
        }
    }

    @ProxyCall()
    takeBotPlace(botToReplace: Player) {
        if (!botToReplace.isBotPlayer && this.isObserver(this.caller)) return;
        let index = 0;
        for (const player of this.players) {
            if (botToReplace.user.username === player.user.username) {
                const newPlayer: HumanPlayer = new HumanPlayer(this.caller, player.currentLetters, player.points);
                this.players.splice(index, 1, newPlayer);
                break;
            }
            index++;
        }

        this.emitPropUpdate('players');
        this.emitMessageToPlayer(this.caller, 'takeIndex', index);
    }
    @ProxyCall()
    abandonGame(leavingPlayerId: string) {
        this.validateCallerPlayerInGame();
        let index = 0;
        for (const player of this.players) {
            if (leavingPlayerId === player.user.id) {
                const newBot: BotPlayer = new BotPlayer(
                    User.makeBot('Joueur virtuel ' + (this.countBotPlayers() + 1)),
                    player.currentLetters,
                    player.points,
                );
                this.isBotGame = true;

                this.players.splice(index, 1, newBot);
                break;
            }
            index++;
        }
        this.emitPropUpdate('players');
        if (this.currentlyPlayingIndex === index) {
            this.passTurn();
        }
    }

    killGame() {
        this.timerService.stop();
        this.gameIsDead = true;
    }

    passTurn() {
        this.incrementTurnsPassed(this.currentlyPlayingIndex);
        this.actionMessageService.emitAction(this.getCurrentPlayer().user, ActionCode.Pass, 'gamePage.action.pass');
        this.changeTurn();
    }

    // Prevents double click action from client
    validatePlayerCanAction(): boolean {
        // handles end games or other edge cases
        if (this.currentlyPlayingIndex < 0 || this.currentlyPlayingIndex >= this.players.length) {
            return false;
        }
        if (this.caller.id !== this.getCurrentPlayer().user.id) {
            return false;
        }
        return true;
    }

    validateCallerPlayerInGame(): boolean {
        for (const player of this.players) {
            if (this.caller.id === player.user.id) {
                return true;
            }
        }
        return false;
    }

    terminateGame() {
        this.timerService.stop();
        this.isGameEnded = true;
        this.currentlyPlayingIndex = -1;

        const winner: Player | undefined = this.findWinner();
        if (winner === undefined) {
            this.emitMessage('gameEnded', '');
        } else {
            this.emitMessage('gameEnded', winner.user.username);
        }

        if (!this.isBotGame) {
            this.adjustPlayersStats(winner);
        }
    }

    adjustPlayersStats(winner: Player | undefined) {
        const NO_ID = '-1';
        const winnerId = winner === undefined ? NO_ID : winner.user.id;
        const averageLosingElo: number = this.computeAverageLosingElo(winnerId);
        for (const player of this.players) {
            if (!player.isBotPlayer) {
                const playerWon: boolean = winnerId === player.user.id;
                player.user.adjustUserStats(playerWon, this.cumulativeTime, averageLosingElo, player.points);
            }
        }
        this.globalUserService.adjustGlobalStatistics();
        // Need to pass a second time to update raking
        for (const player of this.players) {
            if (!player.isBotPlayer) {
                player.user.getContainer().get(SelfUserService).updateClientsUser();
            }
        }
    }

    findWinner(): Player | undefined {
        let isTie = true;
        let winner: Player = this.players[0];

        for (const player of this.players) {
            if (player.points > winner.points) {
                winner = player;
            } else if (player.points !== winner.points) {
                isTie = false;
            }
        }

        return isTie ? undefined : winner;
    }

    computeAverageLosingElo(winnerId: string) {
        let averageLosingElo = 0;
        for (const player of this.players) {
            if (player.user.id !== winnerId) {
                averageLosingElo += player.user.statistics.elo;
            }
        }
        return averageLosingElo;
    }

    startGame(gameInfo: GameInfo) {
        this.partialGameInfo.dictionary = gameInfo.dictionary;
        this.partialGameInfo.password = gameInfo.password;
        this.partialGameInfo.time = gameInfo.time;
        this.partialGameInfo.observers = gameInfo.observers;

        this.dictionaryService.setupDictionary(gameInfo.dictionary);

        this.timerService.setTimePerTurn(this.timerService.numberToTime(gameInfo.time));

        for (const humanPlayer of gameInfo.players) {
            this.players.push(new HumanPlayer(humanPlayer, []));
            this.turnsPassedByPlayer.push(0);
        }
        for (const botPlayer of gameInfo.virtualPlayers) {
            this.isBotGame = true;
            this.turnsPassedByPlayer.push(0);
            this.players.push(new BotPlayer(botPlayer, []));
        }
        for (const observer of gameInfo.observers) {
            const observerToAdd: User | undefined = this.globalUserService.getUser(observer.id);
            if (observerToAdd !== undefined) {
                this.observers.push(observerToAdd);
            }
        }

        this.preparePlayers();
        this.emitPropUpdate('partialGameInfo');
        this.emitPropUpdate('players');
        this.emitMessage('initiateClientPlayer');
        this.emitMessage('goToGamePage');
        this.performNewTurnRoutine();
    }

    giveLettersToPlayer(playerIndex: number, nbrLetters: number) {
        const letters: Letter[] = this.letterBankService.retrieveRandomLetters(nbrLetters);
        this.addLettersToPlayer(playerIndex, letters);
    }
    getCurrentPlayer(): Player {
        return this.players[this.currentlyPlayingIndex];
    }
    removeLettersFromPlayer(playerIndex: number, lettersToRemove: Letter[]) {
        if (playerIndex >= this.players.length || playerIndex < 0) return;

        for (const letterToRemove of lettersToRemove) {
            const INVALID_INDEX = -1;
            const currentLetters = this.players[playerIndex].currentLetters;
            const letterToRemoveIndex: number = letterToRemove.isBlankLetter
                ? currentLetters.findIndex((curLetter: Letter) => curLetter.isBlankLetter)
                : currentLetters.findIndex((curLetter: Letter) => curLetter.character === letterToRemove.character);

            if (letterToRemoveIndex > INVALID_INDEX) {
                this.players[playerIndex].currentLetters.splice(letterToRemoveIndex, 1);
            }
        }

        this.emitPropUpdate('players');
    }

    addLettersToPlayer(playerIndex: number, lettersToAdd: Letter[]) {
        if (playerIndex >= this.players.length || playerIndex < 0) return;

        for (const letter of lettersToAdd) {
            this.players[playerIndex].currentLetters.push(letter);
        }

        this.emitPropUpdate('players');
    }

    addPoints(playerIndex: number, newPoints: number) {
        if (playerIndex < this.players.length && playerIndex >= 0) {
            this.players[playerIndex].points += newPoints;
        }

        this.emitPropUpdate('players');
    }

    private countBotPlayers() {
        let amountOfBots = 0;
        for (const player of this.players) {
            if (player instanceof BotPlayer) {
                amountOfBots++;
            }
        }
        return amountOfBots;
    }

    private incrementTurnsPassed(playerIndex: number) {
        ++this.turnsPassedByPlayer[playerIndex];
    }

    private resetTurnsPassed() {
        this.turnsPassedByPlayer = [0, 0];
    }

    private limitHasBeenReached(): boolean {
        for (const turnsPassed of this.turnsPassedByPlayer) {
            if (turnsPassed < LIMIT_PASS) {
                return false;
            }
        }
        return true;
    }

    private isObserver(user: User) {
        for (const oberver of this.observers) {
            if (user.id === oberver.id) {
                return true;
            }
        }
        return false;
    }

    private async executeRandomBotAction(botPlayer: BotPlayer) {
        botPlayer.startActionTime = Date.now();
        const action: Action = await this.botActionGenerator.generateRandomAction(botPlayer);

        const remainingDelay = botPlayer.computeRemainingDelay();
        setTimeout(() => {
            if (this.gameIsDead) return;
            //  this.chatHandler.printMessage('!passer', Emitter.Bot); //TODO : chat message
            switch (action.actionType) {
                case ActionCode.Pass:
                    this.passTurn();
                    break;
                case ActionCode.Exchange:
                    this.executeExchange(action.parameter as string[]);
                    break;
                case ActionCode.Place:
                    this.executePlace(action.parameter as WordMap); // TODO : need resetTurn if I use the service instead
                    break;
            }
        }, remainingDelay);
    }

    private performNewTurnRoutine() {
        if (this.gameIsDead) return;
        if (this.limitHasBeenReached()) {
            this.terminateGame();
            return;
        }
        this.timerService.start(this.passTurn.bind(this));
        if (this.getCurrentPlayer() !== undefined && this.getCurrentPlayer() instanceof BotPlayer) {
            const currentBotPlayer: BotPlayer = this.getCurrentPlayer() as BotPlayer;
            this.executeRandomBotAction(currentBotPlayer);
        }
    }

    private selectFirstPlayer() {
        const amountOfPlayers = this.players.length;
        this.currentlyPlayingIndex = Math.floor(Math.random() * amountOfPlayers);
    }

    private changeTurn() {
        // Done at every end of turn
        this.cumulativeTime += this.timerService.getTurnTimeLength();
        this.currentlyPlayingIndex = (this.currentlyPlayingIndex + 1) % this.players.length;
        this.performNewTurnRoutine();
    }

    private preparePlayers() {
        this.distributeLetters();
        this.selectFirstPlayer();
    }

    private distributeLetters() {
        for (const player of this.players) {
            const letters: Letter[] = this.letterBankService.retrieveRandomLetters(NUM_START_LETTERS);
            player.currentLetters = letters;
        }
        // this.emitPropUpdate('players'); // TODO : remove ? done after
    }
}
