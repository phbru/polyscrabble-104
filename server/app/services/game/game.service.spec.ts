// /* eslint-disable no-unused-expressions -- eslint confused with chai*/
// /* eslint-disable @typescript-eslint/no-unused-expressions -- eslint confused with chai */
// import { BotPlayer } from '@app/classes/bot-player/bot-player';
// import { Player } from '@app/classes/player/player';
// import { BotLevel } from '@app/enums/bot-level';
// import { Color } from '@app/enums/color';
// import { LetterBankService } from '@app/services/letter-bank/letter-bank.service';
// import { TimerService } from '@app/services/timer/timer.service';
// import { Letter } from '@app/types/letter';
// import { expect } from 'chai';
// import * as Sinon from 'sinon';
// import { GameService } from './game-manager.service';

// describe('GameService', () => {
//     let service: GameService;
//     let letterBank: LetterBankService;
//     let timer: TimerService;
//     let mockPlayer1: Player;
//     let mockPlayer2: Player;

//     beforeEach(() => {
//         letterBank = new LetterBankService();
//         timer = new TimerService();
//         // service = new GameService(letterBank, timer);
//     });

//     it('should be created', () => {
//         expect(service).to.exist;
//     });
//     it('should initiate game', () => {
//         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Private property
//         const distributeLettersSpy = Sinon.spy(service as any, 'distributeLetters');
//         // eslint-disable-next-line @typescript-eslint/no-explicit-any -- Private property
//         const selectFirstPlayerSpy = Sinon.spy(service as any, 'selectFirstPlayer');

//         service.preparePlayers();
//         expect(distributeLettersSpy.called).to.be.true;
//         expect(selectFirstPlayerSpy.called).to.be.true;
//     });

//     // it('should start game', () => {
//     //     const timerSpy = Sinon.spy(timer, 'start');
//     //     service.startGame();
//     //     expect(timerSpy.calledOnce).to.be.true;
//     // });

//     // it('should add human player', () => {
//     //     expect(service.players.length).to.equal(0);
//     //     service.addHumanPlayer('John');
//     //     expect(service.players[0]).to.be.instanceOf(Player);
//     //     expect(service.players[0].playerInfo.playerName).to.equal('John');
//     // });

//     // it('should add second human player with different color', () => {
//     //     expect(service.players.length).to.equal(0);
//     //     service.addHumanPlayer('John');
//     //     service.addHumanPlayer('Paul');
//     //     expect(service.players[1]).to.be.instanceOf(Player);
//     //     expect(service.players[1].playerInfo.color).to.equal(Color.Blue);
//     // });

//     // it('should not add human player if players > 2', () => {
//     //     expect(service.players.length).to.equal(0);
//     //     service.addHumanPlayer('John');
//     //     service.addHumanPlayer('Paul');
//     //     service.addHumanPlayer('Jacob');
//     //     expect(service.players.length).to.equal(2);
//     // });

//     // it('should add bot player', () => {
//     //     expect(service.players.length).to.equal(0);
//     //     service.addBotPlayer('Carlito', BotLevel.Beginner);
//     //     expect(service.players[0]).to.be.instanceOf(BotPlayer);
//     //     expect(service.players[0].playerInfo.playerName).to.equal('Carlito');
//     // });

//     describe('with mock players', () => {
//         beforeEach(() => {
//             mockPlayer1 = new Player({ playerName: 'John', color: Color.Green }, []);
//             mockPlayer2 = new Player({ playerName: 'Paul', color: Color.Green }, []);
//             service.players = [mockPlayer1, mockPlayer2];
//         });

//         // it('should select random player', () => {
//         //     const stub = Sinon.stub(Math, 'random');
//         //     const expectedValues = [0, 1];
//         //     const randomReturn1 = 0.3;
//         //     stub.returns(randomReturn1);
//         //     service.selectFirstPlayer();
//         //     expect(service.currentlyPlayingIndex).to.equal(expectedValues[0]);

//         //     const randomReturn2 = 0.9;
//         //     stub.returns(randomReturn2);
//         //     service.selectFirstPlayer();
//         //     expect(service.currentlyPlayingIndex).to.equal(expectedValues[1]);
//         // });

//         it('should update player', () => {
//             const botPlayer = new BotPlayer({ playerName: 'John', color: Color.Green }, [], BotLevel.Beginner);
//             service.updateBotPlayer(0, botPlayer);
//             expect(service.players[0]).to.eq(botPlayer);
//         });

//         it('should give letters to player', () => {
//             const bankLetters = [
//                 { character: 'A', weight: 2, isBlankLetter: false },
//                 { character: 'B', weight: 3, isBlankLetter: false },
//             ];

//             const letterBankStub = Sinon.stub(letterBank, 'retrieveRandomLetters').returns(bankLetters);
//             const spy = Sinon.spy(service, 'addLettersToPlayer');

//             const numLetters = 2;
//             service.giveLettersToPlayer(0, numLetters);

//             expect(letterBankStub.calledOnceWithExactly(numLetters)).to.be.true;
//             expect(spy.calledOnceWithExactly(0, bankLetters)).to.be.true;
//         });

//         it('should do nothing on invalid index', () => {
//             expect(() => {
//                 service.giveLettersToPlayer(3, 3);
//             }).not.to.throw();
//         });

//         describe('validatePlayerHasLetters', () => {
//             beforeEach(() => {
//                 const playerLetters = [
//                     { character: 'A', weight: 2, isBlankLetter: false },
//                     { character: 'B', weight: 3, isBlankLetter: false },
//                 ];
//                 mockPlayer1.currentLetters = playerLetters;
//             });

//             it('should validate players have letters', () => {
//                 const tooManyLetters = [
//                     { character: 'A', weight: 2, isBlankLetter: false },
//                     { character: 'B', weight: 3, isBlankLetter: false },
//                     { character: 'C', weight: 4, isBlankLetter: false },
//                 ];

//                 expect(service.validatePlayerHasLetters(0, service.players[0].currentLetters)).to.be.true;
//                 expect(service.validatePlayerHasLetters(0, tooManyLetters)).to.be.false;
//                 expect(service.validatePlayerHasLetters(1, tooManyLetters)).to.be.false;
//             });

//             it('should validate players has blank letters', () => {
//                 const lettersToCheck = [
//                     { character: 'A', weight: 2, isBlankLetter: false },
//                     { character: 'C', weight: 5, isBlankLetter: true },
//                 ];
//                 service.players[0].currentLetters.push({ character: '*', weight: 0, isBlankLetter: true });

//                 expect(service.validatePlayerHasLetters(0, lettersToCheck)).to.be.true;
//             });

//             it('should return false on invalid index', () => {
//                 expect(service.validatePlayerHasLetters(2, [{ character: 'D', weight: 3, isBlankLetter: false }])).to.be.false;
//             });
//         });

//         describe('removeLettersFromPlayer', () => {
//             beforeEach(() => {
//                 const playerLetters = [
//                     { character: 'A', weight: 2, isBlankLetter: false },
//                     { character: 'B', weight: 3, isBlankLetter: false },
//                 ];
//                 mockPlayer1.currentLetters = playerLetters;
//             });

//             it('should remove all letters from player', () => {
//                 const lettersToRemove = [
//                     { character: 'A', weight: 2, isBlankLetter: false },
//                     { character: 'B', weight: 3, isBlankLetter: false },
//                 ];

//                 service.removeLettersFromPlayer(0, lettersToRemove);
//                 expect(service.players[0].currentLetters.length).to.equal(0);
//             });

//             it('should remove blank letters from player', () => {
//                 const lettersToRemove = [{ character: 'A', weight: 2, isBlankLetter: true }];
//                 service.players[0].currentLetters = [{ character: '*', weight: 2, isBlankLetter: true }];

//                 service.removeLettersFromPlayer(0, lettersToRemove);
//                 expect(service.players[0].currentLetters.length).to.equal(0);
//             });

//             it('should remove some letters from player', () => {
//                 const lettersToRemove = [{ character: 'A', weight: 2, isBlankLetter: false }];

//                 service.removeLettersFromPlayer(0, lettersToRemove);
//                 expect(service.players[0].currentLetters).to.eql([{ character: 'B', weight: 3, isBlankLetter: false }]);
//             });

//             it('should only remove letters player has', () => {
//                 const lettersToRemove = [
//                     { character: 'A', weight: 2, isBlankLetter: false },
//                     { character: 'W', weight: 2, isBlankLetter: false },
//                 ];

//                 service.removeLettersFromPlayer(0, lettersToRemove);
//                 expect(service.players[0].currentLetters).to.eql([{ character: 'B', weight: 3, isBlankLetter: false }]);
//             });

//             it('should do nothing on invalid index', () => {
//                 expect(() => {
//                     service.removeLettersFromPlayer(3, [{ character: 'D', weight: 3, isBlankLetter: false }]);
//                 }).not.to.throw();
//             });
//         });

//         describe('addLettersToPlayer', () => {
//             beforeEach(() => {
//                 const playerLetters = [
//                     { character: 'A', weight: 2, isBlankLetter: false },
//                     { character: 'B', weight: 3, isBlankLetter: false },
//                 ];
//                 mockPlayer1.currentLetters = playerLetters;
//             });

//             it('should add letters to player', () => {
//                 const lettersToAdd = [
//                     { character: 'C', weight: 7, isBlankLetter: false },
//                     { character: 'D', weight: 8, isBlankLetter: false },
//                 ];
//                 const expectedLetters: Letter[] = Object.assign([], service.players[0].currentLetters);
//                 expectedLetters.push(lettersToAdd[0], lettersToAdd[1]);

//                 service.addLettersToPlayer(0, lettersToAdd);
//                 expect(service.players[0].currentLetters).to.eql(expectedLetters);
//             });

//             it('should do nothing when invalid index', () => {
//                 expect(() => {
//                     service.addLettersToPlayer(3, [{ character: 'D', weight: 3, isBlankLetter: false }]);
//                 }).not.to.throw();
//             });
//         });

//         describe('updatePlayerLetters', () => {
//             it('should update player letters', () => {
//                 const letters = [
//                     { character: 'C', weight: 7, isBlankLetter: false },
//                     { character: 'D', weight: 8, isBlankLetter: false },
//                 ];
//                 service.updatePlayerLetters(0, letters);
//                 expect(service.players[0].currentLetters).to.eql(letters);
//             });
//         });

//         describe('addPoints', () => {
//             it('should add points to player', () => {
//                 const POINTS_TO_ADD = 10;
//                 const pointsBefore = service.players[0].points;

//                 service.addPoints(0, POINTS_TO_ADD);
//                 expect(service.players[0].points).to.eql(pointsBefore + POINTS_TO_ADD);
//             });

//             it('should do nothing when invalid index', () => {
//                 expect(() => {
//                     service.addPoints(3, 1);
//                 }).not.to.throw();
//             });
//         });

//         it('should change turn', () => {
//             const timerSpy = Sinon.spy(timer, 'start');
//             service.currentlyPlayingIndex = 0;
//             service.changeTurn();
//             expect(service.currentlyPlayingIndex).to.eql(1);
//             expect(timerSpy.calledOnce).to.be.true;
//             timerSpy.resetHistory();

//             service.currentlyPlayingIndex = 1;
//             service.changeTurn();
//             expect(service.currentlyPlayingIndex).to.eql(0);
//             expect(timerSpy.calledOnce).to.be.true;
//         });

//         it('should distribute letters', () => {
//             const NUM_LETTERS = 7;

//             expect(service.players[0].currentLetters.length).to.equal(0);
//             expect(service.players[1].currentLetters.length).to.equal(0);
//             // eslint-disable-next-line dot-notation -- Test private method
//             service['distributeLetters']();
//             expect(service.players[0].currentLetters.length).to.equal(NUM_LETTERS);
//             expect(service.players[1].currentLetters.length).to.equal(NUM_LETTERS);
//         });
//     });
// });
