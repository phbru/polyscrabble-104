import { MirrorProp } from '@app/services/room-service/mirror-prop/mirror-prop';
import { RoomService } from '@app/services/room-service/room-service';
import { Time } from '@app/types/time';
import { Service } from 'typedi';

type CallbackFunction = () => void;

@Service()
export class TimerService extends RoomService {
    @MirrorProp() timerLength: Time = { minutes: 9999, seconds: 99 };
    private mTime: Time = { minutes: 0, seconds: 0 };
    private mIntervalTimerHandle: NodeJS.Timer;
    private mCallbackFunction: CallbackFunction;

    get time() {
        return this.mTime;
    }

    getTurnTimeLength() {
        return this.timeToNumber(this.timerLength) - this.getNumberTime();
    }

    getNumberTime() {
        return this.timeToNumber(this.time);
    }

    setTimePerTurn(time: Time) {
        this.timerLength = time;
        this.emitPropUpdate('timerLength');
    }

    // callbackFunction will be called when the timer reaches 0
    start(callbackFunction: CallbackFunction) {
        const SECOND_MS = 1000;

        this.stop(); // Make sure no timer is currently running
        this.mCallbackFunction = callbackFunction;
        this.mTime = { minutes: this.timerLength.minutes, seconds: this.timerLength.seconds };
        this.mIntervalTimerHandle = setInterval(() => this.decrementSeconds(), SECOND_MS);
        this.emitPropUpdate('mTime');
    }

    stop() {
        if (this.mIntervalTimerHandle !== undefined) clearInterval(this.mIntervalTimerHandle);
    }

    numberToTime(time: number): Time {
        const minutes = Math.floor(time);
        const secondsInMinute = 60;
        const seconds = (time - minutes) * secondsInMinute;
        return { minutes, seconds };
    }

    timeToNumber(time: Time): number {
        const secondsInMinute = 60;
        return time.minutes + time.seconds / secondsInMinute;
    }

    private decrementSeconds() {
        const SECONDS_PER_MINUTE = 60;
        --this.mTime.seconds;

        if (this.mTime.seconds <= 0) {
            if (this.mTime.minutes <= 0) {
                this.emitMessage('passTurnChat');
                this.emitMessage('passTurnCommand');
                this.stop();
                this.mTime = { minutes: 0, seconds: 0 };
                this.mCallbackFunction();
            } else {
                this.mTime.seconds = SECONDS_PER_MINUTE - 1;
                this.mTime.minutes--;
            }
        }
        this.emitPropUpdate('mTime');
    }
}
