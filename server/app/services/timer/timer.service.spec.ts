// /* eslint-disable @typescript-eslint/no-empty-function -- CallBack is unused */
// /* eslint-disable no-unused-expressions -- ESLint struggles to understand Chai syntax */
// /* eslint-disable @typescript-eslint/no-unused-expressions -- ESLint struggles to understand Chai syntax */
// import { Time } from '@app/types/time';
// import { expect } from 'chai';
// import * as Sinon from 'sinon';
// import { TimerService } from './timer.service';

// describe('TimerService', () => {
//     let service: TimerService;
//     let clock: Sinon.SinonFakeTimers;

//     before(() => {
//         clock = Sinon.useFakeTimers();
//     });

//     after(() => {
//         clock.restore();
//     });

//     beforeEach(() => {
//         service = new TimerService();
//     });

//     it('should be created', () => {
//         expect(service).to.exist;
//     });

//     it('should get time', () => {
//         const time: Time = { minutes: 12, seconds: 34 };
//         // eslint-disable-next-line dot-notation -- access private member
//         service['mTime'] = time;
//         expect(service.time).to.eql(time);
//     });

//     it('should set time per turn', () => {
//         const time: Time = { minutes: 12, seconds: 34 };
//         service.setTimePerTurn(time);
//         expect(service.timerLength).to.eql(time);
//     });

//     describe('start', () => {
//         it('should set initial time', () => {
//             const timerLength: Time = { minutes: 12, seconds: 34 };
//             service.timerLength = timerLength;
//             service.start(() => {});
//             // eslint-disable-next-line dot-notation -- access private member
//             expect(service['mTime']).to.eql(timerLength);
//             service.stop(); // Clear timer
//         });

//         it('should reset timer', () => {
//             const stopSpy = Sinon.spy(service, 'stop');
//             service.start(() => {});
//             expect(stopSpy.calledOnce).to.be.true;
//         });
//     });

//     it('should call decrementSeconds after 1 second', () => {
//         const SECOND_MS = 1000;
//         const spy = Sinon.stub(service, 'decrementSeconds').callThrough();

//         service.start(() => {});
//         clock.tick(SECOND_MS);

//         expect(spy.calledOnce).to.be.true;
//         service.stop(); // Clear timer
//     });

//     it('should stop timer', () => {
//         const SECOND_MS = 1000;
//         const spy = Sinon.stub(service, 'decrementSeconds').callThrough();

//         service.start(() => {});

//         clock.tick(SECOND_MS);

//         expect(spy.calledOnce).to.be.true;
//         spy.resetHistory(); // Reset calls so we can use this spy again

//         service.stop();

//         clock.tick(SECOND_MS);
//         expect(spy.calledOnce).to.be.false;
//     });

//     // From https://stackoverflow.com/questions/41772989/test-a-function-that-contains-a-settimeout

//     it('should call callback when finished', () => {
//         const SECOND_MS = 1000;
//         const SECONDS_IN_MINUTES = 60;
//         const timerLength = { minutes: 2, seconds: 10 };
//         const callbackObj = { callback: () => {} };
//         const spy = Sinon.spy(callbackObj, 'callback');

//         service.timerLength = timerLength;
//         service.start(callbackObj.callback);

//         clock.tick(SECOND_MS * SECONDS_IN_MINUTES * timerLength.minutes + SECOND_MS * timerLength.seconds);

//         expect(spy.calledOnce).to.be.true;
//         service.stop(); // Clear timer
//     });
// });
