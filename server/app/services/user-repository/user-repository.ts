import { User } from '@app/classes/user';
import { DATABASE_NAME, DATABASE_URL, DATABASE_USERNAMES } from '@app/constants/database';
import { Collection, MongoClient } from 'mongodb';
import { Service } from 'typedi';

@Service()
export class UserRepository {
    private collection: Promise<Collection<User>> = new Promise((resolve) => {
        MongoClient.connect(DATABASE_URL, (_err, c: MongoClient) => {
            const collection: Collection<User> = c.db(DATABASE_NAME).collection(DATABASE_USERNAMES);
            collection.createIndex({ username: 1 }, { unique: true });
            resolve(collection);
        });
    });

    // Returns new user instance
    async login(user: User) {
        const foundUser = await (await this.collection).findOne(user);
        if (foundUser !== null) {
            // eslint-disable-next-line no-underscore-dangle
            return User.registered(user.username, user.avatar, user.email, user.password, foundUser._id.toString(), user.statistics);
        }

        return null;
    }

    async signup(user: User) {
        return await (await this.collection).insertOne(user);
    }

    async updateUserName(user: User, newUsername: string) {
        await (
            await this.collection
        ).updateOne(
            { username: user.username },
            {
                $set: { username: newUsername },
            },
        );
    }

    async updateAvatar(user: User, avatarUrl: string) {
        await (
            await this.collection
        ).updateOne(
            { username: user.username },
            {
                $set: { avatar: avatarUrl },
            },
        );
    }

    async getAllUsers() {
        return (await this.collection)
            .find({})
            .toArray()
            .then((users) => {
                const clonedUsers = [];
                for (const user of users) {
                    // eslint-disable-next-line no-underscore-dangle
                    const newUser = User.registered(user.username, user.avatar, user.email, user.password, user._id.toString(), user.statistics);
                    clonedUsers.push(newUser);
                }
                return clonedUsers;
            });
    }

    async removeAllUsers() {
        await (await this.collection).deleteMany({});
    }
}
