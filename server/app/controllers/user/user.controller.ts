/* eslint-disable @typescript-eslint/no-magic-numbers */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-underscore-dangle */
import { User } from '@app/classes/user';
import { GlobalUserService } from '@app/services/global-user/global-user.service';
import { SelfUserService } from '@app/services/self-user/self-user.service';
import { UserRepository } from '@app/services/user-repository/user-repository';
import { Request, Response, Router } from 'express';
import { check, validationResult } from 'express-validator';
import { Service } from 'typedi';

const OK_HTTP_STATUS = 200;
const UNAUTHORIZED_HTTP = 401;

@Service()
export class UserController {
    router: Router;
    constructor(private userRepository: UserRepository, private globalUsersService: GlobalUserService) {
        this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();

        this.router.post(
            '/signup',

            [
                check('password', 'registerPage.invalidPassword')
                    .isLength({ min: 8 })
                    .custom((value, { req }) => {
                        if (value !== req.body.confirmPassword) {
                            throw new Error('registerPage.differentPassword');
                        } else {
                            return value;
                        }
                    }),
            ],

            async (req: Request, res: Response) => {
                const validationErrors = validationResult(req);

                if (!validationErrors.isEmpty()) {
                    res.status(UNAUTHORIZED_HTTP).json(validationErrors);
                } else {
                    req.body.statistics.elo = 1500;

                    await this.userRepository
                        .signup(req.body)
                        .then(async (result) => {
                            const user = User.registered(
                                req.body.username,
                                req.body.avatar,
                                req.body.email,
                                req.body.password,
                                result.insertedId.toString(),
                                req.body.statistics,
                            );

                            this.globalUsersService.addUser(user);
                            this.globalUsersService.setUserOnline(user.id);
                            res.status(OK_HTTP_STATUS).json(user.id);
                        })
                        .catch((error: any) => {
                            res.status(UNAUTHORIZED_HTTP).json(error.code);
                        });
                }
            },
        );

        this.router.post('/login', async (req: Request, res: Response) => {
            delete req.body.statistics; // typescript super god mode s tier dart master who learned typescript in 2 ms
            const user = await this.userRepository.login(req.body);
            if (user) {
                if (this.globalUsersService.isUserOnline(user.id)) {
                    res.status(UNAUTHORIZED_HTTP).json('loginPage.alreadyOnline');
                } else {
                    this.globalUsersService.setUserOnline(user.id);
                    res.status(OK_HTTP_STATUS).json(user);
                }
            } else {
                res.status(UNAUTHORIZED_HTTP).json('loginPage.incorrectCombination');
            }
        });

        this.router.post('/updateUsername', async (req: Request, res: Response) => {
            const user: User = req.body.user;
            const newUsername: string = req.body.newUsername;
            this.userRepository
                .updateUserName(user, newUsername)
                .then(() => {
                    this.globalUsersService.getUser(user.id)?.getContainer().get(SelfUserService).updateUsername(newUsername);
                    this.globalUsersService.emitPropUpdate('onlineUsers');

                    res.status(OK_HTTP_STATUS).json();
                })
                .catch((e) => {
                    // if username already used
                    res.status(UNAUTHORIZED_HTTP).json(e.code);
                });
        });

        this.router.post('/updateAvatar', async (req: Request, res: Response) => {
            const user: User = req.body.user;
            const avatarUrl: string = req.body.avatarUrl;
            this.userRepository
                .updateAvatar(user, avatarUrl)
                .then(() => {
                    this.globalUsersService.getUser(user.id)?.getContainer().get(SelfUserService).updateAvatar(avatarUrl);
                    this.globalUsersService.emitPropUpdate('onlineUsers');

                    res.status(OK_HTTP_STATUS).json();
                })
                .catch((e) => {
                    // aucune raison pour tomber ici pour le moment;
                    res.status(UNAUTHORIZED_HTTP).json(e.code);
                });
        });

        this.router.get('/getAll', async (req: Request, res: Response) => {
            this.userRepository
                .getAllUsers()
                .then((users: User[]) => {
                    res.json(users);
                })
                .catch((error: Error) => {
                    res.status(OK_HTTP_STATUS).send(error.message);
                });
        });

        /* for testing purposes */
        this.router.delete('/reset', async (req: Request, res: Response) => {
            this.userRepository
                .removeAllUsers()
                .then(() => {
                    res.sendStatus(OK_HTTP_STATUS).send();
                })
                .catch((error: Error) => {
                    res.status(OK_HTTP_STATUS).send(error.message);
                });
        });
    }
}
