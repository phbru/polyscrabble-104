export enum Bonus {
    Blank = 0,
    LetterX2,
    LetterX3,
    WordX2,
    WordX3,
    Star,
}
