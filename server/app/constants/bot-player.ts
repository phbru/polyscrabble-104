import { WordMap } from '@app/types/word-map';

export const MAX_BOT_TIME = 4000;
export const EMPTY_WORD_MAP: WordMap = { word: '', map: [] };
