export const MAX_LETTERS_PLAYER = 7;
export const MIN_WORD_BANK = 7;

export const DELAY_BEFORE_BOARD_RESET = 3000;
export const BINGO_BONUS = 50;

export const CENTER_COLUMN = 8;
export const CENTER_LINE = 'H';
