const config = {
    userName: 'equipe104',
    password: 'Z3dX6NLLh7N6YCW4',
    host: 'polyscrabblecluster.mr9iu1h.mongodb.net',
};

export const DATABASE_URL = `mongodb+srv://${config.userName}:${config.password}@${config.host}/?retryWrites=true&w=majority`;
export const DATABASE_COLLECTION_NAMES = 'Noms';
// export const DATABASE_COLLECTION_SCORES = 'Meilleurs_scores'; TODO : Remove
export const DATABASE_COLLECTION_DICTIONARIES = 'Dictionaries';
export const DATABASE_NAME = 'Scrabble';
export const DATABASE_USERNAMES = 'usernames';
